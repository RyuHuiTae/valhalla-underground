﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace DevSupport
{
    /// <summary>
    /// 작성자: 류희태
    /// 로딩 UI가 필요한 비동기 작업들을 관리한다.
    /// </summary>
    public class LoadingProcessMgr : MonoSingleton<LoadingProcessMgr>
    {
        [SerializeField] private RectTransform _loadingImage;
        [SerializeField] private Text _loadingText;

        public bool IsLoading => _loadingTaskCount > 0;
        // ThreadPool을 사용하지만 이 카운트는 Main Thread에서만 바뀌므로 lock을 사용하지 않는다.
        private int _loadingTaskCount = 0;

        private void Awake()
        {
            _loadingImage.gameObject.SetActive(false);
        }

        /// <summary>
        /// 작성자: 류희태
        /// 하나의 매개변수를 제너릭으로 받아서 비동기로 작업을 수행하고 결과를 반환한다.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="func"></param>
        /// <param name="param"></param>
        /// <param name="token"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        private async UniTask<TR> Run<T, TR>(Func<T, CancellationToken, TR> func, T param, CancellationToken token, bool configureAwait = true)
        {
            /* 
             * UniTask.Run(Func<object, T> func, object state, bool configureAwait = true)은 매개변수를 object로
             * 캐스팅 해야된다는게 비동기 함수 추가할 때 답답함. 그리고 어차피 매개변수 하나 쓰는 비동기 함수만 쓰기
             * 때문에 여기에 맞는 Run 함수를 추가함. 단, UniTask를 업데이트 하는데 방해되지 않게 여기에 선언함.
             */
            await UniTask.SwitchToThreadPool();
            var result = func(param, token);
            if (configureAwait)
                await UniTask.Yield();

            return result;
        }

        private async UniTask<TR> Run<T1, T2, TR>(Func<T1, T2, CancellationToken, TR> func, T1 param1, T2 param2, CancellationToken token, bool configureAwait = true)
        {
            /* 
             * UniTask.Run(Func<object, T> func, object state, bool configureAwait = true)은 매개변수를 object로
             * 캐스팅 해야된다는게 비동기 함수 추가할 때 답답함. 그리고 어차피 매개변수 하나 쓰는 비동기 함수만 쓰기
             * 때문에 여기에 맞는 Run 함수를 추가함. 단, UniTask를 업데이트 하는데 방해되지 않게 여기에 선언함.
             */
            await UniTask.SwitchToThreadPool();
            var result = func(param1, param2, token);
            if (configureAwait)
                await UniTask.Yield();

            return result;
        }

        private async UniTask<TR> Run<TR>(Func<CancellationToken, TR> func, CancellationToken token, bool configureAwait = true)
        {
            await UniTask.SwitchToThreadPool();
            var result = func(token);
            if (configureAwait)
                await UniTask.Yield();

            return result;
        }

        private async UniTask Run(Action<CancellationToken> action, CancellationToken token, bool configureAwait = true)
        {
            await UniTask.SwitchToThreadPool();
            action(token);
            if (configureAwait)
                await UniTask.Yield();
        }

        private void ShowLoadingUI(string loadingMessage)
        {
            if (loadingMessage == null)
                return;

            if (_loadingImage)
                _loadingImage.gameObject.SetActive(true);
            if (_loadingText)
                _loadingText.text = loadingMessage;
        }

        private void HideLoadingUI()
        {
            if (_loadingImage)
                _loadingImage.gameObject.SetActive(false);
        }

        private void BeforeProcessLoading(string loadingMessage)
        {
            _loadingTaskCount++;
            if (loadingMessage == null)
                HideLoadingUI();
            else
                ShowLoadingUI(loadingMessage);
        }

        private void AfterProcessLoading()
        {
            _loadingTaskCount--;
            if (!IsLoading)
                HideLoadingUI();
        }

        public void ChangeText(string message)
        {
            if (message == null)
                return;
            MainThreadDispatcher.Post(_ => _loadingText.text = message, null);
        }

        public async UniTask<TR> ProcessLoading<T, TR>(string loadingMessage, Func<T, CancellationToken, TR> func, T param, CancellationToken token)
        {
            BeforeProcessLoading(loadingMessage);

            try
            {
                // 이 작업은 thread pool에서 실행된다.
                var result = await Run(func, param, token);
                return result;
            }
            finally
            {
                // 예외가 발생하면 thread pool에서 main thread로 돌아오지 않아서 강제로 바꿔줘야 함.
                await UniTask.SwitchToMainThread();

                AfterProcessLoading();
            }
        }

        public async UniTask<TR> ProcessLoading<T1, T2, TR>(string loadingMessage, Func<T1, T2, CancellationToken, TR> func, T1 param1, T2 param2, CancellationToken token)
        {
            BeforeProcessLoading(loadingMessage);

            try
            {
                // 이 작업은 thread pool에서 실행된다.
                var result = await Run(func, param1, param2, token);
                return result;
            }
            finally
            {
                // 예외가 발생하면 thread pool에서 main thread로 돌아오지 않아서 강제로 바꿔줘야 함.
                await UniTask.SwitchToMainThread();

                AfterProcessLoading();
            }
        }

        public async UniTask<TR> ProcessLoading<TR>(string loadingMessage, Func<CancellationToken, TR> func, CancellationToken token)
        {
            BeforeProcessLoading(loadingMessage);

            try
            {
                // 이 작업은 thread pool에서 실행된다.
                var result = await Run(func, token);
                return result;
            }
            finally
            {
                // 예외가 발생하면 thread pool에서 main thread로 돌아오지 않아서 강제로 바꿔줘야 함.
                await UniTask.SwitchToMainThread();

                AfterProcessLoading();
            }
        }

        /// <summary>
        /// 작성자: 류희태
        /// 주어진 작업들을 백그라운드에서 실행하고 끝나기 전까지 로딩 창을 표시해준다
        /// </summary>
        public async UniTask ProcessLoading(string loadingMessage, Action<CancellationToken> action, CancellationToken token)
        {
            BeforeProcessLoading(loadingMessage);

            try
            {
                // 이 작업은 thread pool에서 실행된다.
                await Run(action, token);
            }
            finally
            {
                // 예외가 발생하면 thread pool에서 main thread로 돌아오지 않아서 강제로 바꿔줘야 함.
                await UniTask.SwitchToMainThread();

                AfterProcessLoading();
            }
        }
    }
}
