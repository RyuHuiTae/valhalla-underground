﻿using System;
using System.Linq;
using UnityEngine;

namespace DevSupport
{
    /// <summary>
    /// 작성자 : 류희태
    /// 싱글톤 패턴 템플릿
    /// </summary>
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _myInstance = null;
        public static T Instance
        {
            get
            {
                if (_myInstance) return _myInstance;

                var instance = Resources.FindObjectsOfTypeAll(typeof(T));
                if (instance.Length == 0) return null;
                if (instance.Length > 1)
                    throw new InvalidOperationException("여러 개의 싱글톤 객체가 존재합니다: " + typeof(T));

                _myInstance = instance.First() as T;

                // unity null check를 한 후에 instance를 반환함.
                return _myInstance ? _myInstance : null;
            }
        }

        protected virtual void OnDestroy()
        {
            _myInstance = null;
        }
    }
}
