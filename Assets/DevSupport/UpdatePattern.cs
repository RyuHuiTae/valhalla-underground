﻿using System.Collections;
using System.Xml.Serialization;
using UniRx;
using UnityEngine;

namespace DevSupport
{
    /// <summary>
    /// 작성자: 류희태
    /// 기존 Unity 의 매직함수(Update)를 대체하는 UpdateMicroCoroutine 패턴
    /// 8배의 성능차이를 보여줌.
    /// </summary>
    public abstract class UpdatePattern : MonoBehaviour
    {
        protected bool isLoop = true;

        protected virtual void OnEnable()
        {
            isLoop = true;
            MainThreadDispatcher.StartUpdateMicroCoroutine(Event());
        }

        protected abstract IEnumerator Event();
        
        protected virtual void OnDisable()
        {
            isLoop = false;
        }
    }
}
