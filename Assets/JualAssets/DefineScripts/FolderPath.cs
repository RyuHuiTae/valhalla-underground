﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FolderPath
{
    public const string LAYER_FOLDER = "Prefabs/Layers/";
    public const string POPUP_FOLDER = "Prefabs/Popups/";
    public const string TOP_POPUP_FOLDER = "Prefabs/TopPopup/";
    public const string UIROOT_FOLDER = "Assets/JualAssets/Prefabs/UIRoot/";
    public const string BACKGROUND_FOLDER = "Prefabs/Background/";
    public const string SOUND_FOLDER = "Sounds/";
}
