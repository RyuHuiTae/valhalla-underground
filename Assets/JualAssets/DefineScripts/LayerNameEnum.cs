﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Layer
{
    BattleLayer,
    StageSelectLayer,
    TitleLayer,
    GameOverLayer,
    IntroLayer,
    EndingLayer,
}