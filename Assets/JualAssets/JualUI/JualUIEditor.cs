﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class JualUIEditor : EditorWindow
{
    [MenuItem("GameObject/JUAL UI/Button", false, 10)]
    static void AddButton(MenuCommand menuCommand)
    {
        GameObject buttonObj = new GameObject("UIButton");
        buttonObj.AddComponent<UIButton>();

        GameObjectUtility.SetParentAndAlign(buttonObj, menuCommand.context as GameObject);
        Undo.RegisterCreatedObjectUndo(buttonObj, "Create " + buttonObj.name);

        Selection.activeObject = buttonObj;
    }
}

#endif