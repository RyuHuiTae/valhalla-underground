﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CommonLayer : MonoBehaviour
{
    public virtual void EnterLayer()
    {
    }

    public virtual void ExitLayer()
    {
        Destroy(gameObject);
    }

    void OnValidate()
    {
        #if UNITY_EDITOR
        if (transform.parent != null && transform.parent.name == "Canvas_depth20")
            return;
        
        UnityEditor.Experimental.SceneManagement.PrefabStage prefabStage = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(gameObject);

        bool hasUIRoot = gameObject.GetComponentsInParent<UIRoot>().Length > 0;
        
        if (prefabStage == null || hasUIRoot)
            return;

        GameObject uiRoot = Instantiate(AssetDatabase.LoadAssetAtPath<GameObject>(FolderPath.UIROOT_FOLDER + "UIRoot.prefab"));
        Transform parent = uiRoot.transform.Find("Canvas_depth20");

        uiRoot.hideFlags = HideFlags.DontSave;
        parent.hideFlags = HideFlags.DontSave;
        
        SceneManager.MoveGameObjectToScene(uiRoot, prefabStage.scene);

        transform.SetParent(parent, false);
        #endif
    }
}