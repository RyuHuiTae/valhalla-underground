﻿using System.Collections.Generic;
using UnityEngine;

public class LayerManager : SingletonMonoBehaviour<LayerManager>
{
    private List<GameObject> layerParents = new List<GameObject>();
    private UIRoot uiRoot;
    private CommonLayer currentLayer;
    public UIRoot GetUIRoot() => uiRoot;
    public CommonLayer GetCurrentLayer() => currentLayer;

    public void ChangeLayer(Layer layerNameEnum, string depthTag = UIUtils.DEPTH_20)
    {
        GameObject nextLayerPrefab = VResources.Load<GameObject>(FolderPath.LAYER_FOLDER + layerNameEnum);

        if (nextLayerPrefab == null)
        {
            return;
        }

        if (currentLayer != null)
        {
            currentLayer.ExitLayer();
        }

        if (uiRoot == null)
        {
            uiRoot = FindObjectOfType<UIRoot>();
        }

        GameObject layerParent = FindLayerParent(depthTag);

        currentLayer = GameUtils.Instance.CreateObject(nextLayerPrefab, layerParent.transform)
            .GetComponent<CommonLayer>();
        currentLayer.EnterLayer();
    }

    public GameObject FindLayerParent(string depthTag)
    {
        GameObject layerParent = layerParents.Find(x => x.CompareTag(depthTag));

        if (layerParent == null)
        {
            layerParent = GameObject.FindGameObjectWithTag(depthTag);
            layerParents.Add(layerParent);
        }

        return layerParent;
    }
}