﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LocalUtils
{
    private static LanguageType currLanguageType = LanguageType.ko;

    public static void SetLanguageType(LanguageType languageType)
    {
        currLanguageType = languageType;
    }

    public static string GetText(string localId)
    {
        if (MetaInfoManager.metaLocalizingInfos == null || MetaInfoManager.metaLocalizingInfos.Count <= 0)
        {
            MetaInfoManager.LocalizingInit();
        }

        MetaLocalizingInfo localizingInfo = null;
        if (MetaInfoManager.metaLocalizingInfos.ContainsKey(localId))
        {
            localizingInfo = MetaInfoManager.metaLocalizingInfos[localId]; 
        }

        if (localizingInfo == null)
        {
            return localId;
        }

        switch (currLanguageType)
            {
                case LanguageType.ko:
                    return localizingInfo.ko; 
                case LanguageType.ja:
                    return localizingInfo.ja;
                case LanguageType.en:
                    return localizingInfo.en;
                case LanguageType.zhchs:
                    return localizingInfo.zhchs;
                case LanguageType.zhcht:
                    return localizingInfo.zhcht;
            }

        return string.Empty;
    }

    public static string GetLocalText(this string localId)
    {
        return GetText(localId);
    }

    public static LanguageType CurrLanguageType => currLanguageType;
}

public enum LocalString
{
    weaponCantUpgrade_001,
    weaponCantUpgrade_002,
    LearnSkillWarning,
    ReinforceLauncherWarning,
    skillCantGain_001,
    skillCantGain_002,
    weaponLevelUpPopup,
    skillGainConfirmPopup,
    teleportConfirmPopup_001,
    levelUpConfirmPopup_001,
    reinforceConfirmPopup_001,
    weaponCantReinforce_001,
    weaponCantReinforce_002,
    equipWeaponConfirmPopup_001,
    skillUpgradeConfirmPopup_001,
    skillUpgradeConfirmPopup_002
}