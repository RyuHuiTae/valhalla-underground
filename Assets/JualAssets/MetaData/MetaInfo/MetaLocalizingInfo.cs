﻿using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine;

public class MetaLocalizingInfo
{
    public string ko;
    public string ja;
    public string en;
    public string zhchs;
    public string zhcht;
    
    public MetaLocalizingInfo(JsonData data)
    {
        ko = data.GetStringBackend(nameof(ko));
        ja = data.GetStringBackend(nameof(ja));
        en = data.GetStringBackend(nameof(en));
        zhchs = data.GetStringBackend(nameof(zhchs));
        zhcht = data.GetStringBackend(nameof(zhcht));
    }
}
