﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LitJson;
using UnityEngine;

public static class MetaInfoManager
{
    private const string META_LOCALIZING_INFO = "LocalizingInfo";
    
    public static Dictionary<string, MetaLocalizingInfo> metaLocalizingInfos =
        new Dictionary<string, MetaLocalizingInfo>();

    public static void LocalizingInit()
    {
        JsonData localizingDataList = JsonMapper.ToObject(PlayerPrefs.GetString(META_LOCALIZING_INFO)).GetBackendData();

        metaLocalizingInfos.Clear();
        foreach (JsonData localizingData in localizingDataList)
        {
            metaLocalizingInfos.Add(localizingData.GetStringBackend("localId"), new MetaLocalizingInfo(localizingData));
        }
    }

    public static void Init()
    {
        
    }
}