﻿using System;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animation))]
public class CommonPopup : MonoBehaviour
{
    [SerializeField] private AnimationClip[] enterAnimClip;
    [SerializeField] private AnimationClip[] exitAnimClip;
    [SerializeField] protected Animation popupAnimation;

    protected Action callbackCancel;

    public PopupName PopupName { get; private set; }
    public bool IsBackButtonEnable { get; private set; }

    public virtual async void Enter(bool playAnim = true, bool isPlayAnimAll = false)
    {
        if (playAnim && 
            popupAnimation != null && 
            enterAnimClip != null)
        {
            await WaitAnimation(enterAnimClip, EnterAnimEndEvent, isPlayAnimAll, false);
        }
    }

    public virtual async void Exit(bool playAnim = true, bool isPlayAnimAll = false)
    {
        if (playAnim && 
            popupAnimation != null && 
            exitAnimClip != null)
        {
            await WaitAnimation(exitAnimClip, ExitAnimEndEvent, isPlayAnimAll, true);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    protected virtual void EnterAnimEndEvent()
    {
        
    }
    
    protected virtual void ExitAnimEndEvent()
    {
        
    }

    protected async UniTask WaitAnimation(AnimationClip[] clips, Action callback,
        bool isPlayAnimAll = false, bool isDestroy = false)
    {
        if (clips != null)
        {
            foreach (var clip in clips)
            {
                if (!clip)
                    continue;

                if (!popupAnimation)
                    continue;
                
                popupAnimation.clip = clip;
                popupAnimation.Play();

                var isPlaying = popupAnimation.isPlaying;
                while (isPlaying)
                {
                    await UniTask.WaitForEndOfFrame();
                    isPlaying = popupAnimation != null && popupAnimation.isPlaying;
                }

                if (!isPlayAnimAll)
                    break;
            }
        }            
            
        callback?.Invoke();

        if (isDestroy && this != null && this.gameObject != null)
        {
            Destroy(gameObject);
        }
    }
    
    public void SetCallbackCancel(Action callbackCancel)
    {
        this.callbackCancel = callbackCancel;
    }

    public void SetPopupName(PopupName popupName)
    {
        this.PopupName = popupName;
    }

    public void SetBackButtonEnable(bool isBackButtonEnable)
    {
        this.IsBackButtonEnable = isBackButtonEnable;
    }

    public virtual void OnClickCancelButton()
    {
        callbackCancel?.Invoke();
        PopupManager.Instance.HidePopup(this);
    }
    
    void OnValidate()
    {
        if (popupAnimation == null)
        {
            popupAnimation = GetComponent<Animation>();
            popupAnimation.playAutomatically = false;
        }
#if UNITY_EDITOR
        if (transform.parent != null && transform.parent.name == "Canvas_depth40")
            return;
        
        UnityEditor.Experimental.SceneManagement.PrefabStage prefabStage = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(gameObject);

        bool hasUIRoot = gameObject.GetComponentsInParent<UIRoot>().Length > 0;
        
        if (prefabStage == null || hasUIRoot)
            return;

        GameObject uiRoot = Instantiate(AssetDatabase.LoadAssetAtPath<GameObject>(FolderPath.UIROOT_FOLDER + "UIRoot.prefab"));
        Transform parent = uiRoot.transform.Find("Canvas_depth40");

        uiRoot.hideFlags = HideFlags.DontSave;
        parent.hideFlags = HideFlags.DontSave;
        
        SceneManager.MoveGameObjectToScene(uiRoot, prefabStage.scene);

        transform.SetParent(parent, false);
#endif
    }
}
