﻿using System.Collections.Generic;
using UnityEngine;

public class PopupManager : SingletonMonoBehaviour<PopupManager>
{
    private List<GameObject> popupParents = new List<GameObject>();
    private List<CommonPopup> popupList = new List<CommonPopup>();

    public CommonPopup CreatePopup(PopupName popupNameEnum, string depthTag = UIUtils.DEPTH_40, bool playAnim = true)
    {
        GameObject newPopupPrefab = VResources.Load<GameObject>(FolderPath.POPUP_FOLDER + popupNameEnum);

        if (newPopupPrefab == null)
        {
            return null;
        }

        GameObject popupParent = FindPopupParent(depthTag);

        CommonPopup commonPopup = GameUtils.Instance.CreateObject(newPopupPrefab, popupParent.transform)
            .GetComponent<CommonPopup>();
        commonPopup.transform.SetParent(popupParent.transform);
        commonPopup.transform.localPosition = Vector3.zero;
        commonPopup.transform.localScale = Vector3.one;
        commonPopup.SetPopupName(popupNameEnum);
        commonPopup.SetBackButtonEnable(IsBackButtonEnable(popupNameEnum));
        commonPopup.Enter(playAnim);

        popupList.Add(commonPopup);

        return commonPopup;
    }

    private bool IsBackButtonEnable(PopupName popupNameEnum)
    {
        switch (popupNameEnum)
        {
            //백버튼 허용 안할 팝업들
            case PopupName.BackgroundPopup:
            case PopupName.EventPopup:
            case PopupName.PuzzlePopup:
            case PopupName.InventoryPopup:
            case PopupName.RestPopup:
            case PopupName.ShopPopup:
            case PopupName.SpyingPopup:
            case PopupName.TutorialPopup:
            case PopupName.TweenPopup:
            case PopupName.BarrierEventPopup:
            case PopupName.HeartLosePopup:
            case PopupName.PuzzleInfoPopup:
            case PopupName.StarterDeckPopup:
            case PopupName.TopUI:
            case PopupName.UpgradePopup:
            case PopupName.BattleScenePopup:
                return false;
        }

        return true;
    }
    
    private void Update()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.Android)
        {
            //백 버튼
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                var array = popupList.ToArray();
                for (var i = array.Length - 1; i >= 0; i--)
                {
                    if (!array[i].IsBackButtonEnable)
                        continue;

                    if (!array[i]) 
                        continue;
                    
                    array[i].OnClickCancelButton();
                    return;
                }
                
                var quitGamePopup = GetPopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                quitGamePopup.Init("종료", "정말 종료하시겠습니까?", ConfirmPopupType.YesNo, () =>
                {
#if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
                });
                
            }
        }
    }

    /// <summary>
    /// 해당 UI가 열려있으면 가져오고 아니면 NULL 반환
    /// </summary>
    public CommonPopup GetOpenedPopup(PopupName popupNameEnum)
    {
        var findPopup = popupList.FindLast(v => v.PopupName == popupNameEnum);
        return findPopup;
    }

    /// <summary>
    /// 이미 열려있으면 그 UI를 가져오고 아니라면 새로 생성한다.
    /// </summary>
    public CommonPopup GetPopup(PopupName popupNameEnum, string depthTag = UIUtils.DEPTH_40, bool playAnim = true)
    {
        var findPopup = popupList.FindLast(v => v.PopupName == popupNameEnum);
        if (findPopup == null)
        {
            return CreatePopup(popupNameEnum, depthTag, playAnim);
        }
        
        return findPopup;
    }

    public GameObject FindPopupParent(string depthTag)
    {
        GameObject popupParent = popupParents.Find(x => x.CompareTag(depthTag));

        if (popupParent == null)
        {
            popupParent = GameObject.FindGameObjectWithTag(depthTag);
            popupParents.Add(popupParent);
        }

        return popupParent;
    }

    public void HidePopup(CommonPopup commonPopup, bool playAnim = true, bool isPlayAnimAll = false)
    {
        if (popupList == null || popupList.Count <= 0)
            return;

        CommonPopup recentPopup = popupList.FindLast(x => x == commonPopup);
        if (recentPopup != null)
        {
            recentPopup.Exit(playAnim, isPlayAnimAll);
            popupList.Remove(recentPopup);
        }
    }

    public void HideAllPopup(bool playAnim = true,  bool isPlayAnimAll = false)
    {
        if (popupList == null || popupList.Count <= 0)
            return;

        foreach (var popup in popupList)
        {
            popup.Exit(playAnim, isPlayAnimAll);
        }
        popupList.Clear();
    }

    public void CheckTutorial(TutorialTextType type)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)) 
            return;
        
        var index = (int) type; 
        
        var tutorialCheckDataArray = playerDB.Data.GetTutorialCheckData();
        if (tutorialCheckDataArray[index]) 
            return;
        
        var tutorialPopup = PopupManager.Instance.GetPopup(PopupName.TutorialPopup, UIUtils.DEPTH_60) as TutorialPopup;
        tutorialPopup?.Init(type);
    }
}