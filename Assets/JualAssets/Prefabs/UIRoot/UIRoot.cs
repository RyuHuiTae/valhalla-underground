﻿using UnityEngine;

public class UIRoot : MonoBehaviour
{

    public Vector2 GetResolution()
    {
        Vector2 resolution = new Vector2(Screen.width, Screen.height);
        return resolution;
    }
}
