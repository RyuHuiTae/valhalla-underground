﻿using UnityEngine;

public class SoundManager : SingletonMonoBehaviour<SoundManager>
{
    private AudioSource bgmSource;
    private AudioSource effSource;
    
    private bool isBGMOn;
    private bool isEffSoundOn;
    private float bgmVolume;
    private float effVolume;

    const string BGM_ON_OFF_KEY = "BGM_ON_OFF";
    const string EFF_SOUND_ON_OFF_KEY = "EFF_SOUND_ON_OFF";

    private const string BGM_SOUND_KEY = "BGM_SOUND";
    private const string EFF_SOUND_KEY = "EFF_SOUND";
    

    public void PlayOneShot(string filePath, float volumeScale = 1.0f)
    {
        if (!isEffSoundOn)
            return;
        
        AudioClip clip = LoadClip(filePath);
        if (clip != null)
        {
            effSource.PlayOneShot(clip, volumeScale * effVolume);    
        }
    }

    public void PlayBackgroundMusic(string filePath, float volumeScale = 1.0f)
    {
        if (!isBGMOn)
            return;
        
        AudioClip clip = LoadClip(filePath);
        if (clip != null)
        {
            bgmSource.clip = clip;
            bgmSource.Play();
            bgmSource.loop = true;
            bgmSource.volume = volumeScale * bgmVolume;    
        }
        else
        {
            bgmSource.clip = null;
        }
    }

    public void StopBackgroundMusic()
    {
        bgmSource.Stop();
    }

    AudioClip LoadClip(string filePath)
    {
        return VResources.Load<AudioClip>(FolderPath.SOUND_FOLDER + filePath);
    }

    public void SetBGMOnOff(bool isOn)
    {
        PlayerPrefs.SetString(BGM_ON_OFF_KEY, isOn.ToString().ToLower());
        if (isOn)
        {
            bgmSource.Play();
        }
        else
        {
            bgmSource.Stop();
        }
        isBGMOn = isOn;
    }

    public void SetEffOnOff(bool isOn)
    {
        PlayerPrefs.SetString(EFF_SOUND_ON_OFF_KEY, isOn.ToString().ToLower());
        isEffSoundOn = isOn;
    }

    public void SetBGMSound(float value)
    {
        bgmVolume = value;
        SaveUtils.SetFloat(BGM_SOUND_KEY, value);
    }

    public void SetEffSound(float value)
    {
        effVolume = value;
        SaveUtils.SetFloat(EFF_SOUND_KEY, value);
    }

    public bool GetBGMON()
    {
        return isBGMOn;
    }

    public bool GetEffOn()
    {
        return isEffSoundOn;
    }

    public float GetBGMVolume() => bgmVolume;
    public float GetEffVolume() => effVolume;
    
    protected override void Awake()
    {
        base.Awake();
        
        bgmSource = gameObject.AddComponent<AudioSource>();
        effSource = gameObject.AddComponent<AudioSource>();
        
        isBGMOn = bool.Parse(PlayerPrefs.GetString(BGM_ON_OFF_KEY, "true"));
        isEffSoundOn = bool.Parse(PlayerPrefs.GetString(EFF_SOUND_ON_OFF_KEY, "true"));

        bgmVolume = SaveUtils.GetFloat(BGM_SOUND_KEY, 1.0f);
        effVolume = SaveUtils.GetFloat(EFF_SOUND_KEY, 1.0f);
    }
}