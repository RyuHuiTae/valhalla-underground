﻿public class SoundName
{
    public const string SHOT_SOUND = "shot";
    public const string ENEMY_DIE = "zombieDie";
    public const string BTN_CLICK = "btnClick";
    public const string BACKGROUND_MUSIC001 = "backgroundGame";
    public const string LOSE001 = "lose1";
    public const string LOSE002 = "lose2";
    public const string ENEMY_BOUNCE = "enemyBounce";
    public const string WIN001 = "win1";
}