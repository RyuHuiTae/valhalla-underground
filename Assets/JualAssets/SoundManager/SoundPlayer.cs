﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    [SerializeField] private string soundPath;
    [SerializeField] private bool playOnEnable;
    [SerializeField] private SoundType soundType;

    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();    
        }
    }

    void Play()
    {
        SoundManager.Instance.PlayOneShot(soundType.ToString().ToLower() + "/" + soundPath);
    }
    
    public void PlaySound(string filePath)
    {
        SoundManager.Instance.PlayOneShot(soundType.ToString().ToLower() + "/" + filePath);
    }
}

public enum SoundType
{
    BGM,
    EFF
}