﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class UIVibrateObject : MonoBehaviour
{
    [SerializeField] private Rect vibrateRect;
    [SerializeField] private RectTransform target;
    
    [SerializeField] private bool isVibrate = false;
    private float currTime = 0.0f;
    private float endTime = 0.0f;
    private Vector2 originPos = new Vector2(-111.0f, -111.0f);

    private Vector2 NOT_ORIGIN_POS = new Vector2(-111.0f, -111.0f);


    public void StartVibrate(float time)
    {
        isVibrate = true;
        currTime = 0.0f;
        endTime = time;
        if (originPos == NOT_ORIGIN_POS)
        {
            originPos = target.anchoredPosition;    
        }
    }

    void EndVibrate()
    {
        isVibrate = false;
        target.anchoredPosition = originPos;
        currTime = 0.0f;
    }

    public void SetRect(Rect vibrateRect)
    {
        this.vibrateRect = vibrateRect;
    }

    void FixedUpdate()
    {
        if (isVibrate)
        {
            currTime += Time.deltaTime;

            Vector2 vibratePos = new Vector3(
                Random.Range(vibrateRect.xMin, vibrateRect.width),
                Random.Range(vibrateRect.yMin, vibrateRect.height));

            vibratePos += originPos;
            

            target.anchoredPosition = vibratePos;
            
            if (endTime < currTime)
            {
                EndVibrate();
            }
        }
    }
}