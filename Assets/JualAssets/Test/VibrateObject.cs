﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class VibrateObject : MonoBehaviour
{
    [SerializeField] private Rect vibrateRect;
    [SerializeField] private Transform target;
    
    [SerializeField] private bool isVibrate = false;
    private float currTime = 0.0f;
    private float endTime = 0.0f;
    private Vector2 originPos;
    
    public void StartVibrate(float time)
    {
        isVibrate = true;
        currTime = 0.0f;
        endTime = time;
        originPos = transform.localPosition;
    }

    public void EndVibrate()
    {
        isVibrate = false;
        target.localPosition = originPos;
        currTime = 0.0f;
    }

    public void SetRect(Rect vibrateRect)
    {
        this.vibrateRect = vibrateRect;
    }

    void Update()
    {
        if (isVibrate)
        {
            currTime += Time.deltaTime;

            Vector3 vibratePos = new Vector3(
                Random.Range(vibrateRect.xMin, vibrateRect.width),
                Random.Range(vibrateRect.yMin, vibrateRect.height));
            
            

            target.localPosition = vibratePos;
            
            if (endTime < currTime)
            {
                EndVibrate();
            }
        }
    }
}