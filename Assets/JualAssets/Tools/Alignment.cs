﻿public enum Alignment
{
    TOP_LEFT,
    TOP_CENTER,
    TOP_RIGHT,
    MIDDLE_LEFT,
    MIDDLE_CENTER,
    MIDDLE_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_CENTER,
    BOTTOM_RIGHT
}

public enum Direction
{
    LEFT,
    RIGHT,
    UP,
    DOWN,
    EMPTY,
}