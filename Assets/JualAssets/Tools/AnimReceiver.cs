﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnimReceiver : MonoBehaviour
{
    private Dictionary<string, Action> _dictCallback = new Dictionary<string, Action>();

    public void AddEvent(Action action)
    {
        if (!_dictCallback.ContainsKey(action.Method.Name))
        {
            _dictCallback.Add(action.Method.Name, action);
        }
    }

    public void RemoveEvent(Action action)
    {
        if(_dictCallback.ContainsKey(action.Method.Name))
        {
            _dictCallback.Remove(action.Method.Name);
        }
    }

    public void OnCalled(string methodName)
    {
        if (_dictCallback.ContainsKey(methodName))
        {
            _dictCallback[methodName]?.Invoke();
        }
    }
}