﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoOrderInLayerSort : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;

    void Update()
    {
        spriteRenderer.sortingOrder = (int)(transform.position.y * -100.0f);
    }
    private void OnValidate()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
}
