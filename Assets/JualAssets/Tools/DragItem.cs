﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DragItem : MonoBehaviour,
    IBeginDragHandler, IDragHandler, IEndDragHandler,
    IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private bool isDraggable = true;
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private DragItem mouseOverItem;

    private Vector2 offset;

    public bool IsDraggable
    {
        get => isDraggable;
        set => isDraggable = value;
    }

    public DragItem MouseOverItem
    {
        get => mouseOverItem;
    }

    public class UnityEventDataDragItem : UnityEvent<PointerEventData, DragItem>
    {
    }

    public class UnityEventPointerEventData : UnityEvent<PointerEventData>
    {
    }

    private UnityEventDataDragItem eventBeginDrag = new UnityEventDataDragItem();
    private UnityEventDataDragItem eventDrag = new UnityEventDataDragItem();
    private UnityEventDataDragItem eventEndDrag = new UnityEventDataDragItem();
    private UnityEventDataDragItem eventPointerEnter = new UnityEventDataDragItem();
    private UnityEventDataDragItem eventPointerExit = new UnityEventDataDragItem();

    //겹치는 PointerBegin, PointerExit 이벤트에 대응을 하고 싶었으나
    //그럴수 없어 TotalWarPointerEvent클래스에서 호출해주는 UnityEvent를 만듭니다.
    private UnityEventPointerEventData mouseOverEvent = new UnityEventPointerEventData();
    private UnityEventPointerEventData mouseExitEvent = new UnityEventPointerEventData();

    public UnityEventDataDragItem EventBeginDrag
    {
        get => eventBeginDrag;
        set => eventBeginDrag = value;
    }

    public UnityEventDataDragItem EventDrag
    {
        get => eventDrag;
        set => eventDrag = value;
    }

    public UnityEventDataDragItem EventEndDrag
    {
        get => eventEndDrag;
        set => eventEndDrag = value;
    }

    public UnityEventDataDragItem EventPointerEnter
    {
        get => eventPointerEnter;
        set => eventPointerEnter = value;
    }

    public UnityEventDataDragItem EventPointerExit
    {
        get => eventPointerExit;
        set => eventPointerExit = value;
    }

    public UnityEventPointerEventData MouseOverEvent
    {
        get => mouseOverEvent;
        set => mouseOverEvent = value;
    }

    public UnityEventPointerEventData MouseExitEvent
    {
        get => mouseExitEvent;
        set => mouseExitEvent = value;
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!isDraggable)
            return;

        Vector2 position = new Vector2();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform.parent.GetComponent<RectTransform>(),
            eventData.position, Camera.main,
            out position);

        offset = position - (Vector2) rectTransform.localPosition;

        eventBeginDrag?.Invoke(eventData, this);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!isDraggable)
            return;

        Vector2 position = new Vector2();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform.parent.GetComponent<RectTransform>(),
            eventData.position, Camera.main,
            out position);
        rectTransform.localPosition = position - offset;

        DragItem[] dragItems = FindObjectsOfType<DragItem>();
        bool isContain = false;
        foreach (DragItem dragItem in dragItems)
        {
            if (dragItem == this)
                continue;
            
            isContain = RectTransformUtility.RectangleContainsScreenPoint(dragItem.GetComponent<RectTransform>(),
                eventData.position, Camera.main);
            if (!isContain)
                continue;

            if (mouseOverItem == null)
            {
                mouseOverItem = dragItem;
                mouseOverItem.OnMouseOverEvent(eventData);
                break;
            }
            else
            {
                if (mouseOverItem == dragItem)
                {
                    break;
                }
                else
                {
                    mouseOverItem.OnMouseExitEvent(eventData);
                    mouseOverItem = dragItem;
                    mouseOverItem.OnMouseOverEvent(eventData);
                    break;
                }
            }
        }

        if (!isContain)
        {
            if (mouseOverItem != null)
            {
                mouseOverItem.OnMouseExitEvent(eventData);
                mouseOverItem = null;
            }
        }

        eventDrag?.Invoke(eventData, this);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isDraggable)
            return;

        eventEndDrag?.Invoke(eventData, this);
        if (mouseOverItem != null)
        {
            mouseOverItem.OnMouseExitEvent(eventData);
            mouseOverItem = null;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        eventPointerEnter?.Invoke(eventData, this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        eventPointerExit?.Invoke(eventData, this);
    }

    public void OnMouseOverEvent(PointerEventData mousePos)
    {
        mouseOverEvent?.Invoke(mousePos);
    }

    public void OnMouseExitEvent(PointerEventData mousePos)
    {
        mouseExitEvent?.Invoke(mousePos);
    }

    void OnValidate()
    {
        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
    }
}