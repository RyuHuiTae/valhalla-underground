﻿using System.Collections;
using UnityEngine;

namespace JualAssets.Tools
{
    public class ParticleActive : MonoBehaviour
    {
        [SerializeField] private ParticleSystem particle;

        void OnEnable()
        {
            StartCoroutine(DisableParticle());
        }

        IEnumerator DisableParticle()
        {
            yield return new WaitForSeconds(particle.main.startLifetimeMultiplier);
            gameObject.SetActive(false);
        }

        void OnValidate()
        {
            if (particle == null)
            {
                particle = GetComponent<ParticleSystem>();    
            }
        }
    }
}
