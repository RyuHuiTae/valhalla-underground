﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SpriteChanger : MonoBehaviour
{
    [SerializeField] private Image image;

    [Header("이 SpriteChanger가 버튼일 경우에만 붙이세요.")] [SerializeField]
    private UIButton button;

    public void SetEmpty()
    {
        image.enabled = false;
    }

    public void ChangeSprite(string atlasPath, string spriteName)
    {
        image.enabled = true;

        if (atlasPath.IsNullOrEmpty())
        {
            return;
        }

        SpriteAtlas spriteAtlas = VResources.Load<SpriteAtlas>(atlasPath);
        if (spriteAtlas == null)
            return;

        image.sprite = spriteAtlas.GetSprite(spriteName);

        if (button != null)
        {
            button.SetBaseSprite(image.sprite);
        }
    }

    public void SetColor(Color32 color32)
    {
        if (button != null)
        {
            button.SetColor(color32);
        }
        else
        {
            image.color = color32;
        }
    }

    public Sprite GetSprite()
    {
        return image.sprite;
    }

    void OnValidate()
    {
        image = GetComponent<Image>();
    }
}