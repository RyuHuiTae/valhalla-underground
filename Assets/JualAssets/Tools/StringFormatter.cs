﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StringFormatter : MonoBehaviour
{
    [SerializeField] private Text text;

    private string originFormat = string.Empty;

    public void SetText(params object[] args)
    {
        if (originFormat == string.Empty)
        {
            originFormat = text.text;
        }
        
        text.text = string.Format(originFormat, args);
    }

    void OnValidate()
    {
        if (text == null)
        {
            text = GetComponent<Text>();    
        }
    }
}
