﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CaseComponent : MonoBehaviour
{
    [SerializeField] private string key;
    [SerializeField] private List<CaseInitializer> caseInitializers;

    public void Refresh(string key)
    {
        gameObject.SetActive(this.key == key);
        caseInitializers?.ForEach(x => x.Init());
    }

    private void OnValidate()
    {
        caseInitializers = transform.GetComponentsInChildren<CaseInitializer>().ToList();
    }
}