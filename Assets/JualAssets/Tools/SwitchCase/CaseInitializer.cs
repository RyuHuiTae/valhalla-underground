﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CaseInitializer : MonoBehaviour
{
    public abstract void Init();
}
