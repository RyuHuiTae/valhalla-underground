﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SwitchComponent : MonoBehaviour
{
    private List<CaseComponent> caseComponents;

    public void SetCase(string key)
    {
        caseComponents?.ForEach(x => x.Refresh(key));
    }

    void Awake()
    {
        ResetCase();
    }
    
    public void ResetCase()
    {
        caseComponents = gameObject.GetComponentsInChildrenFirstDepth<CaseComponent>(true).ToList();
    }
}
//