﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(UIButton))]
public class UITab : MonoBehaviour
{
    private bool isSelect = false;
    private bool isDisable = false;
    private UITabGroup tabGroup;

    public string key;

    [SerializeField] private Image tabImage;

    [SerializeField] private Sprite selectSprite;
    [SerializeField] private Sprite unSelectSprite;
    [SerializeField] private Sprite disableSprite;
    
    [SerializeField] private UIButton button;
    [SerializeField] private Text titleText;

    public void SetTitle(string title)
    {
        if (titleText != null)
        {
            titleText.text = title;
        }
    }

    public void SetTabGroup(UITabGroup tabGroup)
    {
        this.tabGroup = tabGroup;
    }

    public void SetTab(bool isSelect)
    {
        this.isSelect = isSelect;

        if (isDisable)
        {
            tabImage.sprite = disableSprite != null ? disableSprite : unSelectSprite;
        }
        else
        {
            tabImage.sprite = isSelect ? selectSprite : unSelectSprite;
        }
        
        button.SetBaseSprite(tabImage.sprite);
    }

    public void SetDisable(bool isDisable)
    {
        this.isDisable = isDisable;
    }

    public void OnClickTab()
    {
        tabGroup.SetTab(key);
    }

    public bool IsDisable()
    {
        return isDisable;
    }

    public bool IsSelect()
    {
        return isSelect;
    }

    private void OnValidate()
    {
        tabImage = GetComponent<Image>();
        button = GetComponent<UIButton>();
    }
}
