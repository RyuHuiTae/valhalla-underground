﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class UITabGroup : MonoBehaviour
{
    private List<UITab> tabList = new List<UITab>();
    private string key;

    private Action<string> callbackSetTab;

    public void Init()
    {
        SetChildTabList();
        
        if (tabList.Count > 0)
        {
            SetTab(tabList[0].key);
        }
    }

    public void SetChildTabList()
    {
        tabList.Clear();
        
        List<UITab> tabs = transform.GetComponentsInChildren<UITab>().ToList();

        for (int i = 0; i < tabs.Count; i++)
        {
            if (tabs[i].gameObject.activeInHierarchy)
            {
                tabs[i].SetTabGroup(this);
                tabList.Add(tabs[i]);    
            }
        }
    }

    public void SetCallbackSetTab(Action<string> callbackSetTab)
    {
        this.callbackSetTab = callbackSetTab;
    }

    private UITab FindTab(string key)
    {
        return tabList.Find(x => x.key == key);
    }

    public void SetTab(string key)
    {
        UITab selectTab = FindTab(key);
        
        if (selectTab == null || selectTab.IsDisable() || selectTab.IsSelect())
        {
            return;
        }
        
        this.key = key;
        foreach (UITab tab in tabList)
        {
            tab.SetTab(tab.key == key);
        }
        
        callbackSetTab?.Invoke(key);
    }

    public bool HasKey(string key)
    {
        return FindTab(key) != null;
    }

    public string GetKey()
    {
        return key;
    }
}
