﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LabelArray : MonoBehaviour
{
    [SerializeField] private StringWithReadOnly[] localizingIds;
    [SerializeField] private Text text;

    public void SetText(int index)
    {
        if (localizingIds.Length > index)
        {
            text.text = LocalUtils.GetText(localizingIds[index].id);
        }
    }

    void OnValidate()
    {
        text = GetComponent<Text>();
        #if UNITY_EDITOR
        foreach (var localizingId in localizingIds)
        {
            localizingId.text = LocalUtils.GetText(localizingId.id);
        }
        #endif
    }
}
