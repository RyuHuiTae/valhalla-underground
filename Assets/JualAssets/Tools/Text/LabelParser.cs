﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LabelParser : MonoBehaviour
{
    [SerializeField] private string localId;
    [ReadOnly] [SerializeField] private string labelText;
    [SerializeField] private Text label;

    private void Awake()
    {
        SetText(localId);
    }

    private void OnValidate()
    {
        if (label == null)
        {
            label = GetComponent<Text>();    
        }

        SetText(localId);
    }

    void SetText(string localId)
    {
        label.text = LocalUtils.GetText(localId);
    }
}