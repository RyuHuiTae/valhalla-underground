﻿using System;

[Serializable]
public class StringWithReadOnly
{
    public string id;
    #if UNITY_EDITOR
    [ReadOnly] public string text;
    #endif
}