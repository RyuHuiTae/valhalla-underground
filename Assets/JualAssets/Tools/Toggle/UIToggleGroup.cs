﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIToggleGroup : MonoBehaviour
{
    [SerializeField] private List<Toggle> toggles;

    void OnValidate()
    {
        toggles = GetComponentsInChildren<Toggle>().ToList();
    }
}
