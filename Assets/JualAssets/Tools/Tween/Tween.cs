﻿using System;
using UnityEngine;

public abstract class Tween : MonoBehaviour
{
    public AnimationCurve tweenCurve = new AnimationCurve(
        new Keyframe(0.0f, 0.0f, 0.0f, 0.0f),
        new Keyframe(1.0f, 1.0f, 1.0f, 1.0f));

    public float duration;
    public bool playAutomatically;
    public TweenType tweenType;


    private float currTime;
    private bool isPlay;
    protected bool isReverse;
    protected bool isCalledHalf;
    [SerializeField] private bool isScaledTime;

    private Action callbackFinished;
    protected Action callbackHalf;

    public void SetCallbackFinished(Action callbackFinished, Action callbackHalf = null)
    {
        this.callbackFinished = callbackFinished;
        this.callbackHalf = callbackHalf;
    }

    public bool IsPlay()
    {
        return isPlay;
    }

    public void Stop()
    {
        isPlay = false;
    }

    protected bool IsFinished()
    {
        return duration <= currTime;
    }

    protected float GetEvaluate()
    {
        return tweenCurve.Evaluate(GetRatio());
    }

    protected float GetRatio()
    {
        if (duration <= 0.0f)
        {
            return 0.0f;
        }

        if (isReverse)
        {
            return 1.0f - (currTime / duration);
        }

        return currTime / duration;
    }

    public void TweenForward()
    {
        isPlay = true;
        currTime = 0.0f;
        isReverse = false;
        isCalledHalf = false;
    }

    public void TweenReverse()
    {
        isPlay = true;
        currTime = 0.0f;
        isReverse = true;
        isCalledHalf = false;
    }

    protected abstract void ReverseInit();

    protected abstract void UpdateTween();

    void UpdateFinished()
    {
        if (IsFinished())
        {
            callbackFinished?.Invoke();
            isPlay = false;
            currTime = duration;
            UpdateTween();

            if (tweenType == TweenType.Ping_pong)
            {
                ReverseInit();
            }
            else if (tweenType == TweenType.Ping_pong_Auto)
            {
                ReverseInit();
                TweenForward();
            }
            else if (tweenType == TweenType.Loop)
            {
                TweenForward();
            }
        }
    }

    void Update()
    {
        if (!isPlay)
        {
            return;
        }

        if (isScaledTime)
        {
            currTime += Time.deltaTime;
        }
        else
        {
            currTime += Time.unscaledDeltaTime;
        }

        UpdateTween();
        UpdateFinished();
    }

    protected virtual void Awake()
    {
        isPlay = playAutomatically;

        if (isPlay)
        {
            UpdateTween();
        }
    }

    void OnDisable()
    {
        isPlay = false;
    }
}