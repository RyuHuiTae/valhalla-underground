﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class TweenColor : Tween
{
    public Color from;
    public Color to;

    protected Color curr;

    protected override void ReverseInit()
    {
        Color temp = from;
        from = to;
        to = temp;
    }

    protected override void UpdateTween()
    {
        curr = from + (to - from) * GetEvaluate();
        SetValue(curr);
    }

    public Color GetValue()
    {
        return curr;
    }

    protected abstract void SetValue(Color currValue);
}