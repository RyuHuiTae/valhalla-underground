﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public abstract class TweenFloat : Tween
{
    public float from;
    public float to;

    private float curr;

    protected override void ReverseInit()
    {
        float temp = from;
        from = to;
        to = temp;
    }

    protected override void UpdateTween()
    {
        curr = from + (to - from) * GetEvaluate();
        SetValue(curr);
    }
    

    public float GetValue()
    {
        return curr;
    }

    protected abstract void SetValue(float currValue);
}
