﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenFloatValue : TweenFloat
{
    [HideInInspector] public float value;
    protected override void SetValue(float currValue)
    {
        value = currValue;
    }
}
