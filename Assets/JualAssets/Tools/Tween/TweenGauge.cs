﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TweenGauge : TweenFloat
{
    [SerializeField] private Image image;
    protected override void SetValue(float currValue)
    {
        image.fillAmount = currValue;
    }

    void OnValidate()
    {
        image = GetComponent<Image>();
    }
}
