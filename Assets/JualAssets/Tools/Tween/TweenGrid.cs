﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class TweenGrid : MonoBehaviour
{
    [SerializeField] private Vector2 margin;
    [SerializeField] private int column;
    [SerializeField] private Alignment alignment;
    [SerializeField] private float duration;

    public void ResetPosition()
    {
        Transform[] transforms = GetTransformInChildren();
        List<Vector3> childPositions = new List<Vector3>();

        for (int i = 0; i < transforms.Length; i++)
        {
            float y = 0;
            float x = 0;
            
            if (column > 0)
            {
                y = i / column;
            }
            
            x = i - y * column;

            Vector2 margin = this.margin;
            switch (alignment)
            {
                case Alignment.MIDDLE_LEFT:
                    break;
                case Alignment.MIDDLE_CENTER:
                    break;
                case Alignment.MIDDLE_RIGHT:
                    margin = -margin;
                    break;
            }
            
            childPositions.Add(new Vector2(x * margin.x, y * margin.y));
        }

        if (alignment == Alignment.MIDDLE_CENTER)
        {
            Vector3 marginPos = new Vector3((transforms.Length-1) * margin.x * 0.5f,
                (transforms.Length-1) * margin.y * 0.5f, 0.0f);
            for (int i = 0; i < transforms.Length; i++)
            {
                childPositions[i] -= marginPos;
            }
        }

        for (int i = 0; i < transforms.Length; i++)
        {
            TweenPosition tweenPosition = transforms[i].GetComponent<TweenPosition>();
            if (tweenPosition == null)
            {
                tweenPosition = transforms[i].gameObject.AddComponent<TweenPosition>();
            }

            tweenPosition.from = Vector3.zero;
            tweenPosition.to = childPositions[i];
            tweenPosition.tweenType = TweenType.Once;
            tweenPosition.duration = duration;
            tweenPosition.transform.localPosition = tweenPosition.from;
            tweenPosition.TweenForward();
        }
    }

    private Transform[] GetTransformInChildren()
    {
        List<Transform> transforms = new List<Transform>();

        foreach (Transform tr in transform)
        {
            if (tr.gameObject.activeInHierarchy)
            {
                transforms.Add(tr);    
            }
        }

        return transforms.ToArray();
    }

    void OnValidate()
    {
        ResetPosition();
    }
}
