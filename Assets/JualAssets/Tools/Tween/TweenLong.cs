﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TweenLong : Tween
{
    public long from;
    public long to;

    protected long curr;

    protected override void ReverseInit()
    {
        long temp = from;
        from = to;
        to = temp;
    }

    protected override void UpdateTween()
    {
        curr = (long)(from + (to - from) * GetEvaluate());
        SetValue(curr);
    }
    

    public long GetValue()
    {
        return curr;
    }

    protected abstract void SetValue(long currValue);
}
