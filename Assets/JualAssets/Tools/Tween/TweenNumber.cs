﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TweenNumber : TweenLong
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private bool addComma = true;
    [SerializeField] private string prefix;
    [SerializeField] private string postfix;

    protected override void SetValue(long currValue)
    {
        string numberString = string.Empty;
        
        numberString += prefix;
        
        if (addComma)
        {
            numberString += GameUtils.Instance.ComputeGameNumber(currValue);
        }
        else
        {
            numberString += currValue.ToString();
        }
        numberString += postfix;
        
        text.text = numberString;
    }

    void OnValidate()
    {
        text = GetComponent<TextMeshProUGUI>();
    }
}
