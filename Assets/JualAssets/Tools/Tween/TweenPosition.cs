﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Transform))]
public class TweenPosition : TweenVector
{
    [SerializeField] private Transform target;

    protected override void SetValue(Vector3 currValue)
    {
        target.localPosition = currValue;
    }
    
    private void OnValidate()
    {
        target = GetComponent<Transform>();
    }

    void Reset()
    {
        from = GetComponent<Transform>().localPosition;
    }

    


}
