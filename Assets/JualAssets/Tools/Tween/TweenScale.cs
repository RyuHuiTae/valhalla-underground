﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenScale : TweenVector
{
    [SerializeField] private Transform trScale;
    protected override void SetValue(Vector3 currValue)
    {
        trScale.localScale = currValue;
    }

    private void OnValidate()
    {
        trScale = GetComponent<Transform>();
    }
}
