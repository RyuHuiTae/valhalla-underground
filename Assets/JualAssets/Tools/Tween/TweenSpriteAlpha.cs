using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class TweenSpriteAlpha : TweenFloat
{
    [SerializeField] private SpriteRenderer spriteRenderer;

    private void OnValidate()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected override void SetValue(float currValue)
    {
        var color = spriteRenderer.color;
        color = new Color(color.r, color.g, color.b, currValue);
        spriteRenderer.color = color;
    }
}