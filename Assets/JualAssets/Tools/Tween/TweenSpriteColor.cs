﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class TweenSpriteColor : TweenColor
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    protected override void SetValue(Color currValue)
    {
        spriteRenderer.color = currValue;
    }

    private void OnValidate()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Init()
    {
        spriteRenderer.color = from;
    }
}
