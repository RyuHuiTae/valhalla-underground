﻿public enum TweenType
{
    Once,
    Ping_pong,
    Ping_pong_Auto,
    Loop,
}