﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class TweenVector : Tween
{
    public Vector3 from;
    public Vector3 to;
    
    private Vector3 _from;
    private  Vector3 _to;
    
    protected Vector3 curr;
    
    protected override void Awake()
    {
        _from = from;
        _to = to;
        
        base.Awake();
    }
    
    public void ResetValue()
    {
        from = _from;
        to = _to;
    }
    
    protected override void ReverseInit()
    {
        var temp = from;
        from = to;
        to = temp;
    }

    protected override void UpdateTween()
    {
        var e = GetEvaluate();
        
        curr = from + (to - from) * e;
        SetValue(curr);

        if (isCalledHalf)
            return;

        if (e > 0.5f)
        {
            isCalledHalf = true;
            callbackHalf?.Invoke();
        }
    }

    public Vector3 GetValue()
    {
        return curr;
    }

    protected abstract void SetValue(Vector3 currValue);
}
