﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UITweenAlpha : TweenFloat
{
    [SerializeField] private Graphic target;

    protected override void SetValue(float currValue)
    {
        var color = target.color;
        color = new Color(color.r, color.g, color.b, currValue);
        target.color = color;
    }

    private void OnValidate()
    {
        target = GetComponent<Graphic>();
    }
}
