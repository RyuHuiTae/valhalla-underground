﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITweenColor : TweenColor
{
    [SerializeField] private Graphic graphic;
    
    protected override void SetValue(Color currValue)
    {
        graphic.color = currValue;
    }

    // private void OnValidate()
    // {
    //     graphic = GetComponent<Graphic>();
    // }

    public void ResetColor()
    {
        //graphic.color = from;
        graphic.color = Color.white;
    }
    
}
