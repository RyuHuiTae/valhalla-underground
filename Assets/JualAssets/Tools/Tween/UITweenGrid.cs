﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class UITweenGrid : MonoBehaviour
{
    [SerializeField] private Vector2 margin;
    [SerializeField] private int column;
    [SerializeField] private Vector2 pivot;
    [SerializeField] private Alignment alignment;
    [SerializeField] private float duration;

    public void ResetPosition()
    {
        Transform[] transforms = GetTransformInChildren();
        List<Vector3> childPositions = new List<Vector3>();

        for (int i = 0; i < transforms.Length; i++)
        {
            float y = 0;
            float x = 0;

            if (column > 0)
            {
                y = i / column;
            }

            x = i - y * column;

            Vector2 margin = this.margin;
            switch (alignment)
            {
                case Alignment.MIDDLE_LEFT:
                    break;
                case Alignment.MIDDLE_CENTER:
                    break;
                case Alignment.MIDDLE_RIGHT:
                    margin = -margin;
                    break;
            }

            childPositions.Add(new Vector2(x * margin.x, y * margin.y));
        }

        if (alignment == Alignment.MIDDLE_CENTER)
        {
            int columnX;
            if (transforms.Length > column)
            {
                columnX = column - 1;
            }
            else
            {
                columnX = (transforms.Length - 1) % column;
            }
            float marginX = columnX % column * margin.x * 0.5f;
            float marginY = (transforms.Length - 1) / column * margin.y * 0.5f;
            Vector3 marginPos = new Vector3(marginX, marginY, 0.0f);
            for (int i = 0; i < transforms.Length; i++)
            {
                childPositions[i] -= marginPos;
            }
        }

        for (int i = 0; i < transforms.Length; i++)
        {
            UITweenPosition tweenPosition = transforms[i].GetComponent<UITweenPosition>();
            if (tweenPosition == null)
            {
                tweenPosition = transforms[i].gameObject.AddComponent<UITweenPosition>();
            }

            tweenPosition.GetComponent<RectTransform>().pivot = pivot;

            tweenPosition.from = Vector3.zero;
            tweenPosition.to = childPositions[i];
            tweenPosition.tweenType = TweenType.Once;
            tweenPosition.duration = duration;
            tweenPosition.transform.localPosition = tweenPosition.from;
            tweenPosition.TweenForward();
        }
    }

    private Transform[] GetTransformInChildren()
    {
        List<Transform> transforms = new List<Transform>();

        foreach (Transform tr in transform)
        {
            if (tr.gameObject.activeInHierarchy)
            {
                transforms.Add(tr);
            }
        }

        return transforms.ToArray();
    }
}