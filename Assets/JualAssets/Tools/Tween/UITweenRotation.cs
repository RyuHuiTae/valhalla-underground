using System;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UITweenRotation : TweenVector
{
    [SerializeField] private RectTransform target;

    // private void OnValidate()
    // {
    //     target = GetComponent<RectTransform>();
    // }
    
    protected override void SetValue(Vector3 currValue)
    {
        target.localRotation = Quaternion.Euler(currValue.x, currValue.y, currValue.z);
    }
}
