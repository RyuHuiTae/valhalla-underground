﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UITweenScale : TweenVector
{
    [SerializeField] private RectTransform target;

    // private void OnValidate()
    // {
    //     target = GetComponent<RectTransform>();
    // }

    protected override void SetValue(Vector3 currValue)
    {
        target.localScale = currValue;
    }
    
}