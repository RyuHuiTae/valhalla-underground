﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(UITweenColor))]
[RequireComponent(typeof(UITweenScale))]
[RequireComponent(typeof(UITweenRotation))]
[RequireComponent(typeof(RectTransform))]
public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    [Header("Image")]
    [SerializeField] private Image image;
    [SerializeField] private Sprite baseSprite;
    [SerializeField] private Sprite pointerDownSprite;
    [SerializeField] private Sprite pointerUpSprite;

    [Header("Color")]
    [SerializeField] private Color32 normalColor = new Color32(255, 255, 255, 255);
    [SerializeField] private Color32 pressedColor = new Color32(200, 200, 200, 255);
    [SerializeField] private Color32 disabledColor = new Color32(200, 200, 200, 255);
    [SerializeField] private float fadeDuration = 0.1f;

    [Header("Scale")]
    [SerializeField] private Vector3 normalScale = new Vector3(1.0f, 1.0f, 1.0f);
    [SerializeField] private Vector3 pressedScale = new Vector3(1.0f, 1.0f, 1.0f);
    [SerializeField] private float scaleDuration = 0.1f;

    [Header("Rotation")]
    [SerializeField] private Vector3 normalRotate = new Vector3(0f, 0f, 0f);
    [SerializeField] private Vector3 clickRotate = new Vector3(0f, 0f, 0f);
    [SerializeField] private float rotationDuration = 0f;
    
    [Header("ETC")]
    [SerializeField] private UITweenColor tweenColor;
    [SerializeField] private UITweenScale tweenScale;
    [SerializeField] private UITweenRotation tweenRotation;
    [SerializeField] private RectTransform rectTransform;
    
    [System.Serializable] public class ButtonEvent : UnityEvent{ }
    [Header("Event")]
    public ButtonEvent onPointerDown;
    public ButtonEvent onPointerUp;
    public ButtonEvent onClick;

    [SerializeField] private string buttonClickSoundPath;
    private const string commonButtonClickSoundPath = "eff/button_clean_mono";

    [SerializeField] private bool isEnabled = true;

    void Awake()
    {
        SetEnable(isEnabled);
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!isEnabled)
        {
            return;
        }
        
        image.sprite = pointerDownSprite != null ? pointerDownSprite : baseSprite;

        tweenColor.from = image.color;
        tweenColor.to = pressedColor;
        tweenColor.TweenForward();

        tweenScale.from = rectTransform.localScale;
        tweenScale.to = pressedScale;
        tweenScale.TweenForward();

        PointerDown(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!isEnabled)
        {
            return;
        }
        
        image.sprite = pointerUpSprite != null ? pointerUpSprite : baseSprite;
        
        tweenColor.from = image.color;
        tweenColor.to = normalColor;
        tweenColor.TweenForward();
        
        tweenScale.from = rectTransform.localScale;
        tweenScale.to = normalScale;
        tweenScale.TweenForward();

        PointerUp(eventData);
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isEnabled)
        {
            return;
        }

        if (buttonClickSoundPath != string.Empty)
        {
            SoundManager.Instance.PlayOneShot(buttonClickSoundPath);
        }
        else
        {
            SoundManager.Instance.PlayOneShot(commonButtonClickSoundPath);
        }
        
        tweenRotation.from = normalRotate;
        tweenRotation.to = clickRotate;
        tweenRotation.TweenForward();

        PointerClick(eventData);
        AudioManager.Instance.EffectSound(AudioManager.EffectType.ButtonClick, false, 0.3f);
    }

    protected virtual void PointerDown(PointerEventData eventData)
    {
        onPointerDown?.Invoke();
    }

    protected virtual void PointerUp(PointerEventData eventData)
    {
        onPointerUp?.Invoke();
    }

    protected virtual void PointerClick(PointerEventData eventData)
    {
        onClick?.Invoke();
    }
    
    public void SetEnable(bool isEnable)
    {
        isEnabled = isEnable;
        tweenColor.Stop();
        image.color = isEnabled ?  normalColor : disabledColor;
    }

    public void SetColor(Color32 color32)
    {
        SetNormalColor(color32);
        SetPressedColor(color32);
        SetDisabledColor(color32);
    }

    public void SetNormalColor(Color32 color)
    {
        normalColor = color;
        image.color = normalColor;
    }

    public void SetPressedColor(Color32 color)
    {
        pressedColor = color;
    }

    public void SetDisabledColor(Color32 color)
    {
        disabledColor = color;
    }

    void OnValidate()
    {
        image = GetComponent<Image>();
        tweenColor = GetComponent<UITweenColor>();
        tweenScale = GetComponent<UITweenScale>();
        tweenRotation = GetComponent<UITweenRotation>();
        rectTransform = GetComponent<RectTransform>();

        if (baseSprite == null)
        {
            baseSprite = image.sprite;
        }

        image.sprite = baseSprite;
        image.color = normalColor;
        tweenColor.duration = fadeDuration;
        tweenScale.duration = scaleDuration;
        tweenRotation.duration = rotationDuration;
    }

    public void SetBaseSprite(Sprite sprite)
    {
        baseSprite = sprite;
    }
    
    public void SetActiveButton(bool isActive)
    {
        SetEnable(isActive);
    }
}
