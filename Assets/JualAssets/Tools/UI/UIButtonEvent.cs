﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIButtonEvent : UIButton
{
    public class ButtonEventData : UnityEvent<PointerEventData> { }

    public ButtonEventData onPointerDownEventData = new ButtonEventData();
    public ButtonEventData onPointerUpEventData = new ButtonEventData();
    public ButtonEventData onClickEventData = new ButtonEventData();

    protected override void PointerDown(PointerEventData eventData)
    {
        base.PointerDown(eventData);
        onPointerDownEventData?.Invoke(eventData);
    }
    
    protected override void PointerUp(PointerEventData eventData)
    {
        base.PointerUp(eventData);
        onPointerUpEventData?.Invoke(eventData);
    }
    
    protected override void PointerClick(PointerEventData eventData)
    {
        base.PointerClick(eventData);
        onClickEventData?.Invoke(eventData);
    }
}
