﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixRotator : MonoBehaviour
{
    [SerializeField] private Vector3 rotatePerTick;
    [SerializeField] private float tickSecond;

    void OnEnable()
    {
        StartCoroutine(Rotate());
    }

    void OnDisable()
    {
        StopCoroutine(Rotate());
    }

    IEnumerator Rotate()
    {
        while (true)
        {
            yield return new WaitForSeconds(tickSecond);

            transform.Rotate(rotatePerTick);
        }
    }
}
