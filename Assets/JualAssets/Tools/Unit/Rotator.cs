﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private Vector3 rotateSpeed;
    
    public void SetRotateSpeed(Vector3 rotateSpeed)
    {
        this.rotateSpeed = rotateSpeed;
    }
    
    void Update()
    {
        transform.Rotate(Time.deltaTime * rotateSpeed);
    }
}
