﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopPopupManager : SingletonMonoBehaviour<TopPopupManager>
{
    private const string TOP_POPUP_TAG = "TopPopupCanvas";
    
    private GameObject topPopupParent;

    private List<CommonTopPopup> topPopupList = new List<CommonTopPopup>();

    public CommonTopPopup CreateTopPopup(TopPopupName topPopupName, bool isAccumulate)
    {
        GameObject newTopPopupPrefab = VResources.Load<GameObject>(FolderPath.TOP_POPUP_FOLDER + topPopupName);

        if (newTopPopupPrefab == null)
        {
            return null;
        }
        
        if (topPopupParent == null)
        {
            topPopupParent = GameObject.FindGameObjectWithTag(TOP_POPUP_TAG);
        }

        if (!isAccumulate)
        {
            HideAll();
        }

        CommonTopPopup newTopPopup = GameUtils.Instance.CreateObject(newTopPopupPrefab, topPopupParent.transform)
            .GetComponent<CommonTopPopup>();
        topPopupList.Add(newTopPopup);
        newTopPopup.Init();

        return newTopPopup;
    }

    public void HideAll()
    {
        if (topPopupList == null)
            return;
        
        foreach (var popup in topPopupList)
        {
            Destroy(popup.gameObject);
        }

        topPopupList.Clear();
    }

    public void HideTopPopup(CommonTopPopup topPopup)
    {
        if (topPopupList == null || topPopupList.Count <= 0)
            return;

        CommonTopPopup recentPopup = topPopupList.Find(x => x == topPopup);

        Destroy(recentPopup.gameObject);
        topPopupList.Remove(recentPopup);
    }
}