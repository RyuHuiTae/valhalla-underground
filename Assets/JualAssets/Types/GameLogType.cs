﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameLogType
{
    DetectedCheating,
    IAP_Purchase_Success,
    IAP_Purchase_Failed,
}