﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public static class ComponentUtils
{
    public static T[] GetComponentsInChildrenFirstDepth<T>(this GameObject gameObject, bool includeInActive) where T: Component
    {
        int childCount = gameObject.transform.childCount;

        List<T> components = new List<T>();

        for (int i = 0; i < childCount; i++)
        {
            if (includeInActive || gameObject.activeInHierarchy)
            {
                T component = gameObject.transform.GetChild(i).GetComponent<T>();
                if (component != null)
                {
                    components.Add(component);    
                }
            }
        }

        return components.ToArray();
    }

    public static T GetComponentInParent<T>(this GameObject gameObject, bool includeInActive) where T : Component
    {
        T[] parentComponents = gameObject.GetComponentsInParent<T>();

        for (int i = 0; i < parentComponents.Length; i++)
        {
            if (includeInActive || parentComponents[i].gameObject.activeInHierarchy)
            {
                if (parentComponents[i].gameObject != gameObject)
                {
                    return parentComponents[i];
                }
            }
            
        }

        return null;
    }
    
    public static Vector3 Dir(this Vector3 curPos, Vector3 targetPos)
    {
        return targetPos - curPos;
    }

    
}
