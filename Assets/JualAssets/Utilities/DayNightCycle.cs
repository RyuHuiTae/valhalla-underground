﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class DayNightCycle : MonoBehaviour
{
    [SerializeField] private Gradient filterGradient;
    [SerializeField] private float dayTime;
    [SerializeField] private Image image;
    [SerializeField] private Text text;
    
    float currTime;

    void Update()
    {
        currTime += Time.deltaTime;
        text.text = currTime.ToString();

        if (dayTime <= currTime)
        {
            currTime = 0.0f;
        }

        image.color = filterGradient.Evaluate(currTime / dayTime);
    }
    
}
