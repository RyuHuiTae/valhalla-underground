﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StringEvent : UnityEvent<string>
{
}


public static class EventUtils
{
    private static Dictionary<EventName, UnityEvent> eventVoid = new Dictionary<EventName, UnityEvent>();

    private static Dictionary<EventName, UnityEvent<string>> eventString =
        new Dictionary<EventName, UnityEvent<string>>();


    public static void AddEventListener(EventName eventName, UnityAction action)
    {
        if (!eventVoid.ContainsKey(eventName))
        {
            eventVoid.Add(eventName, new UnityEvent());
        }

        eventVoid[eventName].AddListener(action);
    }

    public static void RemoveEventListener(EventName eventName, UnityAction action)
    {
        if (!eventVoid.ContainsKey(eventName))
        {
            return;
        }

        eventVoid[eventName].RemoveListener(action);
    }

    public static void TriggerEventListener(EventName eventName)
    {
        if (!eventVoid.ContainsKey(eventName))
        {
            return;
        }

        eventVoid[eventName].Invoke();
    }

    public static void AddEventListener(EventName eventName, UnityAction<string> action)
    {
        if (!eventString.ContainsKey(eventName))
        {
            eventString.Add(eventName, new StringEvent());
        }

        eventString[eventName].AddListener(action);
    }

    public static void RemoveEventListener(EventName eventName, UnityAction<string> action)
    {
        if (!eventString.ContainsKey(eventName))
        {
            return;
        }

        eventString[eventName].RemoveListener(action);
    }

    public static void TriggerEventListener(EventName eventName, string value)
    {
        if (!eventString.ContainsKey(eventName))
        {
            return;
        }

        eventString[eventName].Invoke(value);
    }
}

public enum EventName
{
    
}