﻿using System;
using UnityEngine;

#pragma warning disable 1998

public class GameUtils : SingletonMonoBehaviour<GameUtils>
{
    const long K = 1000;
    const long M = 1000000;
    const long B = 1000000000;
    const long T = 1000000000000;

    private bool isOnSpeedUp;

    private float currInstAdTime = 0.0f;

    private DateTime utcTime;
    private float currGameTime = 0.0f;


    public void SetSpeedUpActive(bool isOn)
    {
        isOnSpeedUp = isOn;
    }

    public GameObject CreateObject(GameObject prefab, Transform parent)
    {
        GameObject createObject = Instantiate(prefab, parent);
        return createObject;
    }

    public void ObjectInit(GameObject obj, Transform rootTransform = null)
    {
        obj.transform.parent = rootTransform;
        var localPosition = obj.transform.localPosition;
        localPosition = new Vector3(
            localPosition.x,
            localPosition.y,
            1.0f);
        obj.transform.localPosition = localPosition;
        obj.transform.localScale = Vector3.one;
    }

    public string ComputeGameNumber(long number)
    {
        if (number >= T)
        {
            return GetSingleDecimal(number, T, nameof(T));
        }

        if (number >= B)
        {
            return GetSingleDecimal(number, B, nameof(B));
        }

        if (number >= M)
        {
            return GetSingleDecimal(number, M, nameof(M));
        }

        return number.ToString();
    }

    public string GetSingleDecimal(long number, long standard, string standardName)
    {
        return $"{number / (double) standard:0.0}" + standardName;
    }

    public void SetInitializeInstTime()
    {
        currInstAdTime = 0.0f;
    }


    void FixedUpdate()
    {
        currInstAdTime += Time.unscaledDeltaTime;
        currGameTime += Time.unscaledDeltaTime;
    }

    public DateTime GetUtcTime()
    {
        return utcTime.AddSeconds(currGameTime);
    }

    public DateTime GetKtcTime()
    {
        return utcTime.AddHours(9);
    }
}