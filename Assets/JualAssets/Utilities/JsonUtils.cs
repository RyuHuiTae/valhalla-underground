﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LitJson;
using UnityEngine;

public static partial class JsonUtils
{
    public const string BackendRows = "rows";


    public static bool IsEmptyDateTime(DateTime dateTime)
    {
        return dateTime == new DateTime();
    }

    public static bool HasKey(this JsonData data, string key)
    {
        IDictionary dict = data;

        return dict.Contains(key);
    }

    public static List<float> GetBackendFloatList(this JsonData data, string key)
    {
        List<string> dataStringList = data.GetBackendDataList(key);
        if (dataStringList != null && dataStringList.All(x => !x.IsNullOrEmpty()))
        {
            List<float> dataList = new List<float>();
            foreach (var x in dataStringList)
            {
                if (float.TryParse(x, out float parseFloat))
                {
                    dataList.Add(parseFloat);
                }
                else
                {
                    return null;
                }
            }

            return dataList;
        }

        return null;
    }

    public static List<int> GetBackendIntList(this JsonData data, string key)
    {
        List<string> dataStringList = data.GetBackendDataList(key);
        if (dataStringList != null && dataStringList.All(x => !x.IsNullOrEmpty()))
        {
            List<int> dataList = new List<int>();
            foreach (var x in dataStringList)
            {
                if (int.TryParse(x, out int parseInt))
                {
                    dataList.Add(parseInt);
                }
                else
                {
                    return null;
                }
            }

            return dataList;
        }

        return null;
    }
    
    public static List<double> GetBackendDoubleList(this JsonData data, string key)
    {
        List<string> dataStringList = data.GetBackendDataList(key);
        if (dataStringList != null && dataStringList.All(x => !x.IsNullOrEmpty()))
        {
            List<double> dataList = new List<double>();
            foreach (var x in dataStringList)
            {
                if (double.TryParse(x, out double parseData))
                {
                    dataList.Add(parseData);
                }
                else
                {
                    return null;
                }
            }

            return dataList;
        }

        return null;
    }

    public static List<long> GetBackendLongList(this JsonData data, string key)
    {
        List<string> dataStringList = data.GetBackendDataList(key);
        if (dataStringList != null && dataStringList.All(x => !x.IsNullOrEmpty()))
        {
            List<long> dataList = new List<long>();
            foreach (var x in dataStringList)
            {
                if (long.TryParse(x, out long parseLong))
                {
                    dataList.Add(parseLong);
                }
                else
                {
                    return null;
                }
            }

            return dataList;
        }

        return null;
    }

    /// <summary>
    /// enum을 사용할 시 패치가 클라이언트와 연관이 되므로 해당 프로젝트에선 사용하지 않습니다.
    /// </summary>
    public static List<T> GetBackendEnumList<T>(this JsonData data, string key) where T : Enum
    {
        List<string> dataStringList = data.GetBackendDataList(key);
        if (dataStringList != null && dataStringList.All(x => !x.IsNullOrEmpty()))
        {
            List<T> dataList = new List<T>();
            dataStringList.ForEach(x => dataList.Add((T) Enum.Parse(typeof(T), x)));

            return dataList;
        }

        return null;
    }

    private static List<string> GetBackendDataList(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            string dataList = data.GetStringBackend(key);
            return dataList.Split(',', ' ').ToList().FindAll(x => x != string.Empty);
        }

        return null;
    }

    public static List<string> GetBackendStringList(this JsonData data, string key)
    {
        List<string> strs = data.GetBackendDataList(key);

        if (strs == null)
        {
            strs = new List<string>();
        }
        return strs;
    }

    public static JsonData GetBackendData(this JsonData data)
    {
        if (data.Keys.Count > 0)
        {
            return data[data.Keys.First()];
        }

        return null;
    }

    public static JsonData GetBackendData(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            if (data[key].Keys.Count > 0)
            {
                return data[key][data[key].Keys.First()];
            }
        }

        return null;
    }

    public static string GetStringBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return data[key][data[key].Keys.First()].ToString();
        }

        return string.Empty;
    }

    public static int GetIntBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return int.Parse(data[key][data[key].Keys.First()].ToString());
        }

        return -1;
    }

    public static long GetLongBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return long.TryParse(data[key][data[key].Keys.First()].ToString(), out var parseLong) ? parseLong : 0;
        }

        return -1;
    }

    public static float GetFloatBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            float output = -1.0f;
            bool canParse = float.TryParse(data[key][data[key].Keys.First()].ToString(), out output);
            if (canParse)
            {
                return output;
            }
            else
            {
                return -1.0f;
            }
        }

        return -1.0f;
    }

    public static bool GetBoolBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            bool boolValue;


            if (bool.TryParse(data[key][data[key].Keys.First()].ToString(), out boolValue))
            {
                return boolValue;
            }
        }

        return false;
    }

    public static DateTime GetDateTimeBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            DateTime dateTime;

            if (DateTime.TryParse(data.GetStringBackend(key), out dateTime))
            {
#if DEV_EDITOR
                Debug.LogWarning("뒤끝 DateTime 초기화 성공");
#endif
                return dateTime;
            }
        }
#if DEV_EDITOR
        Debug.LogWarning("뒤끝 DateTime 초기화 실패");
#endif

        return new DateTime();
    }

    public static double GetDoubleBackend(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return double.Parse(data[key][data[key].Keys.First()].ToString());
        }

        return -1.0;
    }

    /// <summary>
    /// enum을 사용할 시 패치가 클라이언트와 연관이 되므로 해당 프로젝트에선 사용하지 않습니다.
    /// </summary>
    public static T GetEnumBackend<T>(this JsonData data, string key) where T : Enum
    {
        return (T) Enum.Parse(typeof(T), data[key][data[key].Keys.First()].ToString());
    }

    public static string GetString(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            if (data[key] != null)
            {
                return data[key].ToString();
            }
        }

        return string.Empty;
    }

    public static Vector2 GetVector2Backend(this JsonData data, string key)
    {
        Vector2 value = Vector2.zero;
        List<string> strList = data.GetBackendStringList(key);
        if (IsConvertableVector2(strList))
        {
            value.x = float.Parse(strList[0]);
            value.y = float.Parse(strList[1]);
        }

        return value;
    }

    public static Color32 GetColor32Backend(this JsonData data, string key)
    {
        Color32 value = new Color32();
        List<string> strList = data.GetBackendStringList(key);

        value.r = byte.Parse(strList[0]);
        value.g = byte.Parse(strList[1]);
        value.b = byte.Parse(strList[2]);
        value.a = byte.Parse(strList[3]);

        return value;
    }

    private static bool IsConvertableVector2(List<string> dataList)
    {
        float number = 0;
        return dataList.All(x => float.TryParse(x, out number)) && dataList.Count == 2;
    }

    public static int GetInt(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return int.Parse(data[key].ToString());
        }

        return -1;
    }

    public static float GetFloat(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return float.Parse(data[key].ToString());
        }

        return -1.0f;
    }

    public static bool GetBool(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return bool.Parse(data[key].ToString());
        }

        return false;
    }

    public static double GetDouble(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return double.Parse(data[key].ToString());
        }

        return -1.0;
    }

    public static long GetLong(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return long.Parse(data[key].ToString());
        }

        return -1;
    }

    public static JsonData GetNode(this JsonData data, string key)
    {
        if (data.HasKey(key))
        {
            return data[key];
        }

        return null;
    }


    //타입이 안맞으면 오류남. 타입에 맞게 잘 사용할것.
    public static T GetEnum<T>(this JsonData data, string key) where T : Enum
    {
        return (T) Enum.Parse(typeof(T), data[key].ToString());
    }
}