﻿using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.Storage;
using UnityEngine;

public static class SaveUtils
{
    
    public static bool HasKey(string key)
    {
        return ObscuredPrefs.HasKey(key);
    }

    public static bool GetBool(string key, bool defaultValue)
    {
        int defaultValueInt = defaultValue ? 1 : 0;

        int value = GetInt(key, defaultValueInt);

        return value == 1;
    }

    public static void SetBool(string key, bool value)
    {
        SetInt(key, value ? 1 : 0);
    }

    public static string GetString(string key, string defaultValue = "")
    {
        return ObscuredPrefs.GetString(key, defaultValue);
    }

    public static int GetInt(string key, int defaultValue = 0)
    {
        return ObscuredPrefs.GetInt(key, defaultValue);
    }

    public static float GetFloat(string key, float defaultValue = 0.0f)
    {
        return ObscuredPrefs.GetFloat(key, defaultValue);
    }

    public static void SetString(string key, string value)
    {
        ObscuredPrefs.SetString(key, value);
    }

    public static void SetInt(string key, int value)
    {
        ObscuredPrefs.SetInt(key, value);
    }

    public static void SetFloat(string key, float value)
    {
        ObscuredPrefs.SetFloat(key, value);
    }

    public static long GetLong(string key, long value = 0)
    {
        return long.Parse(ObscuredPrefs.GetString(key, value.ToString()));
    }

    public static void SetLong(string key, long value)
    {
        ObscuredPrefs.SetString(key, value.ToString());
    }

    public static void DeleteAll()
    {
        ObscuredPrefs.DeleteAll();
    }
}
