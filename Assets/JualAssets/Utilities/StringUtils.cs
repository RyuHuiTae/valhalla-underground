﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public static class StringUtils
{
    public static bool IsNullOrEmpty(this string str)
    {
        return str == string.Empty || str == "null" || str == "Null" || str == null;
    }

    public static int GetParameterNumber(this string stringFormat)
    {
        var input = stringFormat;
        var pattern = @"{(.*?)}";
        var matches = Regex.Matches(input, pattern);
        var totalMatchCount = matches.Count;
        var uniqueMatchCount = matches.OfType<Match>().Select(m => m.Value).Distinct().Count();
        return uniqueMatchCount;
    }
}
