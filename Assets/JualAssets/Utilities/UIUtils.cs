﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UIUtils
{
    public const string BACKGROUND = "background";
    public const string DEPTH_10 = "depth10";
    public const string DEPTH_20 = "depth20";
    public const string DEPTH_30 = "depth30";
    public const string DEPTH_40 = "depth40";
    public const string DEPTH_50 = "depth50";
    public const string DEPTH_60 = "depth60";
}
