﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effects_DestroyEvent : MonoBehaviour
{
    public void destroyEvent()
    {
        Destroy(gameObject);
    }
    
    public void destroyParentEvent()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }
}
