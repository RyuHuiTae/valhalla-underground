﻿using System;
using UnityEngine;
using System.Collections;

public class Sensor_Prototype : MonoBehaviour {

    private int m_ColCount = 0;
    private float m_DisableTimer;

    private void OnEnable()
    {
        m_ColCount = 0;
    }

    public bool State()
    {
        if (m_DisableTimer > 0)
            return false;
        return m_ColCount > 0;
    }

    private void OnCollisionEnter(Collision other)
    {
        m_ColCount++;
    }

    private void OnCollisionExit(Collision other)
    {
        m_ColCount--;
    }

    // void OnTriggerExit(Collider other)
    // {
    //     m_ColCount--;
    // }

    void Update()
    {
        m_DisableTimer -= Time.deltaTime;
    }

    public void Disable(float duration)
    {
        m_DisableTimer = duration;
    }
}
