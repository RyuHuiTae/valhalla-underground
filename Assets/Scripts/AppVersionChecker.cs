// using System;
// using System.Collections;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text.RegularExpressions;
// using UnityEngine;
// using UnityEngine.Networking;
//
// public class AppVersionChecker : MonoBehaviour
// {
//     // Start is called before the first frame update
//     void Start()
//     {
//         StartCoroutine(AppVersionCheck());
//     }
//
//     public static IEnumerator AppVersionCheck(Action<bool> state = null)
//     {
// #if !UNITY_EDITOR && UNITY_ANDROID
//     string _AppID = "https://play.google.com/store/apps/details?id=com.RaGames.EgyptMatchPop";
// #elif UNITY_EDITOR
//         const string appID = "https://play.google.com/store/apps/details?id=com.valhall.valhallaunderground";
// #endif
//         UnityWebRequest webRequest = UnityWebRequest.Get(appID);
//
//         yield return webRequest.SendWebRequest();
//
//         // 정규식으로 전채 문자열중 버전 정보가 담겨진 태그를 검색한다.
//         const string pattern = @"<span class=""htlgb"">[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}<";
//         Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
//         Match match = regex.Match(webRequest.downloadHandler.text);
//
//         // 버전 정보가 담겨진 태그를 찾음
//         // 해당 태그에서 버전 넘버만 가져온다
//         match = Regex.Match(match.Value, "[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}");
//
//         try
//         {
//             var clientVersion = VersionParser(Application.version);
//             var appStoreVersion = VersionParser(match.Value);
//
//             Debug.Log("  Application.version : " + Application.version + ", AppStore version :" + match.Value);
//
//             if (appStoreVersion[0] != clientVersion[0] || appStoreVersion[1] != clientVersion[1] ||
//                 appStoreVersion[2] != clientVersion[2])
//             {
//                 state?.Invoke(true);
//
//                 yield break;
//             }
//         }
//         catch (Exception Ex)
//         {
//             // 비정상 버전정보 파싱중 Exception처리
//
//             Debug.LogError("비정상 버전 정보 Exception : " + Ex);
//             Debug.LogError("  Application.version : " + Application.version + ", AppStore version :" + match.Value);
//         }
//
//         state?.Invoke(false);
//     }
//
//     private static List<int> VersionParser(string version)
//     {
//         var numberList = new List<int>();
//         var split = version.Split('.');
//         for (int i = 0; i < split.Length; i++)
//         {
//             numberList.Add(int.Parse(split[i]));
//         }
//
//         return numberList;
//     }
// }
