using UnityEngine;
using UnityEngine.U2D;

public class AtlasManager : SingletonMonoBehaviour<AtlasManager>
{
    public SpriteAtlas backgroundAtlas;
    public SpriteAtlas iconAtlas;
    public SpriteAtlas uiAtlas;

    public Sprite GetBackgroundSprite(string spriteName)
    {
        return backgroundAtlas.GetSprite(spriteName);
    }
    
    public Sprite GetIconSprite(string spriteName)
    {
        return iconAtlas.GetSprite(spriteName);
    }
    
    public Sprite GetUISprite(string spriteName)
    {
        return uiAtlas.GetSprite(spriteName);
    }
}
