using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class AudioManager : SingletonMonoBehaviour<AudioManager>
{
    public bool bgm = false;
    public bool effect = false;
    
    public AudioSource bgmSource;    
    
    private readonly string BgmBasePath = "Sound/BGM/";
    private readonly string EffectBasePath = "Sound/Interface/";
    
    private Dictionary<BgmType, AudioClip> bgmDic;
    private Dictionary<EffectType, AudioClip> effectDic;
    
    protected override void Awake()
    {
        base.Awake();

        bgmDic = new Dictionary<BgmType, AudioClip>();
        effectDic = new Dictionary<EffectType, AudioClip>();   
        
        var bgmTypes = Enum.GetNames(typeof(BgmType));
        foreach (var bgmType in bgmTypes)
        {
            var type = (BgmType)Enum.Parse(typeof(BgmType), bgmType);
            if (bgmDic.ContainsKey(type))
                continue;

            var audioClip = VResources.Load<AudioClip>(BgmBasePath + bgmType);

            bgmDic.Add(type, audioClip);
        }
        
        var effectTypes = Enum.GetNames(typeof(EffectType));
        foreach (var effectType in effectTypes)
        {
            var type = (EffectType)Enum.Parse(typeof(EffectType), effectType);
            if (effectDic.ContainsKey(type))
                continue;
            
            var audioClip = VResources.Load<AudioClip>(EffectBasePath + effectType);

            effectDic.Add(type, audioClip);
        }
    }

    public void BgmSound(BgmType type)
    {
        bgmSource.clip = bgmDic[type];
        bgmSource.mute = AudioManager.Instance.bgm;
        bgmSource.loop = true;
        bgmSource.volume = 0.3f;
        bgmSource.pitch = 1.0f;
        bgmSource.Play();
    }
    
    public void EffectSound(EffectType type, bool loop, float volume)
    {
        var clip = effectDic[type];
        
        GameObject effectSound = new GameObject(type.ToString() + "-Sound");
        AudioSource audioSource = effectSound.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.mute = AudioManager.Instance.effect;
        audioSource.volume = volume;
        audioSource.loop = loop;
        audioSource.Play();
        
        if (!loop)
            Destroy(effectSound, clip.length);
    }
    
    public void BgmSound(AudioClip clip)
    {
        bgmSource.clip = clip;
        bgmSource.mute = AudioManager.Instance.bgm;
        bgmSource.loop = true;
        bgmSource.volume = 0.3f;
        bgmSource.pitch = 1.0f;
        bgmSource.Play();
    }

    public void EffectSound(string effectName, AudioClip clip, bool loop, float volume)
    {
        GameObject effectSound = new GameObject(effectName + "-Sound");
        AudioSource audioSource = effectSound.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.mute = AudioManager.Instance.effect;
        audioSource.volume = volume;
        audioSource.loop = loop;
        audioSource.Play();
        
        if (!loop)
            Destroy(effectSound, clip.length);
    }

    public async Task SuddenDeath()
    {
        bgmSource.Stop();
        
        var clip = effectDic[EffectType.SuddenDeath];
        
        GameObject effectSound = new GameObject("SuddenDeath-Sound");
        AudioSource audioSource = effectSound.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.mute = AudioManager.Instance.effect;
        audioSource.volume = 0.3f;
        audioSource.loop = false;
        audioSource.Play();
        
        Destroy(effectSound, audioSource.clip.length);

        await Task.Delay(TimeSpan.FromSeconds(audioSource.clip.length / 4));;
        
        bgmSource.pitch = 1.1f;
        bgmSource.Play();
    }
    
    public enum BgmType
    {
        BattleStage,
        BossStage,
        EventStage,
        GameOver,
        Intro,
        LastBossBattle,
        RestStage,
        Shop,
        StageSelect,
        StarterDeck,
        Title,
    }

    public enum EffectType
    {
        FootPrint,
        ButtonClick,
        PieceMounting,
        Combination,
        Purification,
        Perchase,
        Sale,
        Reinforce,
        SuddenDeath,
        BarrierBreaking,
        StageChangeWaterDrop,
    }
}



