using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks.Triggers;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Buff : BuffBase
{
    public BuffData BuffData { get; private set;}

    private GameObject _buffEffect;

    private float _oldAttackSpeedBuffPercent = 0f;
    private float _oldAttackDamageBuffValue = 0f;
    private float _oldAttackValue = 0;
    private float _oldDefenceValue = 0;
    private BattleLayer _battleLayer;

    private Material _stealthMaterial = null;
    private static readonly int SpriteFade = Shader.PropertyToID("_SpriteFade");

    public virtual void SetBuffData(BuffData buffDataParam, Character buffTargetParam, Character casterCharacter)
    {
        target = buffTargetParam;
        targetAi = target.GetComponent<CharacterAI>();
        targetStatus = target.GetComponent<CharacterStatus>();
        BuffData = buffDataParam;
        id = buffDataParam.ID;

        casterStatus = casterCharacter.GetComponent<CharacterStatus>();
        
        if (!this)
            return;
        
        StarDurationTimerCoroutine(BuffData.Duration);
        StartBuff();
    }

    public void CloneBuff(BuffData buffDataParam, float duration, Character buffTargetParam, CharacterStatus casterStatus)
    {
        target = buffTargetParam;
        targetAi = target.GetComponent<CharacterAI>();
        targetStatus = target.GetComponent<CharacterStatus>();
        id = buffDataParam.ID;
        
        this.casterStatus = casterStatus;
        
        BuffData = buffDataParam;
        StarDurationTimerCoroutine(duration);
        StartBuff();
    }
    
    private void StartBuff()
    {
        if (BuffData.EffectPath != "Null")
        {
            var prefab = VResources.Load<GameObject>(BuffData.EffectPath);
            var box = target.GetComponent<BoxCollider>();
            var position = target.transform.position + box.center;
            position = new Vector3(position.x, position.y - (box.size.y / 2f), position.z);
            _buffEffect = Instantiate(prefab, position, Quaternion.identity);
            _buffEffect.transform.parent = target.transform;
        }

        // 버프가 시작했을 때 호출
        // 1. 스크롤 뷰 콘텐츠에 접근
        // 2. 아이디에 맞는 아이콘 생성
        // 3. 
        switch (BuffData.ChangedState)
        {
            case BuffChangedState.None:
                break;
            case BuffChangedState.Barrier:
                var barrier = target.gameObject.AddComponent<Barrier>();
                float barrierHp = casterStatus.Attack.GetStat() * BuffData.DamageCoefficient + BuffData.AppliedValue;
                barrier.SetBarrier(barrierHp, BuffData.Duration, this);
                break;
            case BuffChangedState.Invincibility:
                target.IsInvincibility = true;
                break;
            case BuffChangedState.AttackSpeedIncrease:
                break;
            case BuffChangedState.Immortality: // characterStatus 에서 부여함 
                break;
            case BuffChangedState.ChangeAggroLevel:
                var aggroLevel = (Character.AggroLevel) BuffData.AppliedValue;
                target.CurrentAggroLevel = aggroLevel;
                if (aggroLevel == Character.AggroLevel.Stealth)
                {
                    var targetSpriteRenderer = target.GetComponent<SpriteRenderer>();
                    _stealthMaterial = new Material(targetSpriteRenderer.material);
                    _stealthMaterial.SetFloat(SpriteFade, 0.5f);
                    targetSpriteRenderer.material = _stealthMaterial;
                }
                break;
            case BuffChangedState.AttackDamageIncrease:
                break;
            case BuffChangedState.ChangeAttackDamageWithDefence:
                var defence = targetStatus.Defence.GetStat();
                var attackDamage = targetStatus.Attack.GetStat();
                
                targetStatus.Defence.SetStat(CharacterStat.StatType.Base, attackDamage);
                targetStatus.Attack.SetStat(CharacterStat.StatType.Base, defence);
                
                targetStatus.Attack.UpdateBaseMaxStat();
                targetStatus.Defence.UpdateBaseMaxStat();
                break;
            case BuffChangedState.Aiming:
                var classSkillCoolTime = targetStatus.ClassSkillCoolTime.GetStat();
                var uniqueSkillCoolTime = targetStatus.UniqueSkillCoolTime.GetStat();
                
                targetStatus.ClassSkillCoolTime.SetStat(CharacterStat.StatType.Base, classSkillCoolTime * 2f);
                targetStatus.ClassSkillCoolTime.UpdateBaseMaxStat();
                targetStatus.UniqueSkillCoolTime.SetStat(CharacterStat.StatType.Base, uniqueSkillCoolTime * 2f);
                targetStatus.UniqueSkillCoolTime.UpdateBaseMaxStat();
                
                var skillDamageX3 = targetStatus.SkillAmplification.GetStat() + targetStatus.SkillAmplification.GetStat() * BuffData.AppliedValue;
                targetStatus.SkillAmplification.SetStat(CharacterStat.StatType.Base, skillDamageX3);
                break;
            case BuffChangedState.SelfDestruct:
                targetStatus.SelfDestruct = BuffData.SummonsData;
                break;
            case BuffChangedState.ChargingAttack:
                targetStatus.AttackSpeed.IncreaseStat(CharacterStat.StatType.Base, -targetStatus.AttackSpeed.GetStat() * 0.35f);

                var attackDamage220Percent = targetStatus.Attack.GetStat() + targetStatus.Attack.GetStat() * BuffData.AppliedValue;
                targetStatus.Attack.SetStat(CharacterStat.StatType.Base, attackDamage220Percent);
                break;
            case BuffChangedState.Overdrive:
                targetStatus.receivedDamageAmplificationPercent += 0.35f;
                targetStatus.Attack.IncreaseStat(CharacterStat.StatType.BuffPercent, BuffData.AppliedValue);
                break;
            case BuffChangedState.Preparation:
                _battleLayer = LayerManager.Instance.GetCurrentLayer().GetComponent<BattleLayer>();
                targetStatus.Attack.SetStat(CharacterStat.StatType.Base, targetStatus.Attack.GetBaseMax() / 2f);
                targetStatus.Defence.SetStat(CharacterStat.StatType.Base, targetStatus.Defence.GetBaseMax() / 2f);
                break;
            case BuffChangedState.SwitchStance:
                targetStatus.Defence.SetStat(CharacterStat.StatType.Base, targetStatus.Defence.GetStat() * BuffData.AppliedValue);
                targetStatus.Defence.UpdateBaseMaxStat();
                break;
            default:    
                throw new ArgumentOutOfRangeException();
        }
    }
    
    // Update is called once per frame
    private void Update()
    {
        // TODO: 매 프레임에 불리기엔 좀 부담
        UpdateBuffState();
    }

    private void UpdateBuffState()
    {
        // 실시간으로 값을 갱신해야 되는 버프들의 로직
        // 1. 버프 타입에 따라 값을 갱신시킴
        var baseValue = 0f;
        switch (BuffData.IsWhatBase)
        {
            case IsWhatBase.None:
                break;
            case IsWhatBase.HpPercent:
                baseValue = 1f - target.GetCurrentHpPercent();
                break;
            case IsWhatBase.Distance:
                break;
            case IsWhatBase.TimeSpent:
                baseValue = _battleLayer.StartTime - _battleLayer.TimerTime;
                break;
            case IsWhatBase.AttackCount:
                baseValue = casterStatus.AttackCount;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        switch (BuffData.ChangedState)
        {
            case BuffChangedState.None:
                break;
            case BuffChangedState.Barrier:
                break;
            case BuffChangedState.Invincibility:
                break;
            case BuffChangedState.AttackSpeedIncrease:
                float newAttackSpeedBuffPercent = baseValue * BuffData.AppliedValue;
                
                targetStatus.AttackSpeed.UpdateStat(CharacterStat.StatType.BuffPercent, _oldAttackSpeedBuffPercent, newAttackSpeedBuffPercent);
                _oldAttackSpeedBuffPercent = newAttackSpeedBuffPercent;
                break;
            case BuffChangedState.Immortality: // characterStatus 에서 부여함 
                break;
            case BuffChangedState.ChangeAggroLevel:
                break;
            case BuffChangedState.AttackDamageIncrease:
                float newAttackDamageBuffValue = (baseValue * 100f) * BuffData.AppliedValue;
                
                targetStatus.Attack.UpdateStat(CharacterStat.StatType.Base, _oldAttackDamageBuffValue, newAttackDamageBuffValue);
                _oldAttackDamageBuffValue = newAttackDamageBuffValue;
                break;
            case BuffChangedState.ChangeAttackDamageWithDefence:
                break;
            case BuffChangedState.Aiming:
                break;
            case BuffChangedState.SelfDestruct:
                break;
            case BuffChangedState.ChargingAttack:
                break;
            case BuffChangedState.Overdrive:
                break;
            case BuffChangedState.Preparation:
                var newAttackValue = targetStatus.Attack.GetBaseMax() * BuffData.AppliedValue * baseValue;
                var newDefenceValue = targetStatus.Defence.GetBaseMax() * BuffData.AppliedValue * baseValue;
                
                targetStatus.Attack.UpdateStat(CharacterStat.StatType.Base, _oldAttackValue, newAttackValue);
                targetStatus.Defence.UpdateStat(CharacterStat.StatType.Base, _oldDefenceValue, newDefenceValue);
                _oldAttackValue = newAttackValue;
                _oldDefenceValue = newDefenceValue;
                break;
            case BuffChangedState.SwitchStance:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    private void OnDestroy()
    {
        DestroyBuff();
    }

    private void DestroyBuff()
    {
        // duration이 다 되었을 때 버프를 삭제시키면서 호출됨
        // 1. 아이콘 삭제
        // 2. 버프 해제
        switch (BuffData.ChangedState)
        {
            case BuffChangedState.None:
                break;
            case BuffChangedState.Barrier:
                target.GetComponent<Barrier>().CrashBarrier();
                break;
            case BuffChangedState.Invincibility:
                target.IsInvincibility = false;
                break;
            case BuffChangedState.AttackSpeedIncrease:
                targetStatus.AttackSpeed.IncreaseStat(CharacterStat.StatType.BuffPercent, -_oldAttackSpeedBuffPercent);
                break;
            case BuffChangedState.Immortality:
                target.GetComponent<CharacterStatus>().isImmortality = false;
                break;
            case BuffChangedState.ChangeAggroLevel:
                target.CurrentAggroLevel = (Character.AggroLevel.Normal);
                // TODO: 디스펠이 있을 경우 머티리얼이 사라지게 되어있으니까 개선해야함 
                if (_stealthMaterial)
                {
                    Destroy(_stealthMaterial);
                }
                break;
            case BuffChangedState.AttackDamageIncrease:
                targetStatus.Attack.UpdateStat(CharacterStat.StatType.Base, _oldAttackSpeedBuffPercent, 0f);
                break;
            case BuffChangedState.ChangeAttackDamageWithDefence:
                break;
            case BuffChangedState.Aiming:
                break;
            case BuffChangedState.SelfDestruct:
                break;
            case BuffChangedState.ChargingAttack:
                break;
            case BuffChangedState.Overdrive:
                targetStatus.receivedDamageAmplificationPercent -= 0.5f;
                targetStatus.Attack.IncreaseStat(CharacterStat.StatType.BuffPercent, -BuffData.AppliedValue / 100f);
                break;
            case BuffChangedState.Preparation:
                break;
            case BuffChangedState.SwitchStance:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        if (_buffEffect)
            Destroy(_buffEffect.gameObject);
        Destroy(gameObject);   
    }
}
