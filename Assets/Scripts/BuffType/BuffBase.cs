using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

// TODO: 클론 인터페이스 상속
public class BuffBase : MonoBehaviour
{
    private Image buffIcon;
    protected Character target;
    protected CharacterAI targetAi;
    protected CharacterStatus targetStatus;

    public CharacterStatus casterStatus { get; protected set; }

    public float CurrentDuration { get; private set; }
    protected float maxDuration;
    protected int id;
    
    private void Awake()
    {
        buffIcon = transform.GetChild(0).GetComponent<Image>();
    }
    
    protected async void StarDurationTimerCoroutine(float duration)
    {
        CurrentDuration = duration;
        maxDuration = duration;
        if (CurrentDuration == 0f) // 시작하자마자 한번만 적용되고 무한으로 지속되는 버프 
        {
            CurrentDuration = float.MaxValue;
        }
        
        while (CurrentDuration > 0f)
        {
            if (CurrentDuration > 0f)
            {
                CurrentDuration -= Time.deltaTime;
            }

            await UniTask.WaitForFixedUpdate();
        }
        
        if (this)
            Destroy(gameObject);
    }
    
    public static void GiveBuffBase(ICustomData buffDataBase, GameObject target, Character casterCharacter)
    {
        // 이미 가지고 있는 버프 일 경우 시간만 갱신함
        var targetCharacter = target.GetComponent<Character>();
        var parent = targetCharacter.GetBuffsLayoutTransform();
        var buffs = parent.GetComponentsInChildren<Buff>();
        var deBuffs = parent.GetComponentsInChildren<Debuff>();
        if (buffDataBase.GetType() == typeof(BuffData))
        {
            for (int i = 0; i < buffs.Length; i++)
            {
                if (buffs[i].id == buffDataBase.ID)
                {
                    buffs[i].ResetDuration();
                    return;
                }
            }
        }
        else
        {
            for (int i = 0; i < deBuffs.Length; i++)
            {
                if (deBuffs[i].id == buffDataBase.ID)
                {
                    deBuffs[i].ResetDuration();
                    return;
                }
            }
        }

        var buffBasePrefab = VResources.Load<GameObject>("Prefabs/Buff/BuffBase");
        var buffGameObject = Instantiate(buffBasePrefab, parent);

        if (buffDataBase.GetType() == typeof(BuffData))
        {
            var buff = buffGameObject.AddComponent<Buff>();
            var buffData = buffDataBase as BuffData;
            buff.SetBuffData(buffData, targetCharacter, casterCharacter);
            buff.SetIcon(buffData.IconName);
        }
        else if (buffDataBase.GetType() == typeof(DebuffData))
        {
            var debuff = buffGameObject.AddComponent<Debuff>();
            var debuffData = buffDataBase as DebuffData;
            debuff.SetBuffData(debuffData, targetCharacter, casterCharacter);
            debuff.SetIcon(debuffData.IconName);
        }
    }

    private void SetIcon(string iconName)
    {
        if (!buffIcon)
            return;

        var iconPrefab = AtlasManager.Instance.GetIconSprite(iconName);
        buffIcon.sprite = iconPrefab;
    }
    
    private void ResetDuration()
    {
        CurrentDuration = maxDuration;
    }
}
