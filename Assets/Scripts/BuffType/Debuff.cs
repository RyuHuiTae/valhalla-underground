using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuff : BuffBase
{
    public DebuffData DeBuffData { get; private set; }
    private GameObject debuffEffect;

    public void SetBuffData(DebuffData deBuffDataParam, Character debuffTargetParam, Character casterCharacter)
    {
        target = debuffTargetParam;
        targetStatus = target.GetComponent<CharacterStatus>();
        targetAi = target.GetComponent<CharacterAI>();
        id = deBuffDataParam.ID;
        
        casterStatus = casterCharacter.GetComponent<CharacterStatus>();
        
        DeBuffData = deBuffDataParam;

        if (!this)
            return;
        
        StarDurationTimerCoroutine(DeBuffData.Duration);
        StartDeBuff();
    }

    public void CloneBuff(DebuffData deBuffDataParam, float duration, Character debuffTargetParam, CharacterStatus casterStatus)
    {
        target = debuffTargetParam;
        targetStatus = target.GetComponent<CharacterStatus>();
        targetAi = target.GetComponent<CharacterAI>();
        id = deBuffDataParam.ID;
        
        this.casterStatus = casterStatus;
        
        DeBuffData = deBuffDataParam;
        StarDurationTimerCoroutine(duration);
        StartDeBuff();
    }

    
    private void StartDeBuff()
    {
        if (DeBuffData.EffectPath != "Null")
        {
            var prefab = VResources.Load<GameObject>(DeBuffData.EffectPath);
            var box = target.GetComponent<BoxCollider>();
            var position = target.transform.position + box.center;
            position = new Vector3(position.x, position.y - (box.size.y / 2f), position.z);
            debuffEffect = Instantiate(prefab, position, Quaternion.identity);
        }
        
        // 버프가 시작했을 때 호출
        // 1. 스크롤 뷰 콘텐츠에 접근
        // 2. 아이디에 맞는 아이콘 생성
        // 3. 
        switch (DeBuffData.DeBuffType)
        {
            case DeBuffType.None:
                break;
            case DeBuffType.DamageReceived:
                targetStatus.receivedDamageAmplificationPercent += DeBuffData.AppliedPercentValue * 0.01f;
                break;
            case DeBuffType.AttackSpeed:
                targetStatus.AttackSpeed.IncreaseStat(CharacterStat.StatType.DebuffPercent, DeBuffData.AppliedPercentValue * 0.01f);
                break;
            case DeBuffType.Stun:
                targetAi.Stun();
                break;
            case DeBuffType.CanNotUseUniqueSkill:
                target.GetComponent<UniqueSkillBase>().UniqueSkillPenalty();
                break;
            case DeBuffType.DefenseDecrease:
                targetStatus.Defence.IncreaseStat(CharacterStat.StatType.Base, DeBuffData.AppliedValue);
                break;
            case DeBuffType.ChangingBuffValue:
                targetStatus.IsCurse = true;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    // Update is called once per frame
    private void Update()
    {
        UpdateDeBuffState();
    }

    private void UpdateDeBuffState()
    {
        // 실시간으로 값을 갱신해야 되는 버프들의 로직
        // 1. 값을 갱신시킴
    }
    
    private void OnDestroy()
    {
        DestroyDeBuff();
    }

    private void DestroyDeBuff()
    {
        // duration이 다 되었을 때 버프를 삭제시키면서 호출됨
        // 1. 아이콘 삭제
        // 2. 버프 해제
        switch (DeBuffData.DeBuffType)
        {
            case DeBuffType.None:
                break;
            case DeBuffType.DamageReceived:
                targetStatus.receivedDamageAmplificationPercent -= DeBuffData.AppliedPercentValue * 0.01f;
                break;
            case DeBuffType.AttackSpeed:
                targetStatus.AttackSpeed.IncreaseStat(CharacterStat.StatType.DebuffPercent, -DeBuffData.AppliedPercentValue * 0.01f);
                break;
            case DeBuffType.Stun:
                targetAi.ClearStun();
                break;
            case DeBuffType.CanNotUseUniqueSkill:
                break;
            case DeBuffType.DefenseDecrease:
                targetStatus.Defence.IncreaseStat(CharacterStat.StatType.Base, -DeBuffData.AppliedValue);
                break;
            case DeBuffType.ChangingBuffValue:
                targetStatus.IsCurse = false;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        if (debuffEffect)
            Destroy(debuffEffect.gameObject);
    }
}
