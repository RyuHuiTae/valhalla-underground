using UnityEngine;

public class CameraResolution : MonoBehaviour
{
    private int ScreenSizeX = 0;
    private int ScreenSizeY = 0;

    private void RescaleCamera()
    {
        if (Screen.width == ScreenSizeX && Screen.height == ScreenSizeY) return;
 
        float targetaspect1 = 16.0f / 9.0f;
        float targetaspect2 = 18.0f / 9.0f;

        float windowaspect = (float)Screen.width / (float)Screen.height;
        float targetaspect = targetaspect1;

        if (windowaspect > targetaspect1)
        {
            //화면 비율이 16:9 보다 크므로 18:9 적용
            targetaspect = targetaspect2;
        }

        float scaleheight = windowaspect / targetaspect;
        Camera camera = GetComponent<Camera>();
 
        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;
 
            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
 
            camera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / scaleheight;
 
            Rect rect = camera.rect;
 
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
 
            camera.rect = rect;
        }
 
        ScreenSizeX = Screen.width;
        ScreenSizeY = Screen.height;
    }

    // Use this for initialization
    void Start () 
    {
        RescaleCamera();
    }
   
    // Update is called once per frame
    void Update () 
    {
        RescaleCamera();
    }
}
