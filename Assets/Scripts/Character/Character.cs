using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public enum CharacterTeam
    {
        None,
        Player,
        Enemy
    }
    
    public enum AggroLevel
    {
        None,
        Stealth,
        Normal,
        Aggravation
    }
    
    public int Uid { get; private set; }
    public CharacterTeam Team { get; private set; }
    public CharacterData Data { get; private set; }
    private CharacterStatus CharacterStatus { get; set; }
    private CharacterAI CharacterAI { get; set; }
    public AggroLevel CurrentAggroLevel { get; set; } = AggroLevel.Normal;
    
    public float AttackDamageAmount { get; private set; }
    public float ReceivedDamageAmount { get; private set; }
    public float HealAmount { get; private set; }
    public bool IsDie { get; private set; }
    public bool IsInvincibility { get; set; }
    public bool IsClone => CharacterStatus.IsClone;
    
    [SerializeField] private CharacterDamageText _damageTextPrefab;
    [SerializeField] private Transform _damageTextRoot;
    
    private TweenSpriteColor _damageTween;
    private CharacterCanvas _characterCanvas;
    
    private GameObject _characterIconUi;
    private Image _iconUiImage;
    private Image _grayScaleIconUi;
    private Image _characterDeathCross;
    
    private GameField _gameField;
    
    private static readonly int GrayscaleAmountProperty = Shader.PropertyToID("_GrayscaleAmount");
    
    private void Awake()
    {
        CharacterStatus = GetComponent<CharacterStatus>();
        CharacterAI = GetComponent<CharacterAI>();
        _damageTween = GetComponent<TweenSpriteColor>();
        _characterCanvas = GetComponentInChildren<CharacterCanvas>();
        
        _gameField = GameField.Instance;
    }

    public void Init(int uid, CharacterTeam team, CharacterData data, Vector3 pos, GameObject characterIconUI,
        CharacterSkills characterSkills)
    {
        this.Uid = uid;
        this.Team = team;
        this.Data = data;
        this.transform.position = pos;

        var iconSprite = AtlasManager.Instance.GetIconSprite(characterSkills.uniqueSkill.IconName);
        _characterCanvas.BaseIcon.sprite = iconSprite;
        _characterCanvas.UniqueSkillCoolTimeIcon.sprite = iconSprite;
        
        this._characterIconUi = characterIconUI;
        if (characterIconUI)
        {
            _iconUiImage = characterIconUI.transform.Find("Icon").GetComponent<Image>();
            _grayScaleIconUi = characterIconUI.transform.Find("GrayScaleIcon").GetComponent<Image>();
            _characterDeathCross = characterIconUI.transform.Find("CharacterDeathCross").GetComponent<Image>();

            var hpImage = characterIconUI.transform.Find("HpBarFrame").Find("HpBar").GetComponent<Image>();
            CharacterStatus.Init(Data, ChangeHpEvent, DieEvent, hpImage, _characterCanvas.HpFilledImage);
        }
        else 
            CharacterStatus.Init(Data, ChangeHpEvent, DieEvent, null, _characterCanvas.HpFilledImage);
        
        CharacterAI.Init(
            MoveTo, 
            FindNearEnemy,
            Casting,
            characterSkills);
        
        AttackDamageAmount = 0;
        ReceivedDamageAmount = 0;
        HealAmount = 0;
        IsDie = false;
        
        _damageTween.Init();
    }

    private void Casting(SkillBase skillBase, Character target, float attackSpeed)
    {
        var damage = CharacterStatus.Attack.GetStat() * skillBase.GetDamageCoefficient() + skillBase.GetDamage();
        skillBase.Casting(target, damage, attackSpeed);
    }
    
    private void ChangeHpEvent()
    {
        _gameField.UpdateTotalHp();
    }

    public float ReceiveDamage(float damage, DebuffData debuffData)
    {
        if (debuffData != null && debuffData.ID != 0)
        {
            BuffBase.GiveBuffBase(debuffData, gameObject, this);
        }
        
        if (damage <= 0f) return 0;
        if (IsDie) return 0;
        if (IsInvincibility)
        {
            CreateMessageText("무적");
            return 0;
        }
        if (CharacterStatus.isImmortality)
        {
            CreateMessageText("불사");
            return 0;
        }
        
        var barrier = GetComponent<Barrier>();
        damage = CharacterStatus.ReceiveDamage(damage, barrier);
        CreateDamageText(damage, CharacterDamageText.TextType.Damage);
        if (!barrier)
            ReceivedDamageAmount += damage;
        
        _damageTween.TweenForward();
        return damage;
    }

    public void ReceiveTrueDamage(float damage, DebuffData debuffData = null)
    {
        if (debuffData != null && debuffData.ID != 0)
        {
            BuffBase.GiveBuffBase(debuffData, gameObject, this);
        }

        if (IsInvincibility)
        {
            CreateMessageText("무적");
            return;
        }
        if (CharacterStatus.isImmortality)
        {
            CreateMessageText("불사");
            return;
        }
        
        var barrier = GetComponent<Barrier>();
        damage = CharacterStatus.ReceiveTrueDamage(damage, barrier);
        
        CreateDamageText(damage, CharacterDamageText.TextType.Damage);
        ReceivedDamageAmount += damage;
        
        _damageTween.TweenForward();
    }

    public void DieClone(float damage)
    {
        damage = CharacterStatus.ReceiveTrueDamage(damage, null);
        CreateDamageText(damage, CharacterDamageText.TextType.Damage);
        _damageTween.TweenForward();
    }
    
    public bool DieEvent()
    {
        IsDie = true;
        _characterCanvas.gameObject.SetActive(false);
        GetComponent<BoxCollider>().enabled = false;
        var barrier = GetComponent<Barrier>();
        if (barrier)
        {
            barrier.CrashBarrier();
        }

        if (_characterIconUi)
        {
            _iconUiImage.gameObject.SetActive(false);
            _grayScaleIconUi.gameObject.SetActive(true);
            _characterDeathCross.gameObject.SetActive(true);
            _characterIconUi.GetComponent<ObjectShake>().Shake();
        }

        CharacterAI.Dead();
        
        _gameField.CheckAliveCharacter();

        return IsDie;
    }
    
    public void MoveTo(Vector3 pos)
    {
        var maxDistanceDelta = CharacterStatus.MoveSpeed.GetStat() * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, pos, maxDistanceDelta);
    }

    private void CreateDamageText(float damage, CharacterDamageText.TextType type)
    {
        var newDamageText = Instantiate(_damageTextPrefab, _damageTextRoot);
        newDamageText.Init(damage, type);
        newDamageText.gameObject.SetActive(true);
    }
    
    private void CreateMessageText(string message)
    {
        var newDamageText = Instantiate(_damageTextPrefab, _damageTextRoot);
        newDamageText.Init(message);
        newDamageText.gameObject.SetActive(true);
    }

    public Character FindNearEnemy() => _gameField.FindEnemies(transform.position, Team, IsWhatBase.Distance, Priority.Ascending)?.FirstOrDefault();
    public Character FindAllyCharacter() => _gameField.FindEnemies(transform.position, Team == CharacterTeam.Player ? CharacterTeam.Enemy : CharacterTeam.Player, IsWhatBase.HpPercent, Priority.Ascending)?.FirstOrDefault();
    
    public float GetCurrentHp() => CharacterStatus.Hp.GetStat();
    
    public float GetCurrentHpPercent() => CharacterStatus.GetCurrentHpPercent();
    
    public float GetMaxHp() => CharacterStatus.Hp.GetBaseMax();

    public Transform GetBuffsLayoutTransform() => _characterCanvas.BuffsLayout.transform;

    public void AddHealAmount(float heal)
    {
        HealAmount += heal;
    }

    public void AddAttackDamageAmount(float damage)
    {
        AttackDamageAmount += damage;
    }
    
    public void Resurrection(float hpPercentValue)
    {
        IsDie = false;
        _characterCanvas.gameObject.SetActive(true);
        GetComponent<BoxCollider>().enabled = true;
        if (_iconUiImage)
        {
            _iconUiImage.gameObject.SetActive(true);
            _grayScaleIconUi.gameObject.SetActive(false);   
            _characterDeathCross.gameObject.SetActive(false);
        }
        
        CharacterStatus.Resurrection(hpPercentValue);
        CharacterAI.Resurrection();
    }

    public bool Heal(float heal, Character caster)
    {
        var isSelf = caster.Data.CharacterType == Data.CharacterType; 
        heal = CharacterStatus.Heal(heal, isSelf);

        if (heal == 0)
            return false;
        
        CreateDamageText(heal, CharacterDamageText.TextType.Heal);
        return true;
    }

    public Image GetCharacterBaseIcon() => _characterCanvas.BaseIcon;
    
    public Image GetUniqueSkillCoolTimeIcon() => _characterCanvas.UniqueSkillCoolTimeIcon;
    
    public Image GetClassSkillCoolTimeFilledImage() => _characterCanvas.ClassSkillCoolTimeFilledImage;

    private void OnDestroy()
    {
        if (_characterIconUi)
            Destroy(_characterIconUi);
    }
}
