using System;
using Cysharp.Threading.Tasks;
using DevSupport;
using MonsterLove.StateMachine;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class CharacterAI : MonoBehaviour
{
	private enum AIStates
	{
		Init,
		Stand,
		Move,
		Attack,
		Dead
	}

	private StateMachine<AIStates> _fsm;
	private UniqueSkillBase _uniqueSkillBase;
	private ClassSkillBase _classSkillBase;
	private BasicSkillBase _basicSkillBase;

	private Animator _animator;
	private static readonly int AnimState = Animator.StringToHash("AnimState");
	private static readonly int Attack1 = Animator.StringToHash("Attack1");
	private static readonly int Death = Animator.StringToHash("Death");
	private static readonly int AttackSpeed = Animator.StringToHash("AttackSpeed");
	private static readonly int Hurt = Animator.StringToHash("Hurt");
	
	private SpriteRenderer _spriteRenderer;

	private GameField _gameField;
	private Character _targetEnemy;
	private Vector3 _targetEnemyPosition;

	private Action<Vector3> MoveTo { get; set; }
	private Func<Character> FindNearEnemy { get; set; }
	private Action<SkillBase, Character, float> Casting { get; set; }

	private CharacterStatus _characterStatus;
	
	public void Init(Action<Vector3> moveTo, Func<Character> findNearEnemy, Action<SkillBase, Character, float> casting,
		CharacterSkills characterSkills)
	{
		this.MoveTo = moveTo;
		this.FindNearEnemy = findNearEnemy;
		this.Casting = casting;

		_basicSkillBase.SetSkillData(characterSkills.basicSkill, UpdateTarget);
		_classSkillBase.SetSkillData(characterSkills.classSkill, UpdateTarget);
		_uniqueSkillBase.SetSkillData(characterSkills.uniqueSkill, UpdateTarget);
		
		_characterStatus.AttackSpeed.SubscribeStat(UpdateAnimationAttackSpeed);
	}

	private void UpdateTarget(Character target)
	{
		_targetEnemy = target;
	}
	
	private void Awake()
	{
		_basicSkillBase = GetComponent<BasicSkillBase>();
		_classSkillBase = GetComponent<ClassSkillBase>();
		_uniqueSkillBase = GetComponent<UniqueSkillBase>();

		_characterStatus = GetComponent<CharacterStatus>();
		
		_animator = GetComponent<Animator>();
		_spriteRenderer = GetComponent<SpriteRenderer>();
		_gameField = GameField.Instance;
	}

	private void Start()
    {
	    _fsm = StateMachine<AIStates>.Initialize(this, AIStates.Init);
    }

    private async void Init_Enter()
    {
	    DebugX.Log("<color=green>Enter:init</color>");
	    await UniTask.Delay(TimeSpan.FromSeconds(1f));
	    
	    if (!this) return;
	    if (GetComponent<Character>().IsDie)
	    {
		    _fsm.ChangeState(AIStates.Dead);
	    }
	    else _fsm.ChangeState(AIStates.Stand);
    }

	private void Stand_Enter()
	{
		DebugX.Log("<color=green>Enter:Stand</color>");

		if (_characterStatus.isStun)
		{
			ChangeAnimationByStun();
			return;
		}
		ChangeAnimationByStand();
	}

	private void Stand_Update()
	{
		if (_characterStatus.isStun)
		{
			return;
		}
		
		if (_gameField.IsBattleStart && !_gameField.IsBattleEnd)
		{
			_fsm.ChangeState(AIStates.Move);
		}
	}
	
	private void Move_Enter()
	{
		DebugX.Log("<color=green>Enter:Move</color>");
		DebugX.Log("캐릭터 이동 애니메이션(반복) 출력");
		ChangeAnimationByMove();
	}

	private void Move_Update()
	{
		if (_gameField.IsBattleEnd)
		{
			_fsm.ChangeState(AIStates.Stand);
			return;
		}

		//DebugX.Log("Update:Move 캐릭터 이동 실행 중");
		_targetEnemy = FindNearEnemy?.Invoke();
		
		if(!_targetEnemy) return;
		_targetEnemyPosition = _targetEnemy.transform.position;
		
		MoveToEnemy();
		FlipSpriteAccordanceWithDirection();

		var canAttack = _basicSkillBase.CanCasting(_targetEnemy) || _classSkillBase.CanCasting(_targetEnemy) || _uniqueSkillBase.CanCasting(_targetEnemy);
		if (canAttack)
		{
			_fsm.ChangeState(AIStates.Attack);
		}
	}

	private void Attack_Enter()
	{
		DebugX.Log("<color=green>Enter:Attack</color>");
		ChangeAnimationByAttack();
	}

	private void Attack_Update()
	{
		var isAttacking = _basicSkillBase.IsCasting || _classSkillBase.IsCasting || _uniqueSkillBase.IsCasting;
		if (isAttacking) return;

		if (_gameField.IsBattleEnd || !_targetEnemy || _targetEnemy.IsDie)
		{
			_fsm.ChangeState(AIStates.Move);
			return;
		}
		
		//DebugX.Log("Update:Attack");
		ChangeAnimationByAttack();
			
		_targetEnemyPosition = _targetEnemy.transform.position;
		FlipSpriteAccordanceWithDirection();
		
		if (_uniqueSkillBase.CanCasting(_targetEnemy))
		{
			Casting?.Invoke(_uniqueSkillBase, _targetEnemy, _characterStatus.AttackSpeed.GetStat());
			_animator.SetTrigger(_uniqueSkillBase.GetAnimationTriggerName);
		}
		else if (_classSkillBase.CanCasting(_targetEnemy))
		{
			Casting?.Invoke(_classSkillBase, _targetEnemy, _characterStatus.AttackSpeed.GetStat());
			_animator.SetTrigger(_classSkillBase.GetAnimationTriggerName);
		}
		else if (_basicSkillBase.CanCasting(_targetEnemy))
		{
			Casting?.Invoke(_basicSkillBase, _targetEnemy, _characterStatus.AttackSpeed.GetStat());
			_animator.SetTrigger(_basicSkillBase.GetAnimationTriggerName);
		}
		else
		{
			_fsm.ChangeState(AIStates.Move);
		}
	}

	public void Dead()
	{
		_fsm.ChangeState(AIStates.Dead);
	}

	public void Resurrection()
	{
		_characterStatus.isStun = false;
		_fsm.ChangeState(AIStates.Move);
		_uniqueSkillBase.UniqueSkillPenalty();
		ChangeSpriteLayerByLive();
	}
	
	private void Dead_Enter()
	{
		ChangeAnimationByDeath();
		ChangeSpriteLayerByDeath();
	}

	private void ChangeSpriteLayerByDeath()
	{
		_spriteRenderer.sortingOrder = -1;
	}

	private void ChangeSpriteLayerByLive()
	{
		_spriteRenderer.sortingOrder = 0;
	}
	
	private void Dead_Exit()
	{
		//gameObject.SetActive(true);
	}
	
	private void MoveToEnemy()
	{
		MoveTo?.Invoke(_targetEnemyPosition);
	}

	public void FlipSpriteAccordanceWithDirection()
	{
		_spriteRenderer.flipX = !(transform.position.x < _targetEnemyPosition.x);
	}
	
	private void ChangeAnimationByStand()
	{
		_animator.SetInteger(AnimState, 0);
	}
	
	private void ChangeAnimationByStun()
	{
		_animator.SetInteger(AnimState, 2);
		_animator.SetTrigger(Hurt);
	}
	
	private void ChangeAnimationByMove()
	{
		_animator.SetInteger(AnimState, 1);
	}
	
	private void ChangeAnimationByAttack()
	{
		_animator.SetInteger(AnimState, 0);
	}
	
	private void ChangeAnimationByDeath()
	{
		_animator.SetTrigger(Death);
		_animator.SetInteger(AnimState, -1);
	}

	private void UpdateAnimationAttackSpeed()
	{
		_animator.SetFloat(AttackSpeed, _characterStatus.AttackSpeed.GetStat());
	}

	public void Stun()
	{
		_characterStatus.isStun = true;
		_fsm.ChangeState(AIStates.Stand);
	}

	public void ClearStun()
	{
		_characterStatus.isStun = false;
		if (!GetComponent<Character>().IsDie)
		{
			_fsm.ChangeState(AIStates.Move);
		}
	}
	
	public bool GetCurrentFlip => _spriteRenderer.flipX;
}