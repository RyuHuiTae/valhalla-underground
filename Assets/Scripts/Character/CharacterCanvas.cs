using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class CharacterCanvas : MonoBehaviour
{
    [SerializeField] private Image _hpFilledImage;
    [SerializeField] private Image _classSkillCoolTimeFilledImage;
    [SerializeField] private Image _baseIcon;
    [SerializeField] private Image _uniqueSkillCoolTimeIcon;
    [SerializeField] private RectTransform _buffsLayout;

    public Image HpFilledImage => _hpFilledImage;
    public Image ClassSkillCoolTimeFilledImage => _classSkillCoolTimeFilledImage;
    public Image BaseIcon => _baseIcon;
    public Image UniqueSkillCoolTimeIcon => _uniqueSkillCoolTimeIcon;
    public RectTransform BuffsLayout => _buffsLayout;
    
    // TODO: 기능을 여기로 끌어옴
}
