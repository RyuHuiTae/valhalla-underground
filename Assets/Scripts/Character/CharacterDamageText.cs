using System;
using TMPro;
using UnityEngine;

public class CharacterDamageText : MonoBehaviour
{
    public enum TextType
    {
        Damage,
        Heal,
        Invincibility
    }
    
    [SerializeField] private TextMeshProUGUI damageText;
    [SerializeField] private UITweenPosition tween;

    public void Awake()
    {
        tween.SetCallbackFinished(TweenEndEvent);
    }

    public void Init(float damage, TextType type)
    {
        damageText.text = Mathf.Ceil(damage).ToString();
        damageText.color = type switch
        {
            TextType.Damage => Color.red,
            TextType.Heal => Color.green,
            _ => damageText.color
        };
    }
    
    public void Init(string message)
    {
        damageText.text = message;
        damageText.color = Color.yellow;
    }
    
    private void TweenEndEvent()
    {
        Destroy(gameObject);
    }
}
