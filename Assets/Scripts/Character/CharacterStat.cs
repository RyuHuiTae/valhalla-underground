using System;
using UniRx;

public class CharacterStat : ICloneable
{
    public enum StatType
    {
        Base,
        BuffPercent,
        DebuffPercent,
    }

    private readonly ReactiveProperty<float> _baseMax = new ReactiveProperty<float>();
    
    private readonly ReactiveProperty<float> _baseStat = new ReactiveProperty<float>();
    private readonly ReactiveProperty<float> _buffPercentStat = new ReactiveProperty<float>(); // 0 ~ infinity
    private readonly ReactiveProperty<float> _debuffPercentStat = new ReactiveProperty<float>(); // 1 ~ 0
    
    private readonly ReactiveProperty<float> _calculatedStat = new ReactiveProperty<float>();

    private IDisposable _buffLimitDisposable;
    private IDisposable _debuffLimitDisposable;
    
    public float GetBaseMax() => _baseMax.Value;
    public float GetStat() => _calculatedStat.Value;
    
    public CharacterStat(float baseMaxStat, float buffPercentStat = 0f, float debuffPercentStat = 1f)
    {
        _baseMax.Value = baseMaxStat;
        
        _baseStat.Value = baseMaxStat;
        _buffPercentStat.Value = buffPercentStat;
        _debuffPercentStat.Value = debuffPercentStat;

        _baseStat.Subscribe(_ => CalculateStat());
        _buffPercentStat.Subscribe(_ => CalculateStat());
        _debuffPercentStat.Subscribe(_ => CalculateStat());
        
        CalculateStat();
        
        void CalculateStat()
        {
            var baseStatValue = _baseStat.Value;
            var buffPercentStatValue = _buffPercentStat.Value;
            var debuffPercentStatValue = _debuffPercentStat.Value;
            var calculatedStat = (baseStatValue + (baseStatValue * buffPercentStatValue)) * debuffPercentStatValue;
        
            _calculatedStat.SetValueAndForceNotify(calculatedStat);
        }
    }

    public void SetStat(StatType statType, float value)
    {
        switch (statType)
        {
            case StatType.Base:
                _baseStat.SetValueAndForceNotify(value);
                break;
            case StatType.BuffPercent:
                _buffPercentStat.SetValueAndForceNotify(value);
                break;
            case StatType.DebuffPercent:
                _debuffPercentStat.SetValueAndForceNotify(value);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(statType), statType, null);
        }
    }

    public void IncreaseStat(StatType statType, float value)
    {
        switch (statType)
        {
            case StatType.Base:
                _baseStat.SetValueAndForceNotify(_baseStat.Value + value);
                break;
            case StatType.BuffPercent:
                _buffPercentStat.SetValueAndForceNotify(_buffPercentStat.Value + value);
                break;
            case StatType.DebuffPercent:
                _debuffPercentStat.SetValueAndForceNotify(_debuffPercentStat.Value + value);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(statType), statType, null);
        }
    }
    
    public void UpdateStat(StatType statType, float oldValue, float newValue)
    {
        float baseStatValue = 0f;

        switch (statType)
        {
            case StatType.Base:
                baseStatValue = _baseStat.Value;
                break;
            case StatType.BuffPercent:
                baseStatValue = _buffPercentStat.Value;
                break;
            case StatType.DebuffPercent:
                baseStatValue = _debuffPercentStat.Value;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(statType), statType, null);
        }
        
        baseStatValue -= oldValue;
        baseStatValue += newValue;
        
        switch (statType)
        {
            case StatType.Base:
                _baseStat.SetValueAndForceNotify(baseStatValue);
                break;
            case StatType.BuffPercent:
                _buffPercentStat.SetValueAndForceNotify(baseStatValue);
                break;
            case StatType.DebuffPercent:
                _debuffPercentStat.SetValueAndForceNotify(baseStatValue);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(statType), statType, null);
        }
    }

    public void UpdateBaseMaxStat()
    {
        _baseMax.SetValueAndForceNotify(_calculatedStat.Value);
    }
    
    public void SubscribeStat(Action action)
    {
        _calculatedStat.Subscribe(_ => action?.Invoke());
    }

    public void SubscribeBaseMax(Action action)
    {
        _baseMax.Subscribe(_ => action?.Invoke());
    }

    public void LimitBuffPercent(float limit)
    {
        _buffLimitDisposable?.Dispose();
        _buffLimitDisposable = _buffPercentStat.Subscribe(Limit);
        Limit(_buffPercentStat.Value);
        
        void Limit(float percent)
        {
            if (percent < -limit)
            {
                _buffPercentStat.SetValueAndForceNotify(-limit);
            }
        }
    }
    
    public void LimitDebuffPercent(float limit)
    {
        _debuffLimitDisposable?.Dispose();
        _debuffLimitDisposable = _debuffPercentStat.Subscribe(percent =>
        {
            if (percent < limit)
            {
                _debuffPercentStat.SetValueAndForceNotify(limit);
            }
        });
    }
    
    public object Clone()
    {
        return new CharacterStat(_baseStat.Value, _buffPercentStat.Value, _debuffPercentStat.Value);
    }
    
    ~CharacterStat()
    {
        _baseStat.Dispose();
        _buffPercentStat.Dispose();
        _debuffPercentStat.Dispose();
        _calculatedStat.Dispose();
    }
}