using System;
using DevSupport;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatus : MonoBehaviour
{
    public CharacterStat Hp { get; private set; }
    public CharacterStat Defence { get; private set; }
    public CharacterStat MoveSpeed { get; private set; }
    public CharacterStat Attack { get; private set; }
    public CharacterStat AttackSpeed { get; private set; }
    public CharacterStat ClassSkillCoolTime { get; private set; }
    public CharacterStat UniqueSkillCoolTime { get; private set; }
    public CharacterStat SkillAmplification { get; private set; }

    [HideInInspector]
    public bool isStun;
    [HideInInspector]
    public bool isImmortality;
    [HideInInspector]
    public float receivedDamageAmplificationPercent;
    public bool IsCurse { get; set; }
    public bool IsClone { get; private set; }
    public SummonsData SelfDestruct { get; set; }
    public int AttackCount { get; set; }

    private Image _hpImage;
    private bool _isExecute = false;
    
    private GameSetting _gameSetting;

    public float GetCurrentHpPercent() => _hpImage.fillAmount;
    
    private void Awake()
    {
        _gameSetting = GameSetting.Instance;
    }

    public void Init(CharacterData data, Action changeHpEvent, Func<bool> dieEvent, Image iconHpImage, Image hpImage)
    {
        Hp = new CharacterStat(data.Hp);
        Defence = new CharacterStat(data.Defense);
        MoveSpeed = new CharacterStat(data.MoveSpeed);
        Attack = new CharacterStat(data.Attack);
        
        AttackSpeed = new CharacterStat(1f);
        ClassSkillCoolTime = new CharacterStat(0f);
        UniqueSkillCoolTime = new CharacterStat(0f);

        SkillAmplification = new CharacterStat(1f);

        ClassSkillCoolTime.SubscribeStat(() => ClassSkillCoolTime.UpdateBaseMaxStat());
        
        Hp.UpdateBaseMaxStat();
        Defence.UpdateBaseMaxStat();
        MoveSpeed.UpdateBaseMaxStat();
        Attack.UpdateBaseMaxStat();
        AttackSpeed.UpdateBaseMaxStat();
        
        Hp.SubscribeBaseMax(() =>
        {
            var fill = Hp.GetStat() / Hp.GetBaseMax();
            hpImage.fillAmount = fill;
        });
        
        receivedDamageAmplificationPercent = 1f;
        
        _hpImage = hpImage;
        _isExecute = false;

        var changingHpAction = new Action(() =>
        {
            if (Hp.GetStat() < 0f)
                Hp.SetStat(CharacterStat.StatType.Base, 0f);
            
            var fill = Hp.GetStat() / Hp.GetBaseMax();
            hpImage.fillAmount = fill;

            if (iconHpImage)
                iconHpImage.fillAmount = fill;

            changeHpEvent?.Invoke();

            if (IsDead() && !_isExecute)
            {
                var uniqueSkill = GetComponent<UniqueSkillBase>();
                var isImmortal = uniqueSkill.IsImmortality();
                if (isImmortal)
                {
                    uniqueSkill.GiveAppliedBuff();
                    isImmortality = true;
                    Hp.SetStat(CharacterStat.StatType.Base, 1f);
                    return;
                }
                
                if (SelfDestruct != null && !_isExecute)
                {
                    SummonsBase.CreateSummons(SelfDestruct, gameObject, gameObject);
                }
                
                _isExecute = dieEvent.Invoke();
            }
            
            bool IsDead()
            {
                return Hp.GetStat() <= 0f;
            }
        });
        Hp.SubscribeStat(changingHpAction);
    }

    public float ReceiveDamage(float damage, Barrier barrier)
    {
        if (isImmortality)
        {
            Hp.SetStat(CharacterStat.StatType.Base, 1f);
            return 0f;
        }

        damage *= receivedDamageAmplificationPercent;
        float defencePercent = CalculateDefencePercent();
        float resultDamage = CalculateDamage();
        
        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (_gameSetting.isInvincibility)
            resultDamage = 0f;
#endif
        
        if (barrier)
        {
            barrier.ReceiveDamage(resultDamage);
            return resultDamage;
        }

        Hp.IncreaseStat(CharacterStat.StatType.Base, -resultDamage);
        return resultDamage;
        
        float CalculateDefencePercent()
        {
            return Defence.GetStat() / (100 + Defence.GetStat());
        }

        float CalculateDamage()
        {
            return damage - (damage * defencePercent);
        }
    }

    public float ReceiveTrueDamage(float damage, Barrier barrier)
    {
        if (isImmortality)
        {
            Hp.SetStat(CharacterStat.StatType.Base, 1f);
            return 0f;
        }
        
        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (_gameSetting.isInvincibility)
            damage = 0f;
#endif
        
        if (barrier)
        {
            barrier.ReceiveDamage(damage);
            return damage;
        }

        Hp.IncreaseStat(CharacterStat.StatType.Base, -damage);
        return damage;
    }
    
    public float Heal(float heal, bool isSelf)
    {
        if (isSelf)
            heal *= 1.2f;

        float hp = Hp.GetStat();
        float maxHp = Hp.GetBaseMax();
        if (heal + hp > maxHp)
        {
            heal = maxHp - hp;
            Hp.SetStat(CharacterStat.StatType.Base, maxHp);
            return heal;
        }
        
        Hp.IncreaseStat(CharacterStat.StatType.Base, heal);
        return heal;
    }

    public void Resurrection(float hpPercentValue)
    {
        float percent = hpPercentValue / 100f;
        Hp.SetStat(CharacterStat.StatType.Base, Hp.GetBaseMax() * percent);
        _isExecute = false;
    }
    
    public void Cloning(float percent, Character caster)
    {
        IsClone = true;
        float calculatedPercent = (percent * 0.01f);
        CharacterStatus casterStatus = caster.GetComponent<CharacterStatus>();
        
        Hp.SetStat(CharacterStat.StatType.Base, casterStatus.Hp.GetStat() * calculatedPercent);
        Hp.UpdateBaseMaxStat();

        Attack.SetStat(CharacterStat.StatType.Base, casterStatus.Attack.GetStat() * calculatedPercent);
        Attack.UpdateBaseMaxStat();
    }
}