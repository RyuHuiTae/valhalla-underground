using SQLite.Attributes;

public class AbilityPieceData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; } 
    public string Name { get; set; }
    public int BuffId { get; set; }
    public BuffData Buff { get; set; }
    public string Desc { get; set; }
}
