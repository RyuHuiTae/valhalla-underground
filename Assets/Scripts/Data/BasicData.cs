using SQLite.Attributes;

public class BasicData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public float Value { get; set; }
}
