public class BattleResultNarrationData : ICustomData
{
    public int ID { get; set; }
    public string Name { get; set; }
    public NarrationBattleType BattleType { get; set; }
    public NarrationResultType ResultType { get; set; }
    public string BattleResultNarration { get; set; }
}

public enum NarrationBattleType
{
    Normal,
    Elite,
    Boss,
    FinalBoss,
}

public enum NarrationResultType
{
    Win,
    Lose,
    WinFinalBoss,
    GameOver,
}