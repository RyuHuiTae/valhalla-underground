public class BattleStageData : ICustomData
{
    public int ID { get; set; }
    public string Name { get; set; }
    public int Floor { get; set; }
    public BattleStageType Type { get; set; }
    public int Level { get; set; }
    public int EnemyPuzzleId1 { get; set; }
    public int StageEnhanceGroupId1 { get; set; }
    public int EnemyPosIndex1 { get; set; }
    public int EnemyPuzzleId2 { get; set; }
    public int StageEnhanceGroupId2 { get; set; }
    public int EnemyPosIndex2 { get; set; }
    public int EnemyPuzzleId3 { get; set; }
    public int StageEnhanceGroupId3 { get; set; }
    public int EnemyPosIndex3 { get; set; }

    public StageType GetStageType()
    {
        if (Type == BattleStageType.NormalBattle)
            return StageType.NormalBattle;
        else if (Type == BattleStageType.EliteBattle)
            return StageType.EliteBattle;
        else if (Type == BattleStageType.Boss)
            return StageType.Boss;
        else if (Type == BattleStageType.FinalBoss)
            return StageType.FinalBoss;

        return StageType.None;
    }
    
    public int[] GetEnemyPuzzleIdArray()
    {
        return new[]
        {
            EnemyPuzzleId1,
            EnemyPuzzleId2,
            EnemyPuzzleId3,
        };
    }
    
    public int[] GetEnhanceGroupIdArray()
    {
        return new[]
        {
            StageEnhanceGroupId1,
            StageEnhanceGroupId2,
            StageEnhanceGroupId3,
        };
    }

    public int[] GetEnemyPosIndexArray()
    {
        return new[]
        {
            EnemyPosIndex1,
            EnemyPosIndex2,
            EnemyPosIndex3,
        };
    }
}

public enum BattleStageType
{
    None,
    NormalBattle,
    EliteBattle,
    Boss,
    FinalBoss,
}
