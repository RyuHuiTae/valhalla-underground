public class BattleStageEnhanceData : ICustomData
{
    public int ID { get; set; }
    public string Name { get; set; }
    public int GroupId { get; set; }
    public int PuzzleDataId { get; set; }
}
