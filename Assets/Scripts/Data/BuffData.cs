using System;using SQLite.Attributes;

public class BuffData : ICustomData, ICloneable
{
    [PrimaryKey] public int ID { get; set; }
    public int Level { get; set; }
    public string Name { get; set; }
    public BuffChangedState ChangedState { get; set; }
    public IsWhatBase IsWhatBase { get; set; }
    public Priority Priority { get; set; }
    public float AppliedValue { get; set; }
    public float ResurrectionHpPercentValue { get; set; }
    public float DamageCoefficient { get; set; }
    public float Duration { get; set; }
    public bool IsImmortality { get; set; }
    public string EffectPath { get; set; }
    public string IconName { get; set; }
    public int SummonId { get; set; }
    public SummonsData SummonsData { get; set; }

    public object Clone()
    {
        return new BuffData()
        {
            ID = this.ID,
            Level = this.Level,
            Name = this.Name,
            ChangedState = this.ChangedState,
            IsWhatBase = this.IsWhatBase,
            Priority = this.Priority,
            AppliedValue = this.AppliedValue,
            DamageCoefficient = this.DamageCoefficient,
            ResurrectionHpPercentValue = this.ResurrectionHpPercentValue,
            Duration = this.Duration,
            IsImmortality = this.IsImmortality,
            EffectPath = this.EffectPath,
            IconName = this.IconName,
            SummonId = this.SummonId,
            SummonsData = this.SummonsData
        };
    }
}

public enum BuffChangedState
{
    None,           //없음
    Barrier,        //보호막
    Invincibility,  //무적
    Immortality,    // 불멸
    AttackSpeedIncrease, // 공속 증가
    ChangeAggroLevel,
    AttackDamageIncrease,
    ChangeAttackDamageWithDefence,
    Aiming,
    SelfDestruct,
    ChargingAttack,
    Overdrive,
    Preparation,
    SwitchStance
}