using SQLite.Attributes;

public class BuyPieceData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public int BuyPieceId1 { get; set; }
    public int BuyPieceId2 { get; set; }
    public int BuyPieceId3 { get; set; }
    public int BuyPieceId4 { get; set; }
    public int BuyPieceId5 { get; set; }
    public int BuyPieceId6 { get; set; }
}
