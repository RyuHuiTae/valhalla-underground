using SQLite.Attributes;

public class BuyPieceRandomData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public int BuyPieceId { get; set; }
    public int PieceDataId { get; set; }
    public int PiecePrice { get; set; }
    public int Floor { get; set; }
}
