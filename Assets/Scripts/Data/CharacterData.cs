using SQLite.Attributes;

public class CharacterData : ICustomData
{
    [PrimaryKey] public int ID { get; set; }
    public int Level { get; set; }
    public string Name { get; set; }
    public CharacterAttackType Type { get; set; }
    public CharacterType CharacterType { get; set; }
    public float Hp { get; set; }
    public float Attack { get; set; }
    public float Defense { get; set; }
    public float MoveSpeed { get; set; }

    public float AttackSpeed = 1.0f;
    public int BasicSkillId { get; set; }
    public int ClassSkillId { get; set; }
    public int UniqueSkillId { get; set; }
    public string Path { get; set; }
    public int AggroLevel { get; set; }
    public bool Playerable { get; set; }

    public SkillData BasicSkill { get; set; }
    public SkillData ClassSkill { get; set; }
    public SkillData UniqueSkill { get; set; }
}

public class CharacterAddStat
{
    public ulong Uid { get; private set; }
    public float Hp { get; set; }               //합
    public float Attack { get; set; }           //합
    public float Defense { get; set; }          //합
    public float MoveSpeed { get; set; }        //합
    public float AttackSpeed { get; set; }      //퍼센트 합
    public float SkillCoolTime { get; set; }    //퍼센트 합

    public CharacterAddStat(ulong uid = 0)
    {
        this.Uid = uid;
        this.Hp = 0;
        this.Attack = 0;
        this.Defense = 0;
        this.MoveSpeed = 0;
        this.AttackSpeed = 0;
        this.SkillCoolTime = 0;
    }

    public static void SetStatChangeValue(CharacterAddStat newStatData, PuzzleData puzzleData)
    {
        var changeStatType = puzzleData.StatPieceData.ChangedStat;
        var value = puzzleData.StatPieceData.AppliedValue;
        
        switch (changeStatType)
        {
            case StatPieceChangedStat.AttackSpeedIncrease:      //합
                newStatData.AttackSpeed += value;
                break;
            case StatPieceChangedStat.AttackDamageIncrease:     //합
                newStatData.Attack += value;
                break;
            case StatPieceChangedStat.DefenseIncrease:          //합
                newStatData.Defense += value;
                break;
            case StatPieceChangedStat.HpIncrease:               //합
                newStatData.Hp += value;
                break;
            case StatPieceChangedStat.MoveSpeedIncrease:        //합
                newStatData.MoveSpeed += value;
                break;
            case StatPieceChangedStat.SkillCoolTimeReduction:   //합
                newStatData.SkillCoolTime += value;
                break;
        }
    }
}

public enum CharacterAttackType
{
    None, //없음
    NearField, //근거리
    Ranged, //원거리
    Supporter, //서포터
}

public enum CharacterType
{
    None,
    Barbarian,
    Rogue,
    DarkKnight,
    StormMage,
    FireMage,
    Druid,
    Priest,
    Abomination,
    Test,
}