public class CustomData : ICustomData
{
    public int ID { get; set; }
    public string Name { get; set; }

    public void Init()
    {
        this.ID = 0;
        this.Name = "데이터 없음";
    }
}