using SQLite.Attributes;

public class DebuffData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public int Level { get; set; }
    public string Name { get; set; }
    public DeBuffType DeBuffType { get; set; }
    public float AppliedValue { get; set; }
    public float AppliedPercentValue { get; set; }
    public float Duration { get; set; }
    public string EffectPath { get; set; }
    public string IconName { get; set; }
}

public enum DeBuffType
{
    None,                   //없음
    DamageReceived,         //화상
    AttackSpeed,            //마비
    Stun,                   //스턴
    DefenseDecrease,        //방어구 파괴
    CanNotUseUniqueSkill,   //패널티
    ChangingBuffValue
}