using System;
using System.Collections.Generic;
using SQLite.Attributes;
using UnityEngine;

public class EventStageChoiceData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public string RandomRewardIdArray { get; set; }
    public ConditionType ConditionType { get; set; }
    public EventBattleType BattleType { get; set; }
    public int ConditionMany { get; set; }
    public string EventChoiceText { get; set; }
    public string EventChangeText { get; set; }
    public string EventChoiceChangeText { get; set; }
    public List<RandomRewardData> RandomRewardDataList { get; set; }

    public int[] GetRandomRewardIdArray()
    {
        var playerPuzzleObject = JsonUtility.FromJson<RandomRewardIdArrayObject>(RandomRewardIdArray);
        return playerPuzzleObject?.randomRewardIdArray;
    }
}

[Serializable]
public class RandomRewardIdArrayObject
{
    public int[] randomRewardIdArray;
}

public enum ConditionType
{
    None,
    LoseMoney,
    LoseLife,
    PollutionClear,
    LosePiece,
    LoseAbilityPiece,
    LoseCharacterPiece,
    LoseStatPiece,
}

public enum EventBattleType
{
    None,
    Normal,
    Elite,
}