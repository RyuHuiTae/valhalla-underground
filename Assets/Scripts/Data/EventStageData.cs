using System.Collections.Generic;
using SQLite.Attributes;

public class EventStageData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public string EventImageName { get; set; }
    public int EventChoiceId1 { get; set; }
    public int EventChoiceId2 { get; set; }
    public int EventChoiceId3 { get; set; }
    public string EventText { get; set; }
    public EventStageAppearType Type { get; set; }
    public int Seq { get; set; }
    
    public bool IsUsedStage { get; set; } = false;

    public List<EventStageChoiceData> EventChoices { get; set; } = new List<EventStageChoiceData>();
}

public enum EventStageAppearType
{
    None,   //기본 (2층~5층)
    Tutorial, //튜토리얼에서 나올 이벤트
    FinalBoss, //최심부 전용 이벤트
}