public class GameIntroData : ICustomData
{
    public int ID { get; set; }
    public string Name { get; set; }
    public int Scene { get; set; }
    public int Paragraph { get; set; }
    public IntroType Type { get; set; }
    public string IntroText { get; set; }
}

public enum IntroType
{
    Intro,
    Outtro,
}
