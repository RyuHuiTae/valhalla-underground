public interface ICustomData
{
    int ID { get; }
    string Name { get; }
}