using System;
using System.Collections.Generic;
using System.Linq;
using SQLite.Attributes;
using UnityEngine;

[Serializable]
public class PuzzlePosIndexObject
{
    public int[] puzzlePosIndexArray;
}

[Serializable]
public class TutorialCheckObject
{
    public bool[] tutorialCheckArray;
}

public class PlayerData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public int Heart { get; set; }
    public int Money { get; set; }
    public int Soul { get; set; }
    public int Floor { get; set; }
    public int BarrierCount { get; set; }
    public int FinalBossLevel { get; set; }
    public int ClearCount { get; set; }
    public bool IsShowIntro { get; set; }
    public int StarterCharacterPuzzleID { get; set; }
    public string PuzzleListJson { get; set; }
    public string InstalledPuzzleListJson { get; set; }
    public string StageListJson { get; set; }
    public string PuzzlePosIndexJson { get; set; }
    public string TutorialCheckJson { get; set; }
    
    public static PlayerData GetNewPlayerData()
    {
        var newPlayerData = new PlayerData()
        {
            ID = 0,
            Name = "player",
            Heart = 3,
            Money = 0,
            Soul = 0,
            Floor = 1,
            BarrierCount = 100,
            FinalBossLevel = 0,
            ClearCount = 0,
            IsShowIntro = false,
            StarterCharacterPuzzleID = 0,
            PuzzleListJson = string.Empty,
            InstalledPuzzleListJson = string.Empty,
            StageListJson = string.Empty,
            PuzzlePosIndexJson = string.Empty,
            TutorialCheckJson = string.Empty,
        };

        return newPlayerData;
    }
    
    /// <summary>
    /// 게임 오버 / 결계 파괴 실패로 인한 리셋 (결계 횟수는 리셋 안함)
    /// </summary>
    public static PlayerData GetResetPlayerData()
    {
        var newPlayerData = new PlayerData()
        {
            ID = 0,
            Name = "player",
            Heart = 3,
            Money = 0,
            Soul = 0,
            Floor = 1,
            FinalBossLevel = 0,
            IsShowIntro = false,
            StarterCharacterPuzzleID = 0,
            PuzzleListJson = string.Empty,
            InstalledPuzzleListJson = string.Empty,
            StageListJson = string.Empty,
            PuzzlePosIndexJson = string.Empty,
        };

        return newPlayerData;
    }
    
    /// <summary>
    /// 게임 클리어로 인한 리셋 (클리어 횟수 추가)
    /// </summary>
    public static PlayerData GetClearPlayerData()
    {
        var newPlayerData = new PlayerData()
        {
            ID = 0,
            Name = "player",
            Heart = 3,
            Money = 0,
            Soul = 0,
            Floor = 1,
            BarrierCount = 100,
            FinalBossLevel = 0,
            IsShowIntro = false,
            StarterCharacterPuzzleID = 0,
            PuzzleListJson = string.Empty,
            InstalledPuzzleListJson = string.Empty,
            StageListJson = string.Empty,
            PuzzlePosIndexJson = string.Empty,
        };

        return newPlayerData;
    }
    
    public List<PlayerPuzzleData> GetPlayerPuzzleData()
    {
        var playerPuzzleObject = JsonUtility.FromJson<PlayerPuzzleObject>(PuzzleListJson);
        return playerPuzzleObject?.playerPuzzleData.ToList() ?? new List<PlayerPuzzleData>();
    }

    public List<PlayerInstalledPuzzleData> GetPlayerInstalledPuzzleData()
    {
        var playerPuzzleObject = JsonUtility.FromJson<PlayerInstalledPuzzleObject>(InstalledPuzzleListJson);
        return playerPuzzleObject?.playerInstalledPuzzleData.ToList() ?? new List<PlayerInstalledPuzzleData>();
    }

    public List<PlayerStageData> GetPlayerStageData()
    {
        var playerStageObject = JsonUtility.FromJson<PlayerStageObject>(StageListJson);
        return playerStageObject?.playerStageData.ToList() ?? new List<PlayerStageData>();
    }

    public int[] GetPuzzlePosIndexData()
    {
        //기본 위치 값
        var defaultPosIndex = new int[] {7, 1, 9};
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return defaultPosIndex;
        
        var puzzlePosIndexObject = JsonUtility.FromJson<PuzzlePosIndexObject>(PuzzlePosIndexJson);
        if (puzzlePosIndexObject == null) 
            return defaultPosIndex;
        
        var posIndexList = puzzlePosIndexObject?.puzzlePosIndexArray?.ToList();

        //null이면 디폴트값 리턴
        if (posIndexList == null)
            return defaultPosIndex;
        
        //1개 이상일때
        if (posIndexList.Count > 0)
        {
            var newIndex = 0;
            //최대 장착 가능한 캐릭터 수 만큼 반복
            while (posIndexList.Count < (int)basicDB.CharacterUseMaxCount())
            {
                if (!posIndexList.Contains(newIndex))
                {
                    //비어있는 인덱스 찾아서 추가
                    posIndexList.Add(newIndex);
                }
                newIndex++;
            }

            return posIndexList.ToArray();
        }
        //비어있다면 디폴트값 리턴
        else return defaultPosIndex;
    }

    public bool[] GetTutorialCheckData()
    {
        var tutorialCheckObject = JsonUtility.FromJson<TutorialCheckObject>(TutorialCheckJson);
        if (tutorialCheckObject?.tutorialCheckArray != null)
        {
            return tutorialCheckObject.tutorialCheckArray;
        }

        var length = Enum.GetValues(typeof(TutorialTextType)).Length;
                
        var newData = new bool[length];
        newData.Initialize();

        return newData;
    }
    
    public void SetPlayerPuzzleData(List<PlayerPuzzleData> list)
    {
        var newData = new PlayerPuzzleObject {playerPuzzleData = list.ToArray()};
        PuzzleListJson = JsonUtility.ToJson(newData);
    }
    
    public void SetPlayerInstalledPuzzleData(List<PlayerInstalledPuzzleData> list)
    {
        var newData = new PlayerInstalledPuzzleObject {playerInstalledPuzzleData = list.ToArray()};
        InstalledPuzzleListJson = JsonUtility.ToJson(newData);
    }

    public void SetPlayerStageData(List<PlayerStageData> list)
    {
        var newData = new PlayerStageObject {playerStageData = list.ToArray()};
        StageListJson = JsonUtility.ToJson(newData);
    }

    public void SetPuzzlePosIndexData(int[] posIndexArray)
    {
        var newData = new PuzzlePosIndexObject {puzzlePosIndexArray = posIndexArray};
        PuzzlePosIndexJson = JsonUtility.ToJson(newData);
    }

    public void SetTutorialCheckData(bool[] tutorialCheckArray)
    {
        var newData = new TutorialCheckObject {tutorialCheckArray = tutorialCheckArray};
        TutorialCheckJson = JsonUtility.ToJson(newData);
    }
    
    /// <summary>
    /// PlayerDataBase로 호출해야 함
    /// </summary>
    public void SetMoney(int money)
    {
        Money += money;
        if (Money < 0)
        {
            Money = 0;
        }
    }

    /// <summary>
    /// PlayerDataBase로 호출해야 함
    /// </summary>
    public void SetSoul(int soul)
    {
        Soul += soul;
        if (Soul < 0)
        {
            Soul = 0;
        }
    }
    
    /// <summary>
    /// PlayerDataBase로 호출해야 함
    /// </summary>
    public bool SetHeart(int heart)
    {
        Heart += heart;
        
        if (Heart < 0)
        {
            Heart = 0;
            return false;
        }
        
        if (Heart > 3)
        {
            return false;
        }

        return true;
    }

    public void SetNextFloor()
    {
        Floor += 1;
    }

    public void SetFloor(int floor)
    {
        Floor = floor;
    }
    
    public void SetIntroFlag(bool isShow)
    {
        IsShowIntro = isShow;
    }

    public void SetBarrierCount(int count)
    {
        BarrierCount = count;
    }

    public void SetFinalBossLevel(int level)
    {
        FinalBossLevel = level;
    }

    public void SetClearCount(int count)
    {
        ClearCount = count;
    }

    public void SetStarterCharacterPuzzleID(int puzzleId)
    {
        StarterCharacterPuzzleID = puzzleId;
    }
}