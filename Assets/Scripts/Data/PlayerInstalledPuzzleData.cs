using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class PlayerInstalledPuzzleObject
{
    public PlayerInstalledPuzzleData[] playerInstalledPuzzleData;
}

[Serializable]
public class PlayerInstalledPuzzleData
{
    public ulong uid;
    public Vector3 installPos;
    public ulong[] appliedPuzzleUidArray;

    public PlayerInstalledPuzzleData(ulong uid, Vector3 installPos)
    {
        this.uid = uid;
        this.installPos = installPos;
    }

    public List<ulong> GetAppliedPuzzleUidList()
    {
        return appliedPuzzleUidArray?.ToList() ?? new List<ulong>();
    }

    public void SetAppliedPuzzleUidList(List<ulong> list)
    {
        appliedPuzzleUidArray = list.ToArray();
    }
}