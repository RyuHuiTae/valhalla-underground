using System;

[Serializable]
public class PlayerPuzzleObject
{
    public PlayerPuzzleData[] playerPuzzleData;
}

[Serializable]
public class PlayerPuzzleData
{
    public ulong uid;
    public int puzzleID;
    public int puzzleType;
    public int puzzlePollution;

    public PlayerPuzzleData(int puzzleID, int puzzleType)
    {
        var basicDB = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;

        this.uid = PlayerDataBase.GetUID();
        this.puzzleID = puzzleID;
        this.puzzleType = puzzleType;
        this.puzzlePollution = (int) basicDB.PollutionInitValue();
    }

    public PlayerPuzzleData(int puzzleID, int puzzleType, int puzzlePollution)
    {
        this.uid = PlayerDataBase.GetUID();
        this.puzzleID = puzzleID;
        this.puzzleType = puzzleType;
        this.puzzlePollution = puzzlePollution;
    }
}