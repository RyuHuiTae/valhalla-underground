using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerStageObject
{
    public PlayerStageData[] playerStageData;
}

[Serializable]
public class PlayerStageData
{
    public int stageDataId;
    public int stageDataType;
    public int stageNormalLevel;
    public int stageEliteLevel;
    public int stageBossLevel;
    public bool isCurrentSlot;
    public bool isSlotEventEnd;
    public string slotDataJson;

    public PlayerStageData(StageSlotData slotData)
    {
        this.stageDataId = slotData.data.ID;
        this.stageDataType = (int) slotData.type;
        this.stageNormalLevel = slotData.data.NormalBattleLevel;
        this.stageEliteLevel = slotData.data.EliteBattleLevel;
        this.stageBossLevel = slotData.data.BossLevel;
        this.isCurrentSlot = slotData.isCurrentSlot;
        this.isSlotEventEnd = false;
        this.slotDataJson = string.Empty;
    }

    public PlayerBattleStageData GetPlayerBattleStageData()
    {
        var data = JsonUtility.FromJson<PlayerBattleStageData>(slotDataJson);
        return data;
    }

    public void SetPlayerBattleStageData(PlayerBattleStageData data)
    {
        var json = JsonUtility.ToJson(data);
        this.slotDataJson = json;
    }

    public PlayerShopStageData GetPlayerShopStageData()
    {
        var data = JsonUtility.FromJson<PlayerShopStageData>(slotDataJson);
        return data;
    }

    public void SetPlayerShopStageData(PlayerShopStageData data)
    {
        var json = JsonUtility.ToJson(data);
        this.slotDataJson = json;
    }

    public PlayerEventStageData GetPlayerEventStageData()
    {
        var data = JsonUtility.FromJson<PlayerEventStageData>(slotDataJson);
        return data;
    }

    public void SetPlayerEventStageData(PlayerEventStageData data)
    {
        var json = JsonUtility.ToJson(data);
        this.slotDataJson = json;
    }
}

[Serializable]
public class PlayerBattleStageData
{
    public int battleStageDataId;
    public bool isStartBattle;
    
    public PlayerBattleStageData(int battleStageDataId, bool isStartBattle)
    {
        this.battleStageDataId = battleStageDataId;
        this.isStartBattle = isStartBattle;
    }
}


[Serializable]
public class PlayerShopStageData
{
    public int[] randomDataIds;

    public PlayerShopStageData(List<int> list)
    {
        this.randomDataIds = list.ToArray();
    }
}

[Serializable]
public class PlayerEventStageData
{
    public int randomDataId;

    public PlayerEventStageData(int randomDataId)
    {
        this.randomDataId = randomDataId;
    }
}