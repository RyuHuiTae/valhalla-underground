using SQLite.Attributes;

public class PuzzleData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public PuzzleType Type {get; set;}
    public string Name { get; set; }
    public int Shape { get; set; }
    public int CharacterDataId { get; set; }
    public int EnhancePieceDataId { get; set; }
    public int StatPieceDataId { get; set; }
    public int Cost { get; set; }
    
    public string IconName { get; set; }
    public string PieceImageName { get; set; }
    
    public CharacterData CharacterData { get; set; }
    public AbilityPieceData AbilityPieceData { get; set; }
    public StatPieceData StatPieceData { get; set; }
}

public enum PuzzleType
{
    None,
    Character,
    Ability,
    Stat,
}