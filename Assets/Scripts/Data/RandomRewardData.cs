using SQLite.Attributes;

public class RandomRewardData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public RandomRewardType RewardType { get; set; }
    public int RandomRewardId { get; set; }
    public int PieceDataId { get; set; }
    public int MoneyMin { get; set; }
    public int MoneyMax { get; set; }
    public int Life { get; set; }
    public int Pollution { get; set; }
    public int Floor { get; set; }
    public BattleType BattleType { get; set; }
    public int SoulMin { get; set; }
    public int SoulMax { get; set; }

    public static BattleType BattleToStageType(StageType type)
    {
        switch (type)
        {
            case StageType.NormalBattle:
                return BattleType.Normal;
            case StageType.EliteBattle:
                return BattleType.Elite;
            case StageType.Boss:
                return BattleType.Boss;
            default:
                return BattleType.None;
        }
    }
}

public enum RandomRewardType
{
    None,
    Money,
    Life,
    Pollution,
    Piece,
    Soul,
    RandomPieceSelect,
}

public enum BattleType
{
    None,   //어떤 타입이든 상관없음
    Normal,
    Elite,
    Boss,
}