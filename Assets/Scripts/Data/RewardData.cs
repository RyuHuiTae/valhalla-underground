using SQLite.Attributes;

public class RewardData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public RewardType Type { get; set; }
    public int RandomRewardId1 { get; set; }
    public int RandomRewardId2 { get; set; }
    public int RandomRewardId3 { get; set; }
}

public enum RewardType
{
    BattleResultReward,
    SelectPieceReward,
}
