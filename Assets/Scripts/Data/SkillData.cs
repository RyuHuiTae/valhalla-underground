using System;
using SQLite.Attributes;

public class SkillData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public int Level{ get; set; }
    public SkillType SkillType { get; set; }
    public bool IsSummons { get; set; }
    public int NumberOfTargets{ get; set; }
    public float Damage{ get; set; }
    public float DamageCoefficient { get; set; }
    public float CoolDownTime{ get; set; }
    public float Range{ get; set; }
    public float AttackCycle{ get; set; }
    public float SkillTiming{ get; set; }
    public Target Target{ get; set; }
    public bool IsBlinkToTarget { get; set; }
    public IsWhatBase IsWhatBase { get; set; }
    public Priority Priority { get; set; }
    public int AppliedBuffId{ get; set; }
    public BuffData AppliedBuff{ get; set; }
    public int AppliedDebuffId{ get; set; }
    public DebuffData AppliedDebuff{ get; set; }
    public int ContinuallyAppliedDebuffId{ get; set; }
    public DebuffData ContinuallyAppliedDebuff{ get; set; }
    public SummonsData AppliedSummonsData{ get; set; }
    public int AppliedSummonId { get; set; }
    public string EffectPath { get; set; }
    public string Desc { get; set; }
    public float ResurrectionHpPercentValue { get; set; }
    public float Heal { get; set; }
    public float barrier { get; set; }
    public string AnimationTriggerName { get; set; }
    public string IconName { get; set; }
    public string SkillSound1 { get; set; }
    public string SkillSound2 { get; set; }
    public float SoundVolume { get; set; }
}

public enum SkillType
{
    None,       //없음
    Active,     //액티브 스킬
    Buff       //버프 스킬
}

public enum IsWhatBase
{
    None,
    HpPercent,
    Distance,
    TimeSpent,
    AttackCount
}

public enum Priority
{
    None,
    Descending,
    Ascending
}

/// <summary>
/// 적용될 대상 타입
/// </summary>
public enum Target
{
    None,
    Enemy,
    Ally,
    Oneself,
}

public class CharacterSkills
{
    public SkillData basicSkill;
    public SkillData classSkill;
    public SkillData uniqueSkill;
}