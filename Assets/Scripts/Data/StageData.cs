using SQLite.Attributes;

public class StageData : ICustomData
{
    [PrimaryKey] public int ID { get; set; }
    public string Name { get; set; }
    public StageFloorType Type { get; set; }
    public int StageStep1 { get; set; }
    public int StageStep2 { get; set; }
    public StageType StageType { get; set; }
    public int TopNode { get; set; }
    public int MidNode { get; set; }
    public int BotNode { get; set; }
    public int NormalBattleLevel { get; set; }
    public int EliteBattleLevel { get; set; }
    public int BossLevel { get; set; }
}

public enum StageFloorType
{
    None, //없음
    Normal, //보통
    FinalBoss, //최종보스
    Tutorial,   //튜토리얼
}

public enum StageType
{
    None, //없음
    Start, //시작 지점
    Event, //이벤트
    Rest, //휴식
    Shop, //상점
    NormalBattle, //일반 전투
    EliteBattle, //강적 전투
    Boss, //보스
    Random, //랜덤으로 타입을 받아옴
    FinalBoss, //최종보스
}