using SQLite.Attributes;

public class StageRandomData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public float Floor { get; set; }
    public int Max { get; set; }
    public int Type { get; set; }
}
