using SQLite.Attributes;

public class StarterDeckData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public int CharacterPieceId { get; set; }
    public int Money { get; set; }
    public int Soul { get; set; }
}
