using SQLite.Attributes;

public class StatPieceData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public int Level { get; set; }
    public string Name { get; set; }
    public CalculateType CalculateType { get; set; }
    public StatPieceChangedStat ChangedStat { get; set; }
    public bool IsPercentIncrease { get; set; }
    public float AppliedValue { get; set; }
    public string IconName { get; set; }
    public string Desc { get; set; }
}

public enum CalculateType
{
    Plus,   //합
    Times   //곱
}

public enum StatPieceChangedStat
{
    None,
    AttackSpeedIncrease,
    AttackDamageIncrease,
    DefenseIncrease,
    HpIncrease,
    MoveSpeedIncrease,
    SkillCoolTimeReduction,
}