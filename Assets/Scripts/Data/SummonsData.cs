using SQLite.Attributes;

public class SummonsData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public SummonsType SummonsType { get; set; }
    public AppliedDamageType AppliedDamageType { get; set; }
    public float Damage { get; set; }
    public float DamageCoefficient { get; set; }
    public float SummoningStatsPercent { get; set; }
    public bool IsMaxHpPercentDamage { get; set; }
    public float AttackCycle { get; set; }
    public float Duration { get; set; }
    public string EffectPath { get; set; }
    public int AppliedDebuffId { get; set; }
    public DebuffData AppliedDebuff{ get; set; }
    public ProportionalToHp ProportionalToHp { get; set; }
}

public enum ProportionalToHp
{
    None,
    Enemy,
    Own
}

public enum AppliedDamageType
{
    None,
    InstantDamage,
    FixedDotDamage
}

public enum SummonsType
{
    None,
    SpawnCharacter,
    SpawnObject,
    SpawnProjectile,
    SpawnMultiProjectile
}