using SQLite.Attributes;

public class TutorialTextData : ICustomData
{
    [PrimaryKey]
    public int ID { get; set; }
    public string Name { get; set; }
    public string TutorialText { get; set; }
    public TutorialTextType Type { get; set; }
}

public enum TutorialTextType
{
    EnterShop,
    EnterRest,
    EnterNormalBattleSpyingPopup,
    EnterPuzzlePopup,
    EnterStageSelect,
    EnterBattleWithMaxPollution,
    EnterBossBattleSpyingPopup,
    EnterEliteBattleSpyingPopup,
}
