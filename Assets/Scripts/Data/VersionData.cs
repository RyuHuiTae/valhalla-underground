using System.Collections.Generic;
using System.Text;

//db파일 버전 관리
public class Version
{
    public string key;
    public string version;

    public Version(string key, string version)
    {
        this.key = key;
        this.version = version;
    }

    public Version(Version v)
    {
        this.key = v.key;
        this.version = v.version;
    }

    public override string ToString()
    {
        return string.Format("{0}_{1}", key, version);
    }
}

//db파일 버전 관리 데이터
public class VersionData
{
    private Dictionary<string, Version> versionDic = null;

    public VersionData()
    {
        versionDic = new Dictionary<string, Version>();
    }

    public void AddVersionData(string infoString)
    {
        if (infoString == string.Empty)
            return;

        var infoArray = infoString.Split('/');
        foreach (var info in infoArray)
        {
            if (info != string.Empty)
            {
                var split = info.Split('_');
                var fileName = split[0];
                var fileVersion = split[1];

                AddNewVersionData(fileName, fileVersion);
            }
        }
    }

    private Version AddNewVersionData(string fileName, string version = "-1")
    {
        var value = new Version(fileName, version);
        if (!versionDic.ContainsKey(fileName))
        {
            versionDic.Add(fileName, value);
        }
        return value;
    }

    //key에 맞는 버전 반환 (key는 주로 DB 이름)
    public Version GetVersionValue(string key)
    {
        if (versionDic.ContainsKey(key))
        {
            return versionDic[key];
        }
        return null;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();

        foreach (var file in versionDic)
        {
            var ver = file.Value;
            sb.AppendFormat("{0}/", ver.ToString());
        }

        return sb.ToString();
    }
}
