using System.Linq;
using Cysharp.Threading.Tasks;

public class AbilityPieceDataBase : DataBase<AbilityPieceData>
{
    public override string DBFileName => "EnhancePieceData";
    
    public override UniTask<bool> CombinedData()
    {
        var buffDataList = DataBaseManager.Instance.GetDataList<BuffDataBase>();
        
        foreach (var data in GetDataList().Select(data => data as AbilityPieceData))
        {
            data.Buff = buffDataList.First(v => v.ID == data.BuffId) as BuffData;
        }

        return base.CombinedData();
    }
}
