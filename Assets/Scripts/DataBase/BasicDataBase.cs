public class BasicDataBase : DataBase<BasicData>
{
    public override string DBFileName => "BasicData";

    private BasicData GetBasicData(int idx)
    {
        var findData = GetDataList().Find(v => v.ID == idx);
        return findData as BasicData;
    }

    public float[] CharacterLevelPriceArray()
    {
        return new []
        {
            CharacterLevelPrice1(),
            CharacterLevelPrice2(),
            CharacterLevelPrice3(),
            CharacterLevelPrice4(),
            CharacterLevelPrice5(),
            CharacterLevelPrice6(),
            CharacterLevelPrice7(),
            CharacterLevelPrice8(),
            CharacterLevelPrice9(),
            CharacterLevelPrice10()
        };
    }
    
    public float[] CharacterEnhancePriceArray()
    {
        return new []
        {
            CharacterEnhancePrice1(),
            CharacterEnhancePrice2(),
            CharacterEnhancePrice3(),
            CharacterEnhancePrice4(),
            CharacterEnhancePrice5(),
            CharacterEnhancePrice6(),
            CharacterEnhancePrice7(),
            CharacterEnhancePrice8(),
            CharacterEnhancePrice9(),
        };
    }

    public float[] SuddenDeathDamageArray()
    {
        return new []
        {
            SuddenDeathDamage1F(),
            SuddenDeathDamage2F(),
            SuddenDeathDamage3F(),
            SuddenDeathDamage4F(),
            SuddenDeathDamage5F(),
            SuddenDeathDamage6F(),
            SuddenDeathDamage7F(),
            SuddenDeathDamage8F(),
            SuddenDeathDamage9F(),
            SuddenDeathDamage10F(),
        };
    }
    
    public float SuddenDeathDamage1F() => GetBasicData(0).Value;
    public float SuddenDeathDamage2F() => GetBasicData(1).Value;
    public float SuddenDeathDamage3F() => GetBasicData(2).Value;
    public float SuddenDeathDamage4F() => GetBasicData(3).Value;
    public float SuddenDeathDamage5F() => GetBasicData(4).Value;
    public float SuddenDeathDamage6F() => GetBasicData(5).Value;
    public float SuddenDeathDamage7F() => GetBasicData(6).Value;
    public float SuddenDeathDamage8F() => GetBasicData(7).Value;
    public float SuddenDeathDamage9F() => GetBasicData(8).Value;
    public float SuddenDeathDamage10F() => GetBasicData(9).Value;
    public float BattleLimitTime() => GetBasicData(10).Value;
    public float SuddenDeathDamageInterval() => GetBasicData(11).Value;
    public float MaxFloor() => GetBasicData(12).Value;
    public float RewardRerollCost() => GetBasicData(13).Value;
    public float RewardRerollRaiseValue() => GetBasicData(14).Value;
    public float PollutionInitValue() => GetBasicData(15).Value;
    public float PollutionAddValue() => GetBasicData(16).Value;
    public float ProjectileSpeed() => GetBasicData(17).Value;
    public float CharacterBasePrice() => GetBasicData(18).Value;
    public float EnhancePuzzleBasePrice() => GetBasicData(19).Value;
    public float CharacterLevelPrice1() => GetBasicData(20).Value;
    public float CharacterLevelPrice2() => GetBasicData(21).Value;
    public float CharacterLevelPrice3() => GetBasicData(22).Value;
    public float CharacterLevelPrice4() => GetBasicData(23).Value;
    public float CharacterLevelPrice5() => GetBasicData(24).Value;
    public float CharacterLevelPrice6() => GetBasicData(25).Value;
    public float CharacterLevelPrice7() => GetBasicData(26).Value;
    public float CharacterLevelPrice8() => GetBasicData(27).Value;
    public float CharacterLevelPrice9() => GetBasicData(28).Value;
    public float CharacterLevelPrice10() => GetBasicData(29).Value;
    public float EnhanceLevelPrice() => GetBasicData(30).Value;
    public float CharacterEnhancePrice1() => GetBasicData(31).Value;
    public float CharacterEnhancePrice2() => GetBasicData(32).Value;
    public float CharacterEnhancePrice3() => GetBasicData(33).Value;
    public float CharacterEnhancePrice4() => GetBasicData(34).Value;
    public float CharacterEnhancePrice5() => GetBasicData(35).Value;
    public float CharacterEnhancePrice6() => GetBasicData(36).Value;
    public float CharacterEnhancePrice7() => GetBasicData(37).Value;
    public float CharacterEnhancePrice8() => GetBasicData(38).Value;
    public float CharacterEnhancePrice9() => GetBasicData(39).Value;
    public float InvenPieceControlHoldTime() => GetBasicData(40).Value;
    public float PollutionMaxValue() => GetBasicData(41).Value;
    public float StarterDeckPassCount() => GetBasicData(42).Value;
    public float FinalBossMaxLevel() => GetBasicData(43).Value;
    public float BasicSkillUnlockLevel() => GetBasicData(44).Value;
    public float ClassSkillUnlockLevel() => GetBasicData(45).Value;
    public float UniqueSkillUnlockLevel() => GetBasicData(46).Value;
    public float UniqueSkillCoolTime() => GetBasicData(47).Value;
    public float NormalBattleRewardSelectCount() => GetBasicData(48).Value;
    public float EliteBattleRewardSelectCount() => GetBasicData(49).Value;
    public float BossBattleRewardSelectCount() => GetBasicData(50).Value;
    public float BarrierEventCount() => GetBasicData(51).Value;
    public float SellCostValue() => GetBasicData(52).Value;
    public float PollutionClearCost() => GetBasicData(53).Value;
    public float PollutionClearCostAddValue() => GetBasicData(54).Value;
    public float SkillCoolTimeLimit() => GetBasicData(55).Value;
    public float EliteBattleBonusMoney() => GetBasicData(56).Value;
    public float EliteBattleBonusSoul() => GetBasicData(57).Value;
    public float BossBattleBonusMoney() => GetBasicData(58).Value;
    public float BossBattleBonusSoul() => GetBasicData(59).Value;
    public float TutorialFloor() => GetBasicData(60).Value;
    public float CharacterUseMaxCount() => GetBasicData(61).Value;
}