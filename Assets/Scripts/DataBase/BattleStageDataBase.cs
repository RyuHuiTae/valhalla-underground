using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStageDataBase : DataBase<BattleStageData>
{
    public override string DBFileName => "BattleStageData";
}
