using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStageEnhanceDataBase : DataBase<BattleStageEnhanceData>
{
    public override string DBFileName => "BattleStageEnhanceData";
}
