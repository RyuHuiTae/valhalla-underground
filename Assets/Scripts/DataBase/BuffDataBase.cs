using System.Linq;
using Cysharp.Threading.Tasks;

public class BuffDataBase : DataBase<BuffData>
{
    public override string DBFileName => "BuffData";
    
    public override UniTask<bool> CombinedData()
    {
        var summonsDataList = DataBaseManager.Instance.GetDataList<SummonsDataBase>();
        
        foreach (var data in GetDataList().Select(data => data as BuffData))
        {
            data.SummonsData = summonsDataList.First(v => v.ID == data.SummonId) as SummonsData;
        }
        
        return base.CombinedData();
    }
}
