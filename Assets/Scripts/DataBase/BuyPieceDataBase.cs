using System.Linq;

public class BuyPieceDataBase : DataBase<BuyPieceData>
{
    public override string DBFileName => "BuyPieceData";

    public BuyPieceData GetBuyPieceData()
    {
        return GetDataList().FirstOrDefault() as BuyPieceData;
    }

    public int[] GetBuyPieceIDArray()
    {
        var buyPieceData = GetBuyPieceData();
        int[] buyPieceIDArray =
        {
            buyPieceData.BuyPieceId1,
            buyPieceData.BuyPieceId2,
            buyPieceData.BuyPieceId3,
            buyPieceData.BuyPieceId4,
            buyPieceData.BuyPieceId5,
            buyPieceData.BuyPieceId6,
        };

        return buyPieceIDArray;
    }
}
