using System.Linq;
using Cysharp.Threading.Tasks;

public class CharacterDataBase : DataBase<CharacterData>
{
    public override string DBFileName => "CharacterData";

    public override UniTask<bool> CombinedData()
    {
        var skillDataList = DataBaseManager.Instance.GetDataList<SkillDataBase>();

        foreach (var data in GetDataList().Select(data => data as CharacterData))
        {
            data.BasicSkill = skillDataList.First(v => v.ID == data.BasicSkillId) as SkillData;
            data.ClassSkill = skillDataList.First(v => v.ID == data.ClassSkillId) as SkillData;
            data.UniqueSkill = skillDataList.First(v => v.ID == data.UniqueSkillId) as SkillData;
        }
        
        return base.CombinedData();
    }
}
