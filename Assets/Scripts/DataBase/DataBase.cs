using System;
using System.Collections.Generic;
using System.Text;
using Cysharp.Threading.Tasks;
using SqlCipher4Unity3D;

public abstract class DataBase<T> : IDataBase where T : ICustomData, new()
    {
        //읽어온 모든 데이터들의 리스트
        private List<ICustomData> dataList = new List<ICustomData>();
        //에러가 발생하면 메세지가 추가된다.
        protected StringBuilder errorMsgSB = new StringBuilder();

        //DB 파일 이름
        public abstract string DBFileName { get; }

        //에러가 없다면 메세지도 없다
        public string GetErrorMsg() => errorMsgSB?.ToString();
        //이 데이터베이스가 가진 데이터(클래스)이름 반환
        public string GetDataName() => typeof(T).Name;

        public object CreateNewData() => new T();

        public List<ICustomData> GetDataList() => dataList;

        //데이터를 가져온다. 타입이 맞지 않다면 새로 만들어서 리턴
        public virtual ICustomData GetData(object obj)
        {
            var findData = GetDataList().Find(v =>
            {
                var convertObj = Convert.ChangeType(obj, v.ID.GetType());
                return convertObj.Equals(v.ID);
            });

            if (findData == null)
            {
                findData = CreateNewData() as ICustomData;
            }
            return findData;
        }

        //DB에서 읽어온 데이터를 리스트에 추가
        public virtual async UniTask AddData(SQLiteAsyncConnection connection, string query)
        {
            var result = await connection.QueryAsync<T>(query);
            foreach (var data in result)
            {
                dataList.Add(data);
            }
        }

        //읽어온 데이터로 추가 작업이 필요한 경우
        public virtual UniTask<bool> ReadData() => UniTask.FromResult(true);

        //그 추가 작업이 다른 디비의 참조가 필요한 경우
        public virtual UniTask<bool> CombinedData() => UniTask.FromResult(true);
    }