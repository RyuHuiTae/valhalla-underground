using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Cysharp.Threading.Tasks;
using DevSupport;
using SqlCipher4Unity3D;
using UnityEngine;

public class DataBaseManager : SingletonMonoBehaviour<DataBaseManager>
{
    //에러가 발생한 경우 메세지가 담겨있음
    public string ErrorMsg
    {
        get
        {
            var msg = sb?.ToString();
            sb?.Clear();

            return msg;
        }
    }

    //디비 로드가 끝났는지
    public bool IsLoadEnd { get; private set; }
    
    //기본 PATH
    public string BasePath => projectName + "_data";

    //경로에 사용할 프로젝트 이름
    private string projectName;

    //에러 메세지를 담을 sb
    private StringBuilder sb;

    //데이터베이스를 저장하는 Dic
    private Dictionary<Type, IDataBase> databaseDic;

    private const string PLAYER_DATA_PATH = "/db/PlayerData";
    private const string PLAYER_DATA_NAME = "/PlayerData_0.db";

    //Path에 사용할 projectName
    public void Init(string projectName)
    {
        this.projectName = projectName;
        this.IsLoadEnd = false;
        
        sb = new StringBuilder();
        databaseDic = new Dictionary<Type, IDataBase>();
    }

    public IDataBase GetDataBase<T>() where T : IDataBase
    {
        return !databaseDic.ContainsKey(typeof(T)) ? null : databaseDic[typeof(T)];
    }

    public IEnumerable<ICustomData> GetDataList<T>() where T : IDataBase
    {
        return !databaseDic.ContainsKey(typeof(T)) ? null : databaseDic[typeof(T)].GetDataList();
    }

    public async UniTask Process(IDataBase[] database)
    {
        DebugX.Log("Load DataBase...");
        await LoadDataBase(database);
        
        DebugX.Log("Read DataBase...");
        await ReadDataBase(database);
        
        DebugX.Log("Combined DataBase...");
        await CombinedDataBase(database);
        
        DebugX.Log("Database Load End");
    }

    private async UniTask LoadDataBase(IDataBase[] database)
    {
        foreach (var db in database)
        {
            await LoadDataBaseFile(db);

            var msg = db.GetErrorMsg();
            if (msg != string.Empty)
            {
                sb.Append(msg);
            }
        }
    }

    private async UniTask ReadDataBase(IDataBase[] database)
    {
        foreach (var db in database)
        {
            await db.ReadData();
            databaseDic.Add(db.GetType(), db);
        }
    }

    private async UniTask CombinedDataBase(IDataBase[] database)
    {
        foreach (var db in database)
        {
            await db.CombinedData();
        }

        IsLoadEnd = true;
    }

    //해당 데이터베이스를 로드한다.
    private async UniTask LoadDataBaseFile(IDataBase dataBase)
    {
        var key = dataBase.DBFileName;

        var versionData = LoadVersionFile(BasePath);
        var findVersion = versionData.GetVersionValue(key);

        if (findVersion == null)
        {
            sb.AppendLine($"{key} is NULL");
            return;
        }

        var filePath = $"{BasePath}/db/{findVersion.key}/{findVersion.ToString()}.db";
        var fullPath = GetDBFullPath(filePath);

        try
        {
            var connection = new SQLiteAsyncConnection(fullPath, GameSetting.Instance.pass);
            await dataBase.AddData(connection, $"SELECT * FROM {findVersion.key}");
        }
        catch (Exception e)
        {
            sb.AppendFormat("DB Key : {0}\nPath : {1}\n{2}\n", findVersion.key, fullPath, e.Message);
        }
    }

    private VersionData LoadVersionFile(string dbBasePath)
    {
        var versionInfo = ReadVersionFile(dbBasePath + "/version.txt");

        var versionData = new VersionData();
        versionData.AddVersionData(versionInfo);

        return versionData;
    }

    private string ReadVersionFile(string path)
    {
        var filePath = Application.streamingAssetsPath + "/" + path;

        if (Application.platform == RuntimePlatform.Android)
        {
#pragma warning disable 618
            var www = new WWW(filePath);
#pragma warning restore 618
            while (!www.isDone)
            {
            }

            return www.text;
        }
        else
        {
            return File.ReadAllText(filePath);
        }
    }

    private string GetFullPath(string path)
    {
        var filePath = Application.streamingAssetsPath + "/" + path;
        var copyFilePath = Application.persistentDataPath + "/" + path;

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            return Path.GetFullPath(filePath);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            return copyFilePath;
        }
        else return string.Empty;
    }
    
    private string GetDBFullPath(string path)
    {
        var filePath = Application.streamingAssetsPath + "/" + path;
        var copyFilePath = Application.persistentDataPath + "/" + path;

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            return Path.GetFullPath(filePath);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            if (File.Exists(copyFilePath))
            {
                return copyFilePath;
            }
            
#pragma warning disable 618
            var www = new WWW(filePath);
#pragma warning restore 618
            while (!www.isDone)
            {
            }

            if (!Directory.Exists(Path.GetDirectoryName(copyFilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(copyFilePath));

            File.WriteAllBytes(copyFilePath, www.bytes);

            return copyFilePath;
        }
        else return string.Empty;
    }

    public async UniTask CheckPlayerData()
    {
        DebugX.Log("Check Player Data...");

        var dbPath = $"{BasePath}{PLAYER_DATA_PATH}";
        var dbFullPath = GetFullPath(dbPath);

        if (!Directory.Exists(dbFullPath))
            Directory.CreateDirectory(dbFullPath);

        var filePath = $"{BasePath}{PLAYER_DATA_PATH}{PLAYER_DATA_NAME}";
        var fileFullPath = GetFullPath(filePath);

        var isExistsFile = File.Exists(fileFullPath);
        if (isExistsFile)
        {
#if UNITY_EDITOR
            //파일이 존재해도 하드 리셋을 해야한다면 리셋
            if (GameSetting.Instance.isResetPlayerData)
            {
                var fi = new FileInfo(fileFullPath);
                fi.Delete();
            }
            else return;
#elif !UNITY_EDITOR
            //파일이 존재하면 즉시 리턴
            return;
#endif
        }

        //파일이 없으면 새로 생성
        await CreateNewPlayerData();
        
        DebugX.Log("Check End");
    }

    private SQLiteAsyncConnection GetPlayerDataConnection()
    {
        var filePath = $"{BasePath}{PLAYER_DATA_PATH}{PLAYER_DATA_NAME}";
        var fileFullPath = GetFullPath(filePath);
        
        var connection = new SQLiteAsyncConnection(fileFullPath, GameSetting.Instance.pass);
        return connection;
    }
    
    private async UniTask CreateNewPlayerData()
    {
        var newPlayerData = PlayerData.GetNewPlayerData();

        var asyncConnection = GetPlayerDataConnection();

        DebugX.Log("Create New Player Data...");
        
        try
        {
            DebugX.Log("Create Table...");
            await asyncConnection.CreateTableAsync<PlayerData>();
            
            DebugX.Log("Insert...");
            await asyncConnection.InsertAsync(newPlayerData);
        }
        catch (SQLiteException e)
        {
            DebugX.LogError(e.Message);
        }
        finally
        {
            await asyncConnection.CloseAsync();
            DebugX.Log("Create End!");
        }
    }
    
    public async UniTask UpdatePlayerData(PlayerData data)
    {
        var asyncConnection = GetPlayerDataConnection();

        try
        {
            DebugX.Log("Update Player Data...");
            await asyncConnection.UpdateAsync(data);
        }
        catch
        {
            DebugX.Log("Update Fail!");
            
            //업데이트 실패시
            //드랍하고 다시 생성한 뒤 삽입
            DebugX.Log("Drop Table...");
            await asyncConnection.DropTableAsync<PlayerData>();
            
            DebugX.Log("Create Table...");
            await asyncConnection.CreateTableAsync<PlayerData>();
            
            DebugX.Log("Insert...");
            await asyncConnection.InsertAsync(data);
        }
        finally
        {
            await asyncConnection.CloseAsync();
            DebugX.Log("Update End!");
        }
    }
}