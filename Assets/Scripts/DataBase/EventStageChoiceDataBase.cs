using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;

public class EventStageChoiceDataBase : DataBase<EventStageChoiceData>
{
    public override string DBFileName => "EventStageChoiceData";

    public override UniTask<bool> CombinedData()
    {
        var randomRewardDataList = DataBaseManager.Instance.GetDataList<RandomRewardDataBase>();

        foreach (var data in GetDataList())
        {
            var changedData = data as EventStageChoiceData;
            changedData.RandomRewardDataList = new List<RandomRewardData>();
            
            foreach (var id in changedData.GetRandomRewardIdArray())
            {
                var newData = randomRewardDataList
                    .FirstOrDefault(v => ((RandomRewardData) v).RandomRewardId == id) as RandomRewardData;

                changedData.RandomRewardDataList.Add(newData);
            }
        }

        return base.CombinedData();
    }
}
