using System.Linq;
using Cysharp.Threading.Tasks;

public class EventStageDataBase : DataBase<EventStageData>
{
    public override string DBFileName => "EventStageData";

    public override UniTask<bool> CombinedData()
    {
        var characterDataList = DataBaseManager.Instance.GetDataList<EventStageChoiceDataBase>();

        foreach (var data in GetDataList())
        {
            var changedData = data as EventStageData;
            changedData.EventChoices.Add(characterDataList.FirstOrDefault(v => v.ID == changedData.EventChoiceId1) as EventStageChoiceData);
            changedData.EventChoices.Add(characterDataList.FirstOrDefault(v => v.ID == changedData.EventChoiceId2) as EventStageChoiceData);
            changedData.EventChoices.Add(characterDataList.FirstOrDefault(v => v.ID == changedData.EventChoiceId3) as EventStageChoiceData);
        }

        return base.CombinedData();
    }
}
