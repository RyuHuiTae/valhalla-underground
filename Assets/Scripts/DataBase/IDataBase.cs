using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using SqlCipher4Unity3D;

public interface IDataBase
{
    string DBFileName { get; }

    string GetErrorMsg();
    string GetDataName();

    object CreateNewData();

    List<ICustomData> GetDataList();
    ICustomData GetData(object obj);

    UniTask AddData(SQLiteAsyncConnection connection, string query);
    UniTask<bool> ReadData();
    UniTask<bool> CombinedData();
}
