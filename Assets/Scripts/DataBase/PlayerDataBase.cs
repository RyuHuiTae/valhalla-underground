using System;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Cysharp.Threading.Tasks;
using DevSupport;

public class PlayerDataBase : DataBase<PlayerData>
{
    //0번 스킵
    private static ulong uid = 1;

    //true : 디비 업데이트 허용
    //false : 디비 업데이트 제한
    private bool isAllowUpdateData = true;
    
    public bool IsHeartChanged { get; set; }
    public bool IsMoneyChanged { get; set; }
    public bool IsSoulChanged { get; set; }
    
    public int PreMoney { get; private set; }
    public int PreSoul { get; private set; }

    public ObscuredInt Heart { get; private set; }
    public ObscuredInt Money { get; private set; }
    public ObscuredInt Soul { get;  private set;}
    
    private bool isAlreadyUpdating = false;
    private Queue<Action> updateCallback = new Queue<Action>();
    
    public PlayerData Data { get; private set; }
    public override string DBFileName => "PlayerData";

    public override UniTask<bool> ReadData()
    {
        var data = GetDataList().FirstOrDefault() as PlayerData;
        if (data == null)
        {
            DebugX.LogError("playerData is NULL");
            return base.ReadData();
        }

        Data = data;
        if (Data.GetPlayerPuzzleData().Count > 0)
        {
            var lastUid = Data.GetPlayerPuzzleData().Max(v => v.uid);
            uid = lastUid + 1;
        }

        InitObscuredValue();

        return base.ReadData();
    }

    public void RandomizeCryptoKey()
    {
        Heart.RandomizeCryptoKey();
        Money.RandomizeCryptoKey();
        Soul.RandomizeCryptoKey();
    }
    
    public List<CharacterAddStat> GetPlayerCharacterAddStatList()
    {
        var addStatList = new List<CharacterAddStat>();
        
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return addStatList;
        
        var puzzleDataList = puzzleDB.GetDataList()
            .Select(v => v as PuzzleData)
            .ToList();
        
        var playerPuzzleDataList = Data.GetPlayerPuzzleData();
        var installedPlayerPuzzleDataList = Data.GetPlayerInstalledPuzzleData();
        
        foreach (var data in installedPlayerPuzzleDataList)
        {
            var findData = playerPuzzleDataList.Find(v => v.uid == data.uid);
            
            if (findData == null)
                continue;
            
            //설치된 퍼즐이 캐릭터일때 
            if (findData.puzzleType != (int) PuzzleType.Character) 
                continue;

            //새로운 스탯 데이터 리스트를 만들고
            var newStatData = new CharacterAddStat(findData.uid);
            
            //이 퍼즐에 적용된 퍼즐들 탐색
            foreach (var appliedUid in data.appliedPuzzleUidArray)
            {
                var findAppliedData = playerPuzzleDataList.Find(v => v.uid == appliedUid);
                if (findAppliedData == null)
                {
                    DebugX.LogError("findAppliedData is NULL");
                    continue;
                }

                //캐릭터에 적용된 퍼즐이 스탯피스 일때
                if (findAppliedData.puzzleType != (int) PuzzleType.Stat)
                    continue;
                
                var findPuzzleData = puzzleDataList.Find(v => v.ID == findAppliedData.puzzleID);
                CharacterAddStat.SetStatChangeValue(newStatData, findPuzzleData);
            }
            addStatList.Add(newStatData);
        }

        return addStatList;
    }
    
    private async void UpdateProcess()
    {
        if (isAlreadyUpdating)
            return;
        
        isAlreadyUpdating = true;
        
        while (updateCallback.Count > 0)
        {
            if (isAllowUpdateData)
            {
                await DataBaseManager.Instance.UpdatePlayerData(Data);
            }
            updateCallback.Dequeue()?.Invoke();
            
        }
        isAlreadyUpdating = false;
    }
    
    public void UpdatePlayerDataBase(Action updateEndEvent)
    {
        if (Data == null)
            return;

        updateCallback.Enqueue(updateEndEvent);

        UpdateProcess();
    }

    public void ResetPlayerDataBase(Action updateEndEvent)
    {
        //게임 오버 / 결계 파괴 실패로 세이브 데이터 소프트 리셋 작업
        var barrierCount = this.Data.BarrierCount;
        var clearCount = this.Data.ClearCount;
        var tutorialCheckData = this.Data.GetTutorialCheckData();

        var resetData = PlayerData.GetResetPlayerData();
        resetData.SetBarrierCount(barrierCount);
        resetData.SetClearCount(clearCount);
        resetData.SetTutorialCheckData(tutorialCheckData);
        
        this.Data = resetData;
        UpdatePlayerDataBase(updateEndEvent);
        
        InitObscuredValue();
    }

    public void ClearResetPlayerDataBase(Action updateEndEvent)
    {
        //게임 클리어로 인한 세이브 데이터 리셋 작업
        var clearCount = this.Data.ClearCount;
        clearCount += 1;
        
        var tutorialCheckData = this.Data.GetTutorialCheckData();
        
        var clearData = PlayerData.GetClearPlayerData();
        clearData.SetClearCount(clearCount);
        clearData.SetTutorialCheckData(tutorialCheckData);
        
        this.Data = clearData;
        UpdatePlayerDataBase(updateEndEvent);
        
        InitObscuredValue();
    }
    
    public void SetUpdateFlag(bool isAllow)
    {
        this.isAllowUpdateData = isAllow;
    }

    public void InitObscuredValue()
    {
        Money = Data.Money;
        Soul = Data.Soul;
        Heart = Data.Heart;
    }
    
    public void SetMoney(int money)
    {
        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (GameSetting.Instance.isAllowMoneySoulCheat)
        {
            if (money < 0)
                money = 0;
        }
#endif
        PreMoney = Data.Money;
        IsMoneyChanged = true;
        Data.SetMoney(money);

        Money = Data.Money;
    }
    
    public void SetSoul(int soul)
    {
        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (GameSetting.Instance.isAllowMoneySoulCheat)
        {
            if (soul < 0)
                soul = 0;
        }
#endif
        PreSoul = Data.Soul;
        IsSoulChanged = true;
        Data.SetSoul(soul);

        Soul = Data.Soul;
    }
    
    public void SetHeart(int heart)
    {
        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (GameSetting.Instance.isAllowHeartCheat)
        {
            if (heart < 0)
                heart = 0;
        }
#endif
        IsHeartChanged = Data.SetHeart(heart);
        
        Heart = Data.Heart;
    }
    
    public static ulong GetUID()
    {
        var curUid = uid;
        uid += 1;
        
        return curUid;
    }
}
