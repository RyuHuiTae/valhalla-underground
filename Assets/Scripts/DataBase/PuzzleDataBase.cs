using System.Linq;
using Cysharp.Threading.Tasks;

public class PuzzleDataBase : DataBase<PuzzleData>
{
    public override string DBFileName => "PuzzleData";

    public override UniTask<bool> CombinedData()
    {
        var characterDataList = DataBaseManager.Instance.GetDataList<CharacterDataBase>();
        var abilityPieceDataList = DataBaseManager.Instance.GetDataList<AbilityPieceDataBase>();
        var statPieceDataList = DataBaseManager.Instance.GetDataList<StatPieceDataBase>();

        foreach (var data in GetDataList())
        {
            var puzzleData = data as PuzzleData;
            puzzleData.CharacterData = characterDataList.FirstOrDefault(v => v.ID == puzzleData.CharacterDataId) as CharacterData;
            puzzleData.AbilityPieceData = abilityPieceDataList.FirstOrDefault(v => v.ID == puzzleData.EnhancePieceDataId) as AbilityPieceData;
            puzzleData.StatPieceData = statPieceDataList.FirstOrDefault(v=>v.ID == puzzleData.StatPieceDataId) as StatPieceData;
        }

        return base.CombinedData();
    }
}
