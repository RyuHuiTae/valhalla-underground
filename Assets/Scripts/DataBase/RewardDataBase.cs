using System.Linq;
using Cysharp.Threading.Tasks;

public class RewardDataBase : DataBase<RewardData>
{
    public override string DBFileName => "RewardData";
    
    public RewardData GetBattleResultRewardData()
    {
        return GetRewardData(RewardType.BattleResultReward);
    }
    
    public RewardData GetSelectPieceRewardData()
    {
        return GetRewardData(RewardType.SelectPieceReward);
    }
    
    private RewardData GetRewardData(RewardType type)
    {
        var findData = GetDataList()
            .Select(v => v as RewardData)
            .FirstOrDefault(v => v.Type == type);

        return findData;
    }
}
