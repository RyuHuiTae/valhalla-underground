using System.Linq;
using Cysharp.Threading.Tasks;

public class SkillDataBase : DataBase<SkillData>
{
    public override string DBFileName => "SkillData";
    
    public override UniTask<bool> CombinedData()
    {
        var buffDataList = DataBaseManager.Instance.GetDataList<BuffDataBase>();
        var deBuffDataList = DataBaseManager.Instance.GetDataList<DebuffDataBase>();
        var summonsDataList = DataBaseManager.Instance.GetDataList<SummonsDataBase>();
        
        foreach (var data in GetDataList().Select(data => data as SkillData))
        {
            data.AppliedBuff = buffDataList.First(v => v.ID == data.AppliedBuffId) as BuffData;
            
            data.AppliedDebuff = deBuffDataList.First(v => v.ID == data.AppliedDebuffId) as DebuffData;
            data.ContinuallyAppliedDebuff = deBuffDataList.First(v => v.ID == data.ContinuallyAppliedDebuffId) as DebuffData;
            data.AppliedSummonsData = summonsDataList.First(v => v.ID == data.AppliedSummonId) as SummonsData;
        }

        return base.CombinedData();
    }
}
