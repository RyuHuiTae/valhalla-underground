using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageRandomDataBase : DataBase<StageRandomData>
{
    public override string DBFileName => "StageRandomData";
}
