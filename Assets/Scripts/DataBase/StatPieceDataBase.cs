using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatPieceDataBase : DataBase<StatPieceData>
{
    public override string DBFileName => "StatPieceData";
}
