using System.Linq;
using Cysharp.Threading.Tasks;

public class SummonsDataBase : DataBase<SummonsData>
{
    public override string DBFileName => "SummonsData";
    
    public override UniTask<bool> CombinedData()
    {
        var deBuffDataList = DataBaseManager.Instance.GetDataList<DebuffDataBase>();
        
        foreach (var data in GetDataList().Select(data => data as SummonsData))
        {
            data.AppliedDebuff = deBuffDataList.First(v => v.ID == data.AppliedDebuffId) as DebuffData;
        }
        
        return base.CombinedData();
    }
}
