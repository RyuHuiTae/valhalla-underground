using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTextDataBase : DataBase<TutorialTextData>
{
    public override string DBFileName => "TutorialTextData";
}
