using UnityEditor;

[CustomEditor(typeof(GameSetting))]
public class GameSettingEditor : Editor
{
    private GameSetting setting;
    public void OnEnable()
    {
        setting = target as GameSetting;
    }

    public override void OnInspectorGUI ()
    {
        setting.pass = EditorGUILayout.TextField("PASS", setting.pass);
        EditorGUILayout.Separator();
        
        EditorGUILayout.LabelField("게임 시작 전 세팅");
        EditorGUI.indentLevel += 1;
        setting.isResetPlayerData = EditorGUILayout.Toggle("세이브 하드 리셋", setting.isResetPlayerData);
        setting.isAddAllPiece = EditorGUILayout.Toggle("시작시 모든 퍼즐 획득", setting.isAddAllPiece);
        setting.isAddMoneySoul = EditorGUILayout.Toggle("시작시 돈/소울 획득", setting.isAddMoneySoul);
        setting.isMoveBoss = EditorGUILayout.Toggle("시작시 5층 보스로 이동", setting.isMoveBoss);
        setting.isMoveFinalBoss = EditorGUILayout.Toggle("시작시 최종보스 층으로 이동", setting.isMoveFinalBoss);
        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Separator();
        
        EditorGUILayout.LabelField("치트 (실시간 적용)");
        EditorGUI.indentLevel += 1;
        setting.isIgnoreBattle = EditorGUILayout.Toggle("전투 무시", setting.isIgnoreBattle);
        setting.isAllowBattleCheat = EditorGUILayout.Toggle("전투 항상 승리", setting.isAllowBattleCheat);
        setting.isAllowHeartCheat = EditorGUILayout.Toggle("목숨 감소 안함", setting.isAllowHeartCheat);
        setting.isAllowMoneySoulCheat = EditorGUILayout.Toggle("돈/소울 감소 안함", setting.isAllowMoneySoulCheat);
        setting.isInvincibility = EditorGUILayout.Toggle("모든 캐릭터 무적", setting.isInvincibility);
        EditorGUILayout.Separator();
        
        setting.nextStageType = (StageEditorType)EditorGUILayout.EnumPopup("생성할 스테이지 타입", setting.nextStageType);
        
        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Separator();
    }
}
