using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;
using UnityEngine.UI;

public class GameField : MonoSingleton<GameField>
{
    [SerializeField] private List<Character> characterList;
    [SerializeField] private List<Character> playerList;
    [SerializeField] private List<Character> enemyList;
    [SerializeField] private Transform characterCreateRoot;

    private readonly int POS_ROW_COUNT = 4;
    
    private readonly float PLAYER_POS_X_VALUE = -7;
    private readonly float ENEMY_POS_X_VALUE = 4;
    private readonly float POS_Y_VALUE = 0;

    private readonly float POS_X_ADD_VALUE = 1;
    private readonly float POS_Y_ADD_VALUE = -1.5f;

    public bool IsBattleStart { get; private set; } = false;
    public bool IsBattleEnd { get; private set; } = false;
    
#if UNITY_EDITOR
    public bool IsTestBattle { get; set; } = false;
#endif
    
    public event Action UpdateHpEvent;
    public event Action<float> UpdatePlayerTotalHpEvent;
    public event Action<float> UpdateEnemyTotalHpEvent;
    public event Func<Character.CharacterTeam, PuzzleData, GameObject> CreateCharacterIconUi;

    public async UniTask StartSuddenDeath()
    {
        if (IsBattleEnd)
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDataBase))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
            return;

        var floor = playerDataBase.Data.Floor;
        var damage = basicDataBase.SuddenDeathDamageArray().ElementAtOrDefault(floor - 1);
        var interval = basicDataBase.SuddenDeathDamageInterval();

        await AudioManager.Instance.SuddenDeath();

        while (!IsBattleEnd)
        {
            foreach (var character in characterList)
            {
                if (!character.IsDie)
                    character.ReceiveTrueDamage(damage);
            }

            await UniTask.Delay(TimeSpan.FromSeconds(interval));
        }
    }

    public void BattleStart()
    {
        IsBattleStart = true;
        IsBattleEnd = false;

        var battleLayer = LayerManager.Instance.GetCurrentLayer() as BattleLayer;
        battleLayer.BattleStart();

        //AudioManager.Instance.BgmSound(AudioManager.Instance.bgmList[(int) AudioManager.BgmType.Battle]);
    }

    private void BattleEndEvent()
    {
        if (IsBattleEnd)
            return;

        IsBattleStart = false;
        IsBattleEnd = true;

        var playerAliveCount = 0;
        foreach (var player in playerList)
        {
            var clone = player.gameObject.GetComponent<Clone>();

            if (clone != null)
                continue;

            if (!player.IsDie)
                playerAliveCount++;
        }

        var enemyAliveCount = 0;
        foreach (var enemy in enemyList)
        {
            var clone = enemy.gameObject.GetComponent<Clone>();

            if (clone != null)
                continue;

            if (!enemy.IsDie)
                enemyAliveCount++;
        }

        var winTeam = Character.CharacterTeam.None;
        if (playerAliveCount > enemyAliveCount)
        {
            winTeam = Character.CharacterTeam.Player;
        }
        else if (playerAliveCount < enemyAliveCount)
        {
            winTeam = Character.CharacterTeam.Enemy;
        }

        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (GameSetting.Instance.isAllowBattleCheat)
        {
            winTeam = Character.CharacterTeam.Player;
        }
#endif

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)) 
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB)) 
            return;

        var puzzleList = playerDB.Data.GetPlayerPuzzleData();
        var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();

        //설치된 퍼즐의 UID로 인벤토리에 있는 퍼즐을 가져오고
        //강화 피스들만 오염도 증가
        foreach (var installedPuzzle in installedPuzzleList)
        {
            var uid = installedPuzzle.uid;
            var findPuzzle = puzzleList.Find(v => v.uid == uid);

            if ((PuzzleType) findPuzzle.puzzleType == PuzzleType.Ability || 
                (PuzzleType) findPuzzle.puzzleType == PuzzleType.Stat)
            {
                //강화 피스 오염도 증가
                findPuzzle.puzzlePollution += (int) basicDB.PollutionAddValue();
            }
        }

        //변경된 값 적용
        playerDB.Data.SetPlayerPuzzleData(puzzleList);

        //플레이어 스테이지 데이터에서
        var list = playerDB.Data.GetPlayerStageData();
        //현재 스테이지(슬롯)를 찾는다
        var findData = list.Find(v => v.isCurrentSlot);

        if (findData == null)
        {
#if UNITY_EDITOR
            //테스트 전투인 경우
            if (IsTestBattle)
            {
                OpenBattleResult(winTeam);    
            }
#endif
            return;
        }

        var stageType = (StageType)findData.stageDataType;
            
        //승리
        if (winTeam == Character.CharacterTeam.Player)
        {
            //현재 슬롯이 최종 보스인 경우
            if (stageType == StageType.FinalBoss)
            {
                var level = playerDB.Data.FinalBossLevel;
                playerDB.Data.SetFinalBossLevel(level + 1);

                //전투 스테이지 데이터 초기화
                findData.SetPlayerBattleStageData(null);
            }
            else
            {
                //해당 슬롯을 진행했다고 판정
                findData.isSlotEventEnd = true;
            }

            //스테이지 데이터 갱신
            playerDB.Data.SetPlayerStageData(list);

            //디비 업데이트
            playerDB.UpdatePlayerDataBase(() => { OpenBattleResult(winTeam); });
        }
        //패배
        else if (winTeam == Character.CharacterTeam.Enemy)
        {
            //전투 스테이지 데이터에서
            var battleStageData = findData.GetPlayerBattleStageData();
            //재전투를 위해 전투시작 플래그 변경
            battleStageData.isStartBattle = false;
            //전투 스테이지 데이터 갱신
            findData.SetPlayerBattleStageData(battleStageData);
            //스테이지 데이터 갱신
            playerDB.Data.SetPlayerStageData(list);

            //목숨 -1
            playerDB.SetHeart(-1);
            //디비 업데이트
            playerDB.UpdatePlayerDataBase(() => { OpenBattleResult(winTeam); });
        }
        //무승부
        else OpenBattleResult(winTeam);
    }

    private void OpenBattleResult(Character.CharacterTeam winTeam)
    {
        var battleResultUI = PopupManager.Instance.CreatePopup(PopupName.BattleResultPopup, UIUtils.DEPTH_30) as BattleResultPopup;
        battleResultUI?.Init(playerList, winTeam);
    }

    public void DestroyObject()
    {
        IsBattleEnd = true;
        
        foreach (var character in characterList)
        {
            if (character)
                Destroy(character.gameObject);
        }

        var projectiles = FindObjectsOfType<Projectile>();
        foreach (var projectile in projectiles)
        {
            Destroy(projectile.gameObject);
        }

        var summonedObjects = FindObjectsOfType<SummonedObject>();
        foreach (var summonedObject in summonedObjects)
        {
            Destroy(summonedObject.gameObject);
        }

        characterList.Clear();
        playerList.Clear();
        enemyList.Clear();
    }

    public void ChangeLayerToStageSelect()
    {
        DestroyObject();

        LayerManager.Instance.ChangeLayer(Layer.StageSelectLayer, UIUtils.DEPTH_10);

        var curLayer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
        if (curLayer != null)
        {
            curLayer.Init();
        }
    }

    private Character CreateCharacter(int id, Character.CharacterTeam team, Vector3 pos)
    {
        var dataBaseManager = DataBaseManager.Instance;
        var puzzleDataList = dataBaseManager.GetDataList<PuzzleDataBase>();

        var findPuzzleData = puzzleDataList
            .Select(v => v as PuzzleData)
            .FirstOrDefault(v => v.CharacterDataId == id);

        var data = findPuzzleData.CharacterData;
        var path = team == Character.CharacterTeam.Player ? data.Path : data.Path + "_Enemy";
        var newObj = Instantiate(VResources.Load<GameObject>(path), characterCreateRoot);
        var newCharacter = newObj.GetComponent<Character>();

        var skillDataList = dataBaseManager.GetDataList<SkillDataBase>();
        var characterSkills = new CharacterSkills()
        {
            basicSkill = skillDataList.ElementAt(data.BasicSkillId) as SkillData,
            classSkill = skillDataList.ElementAt(data.ClassSkillId) as SkillData,
            uniqueSkill = skillDataList.ElementAt(data.UniqueSkillId) as SkillData
        };

        var characterIconUi = CreateCharacterIconUi?.Invoke(team, findPuzzleData);
        newCharacter.Init(characterList.Count, team, data, pos, characterIconUi, characterSkills);
        characterList.Add(newCharacter);

        switch (team)
        {
            case Character.CharacterTeam.Player:
                playerList.Add(newCharacter);
                UpdatePlayerTotalHpEvent?.Invoke(GetPlayerTotalMaxHp());
                break;
            case Character.CharacterTeam.Enemy:
                enemyList.Add(newCharacter);
                UpdateEnemyTotalHpEvent?.Invoke(GetEnemyTotalMaxHp());
                break;
        }

        return newCharacter;
    }

    public Character CreateCharacter(Character character, Vector3 pos, bool isClone = false)
    {
        var newObj = Instantiate(character, characterCreateRoot);
        var newCharacter = newObj.GetComponent<Character>();
        var team = character.Team;
        var data = character.Data;
        //var iconHpImage = CreateCharacterIconUi?.Invoke(team, data);

        var dataBaseManager = DataBaseManager.Instance;
        var skillDataList = dataBaseManager.GetDataList<SkillDataBase>();
        var skills = new CharacterSkills()
        {
            basicSkill = skillDataList.ElementAt(character.Data.BasicSkillId) as SkillData,
            classSkill = skillDataList.ElementAt(character.Data.ClassSkillId) as SkillData,
            uniqueSkill = skillDataList.ElementAt(character.Data.UniqueSkillId) as SkillData,
        };

        newCharacter.Init(characterList.Count, team, data, pos, null, skills);
        characterList.Add(newCharacter);
        if (isClone)
            return newCharacter;
        
        switch (team)
        {
            case Character.CharacterTeam.Player:
                playerList.Add(newCharacter);
                UpdatePlayerTotalHpEvent?.Invoke(GetPlayerTotalMaxHp());
                break;
            case Character.CharacterTeam.Enemy:
                enemyList.Add(newCharacter);
                UpdateEnemyTotalHpEvent?.Invoke(GetEnemyTotalMaxHp());
                break;
        }

        return newCharacter;
    }

    public void CreatePlayerCharacter()
    {
        var posIndexQueue = new Queue<int>();

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        var puzzleList = playerDB.Data.GetPlayerPuzzleData();
        var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();
        var puzzleDataList = puzzleDB.GetDataList();
        var puzzlePosIndexArray = playerDB.Data.GetPuzzlePosIndexData();

        foreach (var index in puzzlePosIndexArray)
        {
            posIndexQueue.Enqueue(index);
        }
        
        var statDataList = playerDB.GetPlayerCharacterAddStatList();
        
        var abilityDataBase = DataBaseManager.Instance.GetDataBase<AbilityPieceDataBase>();
        var abilityDataList = abilityDataBase.GetDataList();

        int characterCount = 0;
        for (var index = 0; index < installedPuzzleList.Count; index++)
        {
            var puzzleData = installedPuzzleList[index];
            var findPlayerPuzzleData = puzzleList.Find(v => v.uid == puzzleData.uid);
            if (findPlayerPuzzleData == null)
            {
                DebugX.LogError("보드판에 설치된 퍼즐과 인벤에 있는 퍼즐이 매칭되지 않음");
                continue;
            }

            if ((PuzzleType) findPlayerPuzzleData.puzzleType == PuzzleType.Character)
            {
                if (findPlayerPuzzleData.puzzleID == 0)
                    continue;

                var posIndex = posIndexQueue.Dequeue();
                var characterPos = new Vector3(
                    PLAYER_POS_X_VALUE + (POS_X_ADD_VALUE * (int)(posIndex % POS_ROW_COUNT)), 
                    POS_Y_VALUE + (POS_Y_ADD_VALUE * (int)(posIndex / POS_ROW_COUNT)), 
                    0);
                
                var puzzle = puzzleDataList.Find(v => v.ID == findPlayerPuzzleData.puzzleID) as PuzzleData;
                var character = CreateCharacter(puzzle.CharacterDataId, Character.CharacterTeam.Player, characterPos);

                var stat = statDataList[characterCount];
                characterCount++;
                ApplyStatPiece(character, stat);

                var appliedPuzzleUidList = puzzleData.GetAppliedPuzzleUidList();
                foreach (var uid in appliedPuzzleUidList)
                {
                    var findPlayerPieceData = puzzleList.Find(v => v.uid == uid);
                    if (findPlayerPieceData == null)
                    {
                        DebugX.LogError("findPlayerPuzzleData is NULL");
                        continue;
                    }
                    
                    var findPuzzleData = puzzleDataList.Find(v => v.ID == findPlayerPieceData.puzzleID) as PuzzleData;
                    if (findPuzzleData == null)
                    {
                        DebugX.LogError("findPuzzleData is NULL");
                        continue;
                    }
                    
                    if (findPuzzleData.Type == PuzzleType.Ability)
                    {
                        var findAbilityData =
                            abilityDataList.Find(v => v.ID == findPuzzleData.EnhancePieceDataId) as AbilityPieceData;
                        if (findAbilityData != null)
                        {
                            var buff = findAbilityData.Buff;
                            BuffBase.GiveBuffBase(buff, character.gameObject, character);
                        }
                        else
                            DebugX.LogError("abilityId : " + findAbilityData.ID +
                                            " 에 해당되는 데이터가 없습니다. (EnhancePieceData)");
                    }
                }
            }
        }
    }

    public void CreateEnemyCharacter()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BattleStageDataBase>() is BattleStageDataBase battleStageDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BattleStageEnhanceDataBase>() is BattleStageEnhanceDataBase
            battleStageEnhanceDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<AbilityPieceDataBase>() is AbilityPieceDataBase enhanceDataBase))
            return;

        var playerStageDataList = playerDB.Data.GetPlayerStageData();
        var puzzleDataList = puzzleDB.GetDataList();
        var battleStageDataList = battleStageDB.GetDataList();

        var findData = playerStageDataList.Find(v => v.isCurrentSlot);
        var playerBattleStageData = findData?.GetPlayerBattleStageData();
        if (playerBattleStageData != null)
        {
            var findBattleStageData = battleStageDataList
                .Find(v => v.ID == playerBattleStageData.battleStageDataId) as BattleStageData;

            var puzzleIDArray = findBattleStageData.GetEnemyPuzzleIdArray();
            var enhanceGroupIDArray = findBattleStageData.GetEnhanceGroupIdArray();
            var posIndexArray = findBattleStageData.GetEnemyPosIndexArray();
            var enhanceDataList = enhanceDataBase.GetDataList();
            var battleStageEnhanceDataList = battleStageEnhanceDB.GetDataList();

            var characterList = new List<Character>();

            for (int i = 0; i < puzzleIDArray.Length; i++)
            {
                var id = puzzleIDArray[i];
                var posIndex = posIndexArray[i];
                
                if (id == 0)
                    continue;

                var characterPos = new Vector3(
                    ENEMY_POS_X_VALUE + (POS_X_ADD_VALUE * (int)(posIndex % POS_ROW_COUNT)), 
                    POS_Y_VALUE + (POS_Y_ADD_VALUE * (int)(posIndex / POS_ROW_COUNT)), 
                    0);
                
                var puzzle = puzzleDataList.Find(v => v.ID == id) as PuzzleData;
                var character = CreateCharacter(puzzle.CharacterDataId, Character.CharacterTeam.Enemy, characterPos);
                
                character.GetComponent<SpriteRenderer>().flipX = true;
                characterList.Add(character);
            }

            for (int i = 0; i < characterList.Count; i++)
            {
                var character = characterList[i];
                var groupID = enhanceGroupIDArray[i];

                if (groupID == 0)
                    continue;

                //각 캐릭터에 적용된 스탯 피스 합
                var addStat = GetEnemyCharacterAddStat(groupID);
                ApplyStatPiece(character, addStat);
                
                var findStageEnhanceDataList =
                    battleStageEnhanceDataList.FindAll(v => ((BattleStageEnhanceData) v).GroupId == groupID);
                foreach (var data in findStageEnhanceDataList)
                {
                    var stageEnhanceData = data as BattleStageEnhanceData;
                    var findPuzzleData = puzzleDataList.Find(v => v.ID == stageEnhanceData.PuzzleDataId) as PuzzleData;
                    var findAbilityPieceData = enhanceDataList.Find(v => v.ID == findPuzzleData.EnhancePieceDataId) as AbilityPieceData;

                    if (findPuzzleData == null || findAbilityPieceData == null)
                    {
                        DebugX.LogError("AbilityId : " + findAbilityPieceData.ID + " 에 해당되는 데이터가 없습니다. (AbilityPieceData)");
                        continue;
                    }
                    
                    if (findPuzzleData.Type != PuzzleType.Ability)
                        continue;
                    
                    var buff = findAbilityPieceData.Buff;
                    BuffBase.GiveBuffBase(buff, character.gameObject, character);
                }
            }
        }
    }
    
#if UNITY_EDITOR
    public void CreateTestCharacter(Character.CharacterTeam team, List<PlayerInstalledPuzzleData> installedPuzzleList, int[] posIndexArray)
    {
        var posIndexQueue = new Queue<int>();
        foreach (var index in posIndexArray)
        {
            posIndexQueue.Enqueue(index);
        }
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        var puzzleList = playerDB.Data.GetPlayerPuzzleData();
        var puzzleDataList = puzzleDB.GetDataList();

        var statDataList = playerDB.GetPlayerCharacterAddStatList();
        
        var abilityDataBase = DataBaseManager.Instance.GetDataBase<AbilityPieceDataBase>();
        var abilityDataList = abilityDataBase.GetDataList();
        
        for (var index = 0; index < installedPuzzleList.Count; index++)
        {
            var puzzleData = installedPuzzleList[index];
            if (puzzleData == null)
            {
                //posQueue.Dequeue();
                continue;
            }
            
            var posIndex = posIndexQueue.Dequeue();
            var characterPos = Vector3.zero;

            if (team == Character.CharacterTeam.Player)
            {
                characterPos = new Vector3(
                    PLAYER_POS_X_VALUE + (POS_X_ADD_VALUE * (int)(posIndex % POS_ROW_COUNT)), 
                    POS_Y_VALUE + (POS_Y_ADD_VALUE * (int)(posIndex / POS_ROW_COUNT)), 
                    0);
            }
            else if (team == Character.CharacterTeam.Enemy)
            {
                characterPos = new Vector3(
                    ENEMY_POS_X_VALUE + (POS_X_ADD_VALUE * (int)(posIndex % POS_ROW_COUNT)), 
                    POS_Y_VALUE + (POS_Y_ADD_VALUE * (int)(posIndex / POS_ROW_COUNT)), 
                    0);
            }
            
            var findPlayerPuzzleData = puzzleList.Find(v => v.uid == puzzleData.uid);
            if (findPlayerPuzzleData == null)
            {
                DebugX.LogError("보드판에 설치된 퍼즐과 인벤에 있는 퍼즐이 매칭되지 않음");
                continue;
            }

            if ((PuzzleType) findPlayerPuzzleData.puzzleType == PuzzleType.Character)
            {
                if (findPlayerPuzzleData.puzzleID == 0)
                    continue;

                var puzzle = puzzleDataList.Find(v => v.ID == findPlayerPuzzleData.puzzleID) as PuzzleData;
                
                //어보미네이션을 위한 임시 값
                if (puzzle.CharacterData.CharacterType == CharacterType.Abomination)
                {
                    puzzle.CharacterData.Path = puzzle.CharacterData.Path.Replace("_Enemy", "");
                    
                    if (team == Character.CharacterTeam.Player)
                        puzzle.CharacterData.Path += "_Enemy";
                }
                
                var character = CreateCharacter(puzzle.CharacterDataId, 
                    team,
                    characterPos);

                var stat = statDataList.Find(v => v.Uid == findPlayerPuzzleData.uid);
                ApplyStatPiece(character, stat);

                var appliedPuzzleUidList = puzzleData.GetAppliedPuzzleUidList();
                foreach (var uid in appliedPuzzleUidList)
                {
                    var findPlayerPieceData = puzzleList.Find(v => v.uid == uid);
                    if (findPlayerPieceData == null)
                    {
                        DebugX.LogError("findPlayerPuzzleData is NULL");
                        continue;
                    }
                    
                    var findPuzzleData = puzzleDataList.Find(v => v.ID == findPlayerPieceData.puzzleID) as PuzzleData;
                    if (findPuzzleData == null)
                    {
                        DebugX.LogError("findPuzzleData is NULL");
                        continue;
                    }
                    
                    if (findPuzzleData.Type == PuzzleType.Ability)
                    {
                        var findAbilityData =
                            abilityDataList.Find(v => v.ID == findPuzzleData.EnhancePieceDataId) as AbilityPieceData;
                        if (findAbilityData != null)
                        {
                            var buff = findAbilityData.Buff;
                            BuffBase.GiveBuffBase(buff, character.gameObject, character);
                        }
                        else
                            DebugX.LogError("abilityId : " + findAbilityData.ID +
                                            " 에 해당되는 데이터가 없습니다. (EnhancePieceData)");
                    }
                }
            }
        }
        
    }
#endif
    
    private void ApplyStatPiece(Character character, CharacterAddStat characterAddStat)
    {
        var characterStatus = character.GetComponent<CharacterStatus>();
        characterStatus.Hp.IncreaseStat(CharacterStat.StatType.Base, characterAddStat.Hp);
        characterStatus.Hp.UpdateBaseMaxStat();
        UpdateTotalMaxHp();

        characterStatus.Defence.IncreaseStat(CharacterStat.StatType.Base, characterAddStat.Defense);
        characterStatus.Defence.UpdateBaseMaxStat();
        
        characterStatus.MoveSpeed.IncreaseStat(CharacterStat.StatType.Base, characterAddStat.MoveSpeed);
        characterStatus.MoveSpeed.UpdateBaseMaxStat();
        
        characterStatus.Attack.IncreaseStat(CharacterStat.StatType.Base, characterAddStat.Attack);
        characterStatus.Attack.UpdateBaseMaxStat();
        
        characterStatus.AttackSpeed.IncreaseStat(CharacterStat.StatType.Base, characterAddStat.AttackSpeed * 0.01f);
        characterStatus.AttackSpeed.UpdateBaseMaxStat();
        
        var classSkillBase = character.GetComponent<ClassSkillBase>();
        characterStatus.ClassSkillCoolTime.IncreaseStat(CharacterStat.StatType.BuffPercent, -(characterAddStat.SkillCoolTime * 0.01f));
        classSkillBase.UpdateSkillCoolTime();
    }
     
    public CharacterAddStat GetEnemyCharacterAddStat(int groupId)
    {
        var newStatData = new CharacterAddStat();
        
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return newStatData;
        
        if (!(DataBaseManager.Instance.GetDataBase<BattleStageEnhanceDataBase>() is BattleStageEnhanceDataBase battleStageEnhanceDB))
            return newStatData;
        
        var puzzleDataList = puzzleDB.GetDataList();
        var battleStageEnhanceDataList = battleStageEnhanceDB.GetDataList();
        
        var findStageEnhanceDataList =
            battleStageEnhanceDataList.FindAll(v => ((BattleStageEnhanceData) v).GroupId == groupId);
            
        foreach (var data in findStageEnhanceDataList)
        {
            var stageEnhanceData = data as BattleStageEnhanceData;
            var findPuzzleData = puzzleDataList.Find(v => v.ID == stageEnhanceData.PuzzleDataId) as PuzzleData;
                
            CharacterAddStat.SetStatChangeValue(newStatData, findPuzzleData);
        }

        return newStatData;
    }
    
    public IEnumerable<Character> FindEnemies(Vector3 playerPosition, Character.CharacterTeam type, IsWhatBase isWhatBase, Priority priority, float range = float.MaxValue)
    {
        // if (!IsBattleStart)
        //     return null;
        
        IEnumerable<Character> enemies = type switch
        {
            Character.CharacterTeam.Enemy => playerList,
            Character.CharacterTeam.Player => enemyList,
            _ => null
        };

        if (enemies == null)
        {
            DebugX.LogError($"{type}에 해당하는 캐릭터 타입 없음");
            return null;
        }
        
        var enemiesInRange = enemies.Where(x => 
                Vector3.Distance(playerPosition, x.transform.position) < range && !x.IsDie).ToArray();

        if (enemiesInRange.Length == 0)
        {
            //BattleEndEvent();
            //return null;
            return new List<Character>();
        }
        
        Character[] sortedEnemies;
        switch (priority)
        {
            case Priority.Descending:
                sortedEnemies = enemiesInRange.OrderByDescending(obj =>
                {
                    switch (isWhatBase)
                    {
                        case IsWhatBase.HpPercent:
                            return obj.GetCurrentHpPercent();
                        case IsWhatBase.Distance:
                            return Vector3.Distance(playerPosition, obj.transform.position);
                        default:
                            throw new ArgumentOutOfRangeException(nameof(isWhatBase), isWhatBase, null);
                    }
                }).ToArray();
                break;
            case Priority.Ascending:
                sortedEnemies = enemiesInRange.OrderBy(obj =>
                {
                    switch (isWhatBase)
                    {
                        case IsWhatBase.HpPercent:
                            return obj.GetCurrentHpPercent();
                        case IsWhatBase.Distance:
                            return Vector3.Distance(playerPosition, obj.transform.position);
                        default:
                            throw new ArgumentOutOfRangeException(nameof(isWhatBase), isWhatBase, null);
                    }
                }).ToArray();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(priority), priority, null);
        }

        var sortedEnemiesByAggro = new List<Character>();
        for (var i = (int)Character.AggroLevel.Aggravation; i > 0; i--)
        {
            var aggroLevel = (Character.AggroLevel)i;
            for (var j = 0; j < sortedEnemies.Length; j++)
            {
                var enemy = sortedEnemies[j];
                if (enemy.CurrentAggroLevel == aggroLevel)
                {
                    sortedEnemiesByAggro.Add(enemy);
                }
            }
        }
        
        return sortedEnemiesByAggro;
    }

    private bool CheckAlivePlayerCharacter()
    {
        var aliveCount = playerList.Count(character => !character.IsDie);
        return aliveCount > 0;
    }

    private bool CheckAliveEnemyCharacter()
    {
        var aliveCount = enemyList.Count(character => !character.IsDie);
        return aliveCount > 0;
    }
    
    public void CheckAliveCharacter()
    {
        if (!CheckAlivePlayerCharacter() ||
            !CheckAliveEnemyCharacter())
        {
            BattleEndEvent();    
        }
    }

    public void UpdateTotalHp()
    {
        UpdateHpEvent?.Invoke();
    }

    private float GetPlayerTotalMaxHp()
    {
        return GetTotalMaxHP(playerList);
    }

    private float GetEnemyTotalMaxHp()
    {
        return GetTotalMaxHP(enemyList);
    }

    public float GetPlayerTotalCurrentHp()
    {
        return GetTotalCurrentHP(playerList);
    }

    public float GetEnemyTotalCurrentHp()
    {
        return GetTotalCurrentHP(enemyList);
    }

    private float GetTotalCurrentHP(IEnumerable<Character> list)
    {
        return list
            .Where(x => x.IsClone == false)
            .Sum(data => data.GetCurrentHp());
    }

    private float GetTotalMaxHP(IEnumerable<Character> list)
    {
        return list
            .Where(x => x.IsClone == false)
            .Sum(data => data.GetMaxHp());
    }

    private void UpdateTotalMaxHp()
    {
        UpdatePlayerTotalHpEvent?.Invoke(GetPlayerTotalCurrentHp());
        UpdateEnemyTotalHpEvent?.Invoke(GetEnemyTotalMaxHp());
    }
    
    public List<Character> GetAllys(Character.CharacterTeam team)
    {
        return team switch
        {
            Character.CharacterTeam.Enemy => enemyList,
            Character.CharacterTeam.Player => playerList,
            _ => null
        };
    }
}