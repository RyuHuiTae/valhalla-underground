/// <summary>
/// 에디터용 타입
/// </summary>
public enum StageEditorType
{
    None,
    Event, //이벤트
    Rest, //휴식
    Shop, //상점
}

public class GameSetting : SingletonMonoBehaviour<GameSetting>
{
    public string pass = "23jf&325kjs*312p$jfpa1@d93_1dk";
    public bool isResetPlayerData;
    public bool isAddAllPiece;
    public bool isMoveBoss;
    public bool isMoveFinalBoss;
    public bool isAddMoneySoul;
    public bool isIgnoreBattle;
    public bool isAllowBattleCheat;
    public bool isAllowHeartCheat;
    public bool isAllowMoneySoulCheat;
    public bool isInvincibility;
    public StageEditorType nextStageType;
}
