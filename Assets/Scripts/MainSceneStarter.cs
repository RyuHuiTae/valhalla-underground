using System;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;

public class MainSceneStarter : MonoBehaviour
{
    private const int RandomizeMillisecondsDelay = 1000;

    private async void Start()
    {
        // 화면이 꺼지지 않도록 설정
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    
        LayerManager.Instance.ChangeLayer(Layer.TitleLayer);
        var title = LayerManager.Instance.GetCurrentLayer() as TitleLayer;
        title.Init(LoadingEndEvent);

        var db = DataBaseManager.Instance;
        db.Init("valhalla-underground");

        await db.CheckPlayerData();
        await CheckDataBase(db);

        await RandomizeCryptoKey();
    }

    private async UniTask CheckDataBase(DataBaseManager manager, Action dataLoadEndEvent = null)
    {
        var array = new IDataBase[]
        {
            new PlayerDataBase(),
            new GameIntroDataBase(),
            new CharacterDataBase(),
            new SkillDataBase(),
            new BuffDataBase(),
            new DebuffDataBase(),
            new StageDataBase(),
            new StageRandomDataBase(),
            new RewardDataBase(),
            new RandomRewardDataBase(),
            new BasicDataBase(),
            new SummonsDataBase(),
            new PuzzleDataBase(),
            new AbilityPieceDataBase(),
            new StarterDeckDataBase(),
            new BuyPieceDataBase(),
            new BuyPieceRandomDataBase(),
            new EventStageChoiceDataBase(),
            new EventStageDataBase(),
            new BattleStageDataBase(),
            new BattleStageEnhanceDataBase(),
            new BattleResultNarrationDataBase(),
            new StatPieceDataBase(),
            new TutorialTextDataBase(),
        };
        
        await LoadDataBase(manager, array, dataLoadEndEvent);
    }
    
    private async UniTask LoadDataBase(DataBaseManager manager, IDataBase[] array, Action loadEndEvent)
    {
        await manager.Process(array);
        
        var msg = manager.ErrorMsg;
        if (msg != string.Empty)
        {
            Debug.Log(msg);
        }
        
        loadEndEvent?.Invoke();
    }

    private async UniTask RandomizeCryptoKey()
    {
        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;

        while (gameObject != null)
        {
            playerDB?.RandomizeCryptoKey();

            await UniTask.Delay(RandomizeMillisecondsDelay);
        }
    }
    
    protected virtual void LoadingEndEvent()
    {
        
    }
}