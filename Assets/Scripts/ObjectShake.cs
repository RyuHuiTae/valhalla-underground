using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObjectShake : MonoBehaviour
{
    private Vector3 originPosition; 
    private Quaternion originRotation;

    [SerializeField] float ShakeDecay = 0.002f;
    [SerializeField] float ShakeIntensity = 0.15f;

    private float shakeIntensity;
    private bool isShaking = false;

    public void Shake()
    {
        if (isShaking) return;
        
        originPosition = transform.localPosition; 
        originRotation = transform.localRotation; 
        shakeIntensity = ShakeIntensity;

        ShakeObject();
    }

    private async void ShakeObject()
    {
        isShaking = true;
        
        while (true)
        {
            if (shakeIntensity < 0)
            {
                isShaking = false;
                return;
            }
            
            if (!this) return;
            
            transform.localPosition = originPosition + Random.insideUnitSphere * shakeIntensity; 
            transform.localRotation = new Quaternion( 
                originRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f, 
                originRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f, 
                originRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f, 
                originRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f); 
            shakeIntensity -= ShakeDecay;

            await UniTask.Yield();
        }
    }
}
