using System;
using System.Collections;
using System.Collections.Generic;
using DevSupport;
using UnityEngine;

public class DragMouse : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            var mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            var worldPoint = Camera.main.ScreenToWorldPoint(mousePosition);

            this.transform.position = new Vector3(worldPoint.x, worldPoint.y, transform.position.z);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "DragArea")
        {
            if (this.transform.parent.GetComponent<PieceControl>() != null)
                this.transform.parent.GetComponent<PieceControl>().isHold = true;
            else if (this.transform.parent.GetComponent<EnhancePiece>() != null)
                this.transform.parent.GetComponent<EnhancePiece>().isHold = true;
        }
    }
}
