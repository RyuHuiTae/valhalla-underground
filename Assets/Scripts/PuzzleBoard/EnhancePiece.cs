using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnhancePiece : PieceControl
{
    [SerializeField] private TextMeshProUGUI pollutionText;
     
    public List<PieceControl> applyList = new List<PieceControl>();
    public List<GameObject> arrowList = new List<GameObject>();

    public GameObject applyPoint;

    public Material noneGray;
    public Material gray;

    public void ActivePoint()
    {
        pollutionText.text = pieceData.PiecePollution.ToString() + "%";
        
        if (pieceData.PiecePollution < 100)
        {
            applyPoint.transform.gameObject.SetActive(true);
            pieceImage.material = noneGray;
        }

        if (pieceData.PiecePollution >= 100)
        {
            InActivePoint();
            pieceImage.material = gray;
        }
    }

    public void InActivePoint()
    {
        applyPoint.transform.gameObject.SetActive(false);

        applyList.RemoveRange(0, applyList.Count);

        for (int count = 0; count < arrowList.Count; count++)
            arrowList[count].gameObject.SetActive(false);
    }
}