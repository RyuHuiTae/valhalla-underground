using System;
using System.Collections.Generic;
using System.Linq;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class InventoryList : MonoBehaviour
{
    public InventorySlot characterButton;
    public InventorySlot abilityButton;
    public InventorySlot statButton;

    public List<InventorySlot> inventoryPieceList = new List<InventorySlot>();

    public RectTransform scrollRectTransform;
    public UITweenPosition scrollRectTween;
    public GridLayoutGroup gridLayoutGroup;
    public TextMeshProUGUI pageText;
    
    public InventorySlot CurHoldSlot { get; private set; }
    public InventorySlot CurClickSlot { get; private set; }
    public int CurActiveSlotCount { get; private set; }
    
    private PuzzleInfoPopup infoUI;
    private Action<PieceControl> updateCallBack;
    private Action<PieceControl> mouseDownCallBack;
    private Action<PieceControl> mouseUpCallBack;
    private Action<PieceControl, Collision> enterCallBack;
    private Action<PieceControl, Collision> stayCallBack;
    private Action<PieceControl, Collision> exitCallBack;
    private Action refreshPuzzleData;
    private bool isShowUsing;
    
    private bool isTweenScroll = false;
    private int curPageCount = 1;
    private int pageTotalCount = 1;
    
    private Transform pieceRoot;
    private PuzzleType curListType;

    private const int SELL_SIZE_Y = 110;
    private string depth;
    
    public void Init(Action<PieceControl> updateCallBack,
        Action<PieceControl> mouseDownCallBack,
        Action<PieceControl> mouseUpCallBack,
        Action<PieceControl, Collision> enterCallBack,
        Action<PieceControl, Collision> stayCallBack,
        Action<PieceControl, Collision> exitCallBack,
        Action refreshPuzzleData,
        Transform pieceRoot, string depth)
    {
        this.updateCallBack = updateCallBack;
        this.mouseDownCallBack = mouseDownCallBack;
        this.mouseUpCallBack = mouseUpCallBack;
        this.enterCallBack = enterCallBack;
        this.stayCallBack = stayCallBack;
        this.exitCallBack = exitCallBack;
        this.refreshPuzzleData = refreshPuzzleData;
        this.pieceRoot = pieceRoot;
        this.depth = depth;

        CurClickSlot = null;
        CurHoldSlot = null;
        curListType = PuzzleType.None;

        var x = scrollRectTransform.rect.width - (gridLayoutGroup.spacing.x * 2);
        gridLayoutGroup.cellSize = new Vector2(x, SELL_SIZE_Y);
        
        scrollRectTween.SetCallbackFinished(EndScrollRectTween);
        
        RefreshList();
        ClickFirstSlot();
    }

    public void RefreshList()
    {
        DestroyList();

        var puzzleList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>();

        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();
        var playerInstalledPuzzleDataList = playerDB.Data.GetPlayerInstalledPuzzleData();
        
        //장비된 피스들 먼저 생성
        foreach (var data in playerInstalledPuzzleDataList)
        {
            var findData = playerPuzzleDataList.FirstOrDefault(v => v.uid == data.uid);
            if (findData != null)
            {
                var findPuzzleData = puzzleList.FirstOrDefault(v => v.ID == findData.puzzleID) as PuzzleData;
                AddPiece(findPuzzleData, findData, true);
            }
        }

        foreach (var data in playerPuzzleDataList)
        {
            var findData = playerInstalledPuzzleDataList.FirstOrDefault(v => v.uid == data.uid);
            if (findData == null)
            {
                var findPuzzleData = puzzleList.FirstOrDefault(v => v.ID == data.puzzleID) as PuzzleData;
                
                AddPiece(findPuzzleData, data);
            }
        }

        ShowList();
    }

    private bool CheckSlotType(InventorySlot slot)
    {
        var slotType = slot.pieceData.PuzzleData.Type;
        switch (curListType)
        {
            case PuzzleType.None:
                slot.gameObject.SetActive(true);
                return true;
            case PuzzleType.Character:
            case PuzzleType.Ability:
            case PuzzleType.Stat:
                slot.gameObject.SetActive(slotType == curListType);
                return slotType == curListType;
        }

        return false;
    }
    
    public void ShowList()
    {
        foreach (var slot in inventoryPieceList)
        {
            //피스 장착이던 재조합이던 어디선가 사용중이면 제외 
            if (!slot.IsUsing)
            {
                CheckSlotType(slot);
            }
        }

        curPageCount = 1;
        scrollRectTransform.anchoredPosition = Vector2.zero;
        
        UpdatePageCount();
    }

    public void ShowAllList()
    {
        curListType = PuzzleType.None;
        ShowList();
    }

    public void ShowCharacterList()
    {
        curListType = PuzzleType.Character;
        ShowList();
    }

    public void ShowAbilityList()
    {
        curListType = PuzzleType.Ability;
        ShowList();
    }

    public void ShowStatList()
    {
        curListType = PuzzleType.Stat;
        ShowList();
    }

    public void DestroyList()
    {
        if (inventoryPieceList == null)
            return;

        //이미 생성된 슬롯 모두 삭제
        foreach (var piece in inventoryPieceList)
        {
            Destroy(piece.gameObject);
        }

        inventoryPieceList.Clear();
    }

    public void AddPiece(PuzzleData puzzleData, PlayerPuzzleData playerPuzzleData, bool isInstalled = false)
    {
        if (puzzleData == null)
        {
            DebugX.LogError("PuzzleData is NULL");
            return;
        }

        if (puzzleData.CharacterData == null)
        {
            DebugX.LogError("ID : " + puzzleData.ID + " PuzzleData.CharacterData is NULL");
            return;
        }

        var pieceData = PieceData.GetNewPieceData(puzzleData, playerPuzzleData);
        var pieceControl = PieceLoader.Instance.GetPieceShapeObj(puzzleData.Shape);
        var pieceImage = AtlasManager.Instance.GetIconSprite(puzzleData.PieceImageName); 
        var pieceSprite = PieceLoader.Instance.GetPieceSprite(puzzleData);
        var pieceIcon = PieceLoader.Instance.GetPieceShapeSprite(puzzleData.Shape);

        InventorySlot prefabObj = null;
        switch (puzzleData.Type)
        {
            case PuzzleType.Character:
                prefabObj = characterButton;
                break;
            case PuzzleType.Ability:
                prefabObj = abilityButton;
                break;
            case PuzzleType.Stat:
                //DebugX.LogError("스탯 피스 추가 작업 필요");
                prefabObj = statButton;
                break;
        }

        if (prefabObj == null)
            return;
        
        var piece = Instantiate(prefabObj, this.transform);
        piece.Init(pieceData, pieceControl, 
            pieceImage, pieceSprite, 
            pieceIcon, depth, 
            ClickSlotEvent, HoldSlotEvent,
            updateCallBack, mouseDownCallBack, 
            mouseUpCallBack, enterCallBack, 
            stayCallBack, exitCallBack,
            CheckSlotActive, refreshPuzzleData, 
            pieceRoot);

        inventoryPieceList.Add(piece);

        if (isInstalled)
        {
            piece.ShinyStart();
        }
    }

    public void RemovePiece(InventorySlot slot)
    {
        var findSlot = inventoryPieceList.Find(v => v == slot);
        if (findSlot != null)
        {
            inventoryPieceList.Remove(findSlot);

            Destroy(findSlot.gameObject);
            findSlot = null;
        }
    }

    public void ClickFirstSlot()
    {
        ClickSlotEvent(inventoryPieceList.FirstOrDefault());
    }

    private void ClickSlotEvent(InventorySlot slot)
    {
        if (slot == null)
            return;

        if (CurClickSlot == slot)
        {
            if (infoUI != null && 
                infoUI.gameObject != null)
            {
                CurClickSlot = null;
                return;
            }
        }

        CurClickSlot = slot;

        if (infoUI == null)
            infoUI = PopupManager.Instance.CreatePopup(PopupName.PuzzleInfoPopup, depth) as PuzzleInfoPopup;
        
        infoUI?.Init(slot.pieceData);
    }

    private void HoldSlotEvent(InventorySlot slot)
    {
        CurHoldSlot = slot;
    }

    private void CheckSlotActive(InventorySlot slot)
    {
        CheckSlotType(slot);
        UpdatePageCount();
    }

    public void ClickRightArrow()
    {
        if (isTweenScroll)
            return;
        
        if (curPageCount >= pageTotalCount) 
            return;
        
        var x = gridLayoutGroup.cellSize.x;
        var spacing = gridLayoutGroup.spacing.x;
        
        var originPosX = scrollRectTransform.anchoredPosition.x;
        var newPosX = originPosX - (x + spacing);

        isTweenScroll = true;
        curPageCount++;
            
        scrollRectTween.from = scrollRectTransform.anchoredPosition;
        scrollRectTween.to = new Vector2(newPosX, 0);
        
        scrollRectTween.TweenForward();
    }
    
    public void ClickLeftArrow()
    {
        if (isTweenScroll)
            return;
        
        if (curPageCount <= 1) 
            return;
        
        var x = gridLayoutGroup.cellSize.x;
        var spacing = gridLayoutGroup.spacing.x;

        var originPosX = scrollRectTransform.anchoredPosition.x;
        var newPosX = originPosX + (x + spacing);

        isTweenScroll = true;
        curPageCount--;
            
        scrollRectTween.from = scrollRectTransform.anchoredPosition;
        scrollRectTween.to = new Vector2(newPosX, 0);
        scrollRectTween.TweenForward();
    }

    private void EndScrollRectTween()
    {
        isTweenScroll = false;
        UpdatePageCount();
    }

    public void UpdatePageCount()
    {
        //현재 인벤토리에서 활성화 된 슬롯 수
        CurActiveSlotCount = inventoryPieceList
            .Where(slot => !slot.IsUsing)
            .Count(slot => slot.gameObject.activeSelf);

        var constraintCount = gridLayoutGroup.constraintCount;
        pageTotalCount = CurActiveSlotCount / constraintCount;

        var addOne = CurActiveSlotCount % constraintCount;
        if (addOne != 0)
        {
            pageTotalCount++;
        }

        if (pageTotalCount == 0)
        {
            curPageCount = 0;
        }
        else if (pageTotalCount > 0 && curPageCount == 0)
        {
            curPageCount++;
        }

        if (curPageCount > pageTotalCount)
        {
            ClickLeftArrow();
        }
    
        pageText.text = $"{curPageCount}/{pageTotalCount}";
    }
}