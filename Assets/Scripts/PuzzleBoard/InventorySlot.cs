using System;
using Coffee.UIExtensions;
using DevSupport;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public PieceData pieceData;
    public PieceControl pieceControl;
    public Sprite pieceImage;
    public SpriteRenderer pieceSprite;
    public PieceText pieceText;
    public Image image;
    public PieceControl pieceControlObj;
    
    public UITweenColor tween;
    public UIShiny uiShiny;
    
    private Transform pieceRoot;
    private string depth;
    private Action<InventorySlot> clickEvent;
    private Action<InventorySlot> holdEvent;
    private Action<InventorySlot> checkSlotActive;
    
    private Action<PieceControl> updateCallBack;
    private Action<PieceControl> mouseDownCallBack;
    private Action<PieceControl> mouseUpCallBack;
    private Action<PieceControl, Collision> enterCallBack;
    private Action<PieceControl, Collision> stayCallBack;
    private Action<PieceControl, Collision> exitCallBack;
    private Action refreshPuzzleData;

    public bool IsUsing { get; private set; }
    
    public void Init(PieceData pieceData, 
        PieceControl pieceControl, 
        Sprite pieceImage,
        SpriteRenderer pieceSprite,
        Sprite pieceIcon,
        string depth,
        Action<InventorySlot> clickEvent,
        Action<InventorySlot> holdEvent,
        Action<PieceControl> updateCallBack,
        Action<PieceControl> mouseDownCallBack,
        Action<PieceControl> mouseUpCallBack,
        Action<PieceControl, Collision> enterCallBack,
        Action<PieceControl, Collision> stayCallBack,
        Action<PieceControl, Collision> exitCallBack,
        Action<InventorySlot> checkSlotActive,
        Action refreshPuzzleData,
        Transform pieceRoot)
    {
        this.pieceData = pieceData;
        this.pieceControl = pieceControl;
        this.pieceImage = pieceImage;
        this.pieceSprite = pieceSprite;
        
        this.clickEvent = clickEvent;
        this.holdEvent = holdEvent;
        this.updateCallBack = updateCallBack;
        this.mouseDownCallBack = mouseDownCallBack;
        this.mouseUpCallBack = mouseUpCallBack;
        this.enterCallBack = enterCallBack;
        this.stayCallBack = stayCallBack;
        this.exitCallBack = exitCallBack;
        this.checkSlotActive = checkSlotActive;
        this.refreshPuzzleData = refreshPuzzleData;
        
        this.pieceRoot = pieceRoot;

        this.image.sprite = pieceIcon;
        pieceText.Piece_set(pieceData);
    }
    
    public void CreatePieceControlObj(bool isHold)
    {
        if (pieceSprite == null)
        {
            DebugX.LogError("pieceSprite is NULL");
            return;
        }

        if (pieceRoot == null)
        {
            return;
        }

        pieceControlObj = Instantiate(pieceControl, pieceRoot);
        pieceControlObj.firstIsHoldCheck = true;
        var pieceSpriteObj = Instantiate(pieceSprite, pieceControlObj.transform);
        
        pieceControlObj.Init(isHold, 
            pieceData,
            pieceImage,
            pieceSpriteObj,
            depth,
            updateCallBack,
            mouseDownCallBack, MouseUpCallBack,
            enterCallBack, stayCallBack, exitCallBack,
            ReturnInventory,
            refreshPuzzleData);
        
        
        IsUsing = true;
        this.gameObject.SetActive(false);
        ShinyStart();
    }
    
    public void Pressing()
    {
        CreatePieceControlObj(true);
        tween.ResetColor();
        
        holdEvent?.Invoke(this);
    }

    public void TakeOff()
    {
        holdEvent?.Invoke(null);
    }

    public void ClickSlot()
    {
        clickEvent?.Invoke(this);
    }

    private void MouseUpCallBack(PieceControl pieceControl)
    {
        //콜백 실행 후 피스 삭제 처리 진행
        //삭제 먼저하면 이벤트 순서 꼬임
        mouseUpCallBack?.Invoke(pieceControl);

        if ((pieceControl.points.Count == 0 || !pieceControl.position_check) && pieceControl.returnCheck) //인벤토리에 들어감
        {
            ReturnInventory(pieceControl);
        }
    }

    private void ReturnInventory(PieceControl pieceControl)
    {
        //this.gameObject.SetActive(true);
        IsUsing = false;
        checkSlotActive?.Invoke(this);
        Destroy(pieceControl.gameObject);
        
        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;
        var playerInstalledPuzzleDataList = playerDB.Data.GetPlayerInstalledPuzzleData();

        //설치된 피스 리스트에서 찾아서 없으면 샤이니 스탑
        var findData = playerInstalledPuzzleDataList.Find(v => v.uid == pieceControl.pieceData.Uid);
        if (findData == null)
        {
            ShinyStop();
        }
    }

    public void ShinyStart()
    {
        uiShiny.Play();
    }

    public void ShinyStop()
    {
        uiShiny.Stop();
    }
    
}