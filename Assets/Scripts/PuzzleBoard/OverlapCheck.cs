using System.Collections;
using System.Collections.Generic;
using DevSupport;
using UnityEngine;

public class OverlapCheck : MonoBehaviour
{
    public PieceControl pieceControl;

    private void OnCollisionStay(Collision collision)
    {
        var overlap = collision.gameObject.GetComponent<OverlapCheck>();

        if (overlap != null && pieceControl.isHold)
        {
            pieceControl.overlap = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        var overlap = collision.gameObject.GetComponent<OverlapCheck>();

        if (overlap != null && pieceControl.isHold)
        {
            pieceControl.overlap = false;
        }
    }
}
