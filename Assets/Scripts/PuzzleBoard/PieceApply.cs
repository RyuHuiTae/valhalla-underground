using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceApply : MonoBehaviour
{
    public EnhancePiece enhancePiece;
    
    [SerializeField] private GameObject arrow;

    private void Start()
    {
        enhancePiece = this.transform.parent.parent.GetComponent<EnhancePiece>();
    }

    private void OnCollisionStay(Collision collision)
    {
        var piece = collision.transform.GetComponent<PieceControl>();
        
        if (collision.gameObject.tag == "Piece" && !piece.isHold && !arrow.transform.gameObject.activeSelf)
        {
            if (piece.pieceData.PuzzleData.Type == PuzzleType.Character)
            {
                for (int count = 0; count < enhancePiece.applyList.Count; count++)
                {
                    if (enhancePiece.applyList[count] == piece)
                        return;
                }
                enhancePiece.applyList.Add(piece);
                arrow.transform.gameObject.SetActive(true);
                enhancePiece.RefreshPuzzleData();
                //AddStatValue(piece.pieceData.PuzzleData, enhancePiece.pieceData.PuzzleData);
            }
        }
        else if (collision.gameObject.tag == "Piece" && piece.isHold && arrow.transform.gameObject.activeSelf)
        {
            RemovePieceData(piece);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        var piece = collision.transform.GetComponent<PieceControl>();
        
        if (collision.gameObject.tag == "Piece" && piece.isHold && arrow.transform.gameObject.activeSelf)
        {
            RemovePieceData(piece);
        }
    }

    private void RemovePieceData(PieceControl piece)
    {
        if (piece.pieceData.PuzzleData.Type == PuzzleType.Character)
        {
            for (int count = 0; count < enhancePiece.applyList.Count; count++)
            {
                if (piece == enhancePiece.applyList[count])
                {
                    //removeStatValue(piece.pieceData.PuzzleData, enhancePiece.pieceData.PuzzleData);
                    enhancePiece.applyList.Remove(piece);
                    arrow.transform.gameObject.SetActive(false);
                    enhancePiece.RefreshPuzzleData();
                }
            }
        }
    }

    // public void AddStatValue(PuzzleData character, PuzzleData stat)
    // {
    //     var changeStatType = stat.StatPieceData.ChangedStat;
    //     var value = stat.StatPieceData.AppliedValue;
    //     
    //     switch (changeStatType)
    //     {
    //         case StatPieceChangedStat.AttackSpeedIncrease:      //합
    //             character.CharacterData.AttackSpeedAdd += value;
    //             break;
    //         case StatPieceChangedStat.AttackDamageIncrease:     //합
    //             character.CharacterData.AttackAdd += value;
    //             break;
    //         case StatPieceChangedStat.DefenseIncrease:          //합
    //             character.CharacterData.DefenceAdd += value;
    //             break;
    //         case StatPieceChangedStat.HpIncrease:               //합
    //             character.CharacterData.HpAdd += value;
    //             break;
    //         case StatPieceChangedStat.MoveSpeedIncrease:        //합
    //             character.CharacterData.SpeedAdd += value;
    //             break;
    //         case StatPieceChangedStat.SkillCoolTimeReduction:   //합
    //             character.CharacterData.CoolTimeAdd += value;
    //             break;
    //     }
    // }
    //
    // public void removeStatValue(PuzzleData character, PuzzleData stat)
    // {
    //     var changeStatType = stat.StatPieceData.ChangedStat;
    //     var value = stat.StatPieceData.AppliedValue;
    //     
    //     switch (changeStatType)
    //     {
    //         case StatPieceChangedStat.AttackSpeedIncrease:      //합
    //             character.CharacterData.AttackSpeedAdd -= value;
    //             break;
    //         case StatPieceChangedStat.AttackDamageIncrease:     //합
    //             character.CharacterData.AttackAdd -= value;
    //             break;
    //         case StatPieceChangedStat.DefenseIncrease:          //합
    //             character.CharacterData.DefenceAdd -= value;
    //             break;
    //         case StatPieceChangedStat.HpIncrease:               //합
    //             character.CharacterData.HpAdd -= value;
    //             break;
    //         case StatPieceChangedStat.MoveSpeedIncrease:        //합
    //             character.CharacterData.SpeedAdd -= value;
    //             break;
    //         case StatPieceChangedStat.SkillCoolTimeReduction:   //합
    //             character.CharacterData.CoolTimeAdd -= value;
    //             break;
    //     }
    // }
}