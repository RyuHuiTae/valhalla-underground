using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PieceControl : MonoBehaviour
{
    public PieceData pieceData;
    public EnhancePiece enhancePiece;

    public float max_x, min_x, max_y, min_y = 0;
    public float puzzle_position_x, puzzle_position_y = 0;
    
    public bool position_check = false; //위치를 저장했는지 확인
    public bool overlap = false; //겹치는 확인
    public bool isHold = false;
    public bool isClick = false;
    public bool returnCheck = true;
    public bool pointCheck = true;
    public bool startCheck = true;
    public bool firstIsHoldCheck = false;

    public Vector3 oneself;
    public Image pieceImage;
    public SpriteRenderer pieceSprite;
    private Action<PieceControl> updateCallBack;
    private Action<PieceControl> mouseDownCallBack;
    private Action<PieceControl> mouseUpCallBack;
    private Action<PieceControl, Collision> enterCallBack;
    private Action<PieceControl, Collision> stayCallBack;
    private Action<PieceControl, Collision> exitCallBack;
    private Action<PieceControl> returnInventory;
    private Action refreshPuzzleData;
    private PuzzleInfoPopup infoUI;

    public GameObject[] colliders;
    public List<GameObject> points;
    public List<GameObject> savePoints;
    public GameObject dragArea;
    public GameObject dragMouse;

    private string depth;
    
    private const string UI_LAYER_NAME = "UI";
    private const string OVERUI_LAYER_NAME = "OverUI";

    public void Init(bool isHold, 
        PieceData pieceData, Sprite pieceImage, SpriteRenderer pieceSprite, string depth,
        Action<PieceControl> updateCallBack = null,
        Action<PieceControl> mouseDownCallBack = null,
        Action<PieceControl> mouseUpCallBack = null,
        Action<PieceControl, Collision> enterCallBack = null,
        Action<PieceControl, Collision> stayCallBack = null,
        Action<PieceControl, Collision> exitCallBack = null,
        Action<PieceControl> returnInventory = null,
        Action refreshPuzzleData = null)
    {
        this.isHold = isHold;
        this.isClick = false;
        this.oneself = new Vector3(0, 0, 0);
        this.returnCheck = true;

        this.pieceData = pieceData;
        this.pieceImage.sprite = pieceImage;
        this.pieceSprite = pieceSprite;
        this.updateCallBack = updateCallBack;
        this.mouseDownCallBack = mouseDownCallBack;
        this.mouseUpCallBack = mouseUpCallBack;
        this.enterCallBack = enterCallBack;
        this.stayCallBack = stayCallBack;
        this.exitCallBack = exitCallBack;
        this.returnInventory = returnInventory;
        this.refreshPuzzleData = refreshPuzzleData;

        if (isHold)
        {
            this.pieceSprite.sortingLayerID = SortingLayer.NameToID(OVERUI_LAYER_NAME);
            this.pieceSprite.gameObject.SetActive(true);
        }
        else if (!isHold)
        {
            this.pieceSprite.sortingLayerID = SortingLayer.NameToID(UI_LAYER_NAME);
            this.pieceSprite.gameObject.SetActive(false);
        }
    }
    
    private void Start()
    {
        SetMousePosition();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            GameObject target = null;
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if ((Physics.Raycast(ray.origin, ray.direction * 10, out hit)))
            {
                target = hit.collider.gameObject;
                var pieceControl = target.transform.parent.GetComponent<PieceControl>();
                if (pieceControl != null)
                {
                    pieceControl.isClick = true;
                    pieceControl.pieceSprite.sortingLayerID = SortingLayer.NameToID(OVERUI_LAYER_NAME);
                }
            }
        }

        if (isClick)
        {
            infoUI = PopupManager.Instance.GetPopup(PopupName.PuzzleInfoPopup, depth) as PuzzleInfoPopup;
            infoUI.Init(pieceData);
            this.RefreshPuzzleData();

            dragArea.SetActive(true);
            dragMouse.SetActive(true);

            var mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            var worldPoint = Camera.main.ScreenToWorldPoint(mousePosition);

            dragArea.transform.position = new Vector3(worldPoint.x, worldPoint.y, transform.position.z);
            dragMouse.transform.position = new Vector3(worldPoint.x, worldPoint.y, transform.position.z);

            isClick = false;    
        } 
        else if (Input.GetMouseButtonUp(0))
        {
            dragArea.SetActive(false);
            dragMouse.SetActive(false);
        }
        
        if (isHold)
        {
            infoUI = PopupManager.Instance.GetPopup(PopupName.PuzzleInfoPopup, depth) as PuzzleInfoPopup;
            infoUI.Init(pieceData);
            this.RefreshPuzzleData();
            
            dragArea.SetActive(false);
            dragMouse.SetActive(false);
            
            if (Input.GetMouseButton(0))
            {
                SetMousePosition();
                
                pieceSprite.gameObject.SetActive(true);
                
                mouseDownCallBack?.Invoke(this);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isHold = false;
                pieceSprite.sortingLayerID = SortingLayer.NameToID(UI_LAYER_NAME);
                pieceSprite.gameObject.SetActive(false);

                mouseUpCallBack?.Invoke(this);
            }
        }

        updateCallBack?.Invoke(this);
    }

    private void OnCollisionEnter(Collision collision)
    {
        enterCallBack?.Invoke(this, collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        stayCallBack?.Invoke(this, collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        exitCallBack?.Invoke(this, collision);
    }

    private void SetMousePosition()
    {
        if (!isHold)
        {
            return;
        }

        var mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var worldPoint = Camera.main.ScreenToWorldPoint(mousePosition);
        
        this.transform.position = new Vector3(worldPoint.x, worldPoint.y, transform.position.z);
    }

    public void ReturnInventory()
    {
        returnInventory?.Invoke(this);
    }

    public void RefreshPuzzleData()
    {
        refreshPuzzleData?.Invoke();

        if (infoUI == null)
            infoUI = PopupManager.Instance.GetPopup(PopupName.PuzzleInfoPopup, depth) as PuzzleInfoPopup;
        
        infoUI.RefreshAddStat();
    }
    
}