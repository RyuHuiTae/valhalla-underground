using UnityEngine;

public class PieceData
{
    public ulong Uid { get; private set; }
    public PuzzleData PuzzleData { get; private set; }
    public int PiecePollution { get; private set; }

    public void Init(PuzzleData puzzleData, ulong uid, int pollution)
    {
        this.Uid = uid;
        this.PuzzleData = puzzleData;
        this.PiecePollution = pollution;
    }

    public void SetPiecePollution(int pollution)
    {
        this.PiecePollution = pollution;
    }

    /// <summary>
    /// UID가 필요 없는 경우 (ex 상점 아이템)
    /// </summary>
    public static PieceData GetNewPieceData(PuzzleData puzzleData)
    {
        var newData = new PieceData();
        newData.Init(puzzleData, 0, 0);

        return newData;
    }

    /// <summary>
    /// UID가 필요하거나 있는 경우 (ex 플레이어 소유 퍼즐 아이템)
    /// </summary>
    public static PieceData GetNewPieceData(PuzzleData puzzleData, PlayerPuzzleData playerPuzzleData)
    {
        var newData = new PieceData();
        newData.Init(puzzleData,
            playerPuzzleData.uid,
            playerPuzzleData.puzzlePollution);

        return newData;
    }
}