using UnityEngine;

public class PieceLoader : SingletonMonoBehaviour<PieceLoader>
{
    public Sprite[] typeImageArray;
    public Sprite[] characterTypeArray;
    public Sprite[] duplicateArray;
    
    public PieceControl GetPieceShapeObj(int shape)
    {
        var path = "Prefabs/Puzzle/PieceShape/Piece" + shape;
        
        var obj = VResources.Load<GameObject>(path);
        return obj.GetComponent<PieceControl>();
    }
    
    public Sprite GetPieceShapeSprite(int shape)
    {
        var spriteName = "PieceShape" + shape;
        return AtlasManager.Instance.GetIconSprite(spriteName);
    }
    
    public SpriteRenderer GetPieceSprite(PuzzleData puzzleData)
    {
        var path = "Prefabs/Puzzle/PieceImage/" + puzzleData.PieceImageName;
        
        var obj = VResources.Load<GameObject>(path);
        return obj.GetComponent<SpriteRenderer>();
    }
    
    public Sprite GetTypeImage(int type)
    {
        return typeImageArray[type];
    }
    
    public Sprite GetCharacterTypeImage(int characterType)
    {
        return characterTypeArray[characterType];
    }
    public Sprite GetDuplicateImage(int duplicate)
    {
        return duplicateArray[duplicate];
    }
}
