using System.Collections;
using System.Collections.Generic;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PieceText : MonoBehaviour
{
    public TextMeshProUGUI level_text;
    public TextMeshProUGUI name_text;
    //public TextMeshProUGUI cost_text;
    public Image type_sprite;
    public Image role_sprite;
    public Image icon_sprite;
    public TextMeshProUGUI pollution_Text;

    public void Piece_set(PieceData data)
    {
        if (data.PuzzleData.Type == PuzzleType.Character)    //캐릭터 피스
        {
            type_sprite.sprite = PieceLoader.Instance.GetTypeImage((int) data.PuzzleData.Type);
            role_sprite.sprite = PieceLoader.Instance.GetCharacterTypeImage((int) data.PuzzleData.CharacterData.Type);
            level_text.text = "Lv.";
            if (data.PuzzleData.CharacterData.Level / 10 < 1)
                level_text.text += "0";
            level_text.text += data.PuzzleData.CharacterData.Level;
        }
        else if (data.PuzzleData.Type == PuzzleType.Ability)   //어빌리티 피스
        {
            type_sprite.sprite = PieceLoader.Instance.GetTypeImage((int) data.PuzzleData.Type);
            pollution_Text.text = data.PiecePollution.ToString() + "%";
            role_sprite.sprite = PieceLoader.Instance.GetDuplicateImage(1);
            
            // if (data.PuzzleData.Cost > 1)
            //     role_sprite.sprite = PieceLoader.Instance.GetDuplicateImage(1);
            // else
            //     role_sprite.sprite = PieceLoader.Instance.GetDuplicateImage(0);
        }
        else if (data.PuzzleData.Type == PuzzleType.Stat)   //스탯 피스
        {
            //DebugX.LogError("스탯 피스 추가 작업 필요");
            type_sprite.sprite = PieceLoader.Instance.GetTypeImage((int) data.PuzzleData.Type);
            pollution_Text.text = data.PiecePollution.ToString() + "%";
            role_sprite.sprite = PieceLoader.Instance.GetDuplicateImage(0);
            
            // if (data.PuzzleData.Cost > 1)
            //     role_sprite.sprite = PieceLoader.Instance.GetDuplicateImage(1);
            // else
            //     role_sprite.sprite = PieceLoader.Instance.GetDuplicateImage(0);
        }

        icon_sprite.sprite = PieceLoader.Instance.GetPieceShapeSprite(data.PuzzleData.Shape);
        name_text.text = data.PuzzleData.Name;
        //cost_text.text = "Cost. "+ data.PuzzleData.Cost.ToString();
    }
}
