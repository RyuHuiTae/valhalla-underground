using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DevSupport;
using TMPro;
using UnityEngine;

public class PlayerBoardData : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI cCountText;
    [SerializeField] private TextMeshProUGUI aCountText;
    [SerializeField] private TextMeshProUGUI sCountText;
    [SerializeField] private int aCount;
    [SerializeField] private int sCount;
    
    public PieceControl[] character_data;
    public List<EnhancePiece> enhance_data = new List<EnhancePiece>();
    public List<EnhancePiece> stat_data = new List<EnhancePiece>();
    public List<PieceData> characterTypeList = new List<PieceData>();
    
    private int character_count = 0;

    public void Update()
    {
        aCount = 0;
        sCount = 0;
        
        for (int count = 0; count < enhance_data.Count; count++)
        {
            if (enhance_data[count].applyList.Count != 0)
                aCount++;
        }

        for (int count = 0; count < stat_data.Count; count++)
        {
            if (stat_data[count].applyList.Count != 0)
                sCount++;
        }
        
        aCountText.text = aCount.ToString();
        sCountText.text = sCount.ToString();
    }

    public bool Use_piece(PieceControl pieceControl)
    {
        if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Character)
        {
            if (character_count >= character_data.Length) //캐릭터 사용수 최대치 초과
            {
                pieceControl.ReturnInventory();

                DebugX.Log("배치 캐릭터피스는 최대" + character_data.Length + "개를 넘길 수 없습니다!!");
                
                var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                confirm.Init("알림", $"캐릭터 피스는 최대 {character_data.Length}개까지 부착할 수 있습니다.");
                
                //추가 실패
                return false;
            }
            
            characterTypeList.Add(pieceControl.pieceData);
            
            if (character_count < character_data.Length)
            {
                character_data[character_count] = pieceControl;
                
                character_count++;
                cCountText.text = character_count + "/" + character_data.Length;

                if (character_data.Length == character_count)
                {
                    cCountText.color = Color.red;
                }

                //추가 성공
                return true;
            }
        }
        else if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Ability)
        {
            //강화피스
            enhance_data.Add(pieceControl.enhancePiece);
            pieceControl.enhancePiece.ActivePoint();

            //추가 성공
            return true;
        }
        else if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Stat)
        {
            //DebugX.LogError("스탯 피스 추가 작업 필요");
            stat_data.Add(pieceControl.enhancePiece);
            pieceControl.enhancePiece.ActivePoint();
            
            return true;
        }

        return false;
    }

    public void Unused_piece(PieceControl pieceControl)
    {
        if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Character)
        {
            characterTypeList.Remove(pieceControl.pieceData);
            
            for (int slot = 0; slot < character_data.Length; slot++)
            {
                if (pieceControl == character_data[slot])
                {
                    character_data[slot] = null;
                
                    character_count--;
                    cCountText.text = character_count + "/" + character_data.Length;
                    cCountText.color = Color.white;
                    
                    if (slot < character_data.Length - 1)
                        Line_up_C(slot);
                    break;
                }
            }
        }
        else if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Ability)
        {
            enhance_data.Remove(pieceControl.enhancePiece);
            pieceControl.enhancePiece.InActivePoint();
        }
        else if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Stat)
        {
            stat_data.Remove(pieceControl.enhancePiece);
            pieceControl.enhancePiece.InActivePoint();
        }
    }

    private void Line_up_C(int slot)
    {
        if (character_data[slot + 1])
        {
            character_data[slot] = character_data[slot + 1];
            character_data[slot + 1] = null;
        }

        if (slot + 1 < character_data.Length - 1)
            Line_up_C(slot + 1);
    }
}