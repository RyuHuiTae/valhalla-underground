using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleInfoSlot : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI slotName;
    [SerializeField] private TextMeshProUGUI coolTime;
    [SerializeField] private TextMeshProUGUI desc;

    // public void Init(SkillData data)
    // {
    //     image.sprite = VResources.Load<Sprite>(data.IconPath);
    //     slotName.text = data.Name;
    //     coolTime.text = data.CoolDownTime + "초";
    //     desc.text = data.Desc;
    // }
    //
    // public void Init(EnhancePieceData data)
    // {
    //     image.sprite = VResources.Load<Sprite>(data.IconPath);
    //     slotName.text = "";
    //     coolTime.text = "Passive";
    //     desc.text = data.Desc;
    // }
    //
    // public void Init(PieceData data)
    // {
    //     slotName.text = "오염도";
    //     coolTime.text = data.PiecePollution + "%";
    //     desc.text = "오염도 설명 추가 해줘";
    // }
}
