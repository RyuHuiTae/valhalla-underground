using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicSkillBase : SkillBase
{
    public override void SetSkillData(SkillData skillDataParam, Action<Character> action)
    {
        base.SetSkillData(skillDataParam, action);
    }
    
    protected override void CastingProcess(Character target, float damage)
    {
        base.CastingProcess(target, damage);
        characterStatus.AttackCount++;
    }
}
