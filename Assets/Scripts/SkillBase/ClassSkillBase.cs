using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;
using UnityEngine.UI;

public class ClassSkillBase : SkillBase
{
    private Image _classSkillCoolTimeBar;
    private float _currentCoolTime = 10f;
    private float _currentMaxCoolTime;

    private BasicDataBase _basicDataBase;
    
    private void Awake()
    {
        // DB에 있는 쿨타임 값을 가져옴
        _basicDataBase = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;
    }

    public override void SetSkillData(SkillData skillDataParam, Action<Character> action)
    {
        base.SetSkillData(skillDataParam, action);
        _classSkillCoolTimeBar = character.GetClassSkillCoolTimeFilledImage();
        characterStatus.ClassSkillCoolTime.SetStat(CharacterStat.StatType.Base, skillDataParam.CoolDownTime);
        characterStatus.ClassSkillCoolTime.UpdateBaseMaxStat();
        _currentCoolTime = skillDataParam.CoolDownTime;
        
        float coolTimeLimit = _basicDataBase.SkillCoolTimeLimit() * 0.01f;
        characterStatus.ClassSkillCoolTime.LimitBuffPercent(coolTimeLimit);
        
        StarCoolTimeTimerCoroutine();
    }

    private async void StarCoolTimeTimerCoroutine()
    {
        if (skillData.Level < _basicDataBase.ClassSkillUnlockLevel())
        {
            _classSkillCoolTimeBar.fillAmount = 0f;
            return;   
        }
        
        var battleLayer = LayerManager.Instance.GetCurrentLayer().GetComponent<BattleLayer>();
        await UniTask.WaitUntil(() => battleLayer.TimerTime != 0f);
        
        UpdateSkillCoolTime();
        var battleField = GameField.Instance; 
        while (this)
        {
            if (characterStatus.IsClone)
            {
                _classSkillCoolTimeBar.fillAmount = 0f;
                return;
            }

            if (battleField.IsBattleStart && _currentCoolTime > 0f)
            {
                _currentCoolTime -= Time.deltaTime;
                
                if (skillData != null)
                    _classSkillCoolTimeBar.fillAmount = 1f - _currentCoolTime / _currentMaxCoolTime;
            }
            
            await UniTask.Yield(PlayerLoopTiming.FixedUpdate);
        }
    }

    public void UpdateSkillCoolTime()
    {
        _currentCoolTime = characterStatus.ClassSkillCoolTime.GetStat();
        _currentMaxCoolTime = characterStatus.ClassSkillCoolTime.GetStat();
    }

    public override bool CanCasting(Character target)
    {
        if (skillData.Heal != 0f)
        {
            var healTarget = character.FindAllyCharacter();
            if (healTarget.GetCurrentHpPercent() >= 1f)
            {
                return false;
            }
        }
        
        var isMyselfOrInRange = (base.CanCasting(target) || Target.Oneself == skillData.Target);
        return isMyselfOrInRange && _currentCoolTime <= 0f;
    }

    protected override void CastingProcess(Character target, float damage)
    {
        base.CastingProcess(target, damage * characterStatus.SkillAmplification.GetStat());
        _currentCoolTime = characterStatus.ClassSkillCoolTime.GetBaseMax();
        _currentMaxCoolTime = characterStatus.ClassSkillCoolTime.GetBaseMax();
    }

    public void RestoreCoolTime()
    {
        _currentCoolTime = 0f;
    }
}
