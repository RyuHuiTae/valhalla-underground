using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Triggers;
using DevSupport;
using UnityEngine;

public abstract class SkillBase : MonoBehaviour
{
    public bool IsCasting { get; private set; }
    [SerializeField]
    private string skillName;
    
    protected SkillData skillData;
    protected Character character;
    protected CharacterStatus characterStatus;

    public AudioClip skillsound1;
    public AudioClip skillsound2;

    private Action<Character> _updateTarget; 
    
    public virtual void SetSkillData(SkillData skillDataParam, Action<Character> updateTarget)
    {
        skillData = skillDataParam;
        skillName = skillDataParam.Name;
        character = GetComponent<Character>();
        characterStatus = GetComponent<CharacterStatus>();
        _updateTarget = updateTarget;
    }
    
    public virtual bool CanCasting(Character targetEnemy)
    {
        var targetColliderSize = targetEnemy.GetComponent<BoxCollider>().size.x * 0.5f;
        var targetDistance = skillData.Range + targetColliderSize;
        var distance = Vector3.Distance(transform.position, targetEnemy.transform.position);
        var canCasting = distance < targetDistance;
        
        return canCasting;
    }

    public async void Casting(Character targetEnemy, float damage, float attackSpeed)
    {
        IsCasting = true;
        PlaySkillSound();
        
        // 공속 퍼센트 만큼 수치를 줄여야 한다
        await UniTask.Delay(TimeSpan.FromSeconds(skillData.SkillTiming / attackSpeed));
        if (targetEnemy.IsDie || character.IsDie || characterStatus.isStun) // 때리던 도중 죽거나 스턴 먹은 경우
        {
            IsCasting = false;
            return;
        }
        CastingProcess(targetEnemy, damage);
        
        await UniTask.Delay(TimeSpan.FromSeconds(skillData.AttackCycle / attackSpeed));
        IsCasting = false;
    }

    private void PlaySkillSound()
    {
        //스킬 발동시 사운드 재생
        if (skillData.SkillSound1 != "Null")
        {
            skillsound1 = VResources.Load<AudioClip>(skillData.SkillSound1);
            AudioManager.Instance.EffectSound(skillData.Name, skillsound1, false, skillData.SoundVolume);
        }
    }
    
    /// <summary>
    /// NOTE: 애니메이션이나 사운드와 관련된 구현이 담길 예정
    /// </summary>
    protected virtual void CastingProcess(Character target, float damage)
    {
        if (target == null)
            return;
        
        var casterTeam = character.Team;
        var gameField = GameField.Instance;
        List<Character> findingTargets = null;
        
        var targetColliderSize = target.GetComponent<BoxCollider>().size.x * 0.5f;
        var range = skillData.Range + targetColliderSize;

        switch (skillData.Target)
        {
            case Target.None:
                break;
            case Target.Enemy:
                findingTargets = gameField.FindEnemies(transform.position, casterTeam, skillData.IsWhatBase, skillData.Priority, range).ToList();
                break;
            case Target.Ally:
                findingTargets = gameField.FindEnemies(transform.position, target.Team, skillData.IsWhatBase, skillData.Priority, range).ToList();
                break;
            case Target.Oneself:
                findingTargets = new List<Character>() {GetComponent<Character>()};
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        var targets = new List<Character>();
        for (int i = 0; i < findingTargets.Count; i++)
        {
            if (i == skillData.NumberOfTargets)
                break;
            
            targets.Add(findingTargets[i]);
        }

        if (targets.Count == 0)
            return;

        // 칼럼에 따라 스킬이 결정됨
        switch (skillData.SkillType)
        {
            case SkillType.None:
                break;
            case SkillType.Active:
                for (int i = 0; i < targets.Count; i++)
                {
                    var resultDamage = targets[i].ReceiveDamage(damage, skillData.AppliedDebuff);
                    character.AddAttackDamageAmount(resultDamage);
                }
                break;
            case SkillType.Buff:
                var appliedBuff = skillData.AppliedBuff.Clone() as BuffData;
                appliedBuff.AppliedValue *= characterStatus.SkillAmplification.GetStat();
                if (characterStatus.IsCurse)
                {
                    appliedBuff.AppliedValue += appliedBuff.AppliedValue * (skillData.AppliedDebuff.AppliedPercentValue * 0.01f);
                }
                
                for (int i = 0; i < targets.Count; i++)
                {
                    BuffBase.GiveBuffBase(appliedBuff, targets[i].gameObject, character);
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (skillData.ResurrectionHpPercentValue != 0f)
        {
            var allys = gameField.GetAllys(casterTeam);
            var dieCharacterCount = allys.Count(x => x.IsDie);
            if (dieCharacterCount == 0)
            {
                GetComponent<UniqueSkillBase>().RestoreSkill();
            }
            else
            { 
                var diedAlly = allys.First(x => x.IsDie);
                diedAlly.Resurrection(skillData.ResurrectionHpPercentValue);
                BuffBase.GiveBuffBase(skillData.AppliedDebuff, diedAlly.gameObject, character);   
            }
        }
        
        if (skillData.ContinuallyAppliedDebuff.ID != 0)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                targets[i].ReceiveDamage(0f, skillData.ContinuallyAppliedDebuff);
            }
        }
        
        if (skillData.IsSummons)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                SummonsBase.CreateSummons(skillData.AppliedSummonsData, targets[i].gameObject, gameObject);
            }
        }

        if (skillData.EffectPath != "Null")
        {
            for (int i = 0; i < targets.Count; i++)
            {
                var targetObject = targets[i];
                var prefab = VResources.Load<GameObject>(skillData.EffectPath);
                var box = targetObject.GetComponent<BoxCollider>();
                var position = targetObject.transform.position + box.center;
                position = new Vector3(position.x, position.y - (box.size.y / 2f), position.z);
                Instantiate(prefab, position, Quaternion.identity);
            }
        }
        
        if (skillData.IsBlinkToTarget)
        {
            var targetObject = targets.First();
            var targetPosition = targetObject.gameObject.transform.position;
            var isLeft = targetPosition.x > gameObject.transform.position.x;
            var targetBoxSize = targetObject.GetComponent<BoxCollider>().size;
            var resultPosition = isLeft 
                ? targetPosition + new Vector3(targetBoxSize.x, 0f, 0f) 
                : targetPosition - new Vector3(targetBoxSize.x, 0f, 0f);
            
            transform.SetPositionAndRotation(resultPosition, Quaternion.identity);
            character.GetComponent<CharacterAI>().FlipSpriteAccordanceWithDirection();
            
            if (skillData.SkillSound2 != "Null")
            {
                skillsound2 = VResources.Load<AudioClip>(skillData.SkillSound2);
                AudioManager.Instance.EffectSound(skillData.Name, skillsound2, false, skillData.SoundVolume);
            }
        }

        if (skillData.Heal != 0f)
        {
            var heal = skillData.Heal + characterStatus.Attack.GetStat() * skillData.DamageCoefficient;
            if (characterStatus.IsCurse)
            {
                heal += heal * (skillData.AppliedDebuff.AppliedPercentValue * 0.01f) * characterStatus.SkillAmplification.GetStat();
            }
            
            for (int i = 0; i < targets.Count; i++)
            {
                var isHealSuccess = targets[i].Heal(heal, character);
                if (!isHealSuccess)
                {
                    GetComponent<ClassSkillBase>().RestoreCoolTime();
                }
                character.AddHealAmount(heal);
            }
        }

        var enemyCharacter = findingTargets.First();
        _updateTarget?.Invoke(enemyCharacter.Team == character.Team ? null : enemyCharacter);
    }
    public float GetDamageCoefficient() => skillData.DamageCoefficient;
    public float GetDamage() => skillData.Damage;

    public string GetAnimationTriggerName => skillData.AnimationTriggerName;
}
