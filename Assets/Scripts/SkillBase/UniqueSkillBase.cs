using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;
using UnityEngine.UI;

public class UniqueSkillBase : SkillBase
{
    private int _limitationCount = 1;
    private float _currentCoolTime = 10f;
    
    private Image _characterBaseIcon;
    private Image _characterUniqueSkillCoolTimeIcon;
    private bool _isCharacterUniqueSkillCoolTimeIconNotNull;
    private bool _characterUniqueSkillCoolTimeIconNotNull;

    private void Start()
    {
        _characterUniqueSkillCoolTimeIconNotNull = _characterUniqueSkillCoolTimeIcon != null;
        _isCharacterUniqueSkillCoolTimeIconNotNull = _characterUniqueSkillCoolTimeIcon != null;
    }

    public override void SetSkillData(SkillData skillDataParam, Action<Character> action)
    {
        base.SetSkillData(skillDataParam, action);
        _characterBaseIcon = character.GetCharacterBaseIcon();
        _characterUniqueSkillCoolTimeIcon = character.GetUniqueSkillCoolTimeIcon();
        
        characterStatus.UniqueSkillCoolTime.SetStat(CharacterStat.StatType.Base, skillDataParam.CoolDownTime);
        characterStatus.UniqueSkillCoolTime.UpdateBaseMaxStat();
        StarCoolTimeTimerCoroutine();
    }
    
    public override bool CanCasting(Character target)
    {
        if (IsImmortality())
        {
            return false;
        }
        if (IsResurrection())
        {
            var gameField = GameField.Instance;
            var allys = gameField.GetAllys(character.Team);
            return allys.Any(t => t.IsDie && _limitationCount > 0 && _currentCoolTime <= 0f && !t.IsClone);
        }
            
        var canCasting = _limitationCount > 0 && (base.CanCasting(target) || Target.Oneself == skillData.Target);
        return canCasting && _currentCoolTime <= 0f;
    }

    protected override void CastingProcess(Character target, float damage)
    {
        _limitationCount--;
        GrayScaling();
        
        base.CastingProcess(target, damage * characterStatus.SkillAmplification.GetStat());
    }

    private async void StarCoolTimeTimerCoroutine()
    {
        var basicDB = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;
        if (character.Data.Level < basicDB.UniqueSkillUnlockLevel())
        {
            UniqueSkillPenalty();
            return;
        }

        var battleField = GameField.Instance;
        var battleLayer = LayerManager.Instance.GetCurrentLayer().GetComponent<BattleLayer>();
        await UniTask.WaitUntil(() => battleLayer.TimerTime != 0f);
        _currentCoolTime = characterStatus.UniqueSkillCoolTime.GetStat();
        while (_currentCoolTime > 0f)
        {
            if (!this)
                return;
            
            if (characterStatus.IsClone)
                return;
            
            if (battleField.IsBattleStart)
            {
                _currentCoolTime -= Time.deltaTime;

                if (skillData != null)
                {
                    if (_isCharacterUniqueSkillCoolTimeIconNotNull)
                        _characterUniqueSkillCoolTimeIcon.fillAmount = 1f - _currentCoolTime / characterStatus.UniqueSkillCoolTime.GetStat();
                }
            }
            
            await UniTask.Yield(PlayerLoopTiming.FixedUpdate);
        }

        _currentCoolTime = 0f;

        if (_characterUniqueSkillCoolTimeIconNotNull)
            _characterUniqueSkillCoolTimeIcon.fillAmount = 1f;
    }
    
    public void GiveAppliedBuff()
    {
        BuffBase.GiveBuffBase(skillData.AppliedBuff, gameObject, character);
        _limitationCount--;
        GrayScaling();
    }

    public bool IsImmortality()
    {
        return skillData.AppliedBuffId >= 31 && skillData.AppliedBuffId <= 40 && _limitationCount > 0;
    }

    private bool IsResurrection()
    {
        return skillData.ID >= 131 && skillData.ID <= 140 && _limitationCount > 0;
    }
    
    public void UniqueSkillPenalty()
    {
        _limitationCount = 0;
        GrayScaling();
    }

    public void HideCharacterIcon()
    {
        _characterBaseIcon.gameObject.SetActive(false);
        _characterUniqueSkillCoolTimeIcon.gameObject.SetActive(false);
    }
    
    private void GrayScaling()
    {
        if(!this)
            return;
        
        _characterUniqueSkillCoolTimeIcon.gameObject.SetActive(false);
    }
    
    public void RestoreSkill()
    {
        _characterUniqueSkillCoolTimeIcon.gameObject.SetActive(true);
        _currentCoolTime = 0f;
        _limitationCount = 1;
    }
}
