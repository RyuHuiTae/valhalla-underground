using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    private float _value;
    private float _duration;
    private Buff _connectedBuff;

    public void SetBarrier(float value, float duration, Buff connectedBuff)
    {
        _value = value;
        _duration = duration;
        _connectedBuff = connectedBuff;
        
        StartCoroutine(StarCoolTimeTimerCoroutine());
    }
    
    private IEnumerator StarCoolTimeTimerCoroutine()
    {
        while (this)
        {
            if (_duration > 0f)
            {
                _duration -= Time.deltaTime;
            }
            else
            {
                break;
            }
            
            yield return new WaitForFixedUpdate();
        }

        CrashBarrier();
    }

    public void CrashBarrier()
    {
        if(_connectedBuff)
            Destroy(_connectedBuff);

        if (this)
            Destroy(this);
    }

    public void ReceiveDamage(float damage)
    {
        _value -= damage;

        if (_value < 0f)
        {
            CrashBarrier();
        }
    }
}
