using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone : MonoBehaviour
{
    private Character original;
    private float lifeTime;

    private void Awake()
    {
        var shields = GetComponentsInChildren<ShieldEffect>();
        for (int i = 0; i < shields.Length; i++)
        {
            Destroy(shields[i].gameObject);
        }
    }

    public void SetOriginal(Character originalParam)
    {
        original = originalParam;
    }

    public void LinkWithOriginal(float duration)
    {
        lifeTime = duration;
        StartCoroutine(StarTimeTimerCoroutine());
    }

    private IEnumerator StarTimeTimerCoroutine()
    {
        while (!original.IsDie && lifeTime > 0)
        {
            lifeTime -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        Die();
    }

    private void Die()
    {
        var cloneCharacter = GetComponent<Character>();
        var currentHp = cloneCharacter.GetCurrentHp();
        cloneCharacter.DieClone(currentHp);
    }
}
