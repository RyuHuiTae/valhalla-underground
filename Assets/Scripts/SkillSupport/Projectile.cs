using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float _damage;
    private DebuffData _debuffData;
    private Character _casterCharacter;
    private Character.CharacterTeam _team;
    private Transform _projectileTransform;
    
    private float _speed;
    private Vector3 _dir;
    private bool _flip;
    
    public void SetProjectile(SummonsData summonsData, DebuffData debuffData, Character casterCharacter, Vector3 targetLocalPosition)
    {
        var basicDataBase = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;
        _speed = basicDataBase.ProjectileSpeed();
        _casterCharacter = casterCharacter;
        _debuffData = debuffData;
        _team = casterCharacter.Team;
        _damage = summonsData.DamageCoefficient * casterCharacter.GetComponent<CharacterStatus>().Attack.GetStat() + summonsData.Damage;
        
        _projectileTransform = transform;

        var position = _projectileTransform.position;
        _flip = !(position.x < targetLocalPosition.x);
        _dir = (targetLocalPosition - position).normalized;
        
        _projectileTransform.right = _dir;
        if (_flip)
        {
            _dir *= -1f;
            var eulerAngles = _projectileTransform.rotation.eulerAngles;
            _projectileTransform.rotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z + 180f);
        }
        GetComponent<SpriteRenderer>().flipX = _flip;
    }   

    private void Update()
    {
        MoveToTarget();
    }

    private void MoveToTarget()
    {
        if (!_projectileTransform) return;
        
        var timeSpeed = (_speed * Time.deltaTime);
        var translation = _flip ? -_dir * timeSpeed : _dir * timeSpeed;

        if (_flip)
            _projectileTransform.position += translation;
        else
            _projectileTransform.position += translation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Wall")
        {
            Destroy(gameObject);
            return;
        }
        
        var character = other.gameObject.GetComponent<Character>();
        if (!character) return;
        if (_team == character.Team) return;
        
        var resultDamage = character.ReceiveDamage(_damage, _debuffData);
        _casterCharacter?.AddAttackDamageAmount(resultDamage);
        Destroy(gameObject);
    }
}
