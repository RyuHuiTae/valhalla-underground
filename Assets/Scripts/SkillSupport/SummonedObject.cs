using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;

public class SummonedObject : MonoBehaviour
{
    private SummonsData _summonsData;
    private Character.CharacterTeam _team;
    private float _calculatedDamageCoefficient;
    private Character _caster;
    private CharacterStatus _casterCharacterStatus;

    public void SetSummonedObject(SummonsData summonsData, Character casterCharacter)
    {
        _summonsData = summonsData;
        _team = casterCharacter.Team;
        _caster = casterCharacter;
        _casterCharacterStatus = casterCharacter.GetComponent<CharacterStatus>();
        _calculatedDamageCoefficient = summonsData.DamageCoefficient * _casterCharacterStatus.Attack.GetStat();

        switch (summonsData.AppliedDamageType)
        {
            case AppliedDamageType.None:
                break;
            case AppliedDamageType.InstantDamage:
                StartInstantDamageCoroutine();
                break;
            case AppliedDamageType.FixedDotDamage:
                StartDotDamageCoroutine();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(summonsData.AppliedDamageType), summonsData.AppliedDamageType, null);
        }
    }

    private async void StartInstantDamageCoroutine()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(_summonsData.AttackCycle), ignoreTimeScale: false);
        if (!this) return;
        GetComponent<BoxCollider>().enabled = true;
        
        await UniTask.Delay(TimeSpan.FromSeconds(0.01f), ignoreTimeScale: false);
        if (!this) return;
        Destroy(gameObject);
    }

    private async void StartDotDamageCoroutine()
    {
        var boxCollider = GetComponent<BoxCollider>();
        StartCoroutine(StarDurationTimerCoroutine());
        while (gameObject)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(_summonsData.AttackCycle), ignoreTimeScale: false);
            if (!this) return;
            boxCollider.enabled = true;
        
            await UniTask.Delay(TimeSpan.FromSeconds(0.01f), ignoreTimeScale: false);
            if (!this)
                return;
            boxCollider.enabled = false;
        }
    }
    
    private IEnumerator StarDurationTimerCoroutine()
    {
        var duration = _summonsData.Duration;
        while (duration > 0f)
        {
            duration -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        var character = other.gameObject.GetComponent<Character>();
        if (!character) return;
        if (_team == character.Team) return;

        if (_summonsData.IsMaxHpPercentDamage)
        {
            float percent = _summonsData.Damage / 100f;
            float damage = 0f;
            
            switch (_summonsData.ProportionalToHp)
            {
                case ProportionalToHp.None:
                    break;
                case ProportionalToHp.Enemy:
                    damage = character.GetMaxHp() * percent;
                    break;
                case ProportionalToHp.Own:
                    damage = _caster.GetMaxHp() * percent;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            character.ReceiveTrueDamage((damage + _calculatedDamageCoefficient) * _casterCharacterStatus.SkillAmplification.GetStat(), _summonsData.AppliedDebuff);
        }
        else
        {
            var damage = (_summonsData.Damage + _calculatedDamageCoefficient) * _casterCharacterStatus.SkillAmplification.GetStat();
            switch (_summonsData.AppliedDamageType)
            {
                case AppliedDamageType.None:
                    break;
                case AppliedDamageType.InstantDamage:
                    character.ReceiveDamage(damage, _summonsData.AppliedDebuff);
                    break;
                case AppliedDamageType.FixedDotDamage:
                    character.ReceiveTrueDamage(damage, _summonsData.AppliedDebuff);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(_summonsData.AppliedDamageType), _summonsData.AppliedDamageType, null);
            } 
        }
    }
}
