using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Triggers;
using DevSupport;
using UnityEngine;
using UnityEngine.UI;

public class SmoothColorSwapping : MonoBehaviour
{
    [SerializeField] private List<SwappingData> swappingData;
    [SerializeField] private float tick = 0.1f;
    private Image image;

    private void Awake()
    {
        var backgroundPopup = PopupManager.Instance.GetPopup(PopupName.BackgroundPopup).GetComponent<BackgroundPopup>();
        if (!backgroundPopup)
        {
            DebugX.LogError("배경 팝업이 존재하지 않음");
            return;
        }
            
        image = backgroundPopup.GetImage();
        Play();
    }

    // Start is called before the first frame update
    public async void Play()
    {
        for (int i = 0; i < swappingData.Count; i++)
        {
            SwappingData swapping = swappingData[i];
            var tickCount = (int)(swapping.second / tick);
            var tickColor = (swapping.color - image.color) / tickCount; // 11, 11, 11 / 0, 0, 0
            
            for (int j = 0; j < tickCount; j++)
            {
                image.color += tickColor;
                
                await UniTask.Delay(TimeSpan.FromSeconds(tick));   
            }
        }
    }
    
}

[Serializable]
public class SwappingData
{
    public Color color;
    public float second;
}

