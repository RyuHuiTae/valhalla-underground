using System;
using System.Collections.Generic;
using System.Linq;
using DevSupport;
using Random = UnityEngine.Random;

public class StageManager : SingletonMonoBehaviour<StageManager>
{
    private const int RepeatMaxCount = 5;
    
    public List<StageSlotData> StageSlotDataList { get; private set; }
    public bool IsCreateStage => StageSlotDataList != null;

    public List<StageSlotData> GetStageSlotDataList(Action updateEndCallback)
    {
        StageSlotDataList ??= new List<StageSlotData>();

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return StageSlotDataList;

        var playerStageDataList = playerDB.Data.GetPlayerStageData();

        //저장된 데이터가 없다면 새로 만들어서 반환
        if (playerStageDataList.Count <= 0)
        {
            return CreateNewStageSlotDataList(updateEndCallback);
        }

        //한번 초기화 후 다시 추가함
        StageSlotDataList.Clear();

        var stageDataList = DataBaseManager.Instance.GetDataList<StageDataBase>().ToList();
        foreach (var data in playerStageDataList)
        {
            var findData = stageDataList.Find(v => v.ID == data.stageDataId) as StageData;
            StageSlotDataList.Add(new StageSlotData()
            {
                data = findData,
                type = (StageType) data.stageDataType,
                isCurrentSlot = data.isCurrentSlot,
                isSlotEventEnd = data.isSlotEventEnd,
            });
        }

        return StageSlotDataList;
    }

    public List<StageSlotData> CreateNewStageSlotDataList(Action updateEndCallback)
    {
        StageSlotDataList.Clear();

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return StageSlotDataList;

        if (DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB)
        {
            //최종 보스 층
            if (playerDB.Data.Floor > (int) basicDB.MaxFloor())
            {
                var stageDataList = DataBaseManager.Instance.GetDataList<StageDataBase>();

                var finalBossDataList = stageDataList
                    .Select(v => v as StageData)
                    .Where(v => v.Type == StageFloorType.FinalBoss);

                foreach (var stageData in finalBossDataList)
                {
                    if (stageData.StageStep1 != 0)
                    {
                        var stageType = stageData.StageType;
                        StageSlotDataList.Add(new StageSlotData()
                        {
                            data = stageData,
                            type = stageType,
                            isCurrentSlot = (stageData.StageType == StageType.Start)
                        });
                    }
                }
            }
            //튜토리얼로 지정한 층
            else if (playerDB.Data.Floor == (int) basicDB.TutorialFloor())
            {
                var stageDataList = DataBaseManager.Instance.GetDataList<StageDataBase>();
            
                var tutorialDataList = stageDataList
                    .Select(v => v as StageData)
                    .Where(v => v.Type == StageFloorType.Tutorial);
            
                foreach (var stageData in tutorialDataList)
                {
                    if (stageData.StageStep1 != 0)
                    {
                        var stageType = stageData.StageType;
                        StageSlotDataList.Add(new StageSlotData()
                        {
                            data = stageData,
                            type = stageType,
                            isCurrentSlot = (stageData.StageType == StageType.Start)
                        });
                    }
                }
            }
            //그 외
            else
            {
                var randomTypeQueue = GetRandomStageTypeQueue();
                var stageDataList = DataBaseManager.Instance.GetDataList<StageDataBase>();

                var stageNormalDataList = stageDataList
                    .Select(v => v as StageData)
                    .Where(v => v.Type == StageFloorType.Normal)
                    .ToList();

                foreach (var stageData in stageNormalDataList)
                {
                    if (stageData.StageStep1 == 0) 
                        continue;
                    
                    var stageType = stageData.StageType;
                    if (stageData.StageType == StageType.Random)
                    {
                        var repeatCount = 0;
                        var findSameTypeCount = 0;

                        do
                        {
                            //랜덤으로 받아온 타입
                            stageType = randomTypeQueue.Dequeue();

                            //자신과 같은 라인의 슬롯들 중
                            //일반 전투를 제외하고 자신과 같은 타입의 슬롯 개수
                            findSameTypeCount = StageSlotDataList
                                .Where(v => v.data.StageStep1 == stageData.StageStep1)
                                .Where(v => v.type != StageType.NormalBattle)
                                .Count(v => v.type == stageType);

                            //이미 한개이상 추가된 경우
                            if (findSameTypeCount > 0)
                            {
                                //다시 넣는다
                                randomTypeQueue.Enqueue(stageType);
                            }
                            
                            //반복 횟수 증가
                            repeatCount++;
                            
                            //최대 반복 횟수 5회 
                        } while (findSameTypeCount > 0 && repeatCount < RepeatMaxCount);

#if UNITY_EDITOR
                        if (repeatCount >= RepeatMaxCount)
                            DebugX.LogWarning("최대 반복 횟수 초과로 스테이지 타입 체크 통과");
                        
                        //에디터일때 치트 고려함
                        switch (GameSetting.Instance.nextStageType)
                        {
                            case StageEditorType.Event:
                                stageType = StageType.Event;
                                break;
                            case StageEditorType.Rest:
                                stageType = StageType.Rest;
                                break;
                            case StageEditorType.Shop:
                                stageType = StageType.Shop;
                                break;
                        }
#endif
                    }

                    StageSlotDataList.Add(new StageSlotData()
                    {
                        data = stageData,
                        type = stageType,
                        isCurrentSlot = (stageData.StageType == StageType.Start)
                    });
                }
            }
        }

        var playerStageDataList = playerDB.Data.GetPlayerStageData();
        playerStageDataList.Clear();

        foreach (var slot in StageSlotDataList)
        {
            var playerStageData = new PlayerStageData(slot);
            playerStageDataList.Add(playerStageData);
        }

        playerDB.Data.SetPlayerStageData(playerStageDataList);
        playerDB.UpdatePlayerDataBase(updateEndCallback);

        return StageSlotDataList;
    }

    private Queue<StageType> GetRandomStageTypeQueue()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return new Queue<StageType>();
        
        var randomDataList = DataBaseManager.Instance.GetDataList<StageRandomDataBase>();
        
        var curFloorDataList = randomDataList
            .Select(v => v as StageRandomData)
            .Where(v => v.Floor == playerDB.Data.Floor);

        var stageRandomList = new List<StageType>();
        foreach (var randomData in curFloorDataList)
        {
            var type = (StageType) randomData.Type;
            for (var i = 0; i < randomData.Max; i++)
            {
                stageRandomList.Add(type);
            }
        }

        for (int i = 0; i < stageRandomList.Count; ++i)
        {
            int random1 = Random.Range(0, stageRandomList.Count);
            int random2 = Random.Range(0, stageRandomList.Count);

            var tmp = stageRandomList[random1];
            stageRandomList[random1] = stageRandomList[random2];
            stageRandomList[random2] = tmp;
        }

        return new Queue<StageType>(stageRandomList);
    }

    public void SetCurrentSlot(int id)
    {
        if (StageSlotDataList == null)
            return;

        var prevSlot = StageSlotDataList.Find(v => v.isCurrentSlot);
        prevSlot.isCurrentSlot = false;

        var newSlot = StageSlotDataList.Find(v => v.data.ID == id);
        newSlot.isCurrentSlot = true;

        if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
        {
            var playerStageDataList = playerDB.Data.GetPlayerStageData();

            var findPrevData = playerStageDataList.Find(v => v.stageDataId == prevSlot.data.ID);
            findPrevData.isCurrentSlot = false;

            var findNextData = playerStageDataList.Find(v => v.stageDataId == newSlot.data.ID);
            findNextData.isCurrentSlot = true;

            playerDB.Data.SetPlayerStageData(playerStageDataList);
            playerDB.UpdatePlayerDataBase(null);
        }
    }

    public void ClearStageSlotDataList()
    {
        StageSlotDataList?.Clear();
        StageSlotDataList = null;
    }
}