using System;
using System.Collections.Generic;
using UnityEngine;

public class SummonsBase : MonoBehaviour
{
    public static void CreateSummons(SummonsData summonsData, GameObject target, GameObject caster)
    {
        var targetCharacter = target.GetComponent<Character>();
        var casterCharacter = caster.GetComponent<Character>();
        switch (summonsData.SummonsType)
        {
            case SummonsType.None:
                break;
            case SummonsType.SpawnCharacter:
                // 캐릭터와 똑같은 모델의 분신을 생성
                // 궁을 못쓰게 하고 hp를 조율함
                var targetPosition = target.gameObject.transform.position;
                var originalSize = target.GetComponent<BoxCollider>().size;
                var resultPosition = target.GetComponent<CharacterAI>().GetCurrentFlip 
                    ? targetPosition + new Vector3(originalSize.x, 0f, 0f) 
                    : targetPosition - new Vector3(originalSize.x, 0f, 0f);
                var cloneCharacter = GameField.Instance.CreateCharacter(targetCharacter, resultPosition, true);
                var clone = cloneCharacter.gameObject.AddComponent<Clone>();
                
                clone.SetOriginal(targetCharacter);
                clone.LinkWithOriginal(summonsData.Duration);

                var uniqueSkill = clone.GetComponent<UniqueSkillBase>();
                uniqueSkill.UniqueSkillPenalty();
                uniqueSkill.HideCharacterIcon();

                var cloneCharacterStatus = clone.GetComponent<CharacterStatus>();
                cloneCharacterStatus.Cloning(summonsData.SummoningStatsPercent, casterCharacter);

                var targetBuffs = targetCharacter.GetComponent<Character>().GetBuffsLayoutTransform().GetComponentsInChildren<BuffBase>();
                var buffs = cloneCharacter.GetBuffsLayoutTransform().GetComponentsInChildren<BuffBase>();
                for (int i = 0; i < buffs.Length; i++)
                {
                    if (buffs[i].GetType() == typeof(Buff))
                    {
                        var original = (targetBuffs[i] as Buff);
                        var buff = buffs[i] as Buff;
                        buff.CloneBuff(original.BuffData, original.CurrentDuration, cloneCharacter, original.casterStatus);
                    }
                    else
                    {
                        var original = (targetBuffs[i] as Debuff);
                        var debuff = buffs[i] as Debuff;
                        debuff.CloneBuff(original.DeBuffData, original.CurrentDuration, cloneCharacter, original.casterStatus);
                    }
                }
                break;
            case SummonsType.SpawnObject:
                var objectPrefab = VResources.Load<GameObject>(summonsData.EffectPath);
                var summonedObject = Instantiate(objectPrefab, target.transform.position, Quaternion.identity).GetComponent<SummonedObject>();
                summonedObject.SetSummonedObject(summonsData, casterCharacter);
                break;
            case SummonsType.SpawnProjectile:
                var projectileObject = VResources.Load<GameObject>(summonsData.EffectPath);
                var projectile = Instantiate(projectileObject, caster.transform.position + caster.GetComponent<BoxCollider>().center, caster.transform.rotation).GetComponent<Projectile>();
                projectile.SetProjectile(summonsData, null, casterCharacter, targetCharacter.transform.position + targetCharacter.GetComponent<BoxCollider>().center);
                break;
            case SummonsType.SpawnMultiProjectile:
                Cast2PageBossSkill(summonsData, casterCharacter);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private static void Cast2PageBossSkill(SummonsData summonsData, Character caster)
    {
        var projectileObject = VResources.Load<GameObject>(summonsData.EffectPath);
        var casterPosition = caster.transform.position + caster.GetComponent<BoxCollider>().center;
        var projectileDirList = new List<Vector3>
        {
            new Vector3(casterPosition.x + 1f, casterPosition.y ,casterPosition.z),
            new Vector3(casterPosition.x - 1f, casterPosition.y ,casterPosition.z),
            new Vector3(casterPosition.x, casterPosition.y + 1f,casterPosition.z),
            new Vector3(casterPosition.x, casterPosition.y - 1f,casterPosition.z),
            new Vector3(casterPosition.x + 0.5f, casterPosition.y + 0.5f,casterPosition.z),
            new Vector3(casterPosition.x - 0.5f, casterPosition.y + 0.5f,casterPosition.z),
            new Vector3(casterPosition.x + 0.5f, casterPosition.y - 0.5f,casterPosition.z),
            new Vector3(casterPosition.x - 0.5f, casterPosition.y - 0.5f,casterPosition.z),
        };

        for (int i = 0; i < projectileDirList.Count; i++)
        {
            var projectile = Instantiate(projectileObject, caster.transform.position + caster.GetComponent<BoxCollider>().center, caster.transform.rotation).GetComponent<Projectile>();
            projectile.SetProjectile(summonsData, null, caster, projectileDirList[i]);
        }
    }
}
