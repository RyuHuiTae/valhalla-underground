using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattleScenePopup : CommonPopup
{
    public GameObject root;
    public GameObject allButton;
    public GameObject characterButton;
    public GameObject abilityButton;
    public GameObject statButton;
    public GameObject startButton;
    public GameObject stopButton;
    public GameObject posEditButton;
    public GameObject addButton;
    public GameObject removeButton;
    public GameObject playButton;
    public GameObject editButton;
    public Transform slotRoot;
    public GameObject posEditRoot;
    
    public GameObject playerPosSlotEditRoot;
    public GameObject enemyPosSlotEditRoot;
    
    public InventorySlot characterSlot;
    public InventorySlot abilitySlot;
    public InventorySlot statSlot;
    
    public RectTransform scrollRectTransform;
    public UITweenPosition scrollRectTween;
    public GridLayoutGroup gridLayoutGroup;
    public TextMeshProUGUI pageText;
    
    public int CurActiveSlotCount { get; private set; }
    public SpyingSlot CurSlot { get; private set; }
    public int CurSlotIndex { get; private set; }
    
    public bool isAddMode { get; private set; }
    
    private List<InventorySlot> inventoryPieceList = new List<InventorySlot>();
    private PlayerInstalledPuzzleData[] installedArray = new PlayerInstalledPuzzleData[6];
    
    private List<PlayerSlotData> playerSlotDataList = new List<PlayerSlotData>();
    private List<PlayerSlotData> enemySlotDataList = new List<PlayerSlotData>();
    
    private SpyingPosSlot[] playerEditPosSlot;
    private SpyingPosSlot[] enemyEditPosSlot;
    
    private SpyingPosSlot curPlayerClickEditPosSlot;
    private SpyingPosSlot curEnemyClickEditPosSlot;

    private int[] playerPosIndexArray = new int[] {7, 1, 9};
    private int[] enemyPosIndexArray = new int[] {4, 2, 10};
    
    private bool isShowUsing;
    
    private bool isTweenScroll = false;
    private int curPageCount = 1;
    private int pageTotalCount = 1;
    
    private Transform pieceRoot;
    private PuzzleType curListType;

    private const int SELL_SIZE_Y = 110;
    private string depth;

    public void Awake()
    {
        playerEditPosSlot = playerPosSlotEditRoot.GetComponentsInChildren<SpyingPosSlot>();
        enemyEditPosSlot = enemyPosSlotEditRoot.GetComponentsInChildren<SpyingPosSlot>();
    }

    public void Start()
    {
        curListType = PuzzleType.None;
        
        var x = scrollRectTransform.rect.width - (gridLayoutGroup.spacing.x * 2);
        gridLayoutGroup.cellSize = new Vector2(x, SELL_SIZE_Y);
        
        scrollRectTween.SetCallbackFinished(EndScrollRectTween);

        ClickRemoveModeButton();
        
        var puzzleDB = DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() as PuzzleDataBase;
        foreach (var data in puzzleDB.GetDataList())
        {
            var puzzleData = data as PuzzleData;
            if (puzzleData.Type == 0)
            {
                continue;
            }

            //어보미네이션을 위한 임시 값
            if (puzzleData.Shape == 0)
            {
                puzzleData.Shape = 1;
                puzzleData.PieceImageName = "EnhancePiece_Acceleration_Icon";
            }

            AddPiece(puzzleData, new PlayerPuzzleData(puzzleData.ID, (int)puzzleData.Type));
        }

        ShowList();
    }
    
    public void ShowList()
    {
        foreach (var slot in inventoryPieceList)
        {
            //피스 장착이던 재조합이던 어디선가 사용중이면 제외 
            if (!slot.IsUsing)
            {
                CheckSlotType(slot);
            }
        }

        curPageCount = 1;
        scrollRectTransform.anchoredPosition = Vector2.zero;
        
        UpdatePageCount();
    }
    
    public void AddPiece(PuzzleData puzzleData, PlayerPuzzleData playerPuzzleData, bool isInstalled = false)
    {
        if (puzzleData == null)
        {
            DebugX.LogError("PuzzleData is NULL");
            return;
        }

        if (puzzleData.CharacterData == null)
        {
            DebugX.LogError("ID : " + puzzleData.ID + " PuzzleData.CharacterData is NULL");
            return;
        }

        var pieceData = PieceData.GetNewPieceData(puzzleData, playerPuzzleData);
        var pieceControl = PieceLoader.Instance.GetPieceShapeObj(puzzleData.Shape);
        var pieceImage = AtlasManager.Instance.GetIconSprite(puzzleData.PieceImageName); 
        var pieceSprite = PieceLoader.Instance.GetPieceSprite(puzzleData);
        var pieceIcon = PieceLoader.Instance.GetPieceShapeSprite(puzzleData.Shape);

        InventorySlot prefabObj = null;
        switch (puzzleData.Type)
        {
            case PuzzleType.Character:
                prefabObj = characterSlot;
                break;
            case PuzzleType.Ability:
                prefabObj = abilitySlot;
                break;
            case PuzzleType.Stat:
                prefabObj = statSlot;
                break;
        }

        if (prefabObj == null)
            return;
        
        var piece = Instantiate(prefabObj, slotRoot);
        piece.Init(pieceData, pieceControl, 
            pieceImage, pieceSprite, 
            pieceIcon, depth, 
            ClickSlotEvent, null,
            null, null, 
            null, null, 
            null, null, 
            CheckSlotActive, null, null);

        inventoryPieceList.Add(piece);
    }

    public void ShowAllList()
    {
        curListType = PuzzleType.None;
        ShowList();
    }

    public void ShowCharacterList()
    {
        curListType = PuzzleType.Character;
        ShowList();
    }

    public void ShowAbilityList()
    {
        curListType = PuzzleType.Ability;
        ShowList();
    }

    public void ShowStatList()
    {
        curListType = PuzzleType.Stat;
        ShowList();
    }
    
    private void InitSpyingPosSlot(SpyingPosSlot[] slotArray, IEnumerable<SpyingSlotData> slotDataList, Action<SpyingPosSlot> clickPosSlot)
    {
        for (var i = 0; i < slotArray.Length; i++)
        {
            var slot = slotArray[i];
            slot.Init(i, clickPosSlot);
        }

        foreach (var slotData in slotDataList)
        {
            var posIdx = slotData.GetPosIndex();
            slotArray[posIdx].SetSlotData(slotData);
        }
    }
    
    private void ClickSlotEvent(InventorySlot slot)
    {
        if (slot == null)
            return;

        if (CurSlot == null)
            return;
        
        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;

        var pieceData = slot.pieceData;
        var puzzleData = pieceData.PuzzleData; 
        
        if (isAddMode)
        {
            if (puzzleData.Type == PuzzleType.Character)
            {
                var newData = new PlayerPuzzleData(puzzleData.ID, (int)puzzleData.Type);
                var newInstalledData = new PlayerInstalledPuzzleData(newData.uid, Vector3.zero);
                
                //새로운 데이터로 변경
                installedArray[CurSlotIndex] = newInstalledData;
                
                var puzzleList = playerDB.Data.GetPlayerPuzzleData();
                puzzleList.Add(newData);
                
                var installedList = playerDB.Data.GetPlayerInstalledPuzzleData();
                installedList.Add(newInstalledData);
                
                playerDB.Data.SetPlayerPuzzleData(puzzleList);
                playerDB.Data.SetPlayerInstalledPuzzleData(installedList);
                
                CurSlot.Init(newData.uid, newData.puzzleID, null);
            }
            else
            {
                if (installedArray[CurSlotIndex] == null)
                    return;
                
                var newData = new PlayerPuzzleData(puzzleData.ID, (int)puzzleData.Type);
                var newInstalledData = new PlayerInstalledPuzzleData(newData.uid, Vector3.zero);
                
                var puzzleList = playerDB.Data.GetPlayerPuzzleData();
                puzzleList.Add(newData);
                
                var installedList = playerDB.Data.GetPlayerInstalledPuzzleData();
                installedList.Add(newInstalledData);
                
                var installedCharacterOrigin = installedList.Find(v => v.uid == installedArray[CurSlotIndex].uid);
                
                var appliedPuzzleUidList = installedCharacterOrigin.GetAppliedPuzzleUidList();
                appliedPuzzleUidList.Add(newData.uid);
                installedCharacterOrigin.SetAppliedPuzzleUidList(appliedPuzzleUidList);

                playerDB.Data.SetPlayerInstalledPuzzleData(installedList);
                playerDB.Data.SetPlayerPuzzleData(puzzleList);

                installedArray[CurSlotIndex] = installedCharacterOrigin;
                
                var character=  puzzleList.Find(v => v.uid == installedArray[CurSlotIndex].uid);
                CurSlot.Init(installedArray[CurSlotIndex].uid, character.puzzleID, installedArray[CurSlotIndex].GetAppliedPuzzleUidList().ToArray());
            }
        }
        else
        {
            if (puzzleData.Type == PuzzleType.Character)
            {
                if (installedArray[CurSlotIndex] == null)
                    return;
            
                var puzzleList = playerDB.Data.GetPlayerPuzzleData();
                var findPuzzleData = puzzleList.Find(v => v.uid == installedArray[CurSlotIndex].uid);
            
                var installedList = playerDB.Data.GetPlayerInstalledPuzzleData();
                var findInstalledData = installedList.Find(v => v.uid == findPuzzleData.uid);
            
                installedList.Remove(findInstalledData);
                puzzleList.Remove(findPuzzleData);

                playerDB.Data.SetPlayerPuzzleData(puzzleList);
                playerDB.Data.SetPlayerInstalledPuzzleData(installedList);
                
                installedArray[CurSlotIndex] = null;
                CurSlot.Init(0, 0, null);
            }
            else
            {
                if (installedArray[CurSlotIndex] == null)
                    return;
            
                var puzzleList = playerDB.Data.GetPlayerPuzzleData();
                var findPuzzleData = puzzleList.Find(v => v.uid == installedArray[CurSlotIndex].uid);

                var installedList = playerDB.Data.GetPlayerInstalledPuzzleData();
                var findInstalledData = installedList.Find(v => v.uid == findPuzzleData.uid);

                var appliedPuzzleUidList = findInstalledData.GetAppliedPuzzleUidList();

                PlayerPuzzleData removeData = null;
                foreach (var appliedUid in appliedPuzzleUidList)
                {
                    var findPlayerData2 = puzzleList.Find(v => v.uid == appliedUid);
                    var puzzleID = findPlayerData2.puzzleID;
                    
                    //동일한 ID의 데이터를 찾음
                    if (puzzleID == puzzleData.ID)
                    {
                        removeData = findPlayerData2; 
                        break;
                    }
                }

                if (removeData == null)
                    return;

                var findUid = appliedPuzzleUidList.Find(v => v == removeData.uid);
                appliedPuzzleUidList.Remove(findUid);
                findInstalledData.SetAppliedPuzzleUidList(appliedPuzzleUidList);

                var findInstall = installedList.Find(v => v.uid == removeData.uid);
                installedList.Remove(findInstall);
                playerDB.Data.SetPlayerInstalledPuzzleData(installedList);

                var findD = puzzleList.Find(v => v.uid == removeData.uid);
                puzzleList.Remove(findD);
                playerDB.Data.SetPlayerPuzzleData(puzzleList);
                
                installedArray[CurSlotIndex] = installedList.Find(v => v.uid ==  installedArray[CurSlotIndex].uid);

                var character=  puzzleList.Find(v => v.uid == installedArray[CurSlotIndex].uid);
                CurSlot.Init(installedArray[CurSlotIndex].uid, character.puzzleID, installedArray[CurSlotIndex].GetAppliedPuzzleUidList().ToArray());
            }
        }

        RefreshPosSlotData();
    }
    
    public void ClickSpyingSlot(SpyingSlot slot)
    {
        if (CurSlot != null)
        {
            var back = CurSlot.transform.GetChild(0).gameObject;
            back.SetActive(false);
        }
        
        var selectedBack = slot.transform.GetChild(0).gameObject;
        selectedBack.SetActive(true);

        CurSlot = slot;
        CurSlotIndex = int.Parse(selectedBack.name);
    }

    public void ClickStartButton()
    {
#if UNITY_EDITOR
        
        startButton.SetActive(false);
        stopButton.SetActive(true);
        
        ClickPlayModeButton();
        
        var array = installedArray;
        var playerTeam = new PlayerInstalledPuzzleData[3];
        var enemyTeam = new PlayerInstalledPuzzleData[3];
        
        Array.Copy(array, playerTeam, 3);
        Array.Copy(array, 3, enemyTeam, 0, 3);
        
        GameField.Instance.CreateTestCharacter(Character.CharacterTeam.Player, playerTeam.ToList(), playerPosIndexArray);
        GameField.Instance.CreateTestCharacter(Character.CharacterTeam.Enemy, enemyTeam.ToList(), enemyPosIndexArray);
        
        var battleLayer = LayerManager.Instance.GetCurrentLayer() as BattleLayer;
        battleLayer.TestInit();

        GameField.Instance.BattleStart();
#endif
    }

    public void ClickStopButton()
    {
        startButton.SetActive(true);
        stopButton.SetActive(false);
        
        GameField.Instance.DestroyObject();
        
        var battleResultUI = PopupManager.Instance.GetOpenedPopup(PopupName.BattleResultPopup) as BattleResultPopup;
        if (battleResultUI != null)
            PopupManager.Instance.HidePopup(battleResultUI);
        
        var topUI = PopupManager.Instance.GetOpenedPopup(PopupName.TopUI) as TopUI;
        if (topUI != null)
            PopupManager.Instance.HidePopup(topUI);
    }

    private void ClickPlayerEditPosSlot(SpyingPosSlot clickSlot)
    {
        if (curPlayerClickEditPosSlot != null)
        {
            var clickPosSlotData = clickSlot.SpyingSlotData;
            var curPosSlotData = curPlayerClickEditPosSlot.SpyingSlotData;
            
            clickSlot.ChangeSlot(curPosSlotData);
            curPlayerClickEditPosSlot.ChangeSlot(clickPosSlotData);

            foreach (var slot in playerEditPosSlot)
            {
                slot.SelectIconTween(false);
            }
            
            this.curPlayerClickEditPosSlot = null;
        }
        else
        {
            if (clickSlot.SpyingSlotData == null)
                return;
            
            foreach (var slot in playerEditPosSlot)
            {
                if (slot == clickSlot)
                    continue;
                
                slot.SelectIconTween(true);
            }
            
            this.curPlayerClickEditPosSlot = clickSlot;
        }
    }
    
    private void ClickEnemyEditPosSlot(SpyingPosSlot clickSlot)
    {
        if (curEnemyClickEditPosSlot != null)
        {
            var clickPosSlotData = clickSlot.SpyingSlotData;
            var curPosSlotData = curEnemyClickEditPosSlot.SpyingSlotData;
            
            clickSlot.ChangeSlot(curPosSlotData);
            curEnemyClickEditPosSlot.ChangeSlot(clickPosSlotData);

            foreach (var slot in enemyEditPosSlot)
            {
                slot.SelectIconTween(false);
            }
            
            this.curEnemyClickEditPosSlot = null;
        }
        else
        {
            if (clickSlot.SpyingSlotData == null)
                return;
            
            foreach (var slot in enemyEditPosSlot)
            {
                if (slot == clickSlot)
                    continue;
                
                slot.SelectIconTween(true);
            }
            
            this.curEnemyClickEditPosSlot = clickSlot;
        }
    }

    private void RefreshPosSlotData()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        playerSlotDataList.Clear();
        enemySlotDataList.Clear();
        
        var array = installedArray;
        var playerTeam = new PlayerInstalledPuzzleData[3];
        var enemyTeam = new PlayerInstalledPuzzleData[3];
        
        Array.Copy(array, playerTeam, 3);
        Array.Copy(array, 3, enemyTeam, 0, 3);

        var playerPuzzleList = playerDB.Data.GetPlayerPuzzleData();
        
        {
            var newIndex = 0;
            var posIndexList = playerPosIndexArray.ToList();
            
            //최대 장착 가능한 캐릭터 수 만큼 반복
            while (posIndexList.Count < (int)basicDB.CharacterUseMaxCount())
            {
                if (!posIndexList.Contains(newIndex))
                {
                    //비어있는 인덱스 찾아서 추가
                    posIndexList.Add(newIndex);
                }
                newIndex++;
            }
            
            var posIndexQueue = new Queue<int>();
            foreach (var posIndex in posIndexList)
            {
                posIndexQueue.Enqueue(posIndex);
            }

            for (int i = 0; i < playerTeam.Length; i++)
            {
                var data = playerTeam[i];
                
                if (data == null)
                    continue;

                var findData = playerPuzzleList.Find(v => v.uid == data.uid);
                if (findData == null)
                    continue;

                if (findData.puzzleType == (int) PuzzleType.Character)
                {
                    var uid = data.uid;

                    var puzzleId = findData.puzzleID;
                    var appliedPuzzleUidArray = data.appliedPuzzleUidArray;

                    playerSlotDataList.Add(new PlayerSlotData(uid, puzzleId, appliedPuzzleUidArray, posIndexQueue.Dequeue()));
                }
            }
        }

        {
            var newIndex = 0;
            var posIndexList = enemyPosIndexArray.ToList();
            
            //최대 장착 가능한 캐릭터 수 만큼 반복
            while (posIndexList.Count < (int)basicDB.CharacterUseMaxCount())
            {
                if (!posIndexList.Contains(newIndex))
                {
                    //비어있는 인덱스 찾아서 추가
                    posIndexList.Add(newIndex);
                }
                newIndex++;
            }
            
            var posIndexQueue = new Queue<int>();
            foreach (var posIndex in posIndexList)
            {
                posIndexQueue.Enqueue(posIndex);
            }

            for (int i = 0; i < enemyTeam.Length; i++)
            {
                var data = enemyTeam[i];
                
                if (data == null)
                    continue;
                
                var findData = playerPuzzleList.Find(v => v.uid == data.uid);
                if (findData == null)
                    continue;
                
                if (findData.puzzleType == (int) PuzzleType.Character)
                {
                    var uid = data.uid;

                    var puzzleId = findData.puzzleID;
                    var appliedPuzzleUidArray = data.appliedPuzzleUidArray;

                    enemySlotDataList.Add(new PlayerSlotData(uid, puzzleId, appliedPuzzleUidArray, posIndexQueue.Dequeue()));
                }
            }
        }
    }
    
    public void ClickPosEditButton()
    {
        posEditRoot.SetActive(true);   
        
        InitSpyingPosSlot(playerEditPosSlot, playerSlotDataList, ClickPlayerEditPosSlot);
        InitSpyingPosSlot(enemyEditPosSlot, enemySlotDataList, ClickEnemyEditPosSlot);
    }
    
    public void ClickPosEditCloseButton()
    {
        posEditRoot.SetActive(false);

        curPlayerClickEditPosSlot = null;
        curEnemyClickEditPosSlot = null;
    }
    
    public void ClickPosEditSaveButton()
    {
        foreach (var slot in playerEditPosSlot)
        {
            slot.SpyingSlotData?.SetPosIndex(slot.Index);
        }

        playerPosIndexArray = playerSlotDataList
            .Select(slotData => slotData.posIndex)
            .ToArray();
        
        foreach (var slot in enemyEditPosSlot)
        {
            slot.SpyingSlotData?.SetPosIndex(slot.Index);
        }

        enemyPosIndexArray = enemySlotDataList
            .Select(slotData => slotData.posIndex)
            .ToArray();

        ClickPosEditCloseButton();
    }
    
    public void ClickAddModeButton()
    {
        addButton.SetActive(false);
        removeButton.SetActive(true);

        isAddMode = false;
    }
    
    public void ClickRemoveModeButton()
    {
        addButton.SetActive(true);
        removeButton.SetActive(false);
        
        isAddMode = true;
    }
    
    public void ClickPlayModeButton()
    {
        root.SetActive(false);
        playButton.SetActive(false);
        editButton.SetActive(true);
    }
    
    public void ClickEditModeButton()
    {
        root.SetActive(true);
        playButton.SetActive(true);
        editButton.SetActive(false);
    }
    
    public void OnClickAllButton()
    {
        allButton.gameObject.SetActive(false);
        characterButton.gameObject.SetActive(true);
        abilityButton.gameObject.SetActive(false);
        statButton.gameObject.SetActive(false);
        
        ShowCharacterList();
    }
    
    public void OnClickCharacterButton()
    {
        allButton.gameObject.SetActive(false);
        characterButton.gameObject.SetActive(false);
        abilityButton.gameObject.SetActive(true);
        statButton.gameObject.SetActive(false);
        
        ShowAbilityList();
    }
    
    public void OnClickEnhanceButton()
    {
        allButton.gameObject.SetActive(false);
        characterButton.gameObject.SetActive(false);
        abilityButton.gameObject.SetActive(false);
        statButton.gameObject.SetActive(true);
        
        ShowStatList();
    }
    
    public void OnClickStatButton()
    {
        allButton.gameObject.SetActive(true);
        characterButton.gameObject.SetActive(false);
        abilityButton.gameObject.SetActive(false);
        statButton.gameObject.SetActive(false);
        
        ShowAllList();
    }
    
    private bool CheckSlotType(InventorySlot slot)
    {
        var slotType = slot.pieceData.PuzzleData.Type;
        switch (curListType)
        {
            case PuzzleType.None:
                slot.gameObject.SetActive(true);
                return true;
            case PuzzleType.Character:
            case PuzzleType.Ability:
            case PuzzleType.Stat:
                slot.gameObject.SetActive(slotType == curListType);
                return slotType == curListType;
        }

        return false;
    }
    
    private void CheckSlotActive(InventorySlot slot)
    {
        CheckSlotType(slot);
        UpdatePageCount();
    }
    
    private void EndScrollRectTween()
    {
        isTweenScroll = false;
        UpdatePageCount();
    }
    
    public void ClickRightArrow()
    {
        if (isTweenScroll)
            return;
        
        if (curPageCount >= pageTotalCount) 
            return;
        
        var x = gridLayoutGroup.cellSize.x;
        var spacing = gridLayoutGroup.spacing.x;
        
        var originPosX = scrollRectTransform.anchoredPosition.x;
        var newPosX = originPosX - (x + spacing);

        isTweenScroll = true;
        curPageCount++;
            
        scrollRectTween.from = scrollRectTransform.anchoredPosition;
        scrollRectTween.to = new Vector2(newPosX, 0);
        
        scrollRectTween.TweenForward();
    }
    
    public void ClickLeftArrow()
    {
        if (isTweenScroll)
            return;
        
        if (curPageCount <= 1) 
            return;
        
        var x = gridLayoutGroup.cellSize.x;
        var spacing = gridLayoutGroup.spacing.x;

        var originPosX = scrollRectTransform.anchoredPosition.x;
        var newPosX = originPosX + (x + spacing);

        isTweenScroll = true;
        curPageCount--;
            
        scrollRectTween.from = scrollRectTransform.anchoredPosition;
        scrollRectTween.to = new Vector2(newPosX, 0);
        scrollRectTween.TweenForward();
    }
    
    public void UpdatePageCount()
    {
        //현재 인벤토리에서 활성화 된 슬롯 수
        CurActiveSlotCount = inventoryPieceList
            .Where(slot => !slot.IsUsing)
            .Count(slot => slot.gameObject.activeSelf);

        var constraintCount = gridLayoutGroup.constraintCount;
        pageTotalCount = CurActiveSlotCount / constraintCount;

        var addOne = CurActiveSlotCount % constraintCount;
        if (addOne != 0)
        {
            pageTotalCount++;
        }

        if (pageTotalCount == 0)
        {
            curPageCount = 0;
        }
        else if (pageTotalCount > 0 && curPageCount == 0)
        {
            curPageCount++;
        }

        if (curPageCount > pageTotalCount)
        {
            ClickLeftArrow();
        }
    
        pageText.text = $"{curPageCount}/{pageTotalCount}";
    }
}
