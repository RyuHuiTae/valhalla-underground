public class BattleSceneStarter : MainSceneStarter
{
    protected override void LoadingEndEvent()
    {
        base.LoadingEndEvent();
        BattleTestUIOpen();
    }

    private void BattleTestUIOpen()
    {
        //테스트를 위해 퍼즐들 초기화 (저장은 안함)
        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;
        playerDB.Data.PuzzleListJson = string.Empty;
        playerDB.Data.StageListJson = string.Empty;
        playerDB.Data.InstalledPuzzleListJson = string.Empty;
        playerDB.Data.PuzzlePosIndexJson = string.Empty;
        playerDB.Data.Floor = 6;
        
        LayerManager.Instance.ChangeLayer(Layer.BattleLayer);
        PopupManager.Instance.GetPopup(PopupName.BattleScenePopup);
    }
}