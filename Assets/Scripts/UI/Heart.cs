using System;
using Coffee.UIExtensions;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class Heart : MonoBehaviour
{
    private UIDissolve uiDissolve;

    private void Awake()
    {
        uiDissolve = GetComponent<UIDissolve>();
    }

    public async void SetActiveHeart(bool isActive)
    {
        if (gameObject)
            gameObject.SetActive(true);
        
        if (isActive)
        {
            uiDissolve.reverse = true;
            uiDissolve.Play();
        }
        else
        {
            uiDissolve.reverse = false;
            uiDissolve.Play();
        }

        await UniTask.Delay(TimeSpan.FromSeconds(uiDissolve.duration));
        
        if (!this)
            return;
        
        gameObject.SetActive(isActive);
    }
}
