using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class BattleLayer : CommonLayer
{
    [SerializeField] private Image playerHpFillImage;
    [SerializeField] private Image enemyHpFillImage;

    [SerializeField] private Shake playerHpShake;
    [SerializeField] private Shake enemyHpShake;
    
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private Image suddenDeathImage;

    [SerializeField] private RectTransform playerIcons;
    [SerializeField] private RectTransform enemyIcons;

    [SerializeField] private Material enemyIconMaterial;

    private float _playerTotalMaxHp;
    private float _enemyTotalMaxHp;
    
    private GameField _gameField;

    public float TimerTime { get; private set; }
    public float StartTime { get; private set; }
    
    private void Awake()
    {
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDataBase))
            return;
        
        StartTime = basicDataBase.BattleLimitTime();
        
        _gameField = GameField.Instance;
        _gameField.UpdateHpEvent += UpdatePlayerHp;
        _gameField.UpdateHpEvent += UpdateEnemyHp;
        _gameField.UpdatePlayerTotalHpEvent += UpdatePlayerTotalHp;
        _gameField.UpdateEnemyTotalHpEvent += UpdateEnemyTotalHp;
        _gameField.CreateCharacterIconUi += CreateCharacterIconUi;
        
        var background = PopupManager.Instance.GetPopup(PopupName.BackgroundPopup, UIUtils.BACKGROUND) as BackgroundPopup;
        background.SetBattleBackground();

        if (Camera.main != null)
            Camera.main.transform.rotation = Quaternion.Euler(new Vector3(-20f, 0f, 0f));
    }

    public void Init()
    {
        timerText.text = StartTime.ToString("N2");
        suddenDeathImage.gameObject.SetActive(false);
        
        _gameField.CreatePlayerCharacter();
        _gameField.CreateEnemyCharacter();

        playerHpFillImage.ObserveEveryValueChanged(x => x.fillAmount).Subscribe(_ => playerHpShake.ShakeObject(1f));
        enemyHpFillImage.ObserveEveryValueChanged(x => x.fillAmount).Subscribe(_ => enemyHpShake.ShakeObject(1f));
        
        UpdatePlayerHp();
        UpdateEnemyHp();
    }

#if UNITY_EDITOR
    public void TestInit()
    {
        timerText.color = Color.white;
        timerText.text = StartTime.ToString("N2");
        timerText.gameObject.SetActive(true);
        
        suddenDeathImage.gameObject.SetActive(false);
        PopupManager.Instance.GetPopup(PopupName.BackgroundPopup).GetComponent<BackgroundPopup>().ResetImageColor();

        playerHpFillImage.ObserveEveryValueChanged(x => x.fillAmount).Subscribe(_ => playerHpShake.ShakeObject(1f));
        enemyHpFillImage.ObserveEveryValueChanged(x => x.fillAmount).Subscribe(_ => enemyHpShake.ShakeObject(1f));

        UpdatePlayerHp();
        UpdateEnemyHp();

        _gameField.IsTestBattle = true;
    }
#endif
    
    public async void BattleStart()
    {
        await StartBattleTimer();
        await _gameField.StartSuddenDeath();
    }

    private GameObject CreateCharacterIconUi(Character.CharacterTeam type, PuzzleData data)
    {
        const string path = "Prefabs/Character/CharacterIcon";
        var parent = type == Character.CharacterTeam.Player ? playerIcons : enemyIcons;
        var icon = Instantiate(VResources.Load<GameObject>(path), parent);

        var iconImage = icon.transform.Find("Icon").GetComponent<Image>();
        var grayScaleIcon = icon.transform.Find("GrayScaleIcon").GetComponent<Image>();
        
        iconImage.sprite = AtlasManager.Instance.GetIconSprite(data.IconName);
        grayScaleIcon.sprite = iconImage.sprite;

        if (type == Character.CharacterTeam.Enemy)
        {
            iconImage.transform.localScale = new Vector3(-1f, 1f, 1f);
            grayScaleIcon.transform.localScale = new Vector3(-1f, 1f, 1f);
            
            if (data.CharacterData.CharacterType != CharacterType.Abomination)
            {
                iconImage.material = enemyIconMaterial;
            }
        }

        return icon;
    }

    private async UniTask StartBattleTimer()
    {
        TimerTime = StartTime;
        while (TimerTime >= 0f)
        {
            const float waitTime = 0.01f;
            TimerTime -= waitTime;
            await UniTask.Delay(TimeSpan.FromSeconds(waitTime / Time.timeScale));
            timerText.text = TimerTime.ToString("N2");

            if (_gameField.IsBattleEnd)
                return;

            if (TimerTime < 10f)
            {
                timerText.color = Color.red;
            }
        }

        if (suddenDeathImage)
            suddenDeathImage.gameObject.SetActive(true);

        PopupManager.Instance.GetPopup(PopupName.BackgroundPopup).GetComponent<BackgroundPopup>().StartSuddenDeath();

        if (timerText)
            timerText.gameObject.SetActive(false);
    }

    private void UpdatePlayerHp()
    {
        if (!playerHpFillImage)
            return;

        playerHpFillImage.fillAmount = _gameField.GetPlayerTotalCurrentHp() / (float) _playerTotalMaxHp;
    }

    private void UpdateEnemyHp()
    {
        if (!enemyHpFillImage)
            return;

        enemyHpFillImage.fillAmount = _gameField.GetEnemyTotalCurrentHp() / (float) _enemyTotalMaxHp;
    }

    private void UpdatePlayerTotalHp(float totalMaxHp)
    {
        this._playerTotalMaxHp = totalMaxHp;
    }

    private void UpdateEnemyTotalHp(float totalMaxHp)
    {
        this._enemyTotalMaxHp = totalMaxHp;
    }
    
    private void OnDestroy()
    {
        if (Camera.main != null)
            Camera.main.transform.rotation = Quaternion.identity;

        _gameField.UpdateHpEvent -= UpdatePlayerHp;
        _gameField.UpdateHpEvent -= UpdateEnemyHp;
        _gameField.UpdatePlayerTotalHpEvent -= UpdatePlayerTotalHp;
        _gameField.UpdateEnemyTotalHpEvent -= UpdateEnemyTotalHp;
        _gameField.CreateCharacterIconUi -= CreateCharacterIconUi;

        if (GameField.Instance != null)
            GameField.Instance.DestroyObject();
    }
}