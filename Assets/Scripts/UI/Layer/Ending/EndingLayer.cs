using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class EndingLayer : CommonLayer
{
    public Animator[] modelAnimator; 
    
    public AnimationClip endingClip;
    public Animation endingAnimation;
    
    private bool isDataUpdateEnd = false;
    private bool isChangeLayer = false;
    
    private static readonly int AnimState = Animator.StringToHash("AnimState");
    
    private async void Start()
    {
        this.isDataUpdateEnd = false;
        if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
        {
            playerDB.ClearResetPlayerDataBase(() =>
            {
                isDataUpdateEnd = true;
                StageManager.Instance.ClearStageSlotDataList();
            });
        }

        foreach (var anim in modelAnimator)
        {
            anim.SetInteger(AnimState, 1);
        }
        
        endingAnimation.clip = endingClip;
        endingAnimation.Play();
        
        await WaitDataUpdate();
    }

    public void ClickSkipButton()
    {
        GotoNextLayer(3f);
    }
    
    public void StopWalk()
    {
        foreach (var anim in modelAnimator)
        {
            anim.SetInteger(AnimState, 0);
        }
    }
    
    private async UniTask WaitDataUpdate()
    {
        while (!isDataUpdateEnd || endingAnimation.isPlaying)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(1));

            if (endingAnimation == null)
                return;
        }

        GotoNextLayer(10f);
    }

    private void GotoNextLayer(float duration)
    {
        if (isChangeLayer)
            return;

        isChangeLayer = true;
        
        var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
        tween.Init(false, duration, () =>
        {
            PopupManager.Instance.HideAllPopup();
            LayerManager.Instance.ChangeLayer(Layer.IntroLayer);
            
            var curLayer = LayerManager.Instance.GetCurrentLayer() as IntroLayer;
            if (curLayer != null)
            {
                curLayer.Init(IntroType.Outtro);
            }
        });
    }
}
