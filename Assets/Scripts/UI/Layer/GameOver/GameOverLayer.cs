using System.Collections;
using UnityEngine;

public class GameOverLayer : CommonLayer
{
    [SerializeField] private TextTypingEffect main;
    [SerializeField] private TextTypingEffect sub;

    private bool isDataUpdateEnd = false;
    
    private void Start()
    {
        AudioManager.Instance.BgmSound(AudioManager.BgmType.GameOver);
        
        WaitTime();
        ShowMainText();

        this.isDataUpdateEnd = false;
        if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
        {
            playerDB.ResetPlayerDataBase(() =>
            {
                isDataUpdateEnd = true;
                StageManager.Instance.ClearStageSlotDataList();
            });
        }
    }

    private void ShowMainText()
    {
        var mainText = "You're <color=#FF0000FF>dead forever</color>\nIn Valhalla Underground";
        
        main.SetOnComplete(ShowSubText);
        StartCoroutine(main.TypeText(mainText));
    }
    
    private void ShowSubText()
    {
        var subText = "Please Restart New Champion";
        
        //sub.SetOnComplete(WaitTime);
        StartCoroutine(sub.TypeText(subText));
    }

    private void WaitTime()
    {
        StartCoroutine(WaitTimeCoroutine());
    }
    
    private IEnumerator WaitTimeCoroutine()
    {
        yield return new WaitForSeconds(5);

        while (!isDataUpdateEnd)
        {
            yield return null;
        }
        
        var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
        tween.Init(false, 5f, () =>
        {
            PopupManager.Instance.HideAllPopup();
            LayerManager.Instance.ChangeLayer(Layer.TitleLayer);
            
            var title = LayerManager.Instance.GetCurrentLayer() as TitleLayer;
            title.Init();
        });
    }
}
