using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class IntroLayer : CommonLayer
{
    [SerializeField] private TextTypingEffect main;

    private bool isChangeLayer;
    private Queue<GameIntroData> introDataQueue;
    private IntroType type;

    private bool isClick = true;
    private int curScene = 0;
    private string curText = string.Empty;
    
    public void Init(IntroType type)
    {
        if (!(DataBaseManager.Instance.GetDataBase<GameIntroDataBase>() is GameIntroDataBase introDB))
            return;
        
        AudioManager.Instance.BgmSound(AudioManager.BgmType.Intro);
        
        this.main.SetOnComplete(EndTextEffect);
        this.isChangeLayer = false;
        this.introDataQueue = new Queue<GameIntroData>();
        this.type = type;

        var findDataList = introDB.GetDataList()
            .Select(v => v as GameIntroData)
            .Where(v => v.Type == type)
            .ToList();

        foreach (var data in findDataList)
        {
            introDataQueue.Enqueue(data);
        }
        
        ShowTextEffect();
    }

    public void ClickScreen()
    {
        if (main.IsSkippable())
        {
            main.SkipTypeText();
        }

        isClick = true;
    }
    
    public void ClickSkipButton()
    {
        ChangeLayer();
    }

    private async void ShowTextEffect()
    {
        if (isChangeLayer)
            return;
        
        var nextData = introDataQueue.Dequeue();
        var text = nextData.IntroText + "\n\n";

        if (curScene != nextData.Scene)
        {
            await UniTask.WaitUntil(() => isClick);
            
            isClick = false;
            curText = string.Empty;
        }
        
        main.SetText(text, curText);

        curScene = nextData.Scene;
        curText += text;
    }

    private async void EndTextEffect()
    {
        if (isChangeLayer)
            return;
        
        await UniTask.Delay(TimeSpan.FromSeconds(1));

        if (introDataQueue.Count > 0)
        {
            isClick = false;
            ShowTextEffect();
        }
        else
        {
            await UniTask.Delay(TimeSpan.FromSeconds(1));
            
            ChangeLayer();
        }
    }

    private void ChangeLayer()
    {
        if (isChangeLayer)
            return;

        isChangeLayer = true;

        var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
        tween.Init(false, 2f, () =>
        {
            if (type == IntroType.Intro)
            {
                PopupManager.Instance.CreatePopup(PopupName.StarterDeckPopup, UIUtils.DEPTH_30);
                ExitLayer();
            }
            else if(type == IntroType.Outtro)
            {
                LayerManager.Instance.ChangeLayer(Layer.TitleLayer);
                
                var title = LayerManager.Instance.GetCurrentLayer() as TitleLayer;
                title.Init();
            }
        });
    }
}