using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectLayer : CommonLayer
{
    [SerializeField] private Transform playerRoot;
    [SerializeField] private UITweenPosition stageRootTween;
    [SerializeField] private Transform stageRoot;
    [SerializeField] private StageSlot stagePrefab;

    private GameObject model;
    private Animator modelAnimator;
    private static readonly int AnimState = Animator.StringToHash("AnimState");
    
    private readonly float POS_X = 100;
    private readonly float POS_Y = 25;

    private TopUI topUI;

    private readonly List<StageSlot> slotList = new List<StageSlot>();
    private StageSlot curStageSlot = null;
    //private IEnumerable<StageSlot> moveAbleSlots = null;
    
    private void Awake()
    {
        stageRootTween.SetCallbackFinished(SlotTweenEndEvent);
    }

    public void Init()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        AudioManager.Instance.BgmSound(AudioManager.BgmType.StageSelect);
        
        var puzzleId = playerDB.Data.StarterCharacterPuzzleID;
        var findPuzzleData = puzzleDB.GetDataList().Find(v => v.ID == puzzleId);
        if (findPuzzleData != null)
        {
            var puzzleData = findPuzzleData as PuzzleData;
            var modelPath = puzzleData.CharacterData.Path + "_UI";
            model = Instantiate(VResources.Load<GameObject>(modelPath), playerRoot);
            modelAnimator = model.GetComponent<Animator>();
            modelAnimator.SetInteger(AnimState, 0);
        }

        //목숨이 0 이하 라면
        if (playerDB.Heart <= 0)
        {
            //게임 오버 처리
            PopupManager.Instance.HideAllPopup();
            LayerManager.Instance.ChangeLayer(Layer.GameOverLayer);
            return;
        }
        
        //모든 최종 보스 격파
        if (playerDB.Data.FinalBossLevel > (int)basicDB.FinalBossMaxLevel())
        {
            PopupManager.Instance.HideAllPopup();
            LayerManager.Instance.ChangeLayer(Layer.EndingLayer);
            return;
        }

        //게임을 시작하고 처음으로 스테이지 선택 레이어에 들어왔는지
        var isCreateStage = StageManager.Instance.IsCreateStage;

        var stageList = StageManager.Instance.GetStageSlotDataList(null);
        foreach (var stage in stageList)
        {
            if (stage.data.StageType != StageType.None)
            {
                var newStageSlot = Instantiate(stagePrefab, stageRoot);
                newStageSlot.Init(stage, ClickSlotEvent);

                newStageSlot.transform.localPosition = new Vector3(
                    (newStageSlot.GetSizeDelta().x + POS_X) * (stage.data.StageStep1 - 1),
                    (newStageSlot.GetSizeDelta().y + POS_Y) * stage.data.StageStep2,
                    0);
                
                newStageSlot.gameObject.SetActive(true);
                newStageSlot.SetActiveButton(false);
                
                slotList.Add(newStageSlot);

                if (stage.isCurrentSlot)
                {
                    curStageSlot = newStageSlot;
                }
            }
        }

        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (!isCreateStage)
        {
            if (GameSetting.Instance.isMoveBoss)
            {
                playerDB.Data.SetFloor((int)basicDB.MaxFloor());
            
                var bossSlot = slotList.FirstOrDefault(v => v.SlotData.type == StageType.Boss);
                curStageSlot = bossSlot;
                StageManager.Instance.SetCurrentSlot(curStageSlot.SlotData.data.ID);
            }

            if (GameSetting.Instance.isMoveFinalBoss)
            {
                playerDB.Data.SetFloor((int)basicDB.MaxFloor());
            
                var bossSlot = slotList.FirstOrDefault(v => v.SlotData.type == StageType.Boss);
                curStageSlot = bossSlot;
            
                playerDB.Data.BarrierCount = 0;
                curStageSlot.SlotData.isSlotEventEnd = true;
            
                StageManager.Instance.SetCurrentSlot(curStageSlot.SlotData.data.ID);
            }
        }
#endif
        
        if (curStageSlot != null)
        {
            var pos = curStageSlot.transform.localPosition;
            stageRoot.localPosition = -pos;
            
            //현재 슬롯에서 이동가능한 슬롯들 트윈 시작
            TweenMoveAbleSlots(true);
            
            //더이상 선택하지 못하는 슬롯들 비활성화
            InActiveCannotChoiceSlot();

            //게임 시작 후 처음으로 스테이지 선택에 들어왔고
            //아직 이 슬롯의 이벤트가 실행되지 않았다면
            if (!isCreateStage &&
                !curStageSlot.SlotData.isSlotEventEnd)
            {
                EnterCurrentSlotStage(true);
            }

            //보스와의 전투가 끝났다면
            if (curStageSlot.SlotData.isSlotEventEnd &&
                curStageSlot.SlotData.type == StageType.Boss)
            {
                if (playerDB.Data.Floor < (int)basicDB.MaxFloor())
                {
                    PopupManager.Instance.HideAllPopup();
                    //다음 층으로 이동
                    StartCoroutine(WaitNextFloorCreate(null));
                    return;
                }
                else
                {
                    //아직 결계가 남아있는 경우
                    if (playerDB.Data.BarrierCount > 0)
                    {
                        PopupManager.Instance.HideAllPopup();
                        
                        //최종 보스 진입 전 결계 이벤트
                        var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
                        tween.Init(false, 2f, () =>
                        {
                            PopupManager.Instance.CreatePopup(PopupName.BarrierEventPopup);
                        });
                    }
                    //결계를 이미 부순 경우 바로 최종 보스 층 입장
                    else
                    {
                        //최종 보스 레벨 설정
                        playerDB.Data.SetFinalBossLevel(1);
            
                        //최심부 입장 처리
                        var layer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
                        StartCoroutine(layer.WaitNextFloorCreate(() =>
                        {
                            PopupManager.Instance.HideAllPopup();
                        }));
                    }
                }
            }
            else
            {
                var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
                tween.Init(true, 2f, () =>
                {
                    PopupManager.Instance.CheckTutorial(TutorialTextType.EnterStageSelect);
                });
            }
        }
        
        topUI?.Init(TopUI.SceneTitle.Stage);
    }

    public IEnumerator WaitNextFloorCreate(Action callback)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            yield break;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            yield break;
        
        var isCreateNewStageEnd = false;
        var isTweenEnd = false;
        
        playerDB.Data.SetNextFloor();
        StageManager.Instance.CreateNewStageSlotDataList(() =>
        {
            isCreateNewStageEnd = true;
        });

        var popupManager = PopupManager.Instance;
        var tween = popupManager.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
        tween.Init(false, 2f, () =>
        {
            isTweenEnd = true;
        });

        while (!isCreateNewStageEnd || !isTweenEnd)
        {
            yield return null;
        }

        var layerManager = LayerManager.Instance;
        layerManager.GetCurrentLayer().ExitLayer();
        layerManager.ChangeLayer(Layer.StageSelectLayer, UIUtils.DEPTH_10);
        callback?.Invoke();
        
        var newFloor = layerManager.GetCurrentLayer() as StageSelectLayer;
        if (newFloor != null)
        {
            newFloor.Init();
        }

        var nextFloorAnimationPopup = popupManager.CreatePopup(PopupName.NextFloorAnimationPopup, UIUtils.DEPTH_60)
            .GetComponent<NextFloorAnimationPopup>();
        if (playerDB.Data.Floor <= (int)basicDB.MaxFloor())
        {
            nextFloorAnimationPopup.PlayNextFloorAnimation("지하  층", playerDB.Data.Floor);
        }
        else
        {
            nextFloorAnimationPopup.PlayNextFloorAnimation("최심부");
        }
        
        AudioManager.Instance.EffectSound(AudioManager.EffectType.StageChangeWaterDrop,false, 0.3f);
    }

    public void TweenMoveAbleSlots(bool isTween)
    {
        if (curStageSlot == null)
            return;
        
        var moveAbleSlots = slotList
            .Where(v => 
                v.SlotData.data.ID == curStageSlot.SlotData.data.TopNode ||
                v.SlotData.data.ID == curStageSlot.SlotData.data.MidNode ||
                v.SlotData.data.ID == curStageSlot.SlotData.data.BotNode);
        
        foreach (StageSlot moveSlot in moveAbleSlots)
        {
            moveSlot.SetActiveButton(true);
            if (isTween)
            {
                moveSlot.StartTween();
            }
            else
            {
                moveSlot.StopTween();
            }
        }
    }

    public void InActiveCannotChoiceSlot()
    {
        if (curStageSlot == null)
            return;
        
        var step1 = curStageSlot.SlotData.data.StageStep1;
        var findSlots = slotList.Where(v => v.SlotData.data.StageStep1 <= step1 + 1);

        foreach (var slot in findSlots)
        {
            if (slot != curStageSlot &&
                slot.SlotData.data.ID != curStageSlot.SlotData.data.TopNode &&
                slot.SlotData.data.ID != curStageSlot.SlotData.data.MidNode &&
                slot.SlotData.data.ID != curStageSlot.SlotData.data.BotNode) 
            {
                slot.gameObject.SetActive(false);
            }
        }
    }
    
    private void ClickSlotEvent(StageSlot slot)
    {
        if (stageRootTween.IsPlay())
            return;

        if (curStageSlot != null &&
            curStageSlot.SlotData.data.TopNode != slot.SlotData.data.ID &&
            curStageSlot.SlotData.data.MidNode != slot.SlotData.data.ID &&
            curStageSlot.SlotData.data.BotNode != slot.SlotData.data.ID)
        {
            return;
        }

        TweenMoveAbleSlots(false);

        modelAnimator.SetInteger(AnimState, 1);
        AudioManager.Instance.EffectSound(AudioManager.EffectType.FootPrint,true, 0.3f);
            
        stageRootTween.from = stageRoot.localPosition;
        stageRootTween.to = -slot.transform.localPosition;

        stageRootTween.TweenForward();

        curStageSlot = slot;
        StageManager.Instance.SetCurrentSlot(curStageSlot.SlotData.data.ID);
    }

    private void SlotTweenEndEvent()
    {
        modelAnimator.SetInteger(AnimState, 0);
        
        if(GameObject.Find("FootPrint-Sound") != null)
            Destroy(GameObject.Find("FootPrint-Sound"));
        
        EnterCurrentSlotStage(false);
    }

    public void EnterCurrentSlotStage(bool isForce)
    {
        if (GameField.Instance == null)
            return;

        if (curStageSlot == null)
            return;

        var popupManager = PopupManager.Instance;
        
        topUI?.CloseDetailUI();

        var stageSlotType = curStageSlot.SlotData.type;
        switch (stageSlotType)
        {
            case StageType.NormalBattle:
            case StageType.EliteBattle:
            case StageType.Boss:
            case StageType.FinalBoss:
            {
                //에디터일때 치트 고려함
#if UNITY_EDITOR
                if (GameSetting.Instance.isIgnoreBattle)
                {
                    //스테이지 선택 레이어에서 선택 가능한 슬롯들 트윈 시작
                    TweenMoveAbleSlots(true);
                    //더이상 선택하지 못하는 슬롯들 비활성화
                    InActiveCannotChoiceSlot();
                    return;
                }
#endif
                var spyingPopup = popupManager.CreatePopup(PopupName.SpyingPopup) as SpyingPopup;
                spyingPopup?.Init(isForce);
            }
                break;
            case StageType.Event:
                var eventPopup = popupManager.CreatePopup(PopupName.EventPopup) as EventPopup;
                eventPopup.Init(isForce);
                break;
            case StageType.Rest:
                popupManager.CreatePopup(PopupName.RestPopup);
                break;
            case StageType.Shop:
                var shopPopup = popupManager.CreatePopup(PopupName.ShopPopup, UIUtils.DEPTH_30) as ShopPopup;
                shopPopup.Init(isForce);
                break;
        }
    }
}