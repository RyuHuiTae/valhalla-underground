using System;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class StageSlotData
{
    public StageData data;
    public StageType type;
    public bool isCurrentSlot;
    public bool isSlotEventEnd;
}

public class StageSlot : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI slotName;
    [SerializeField] private UIButton button;
    [SerializeField] private UITweenScale tweenScale;

    private bool isClicked = false;
    private Action<StageSlot> clickSlotEvent;

    public StageSlotData SlotData { get; private set; }
    
    public void Init(StageSlotData data, Action<StageSlot> clickSlotEvent)
    {
        this.SlotData = data;
        this.clickSlotEvent = clickSlotEvent;

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        var stageType = data.type;
        
        //최심부 이벤트인 경우
        if (playerDB.Data.Floor > (int) basicDB.MaxFloor() && 
            stageType == StageType.Event)
        {
            stageType = StageType.FinalBoss;
        }
        
        switch (stageType)
        {
            case StageType.Start:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_empty");
                slotName.transform.parent.gameObject.SetActive(false);
                break;
            case StageType.Boss:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_boss");
                slotName.text = "보스";
                break;
            case StageType.Event:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_event");
                slotName.text = "이벤트";
                break;
            case StageType.Rest:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_rest");
                slotName.text = "휴식";
                break;
            case StageType.Shop:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_store");
                slotName.text = "상점";
                break;
            case StageType.EliteBattle:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_strong_enemy");
                slotName.text = "강적 전투";
                break;
            case StageType.NormalBattle:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_enemy");
                slotName.text = "일반 전투";
                break;
            case StageType.FinalBoss:
                image.sprite = AtlasManager.Instance.GetUISprite("stage_last_boss");
                slotName.text = "???";
                break;
        }
        
        button.SetBaseSprite(image.sprite);
    }

    public Vector2 GetSizeDelta()
    {
        return rectTransform.sizeDelta;
    }

    public void ClickSlot()
    {
        if (isClicked)
            return;

        isClicked = true;
        clickSlotEvent?.Invoke(this);
    }

    public void StartTween()
    {
        tweenScale.TweenForward();
    }

    public void StopTween()
    {
        tweenScale.Stop();
    }

    public void SetActiveButton(bool isActive)
    {
        button.SetActiveButton(isActive);
    }
}