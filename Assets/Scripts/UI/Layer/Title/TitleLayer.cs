using System;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;

public class TitleLayer : CommonLayer
{
    [SerializeField] private TextMeshProUGUI loadingText;
    [SerializeField] private TextMeshProUGUI startText;
    [SerializeField] private TextMeshProUGUI versionText;
    [SerializeField] private UITweenAlpha startTween;
    [SerializeField] private GameObject startButton;

    private Action loadingEndEvent = null;
    
    public async void Init(Action loadingEndEvent = null)
    {
        this.loadingEndEvent = loadingEndEvent;
        this.versionText.text = $"ver {Application.version}";
        
        loadingText.gameObject.SetActive(true);
        startText.gameObject.SetActive(false);
        startButton.gameObject.SetActive(false);
        
        AudioManager.Instance.BgmSound(AudioManager.BgmType.Title);
        
        await WaitLoading();
    }

    private async UniTask WaitLoading()
    {
        var dot = string.Empty;
        var loading = "Loading";

        loadingText.text = loading;
        
        while (!DataBaseManager.Instance.IsLoadEnd)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(1));

            if (dot.Length > 3)
                dot = string.Empty;
            
            dot += ".";
            loadingText.text = loading + dot;
        }
        
        loadingText.gameObject.SetActive(false);
        startText.gameObject.SetActive(true);
        startButton.gameObject.SetActive(true);
        
        startTween.TweenForward();
        
        loadingEndEvent?.Invoke();
    }

    public void ClickNextButton()
    {
        if (!DataBaseManager.Instance.IsLoadEnd)
            return;

        var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
        tween.Init(false, 2f, OpenUI);
    }

    private void OpenUI()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        //에디터일때 치트 고려함
#if UNITY_EDITOR
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;
        
        if (GameSetting.Instance.isAddMoneySoul)
        {
            playerDB.SetMoney(99999);
            playerDB.SetSoul(99999);
        }
        
        if (GameSetting.Instance.isAddAllPiece)
        {
            var list = playerDB.Data.GetPlayerPuzzleData();

            foreach (var data in puzzleDB.GetDataList())
            {
                var puzzleData = data as PuzzleData;
                if (puzzleData.Shape == 0)
                {
                    continue;
                }

                var newData = new PlayerPuzzleData(puzzleData.ID, (int) puzzleData.Type);
                list.Add(newData);
            }

            playerDB.Data.SetPlayerPuzzleData(list);
        }
#endif
        
        if (!playerDB.Data.IsShowIntro)
        {
            LayerManager.Instance.ChangeLayer(Layer.IntroLayer);
            
            var curLayer = LayerManager.Instance.GetCurrentLayer() as IntroLayer;
            if (curLayer != null)
            {
                curLayer.Init(IntroType.Intro);
            }
        }
        else
        {
            LayerManager.Instance.ChangeLayer(Layer.StageSelectLayer, UIUtils.DEPTH_10);

            var curLayer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
            if (curLayer != null)
            {
                curLayer.Init();
            }
        }
    }
}