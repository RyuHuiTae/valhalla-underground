using UnityEngine;
using UnityEngine.UI;

public class BackgroundPopup : CommonPopup
{
    [SerializeField] private Image image;
    
    public void SetBattleBackground()
    {
        image.sprite = AtlasManager.Instance.GetBackgroundSprite("BattleStage");
    }
    
    public void SetStageBackground()
    {
        image.sprite = AtlasManager.Instance.GetBackgroundSprite("Cave");
    }

    public Image GetImage()
    {
        return image;
    }

    public void StartSuddenDeath()
    {
        image.GetComponent<UITweenColor>().TweenForward();
    }
    
    public void ResetImageColor()
    {
        image.color = Color.white;
    }
}
