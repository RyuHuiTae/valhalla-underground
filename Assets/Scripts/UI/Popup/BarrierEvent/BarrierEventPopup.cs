using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BarrierEventPopup : CommonPopup
{
    [SerializeField] private TextTypingEffect text1;
    [SerializeField] private TextTypingEffect text2;

    [SerializeField] private Image text1Back;
    [SerializeField] private Image text2Back;
    
    [SerializeField] private UIButton button1;
    [SerializeField] private UIButton button2;
    [SerializeField] private TextMeshProUGUI button2Text;

    [SerializeField] private UITweenAlpha button1Tween;
    [SerializeField] private UITweenAlpha button2Tween;

    private string mainText;
    private string successText;
    private string failText;
    private bool isSuccess = false;
    private int totalNeedDays = 0;
    private int totalNeedMoney = 0;
    
    public void Start()
    {
        mainText =
            "지하로 내려가자 커다란 결계가 당신의 길을 가로막고 있습니다.\n\n" +
            "아무래도 이 너머가 <color=#FF0000FF>최심부</color>인 것 같군요.\n\n" +
            "괴물 발생 문제를 해결하기 위해선 결계를 부수고 앞으로 나아가야 합니다.";

        successText =
            "남은 돈은 <color=#FFFF00FF>{0}G</color>. 전부 식량으로 바꾸면 <color=#FFFF00FF>{1}</color>일 동안 버틸 수 있겠군요.\n\n" +
            "결계 앞에서 최대한 오래 버틴 끝에 완전히 부수는데 성공합니다.\n\n" +
            "당신은 임무를 완수하기 위해 결의를 다지고 앞으로 나아갑니다.";

        failText =
            "남은 돈은 <color=#FFFF00FF>{0}G</color>. 전부 식량으로 바꾸면 <color=#FFFF00FF>{1}</color>일 동안 버틸 수 있겠군요.\n\n" +
            "결계 앞에서 최대한 오래 버텼으나 역부족이었습니다.\n\n" +
            "완전히 부수려면  <color=#FF0000FF>{2}</color>일이 더 필요해 보이지만, 지금은 물러 설 땝니다.";
        
        text1Back.gameObject.SetActive(true);
        text2Back.gameObject.SetActive(false);
        
        button1.gameObject.SetActive(false);
        button2.gameObject.SetActive(false);
        
        text1.SetOnComplete(MainTextEndEvent);
        StartCoroutine(text1.TypeText(mainText));
        
    }

    private void MainTextEndEvent()
    {
        //버튼 활성화 처리
        button1.gameObject.SetActive(true);
        //button1Tween.TweenForward();
    }

    private void ShowSubText()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;
        
        var money = playerDB.Money;
        var leftDays = playerDB.Data.BarrierCount;
        var totalBarrierCount = (int)basicDB.BarrierEventCount();
        
        var countDays = money / totalBarrierCount;
        
        if (countDays > leftDays)
            countDays = leftDays;
        
        totalNeedDays = leftDays - countDays;
        totalNeedMoney = countDays * totalBarrierCount;
        isSuccess = totalNeedDays <= 0;
        
        if (isSuccess)
        {
            button2Text.text = "최심부에 입장한다";
            
            var format = string.Format(successText, totalNeedMoney, countDays);
            text2.SetOnComplete(SubTextEndEvent);
            StartCoroutine(text2.TypeText(format));
        }
        else
        {
            button2Text.text = "발할라로 돌아간다";
            
            var format = string.Format(failText, totalNeedMoney, countDays, totalNeedDays);
            text2.SetOnComplete(SubTextEndEvent);
            StartCoroutine(text2.TypeText(format));
        }
    }
    
    private void SubTextEndEvent()
    {
        button2.gameObject.SetActive(true);
        //button2Tween.TweenForward();
    }
    
    public void ClickButton1Event()
    {
        GetComponent<Shake>().ShakeObject(10f);
        AudioManager.Instance.EffectSound(AudioManager.EffectType.BarrierBreaking,false,0.3f);

        button1.gameObject.SetActive(false);
        text2Back.gameObject.SetActive(true);
        
        ShowSubText();
    }

    public void ClickButton2Event()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        if (isSuccess)
        {
            //남은 베리어 없음
            playerDB.Data.SetBarrierCount(0);
            
            //비용 감소
            playerDB.SetMoney(-totalNeedMoney);
            
            //최종 보스 레벨 설정
            playerDB.Data.SetFinalBossLevel(1);
            
            //최심부 입장 처리
            var layer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
            StartCoroutine(layer.WaitNextFloorCreate(() =>
            {
                PopupManager.Instance.HideAllPopup();
            }));
        }
        else
        {
            //남은 일수 저장 후 소프트 리셋
            playerDB.Data.SetBarrierCount(totalNeedDays);
            playerDB.ResetPlayerDataBase(() =>
            {
                StageManager.Instance.ClearStageSlotDataList();
            });
                
            //타이틀 이동
            var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
            tween.Init(false, 4f, () =>
            {
                PopupManager.Instance.HideAllPopup();
                LayerManager.Instance.ChangeLayer(Layer.TitleLayer);
                
                var title = LayerManager.Instance.GetCurrentLayer() as TitleLayer;
                title.Init();
            });
        }
    }
}