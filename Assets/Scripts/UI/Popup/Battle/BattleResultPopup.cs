using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class BattleResultPopup : CommonPopup
{
    [SerializeField] private BattleResultSlot[] slotArray;

    [SerializeField] private GameObject playerWinIcon;
    [SerializeField] private GameObject playerLoseIcon;
    [SerializeField] private GameObject playerDrawIcon;
    [SerializeField] private TextTypingEffect narration;
    [SerializeField] private GameObject rewardRoot;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private TextMeshProUGUI soulText;
    [SerializeField] private TextMeshProUGUI pieceText;
    
    private TopUI topUI;
    private bool isClick = false;
    private Character.CharacterTeam winTeam;
    private StageType stageType;

    public void Init(List<Character> playerList, Character.CharacterTeam winTeam)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<RewardDataBase>() is RewardDataBase rewardDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;
        
        var playerStageDataList = playerDB.Data.GetPlayerStageData();
        
        var findData = playerStageDataList.Find(v => v.isCurrentSlot);
        if (findData == null)
        {
#if UNITY_EDITOR
            //테스트 전투인 경우
            if (GameField.Instance.IsTestBattle)
            {
                InitSlot(playerList);
            }
#endif
            return;
        }
        
        this.winTeam = winTeam;
        this.stageType = (StageType) findData?.stageDataType;
        this.rewardRoot.SetActive(false);
        
        ShowNarration();

        InitSlot(playerList);

        switch (winTeam)
        {
            case Character.CharacterTeam.None:
                playerDrawIcon.SetActive(true);
                break;
            case Character.CharacterTeam.Player:
                playerWinIcon.SetActive(true);
                break;
            case Character.CharacterTeam.Enemy:
                playerLoseIcon.SetActive(true);
                break;
        }

        if (winTeam == Character.CharacterTeam.Player)
        {
            var curFloor = playerDB.Data.Floor;
            
            var randomRewardDataList = DataBaseManager.Instance.GetDataList<RandomRewardDataBase>().ToList();
            var rewardData = rewardDB?.GetBattleResultRewardData();
            var battleType = RandomRewardData.BattleToStageType(stageType);
            
            //돈
            var findRandomReward1 = randomRewardDataList
                .Select(v => v as RandomRewardData)
                .Where(v => v.RandomRewardId == rewardData.RandomRewardId1)
                .Where(v=> v.BattleType == BattleType.None || v.BattleType == battleType)
                .FirstOrDefault(v => v.Floor == 0 || v.Floor == curFloor);

            //소울
            var findRandomReward2 = randomRewardDataList
                .Select(v => v as RandomRewardData)
                .Where(v => v.RandomRewardId == rewardData.RandomRewardId2)
                .Where(v=> v.BattleType == BattleType.None || v.BattleType == battleType)
                .FirstOrDefault(v => v.Floor == 0 || v.Floor == curFloor);

            var moneyAmount = Random.Range(findRandomReward1.MoneyMin, findRandomReward1.MoneyMax + 1);
            var soulAmount = Random.Range(findRandomReward2.SoulMin, findRandomReward2.SoulMax + 1);

            switch (stageType)
            {
                case StageType.EliteBattle:
                    moneyAmount += (int)basicDB.EliteBattleBonusMoney();
                    soulAmount += (int)basicDB.EliteBattleBonusSoul();
                    break;
                case StageType.Boss:
                    moneyAmount += (int)basicDB.BossBattleBonusMoney();
                    soulAmount += (int)basicDB.BossBattleBonusSoul();
                    break;
            }
            
            moneyText.text = moneyAmount + " G";
            soulText.text = soulAmount.ToString();

            var rewardCount = stageType switch
            {
                StageType.NormalBattle => (int) basicDB.NormalBattleRewardSelectCount(),
                StageType.EliteBattle => (int) basicDB.EliteBattleRewardSelectCount(),
                StageType.Boss => (int) basicDB.BossBattleRewardSelectCount(),
                _ => 0
            };
            pieceText.text = rewardCount.ToString();

            rewardRoot.SetActive(true);
            
            playerDB.SetMoney(moneyAmount);
            playerDB.SetSoul(soulAmount);
        }
    }

    private void InitSlot(List<Character> playerList)
    {
        var slotQueue = new Queue<BattleResultSlot>();
        foreach (var slot in slotArray)
        {
            slotQueue.Enqueue(slot);
        }

        foreach (var player in playerList)
        {
            var clone = player.gameObject.GetComponent<Clone>();
            if (clone != null)
            {
                continue;
            }

            var slot = slotQueue.Dequeue();
            slot.Init(player);
        }
    }
    
    protected override void EnterAnimEndEvent()
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        topUI.Init(TopUI.SceneTitle.Result);

        //최종보스를 이긴 경우
        // if (winTeam == Character.CharacterTeam.Player && stageType == StageType.FinalBoss)
        // {
        //     var heartLosePopup = PopupManager.Instance.GetPopup(PopupName.HeartLosePopup, UIUtils.DEPTH_50) as HeartLosePopup;
        //     heartLosePopup.Init();    
        // }
    }

    private void ShowNarration()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BattleResultNarrationDataBase>() is BattleResultNarrationDataBase
            narrationDB))
            return;

        var narrationBattleType = NarrationBattleType.Normal;
        var narrationResultType = NarrationResultType.Win;
        
        switch (stageType)
        {
            case StageType.NormalBattle:
                narrationBattleType = NarrationBattleType.Normal;
                break;
            case StageType.EliteBattle:
                narrationBattleType = NarrationBattleType.Elite;
                break;
            case StageType.Boss:
                narrationBattleType = NarrationBattleType.Boss;
                break;
            case StageType.FinalBoss:
                narrationBattleType = NarrationBattleType.FinalBoss;
                break;
        }

        switch (winTeam)
        {
            //전투 승리
            case Character.CharacterTeam.Player:
            {
                //최종 보스를 전부 클리어 한 경우
                if (stageType == StageType.FinalBoss &&
                    playerDB.Data.FinalBossLevel > (int) basicDB.FinalBossMaxLevel())
                {
                    narrationResultType = NarrationResultType.WinFinalBoss;
                }
                else
                {
                    narrationResultType = NarrationResultType.Win;
                }

                break;
            }
            //전투 패배
            case Character.CharacterTeam.Enemy:
                //아직 게임오버는 아닌 경우
                if (playerDB.Heart > 0)
                {
                    narrationResultType = NarrationResultType.Lose;
                }
                else
                {
                    narrationResultType = NarrationResultType.GameOver;
                }

                break;
        }

        var findNarrationList = narrationDB.GetDataList()
            .Select(v => v as BattleResultNarrationData)
            .Where(v => v.BattleType == narrationBattleType)
            .Where(v => v.ResultType == narrationResultType)
            .ToList();
        
        var randomIdx = Random.Range(0, findNarrationList.Count);
        var randomNarrationData = findNarrationList[randomIdx];
        
        var text = randomNarrationData.BattleResultNarration;
        StartCoroutine(narration.TypeText(text));
    }

    public void ClickArrow()
    {
        if (popupAnimation.isPlaying)
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        if (isClick)
            return;

        isClick = true;

        //승리한 경우
        if (winTeam == Character.CharacterTeam.Player)
        {
            var playerStageDataList = playerDB.Data.GetPlayerStageData();
            var findData = playerStageDataList.Find(v => v.isCurrentSlot);

            if (findData == null)
                return;

            //현재 슬롯이 최종 보스인 경우
            if (findData.stageDataType == (int) StageType.FinalBoss)
            {
                //모든 최종 보스 격파
                if (playerDB.Data.FinalBossLevel > (int) basicDB.FinalBossMaxLevel())
                {
                    GameField.Instance.DestroyObject();
                    
                    var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
                    tween.Init(false, 4f, () =>
                    {
                        PopupManager.Instance.HidePopup(this, false);
                        PopupManager.Instance.HideAllPopup();
                        LayerManager.Instance.ChangeLayer(Layer.EndingLayer);
                    });
                }
                //보스가 아직 남은 경우
                else
                {
                    PopupManager.Instance.HidePopup(this, false);
                    PopupManager.Instance.HideAllPopup();
                    GameField.Instance.ChangeLayerToStageSelect();

                    var curLayer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
                    if (curLayer != null)
                    {
                        curLayer.EnterCurrentSlotStage(false);
                    }
                }
            }
            //아닌 경우 보상 팝업 오픈
            else
            {
                var rewardUI = PopupManager.Instance.CreatePopup(PopupName.BattleRewardPopup, UIUtils.DEPTH_30) as BattleRewardPopup;
                rewardUI.Init((StageType)findData.stageDataType);

                PopupManager.Instance.HidePopup(this);
            }
        }
        //공격 실패로 재도전 혹은 패배 처리
        else
        {
            if (!StageManager.Instance.IsCreateStage)
                return;

            //재도전 처리
            if (playerDB.Heart > 0)
            {
                PopupManager.Instance.HideAllPopup();
                GameField.Instance.ChangeLayerToStageSelect();

                var curLayer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
                if (curLayer != null)
                {
                    curLayer.EnterCurrentSlotStage(false);
                }
            }
            //게임 오버
            else
            {
                DebugX.Log("게임 오버");
                GameField.Instance.DestroyObject();

                var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
                tween.Init(false, 2f, () =>
                {
                    PopupManager.Instance.HideAllPopup(false);
                    LayerManager.Instance.ChangeLayer(Layer.GameOverLayer);
                });
            }
        }
    }
    
    public override void OnClickCancelButton()
    {
        topUI?.ClickSettingButton();
    }
}