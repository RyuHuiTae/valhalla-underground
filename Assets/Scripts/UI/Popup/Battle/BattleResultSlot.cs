using TMPro;
using UnityEngine;

public class BattleResultSlot : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI damageText;
    [SerializeField] private TextMeshProUGUI receivedDamageText;
    [SerializeField] private TextMeshProUGUI healText;
    [SerializeField] private Transform modelRoot;
    
    private GameObject model;
    
    public void Init(Character character)
    {
        nameText.text = character.Data.Name;
        damageText.text = Mathf.Ceil(character.AttackDamageAmount).ToString();
        receivedDamageText.text = Mathf.Ceil(character.ReceivedDamageAmount).ToString();
        healText.text = Mathf.Ceil(character.HealAmount).ToString();
        
        var modelPath = character.Data.Path + "_UI";
        model = Instantiate(VResources.Load<GameObject>(modelPath), modelRoot);
    }
}
