using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UnityEngine;

public class BattleRewardPopup : CommonPopup
{
    [SerializeField] private BattleRewardSlot[] slotArray;
    [SerializeField] private TextMeshProUGUI reroll;
    [SerializeField] private TextMeshProUGUI narration;

    [SerializeField] private RectTransform cardMidPos;

    private const string NarrationText = "원하는 보상 카드를 {0}개 선택하세요";

    private TopUI topUI;
    private int rewardCount = 0;
    private BattleRewardSlot curClickSlot;
    private int[] randomRewardIdArray;

    private int rerollCost = int.MaxValue;
    private int rerollRaiseValue = int.MaxValue;

    private bool isPlayingAnim = false;
    private bool isPopupExitAnimEnd = false;

    private StageType stageType;
    
    private void Start()
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        topUI.Init(TopUI.SceneTitle.Result);
    }

    public void Init(StageType type)
    {
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<RewardDataBase>() is RewardDataBase rewardDB))
            return;
        
        this.stageType = type;
        
        var rewardData = rewardDB?.GetSelectPieceRewardData();
        if (rewardData == null)
        {
            DebugX.LogError("RewardDataBase 데이터 없음");
            return;
        }

        randomRewardIdArray = new[]
        {
            rewardData.RandomRewardId1,
            rewardData.RandomRewardId2,
            rewardData.RandomRewardId3,
        };
        
        rerollCost = (int) basicDB.RewardRerollCost();
        rerollRaiseValue = (int) basicDB.RewardRerollRaiseValue();
        
        if (reroll != null)
            reroll.text = $"{rerollCost}G";
        
        switch (type)
        {
            case StageType.NormalBattle:
                rewardCount = (int) basicDB.NormalBattleRewardSelectCount();
                break;
            case StageType.EliteBattle:
                rewardCount = (int) basicDB.EliteBattleRewardSelectCount();
                break;
            case StageType.Boss:
                rewardCount = (int) basicDB.BossBattleRewardSelectCount();
                break;
        }

        narration.text = string.Format(NarrationText, rewardCount);

        InitSlot();
    }

    private void InitSlot()
    {
        if (slotArray == null)
            return;

        var instance = DataBaseManager.Instance;

        var playerDataBase = instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;
        var randomRewardDataList = instance.GetDataList<RandomRewardDataBase>().ToList();
        var puzzleDataList = instance.GetDataList<PuzzleDataBase>().ToList();

        var randomList = new List<RandomRewardData>();
        
        for (var i = 0; i < slotArray.Length; i++)
        {
            //이미 선택됐으면 무시
            if (slotArray[i].IsClicked)
                continue;
            
            var curFloor = playerDataBase.Data.Floor;
            var battleType = RandomRewardData.BattleToStageType(stageType);
            
            //층 수를 고려한다. (0층이면 층수 상관 없음)
            var findRandomRewardList = randomRewardDataList
                .Select(v => v as RandomRewardData)
                .Where(v => v.RandomRewardId == randomRewardIdArray[i])
                .Where(v=> v.BattleType == BattleType.None || v.BattleType == battleType)
                .Where(v => v.Floor == 0 || v.Floor == curFloor)
                .ToList();

            bool isExist = false;
            RandomRewardData randomRewardData = null;
            
            do
            {
                var randomIdx = Random.Range(0, findRandomRewardList.Count);

                randomRewardData = findRandomRewardList[randomIdx];
                if (randomRewardData == null)
                {
                    DebugX.LogError("RandomRewardData 데이터 없음");
                    return;
                }

                isExist = randomList.Exists(v => v.ID == randomRewardData.ID);
                if (isExist)
                {
                    DebugX.LogWarning("RandomRewardData 중복으로 보상 재설정..");
                }

            } while (isExist);
            
            randomList.Add(randomRewardData);

            var findPuzzleData = puzzleDataList.Find(v => v.ID == randomRewardData.PieceDataId) as PuzzleData;
            var moneyAmount = Random.Range(randomRewardData.MoneyMin, randomRewardData.MoneyMax + 1);

            //초기화
            slotArray[i].Init(i, findPuzzleData, moneyAmount, popupAnimation, ClickSlot);
            //버튼 활성화
            slotArray[i].SetActiveButton(true);
        }
    }

    private async UniTask ClickAnimationProcess(BattleRewardSlot clickSlot)
    {
        isPlayingAnim = true;
        
        var animListSlot = new List<BattleRewardSlot>();

        foreach (var slot in slotArray)
        {
            if (slot.IsClicked)
                continue;

            if (slot.GetID() == clickSlot.GetID())
                continue;

            slot.DownAnimPlay();
            animListSlot.Add(slot);
        }

        clickSlot.TweenScaleForward();
        
        var playEndCount = 0;
        while (playEndCount < animListSlot.Count)
        {
            playEndCount = 0;
            foreach (var slot in animListSlot)
            {
                if (!slot.IsAnimPlaying())
                {
                    playEndCount++;
                }
            }

            await UniTask.WaitForEndOfFrame();
        }

        await clickSlot.SelectTweenStart(cardMidPos.anchoredPosition);

        //남은 보상 선택 횟수가 남았다면
        if (rewardCount > 0)
        {
            InitSlot();
            
            foreach (var slot in animListSlot)
            {
                slot.UpAnimPlay();
            }

            playEndCount = 0;
            while (playEndCount < animListSlot.Count)
            {
                playEndCount = 0;
                foreach (var slot in animListSlot)
                {
                    if (!slot.IsAnimPlaying())
                    {
                        playEndCount++;
                    }
                }

                await UniTask.WaitForEndOfFrame();
            }
        }
        
        isPlayingAnim = false;
    }

    private async void ClickSlot(BattleRewardSlot clickSlot)
    {
        this.curClickSlot = clickSlot;

        if (curClickSlot == null)
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var isDBUpdateEnd = false;

        //남은 보상 선택 횟수 감소
        rewardCount -= 1;

        //더이상 남은 보상 선택 횟수가 없으면
        if (rewardCount <= 0)
        {
            //DB업데이트
            playerDB.UpdatePlayerDataBase(() => { isDBUpdateEnd = true; });
        }

        await ClickAnimationProcess(clickSlot);

        var puzzleList = playerDB.Data.GetPlayerPuzzleData();

        //클릭한 슬롯 데이터
        var puzzleData = clickSlot.GetPuzzleData();

        //추가할 데이터 생성
        var newData = new PlayerPuzzleData(puzzleData.ID, (int) puzzleData.Type);
        puzzleList.Add(newData);

        //플레이어 데이터 변경
        //playerDB.Data.SetMoney(clickSlot.GetMoney());
        playerDB.Data.SetPlayerPuzzleData(puzzleList);

        //상단UI 갱신
        topUI.Refresh(TopUI.SceneTitle.Result);

        //더이상 남은 보상 선택 횟수가 없으면
        if (rewardCount <= 0)
        {
            //상단UI 닫음
            topUI.OnClickCancelButton();
            PopupManager.Instance.HidePopup(this);

            while (!isDBUpdateEnd || !isPopupExitAnimEnd)
            {
                await UniTask.WaitForEndOfFrame();
            }

            if (!StageManager.Instance.IsCreateStage)
                return;

            PopupManager.Instance.HideAllPopup();
            GameField.Instance.ChangeLayerToStageSelect();
        }
        else
        {
            narration.text = string.Format(NarrationText, rewardCount);
        }
    }

    protected override void ExitAnimEndEvent()
    {
        isPopupExitAnimEnd = true;
    }

    private async void ClickRerollButton()
    {
        if (popupAnimation.isPlaying)
            return;

        if (isPlayingAnim)
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
            return;

        if (playerDataBase?.Money < rerollCost)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "골드가 부족합니다.");
            return;
        }

        isPlayingAnim = true;
        
        playerDataBase.SetMoney(-rerollCost);
        SetRerollText();

        curClickSlot = null;

        await DissolveSlots(false);

        //slot 재설정
        InitSlot();
        topUI.Refresh(TopUI.SceneTitle.Result);

        await DissolveSlots(true);
        
        isPlayingAnim = false;
    }

    private async UniTask DissolveSlots(bool reverse)
    {
        foreach (var rewardSlot in slotArray)
        {
            if (rewardSlot.IsClicked)
                continue;
            
            rewardSlot.Dissolve(reverse);
        }

        //아직 선택되지 않은 슬롯을 하나 찾는다.
        var findSlot = slotArray.FirstOrDefault(v => !v.IsClicked);
        if (findSlot == null)
            return;
        
        await UniTask.WaitUntil(() => findSlot.IsPlayDissolve == false);
    }

    private void SetRerollText()
    {
        rerollCost *= rerollRaiseValue;

        if (reroll != null)
            reroll.text = $"{rerollCost}G";
    }

    public override void OnClickCancelButton()
    {
        topUI?.ClickSettingButton();
    }
}