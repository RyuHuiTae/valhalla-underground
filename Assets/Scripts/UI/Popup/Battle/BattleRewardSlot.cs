using System;
using System.Linq;
using Coffee.UIExtensions;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattleRewardSlot : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private GameObject characterPieceRoot;
    [SerializeField] private GameObject abilityPieceRoot;
    [SerializeField] private GameObject statPieceRoot;
    [SerializeField] private Image pieceIcon;
    [SerializeField] private TextMeshProUGUI pieceHp;
    [SerializeField] private TextMeshProUGUI pieceAttack;
    [SerializeField] private TextMeshProUGUI pieceDefence;
    [SerializeField] private TextMeshProUGUI pieceSpeed;
    [SerializeField] private TextMeshProUGUI abilityPieceDesc;
    [SerializeField] private TextMeshProUGUI statPieceDesc;
    [SerializeField] private UIButtonEvent button;

    [SerializeField] private PieceText characterText;
    [SerializeField] private PieceText abilityText;
    [SerializeField] private PieceText statText;
    
    [SerializeField] private UIShiny uiShiny;
    
    [SerializeField] private AnimationClip downAnimClip;
    [SerializeField] private AnimationClip upAnimClip;
    [SerializeField] private Animation slotAnimation;

    private UITweenPosition tweenPos;
    private UITweenRotation tweenRot;
    private UITweenScale tweenScale;
    private UIDissolve uiDissolve;
    
    private int id;
    private PuzzleData data;
    private int money;
    private Animation popupAnimation;
    private Action<BattleRewardSlot> slotClick;
    
    public bool IsClicked { get; private set; }

    private void Awake()
    {
        tweenPos = GetComponent<UITweenPosition>();
        tweenRot = GetComponent<UITweenRotation>();
        tweenScale = GetComponent<UITweenScale>();
        uiDissolve = GetComponent<UIDissolve>();
    }

    public void Init(int id, PuzzleData data, int money, Animation popupAnimation, Action<BattleRewardSlot> slotClick)
    {
        this.id = id;
        this.data = data;
        this.money = money;
        this.popupAnimation = popupAnimation;
        this.slotClick = slotClick;

        characterPieceRoot?.SetActive(false);
        abilityPieceRoot?.SetActive(false);
        statPieceRoot?.SetActive(false);
        
        switch (data.Type)
        {
            case PuzzleType.Character:
                CharacterPieceInit(data);
                break;
            case PuzzleType.Ability:
                AbilityPieceInit(data);
                break;
            case PuzzleType.Stat:
                StatPieceInit(data);
                //DebugX.LogError("스탯 피스 추가 작업 필요");
                break;
        }

        characterText.gameObject.SetActive(data.Type == PuzzleType.Character);
        abilityText.gameObject.SetActive(data.Type == PuzzleType.Ability);
        statText.gameObject.SetActive(data.Type == PuzzleType.Stat);
            
        gameObject.SetActive(true);
    }
    
    private void CharacterPieceInit(PuzzleData data)
    {
        var dataList = DataBaseManager.Instance.GetDataList<CharacterDataBase>().ToList();
        var findData = dataList.Find(v => v.ID == data.CharacterDataId) as CharacterData;

        if (findData == null)
        {
            DebugX.LogError("CharacterData 데이터 없음");
            return;
        }
        
        characterText.Piece_set(PieceData.GetNewPieceData(data));
        
        pieceIcon.sprite = AtlasManager.Instance.GetIconSprite(data.IconName);
        pieceHp.text = findData.Hp.ToString();

        pieceAttack.text = findData.Attack.ToString();
        pieceDefence.text = findData.Defense.ToString();
        pieceSpeed.text = findData.MoveSpeed.ToString();
        
        characterPieceRoot?.SetActive(true);
    }
    
    private void AbilityPieceInit(PuzzleData data)
    {
        var dataList = DataBaseManager.Instance.GetDataList<AbilityPieceDataBase>().ToList();
        var findData = dataList.Find(v => v.ID == data.EnhancePieceDataId) as AbilityPieceData;
        
        abilityText.Piece_set(PieceData.GetNewPieceData(data));
        
        pieceIcon.sprite = AtlasManager.Instance.GetIconSprite(data.IconName);
        abilityPieceDesc.text = findData.Desc;
        
        abilityPieceRoot?.SetActive(true);
    }
    
    private void StatPieceInit(PuzzleData data)
    {
        var dataList = DataBaseManager.Instance.GetDataList<StatPieceDataBase>().ToList();
        var findData = dataList.Find(v => v.ID == data.StatPieceDataId) as StatPieceData;
        
        statText.Piece_set(PieceData.GetNewPieceData(data));
        
        pieceIcon.sprite = AtlasManager.Instance.GetIconSprite(data.IconName);
        statPieceDesc.text = findData.Desc;
        
        statPieceRoot?.SetActive(true);
    }
    
    public void SetActiveButton(bool isActive)
    {
        button.SetEnable(isActive);
    }

     public void ClickSlotEvent()
     {
         if (popupAnimation.isPlaying)
             return;
         
         if (IsAnimPlaying())
             return;
        
        if (IsClicked)
            return;

        IsClicked = true;
        SetActiveButton(false);
        
        slotClick?.Invoke(this);
    }

     public void DownAnimPlay()
     {
         if (downAnimClip == null) 
             return;
         
         slotAnimation.clip = downAnimClip;
         slotAnimation.Play();
     }
     
     public void UpAnimPlay()
     {
         if (upAnimClip == null) 
             return;
         
         slotAnimation.clip = upAnimClip;
         slotAnimation.Play();
     }

     public bool IsAnimPlaying()
     {
         return slotAnimation.isPlaying || 
                tweenPos.IsPlay() || 
                //tweenRot.IsPlay() || 
                uiDissolve.isPlay;
     }

     public void TweenScaleForward()
     {
         tweenScale.ResetValue();
         tweenScale.duration = 0.3f;
         tweenScale.TweenForward();
     }
     
     public void TweenScaleReverse()
     {
         tweenScale.ResetValue();
         tweenScale.duration = 0.5f;
         tweenScale.TweenReverse();
     }
     
     public async UniTask SelectTweenStart(Vector3 midPos)
     {
         TweenScaleReverse();
         
         tweenPos.from = rectTransform.anchoredPosition;
         tweenPos.to = midPos;
         tweenPos.duration = 0.4f;
         tweenPos.TweenForward();
         await UniTask.WaitUntil(() => tweenPos.IsPlay() == false);
         
         await UniTask.Delay(TimeSpan.FromSeconds(0.1f));

         // const float secondDuration = 1.5f;
         // tweenRot.duration = secondDuration;
         // tweenRot.TweenForward();

         uiShiny.Play();
         await UniTask.Delay(TimeSpan.FromSeconds(1f));
         //await UniTask.WaitUntil(() => tweenRot.IsPlay() == false);

         Dissolve();
         
         await UniTask.WaitUntil(() => uiDissolve.isPlay == false);
         await PopupManager.Instance.GetPopup(PopupName.TopUI).GetComponent<TopUI>().PlayGetInventoryPuzzleEffect();
         
         gameObject.SetActive(false);
     }
     
    public int GetID()
    {
        return id;
    }

    public int GetMoney()
    {
        return money;
    }

    public PuzzleData GetPuzzleData()
    {
        return data;
    }

    public void Dissolve(bool reverse = false)
    {
        const float duration = 1f;
        uiDissolve.duration = duration;
        uiDissolve.reverse = reverse;
        uiDissolve.Play();
    }

    public bool IsPlayDissolve => uiDissolve.isPlay;
}
