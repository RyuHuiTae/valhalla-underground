using System;
using TMPro;
using UnityEngine;

public enum ConfirmPopupType
{
    Ok,
    YesNo
}

public class ConfirmPopup : CommonPopup
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI mainText;
    public GameObject okButton;
    public GameObject yesButton;
    public GameObject noButton;

    private ConfirmPopupType type;
    private Action yesCallback;

    public void Init(string title, string main, ConfirmPopupType type = ConfirmPopupType.Ok, Action yesCallback = null)
    {
        this.titleText.text = title;
        this.mainText.text = main;
        this.type = type;
        this.yesCallback = yesCallback;
        
        okButton.SetActive(type == ConfirmPopupType.Ok);
        yesButton.SetActive(type == ConfirmPopupType.YesNo);
        noButton.SetActive(type == ConfirmPopupType.YesNo);
    }

    public void ClickYesButton()
    {
        yesCallback?.Invoke();
        OnClickCancelButton();
    }

    public void ClickNoButton()
    {
        OnClickCancelButton();
    }
}
