using System;
using System.Linq;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class EnhanceConfirmPopup : ConfirmPopup
{
    public Image characterIcon;
    public TextMeshProUGUI moneyText;
    public GameObject moneyArea;

    public PieceText characterPieceText;
    public PieceText abilityPieceText;
    public PieceText statPieceText;
    
    public TextMeshProUGUI prevLevelText;
    public TextMeshProUGUI prevHpText;
    public TextMeshProUGUI prevAtkText;
    public TextMeshProUGUI prevDefText;

    public TextMeshProUGUI nextLevelText;
    public TextMeshProUGUI nextHpText;
    public TextMeshProUGUI nextAtkText;
    public TextMeshProUGUI nextDefText;

    public void Init(string title, string main, PieceData data, int price = 0,
        ConfirmPopupType type = ConfirmPopupType.Ok, Action yesCallback = null)
    {
        base.Init(title, main, type, yesCallback);

        characterIcon.sprite = AtlasManager.Instance.GetIconSprite(data.PuzzleData.IconName);
        
        var puzzleType = data.PuzzleData.Type;
        switch (puzzleType)
        {
            case PuzzleType.Character:
                characterPieceText.Piece_set(data);
                break;
            case PuzzleType.Ability:
                abilityPieceText.Piece_set(data);
                break;
            case PuzzleType.Stat:
                //DebugX.LogError("스탯 피스 추가 작업 필요");
                statPieceText.Piece_set(data);
                break;
        }

        characterPieceText.gameObject.SetActive(puzzleType == PuzzleType.Character);
        abilityPieceText.gameObject.SetActive(puzzleType == PuzzleType.Ability);
        statPieceText.gameObject.SetActive(puzzleType == PuzzleType.Stat);
        
        moneyText.text = price.ToString();
        moneyArea.SetActive(price > 0);

        var charData = data.PuzzleData.CharacterData;
        var puzzleList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>();

        //캐릭터의 다음 레벨 데이터 찾음
        var findNextCharacterData = puzzleList
            .Where(v => ((PuzzleData) v).CharacterData.CharacterType == charData.CharacterType)
            .FirstOrDefault(v => ((PuzzleData) v).CharacterData.Level == (charData.Level + 1));

        if (findNextCharacterData == null)
        {
            DebugX.LogError("findNextCharacterData is NULL");
            return;
        }

        prevLevelText.text = "Lv.";
        if (charData.Level / 10 < 1)
            prevLevelText.text += "0";
        prevLevelText.text += charData.Level;
        
        prevHpText.text = charData.Hp.ToString();
        prevAtkText.text = charData.Attack.ToString();
        prevDefText.text = charData.Defense.ToString();
        
        var nextData = findNextCharacterData as PuzzleData;
        var nextCharData = nextData.CharacterData;

        nextLevelText.text = "Lv.";
        if (nextCharData.Level / 10 < 1)
            nextLevelText.text += "0";
        nextLevelText.text += nextCharData.Level;

        nextHpText.text = nextCharData.Hp.ToString();
        nextAtkText.text = nextCharData.Attack.ToString();
        nextDefText.text = nextCharData.Defense.ToString();
    }
}