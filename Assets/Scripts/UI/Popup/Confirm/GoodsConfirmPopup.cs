using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum GoodsType
{
    None,
    Money,
    Heart,
    Soul,
}

public class GoodsConfirmPopup : ConfirmPopup
{
    public Image moneyIcon;
    public Image heartIcon;
    public Image soulIcon;

    public TextMeshProUGUI valueText;

    public void Init(string title, int value, GoodsType goodsType,
        ConfirmPopupType type = ConfirmPopupType.Ok, Action yesCallback = null)
    {
        var main = value > 0 ? "{0}을(를) 획득했습니다!" : "{0}이(가) 소실됐습니다!";

        main = goodsType switch
        {
            GoodsType.Heart => string.Format(main, "목숨"),
            GoodsType.Money => string.Format(main, "골드"),
            GoodsType.Soul => string.Format(main, "소울"),
            _ => main
        };

        base.Init(title, main, type, yesCallback);
        
        moneyIcon.gameObject.SetActive(goodsType == GoodsType.Money);
        heartIcon.gameObject.SetActive(goodsType == GoodsType.Heart);
        soulIcon.gameObject.SetActive(goodsType == GoodsType.Soul);
        
        valueText.text = value > 0 ? "+ " : "- ";
        valueText.text += Mathf.Abs(value).ToString();

        if (goodsType == GoodsType.Money)
            valueText.text += "G";
    }
}
