using System;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PieceConfirmPopup : ConfirmPopup
{
    public Image characterIcon;
    public TextMeshProUGUI moneyText;
    public TextMeshProUGUI soulText;
    public GameObject moneyArea;
    public GameObject soulArea;

    public PieceText characterPieceText;
    public PieceText enhancePieceText;
    public PieceText statPieceText;
    
    public void Init(string title, string main, PieceData data, int moneyPrice = -1, int soulPrice = -1,
        ConfirmPopupType type = ConfirmPopupType.Ok, Action yesCallback = null)
    {
        base.Init(title, main, type, yesCallback);

        characterIcon.sprite = AtlasManager.Instance.GetIconSprite(data.PuzzleData.IconName);
        
        var puzzleType = data.PuzzleData.Type;
        switch (puzzleType)
        {
            case PuzzleType.Character:
                characterPieceText.Piece_set(data);
                break;
            case PuzzleType.Ability:
                enhancePieceText.Piece_set(data);
                break;
            case PuzzleType.Stat:
                statPieceText.Piece_set(data);
                //DebugX.LogError("스탯 피스 추가 작업 필요");
                break;
        }

        characterPieceText.gameObject.SetActive(puzzleType == PuzzleType.Character);
        enhancePieceText.gameObject.SetActive(puzzleType == PuzzleType.Ability);
        statPieceText.gameObject.SetActive(puzzleType == PuzzleType.Stat);
            
        moneyText.text = moneyPrice + "G";
        moneyArea.SetActive(moneyPrice >= 0);

        soulText.text = soulPrice.ToString();
        soulArea.SetActive(soulPrice >= 0);
    }
}