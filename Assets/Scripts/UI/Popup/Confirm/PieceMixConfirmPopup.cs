using System;
using UnityEngine;
using UnityEngine.UI;

public class PieceMixConfirmPopup : ConfirmPopup
{
    public Image characterIcon1;
    public Image characterIcon2;

    public void Init(string title, string main, PuzzleData data1, PuzzleData data2,
        ConfirmPopupType type = ConfirmPopupType.Ok, Action yesCallback = null)
    {
        base.Init(title, main, type, yesCallback);
        
        characterIcon1.sprite = AtlasManager.Instance.GetIconSprite(data1.IconName);
        characterIcon2.sprite = AtlasManager.Instance.GetIconSprite(data2.IconName);
    }
}
