using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class EventPopup : CommonPopup
{
    [SerializeField] private Image eventImage;
    [SerializeField] private TextMeshProUGUI eventName;
    [SerializeField] private TextMeshProUGUI eventDesc;
    [SerializeField] private List<TextMeshProUGUI> eventChoicesText;
    [SerializeField] private List<UIButton> eventChoicesObj;
    [SerializeField] private TextMeshProUGUI ResultEventText;

    private TopUI topUI;

    private EventStageData resultEventStage;
    private EventStageChoiceData selectEventSlotData;
    private List<PlayerStageData> playerStageDataList;
    private PlayerStageData currentSlotData;

    private bool isClicked = false;

    public void Init(bool isForce)
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        AudioManager.Instance.BgmSound(AudioManager.BgmType.EventStage);

        var dataBaseManager = DataBaseManager.Instance;
        var dataList = dataBaseManager.GetDataList<EventStageDataBase>().ToList();

        if (!(dataBaseManager.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(dataBaseManager.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        if (!(dataBaseManager.GetDataBase<StageDataBase>() is StageDataBase stageDB))
            return;

        var stageDataList = stageDB.GetDataList().Select(v => v as StageData).ToList();
        
        //현재 스테이지에 해당되는 데이터
        playerStageDataList = playerDB.Data.GetPlayerStageData();
        //현재 슬롯을 찾는다
        currentSlotData = playerStageDataList.Find(v => v.isCurrentSlot);

        //세이브 데이터에 있는 이벤트 아이디를 사용해서 이벤트 발생시킴
        if (isForce)
        {
            var eventStageData = currentSlotData.GetPlayerEventStageData();
            var id = eventStageData.randomDataId;

            resultEventStage = dataList
                .Find(v => v.ID == id) as EventStageData;

            InitEventStage(resultEventStage);
        }
        //랜덤으로 이벤트 발생
        else
        {
            //튜토리얼인 경우
            if (playerDB.Data.Floor == (int) basicDB.TutorialFloor())
            {
                var findTutorialStageData = dataList
                    .Select(v => v as EventStageData)
                    .Where(v => v.Type == EventStageAppearType.Tutorial)
                    .ToList();
                
                var randomIndex = Random.Range(0, findTutorialStageData.Count);
                resultEventStage = findTutorialStageData.ElementAt(randomIndex);
                resultEventStage.IsUsedStage = true;
            }
            //최심부인 경우
            else if (playerDB.Data.Floor > (int) basicDB.MaxFloor())
            {
                var findStageData = stageDataList.Find(v => v.ID == currentSlotData.stageDataId);
                var seq = findStageData.StageStep1;
                
                var findFinalBossStageData = dataList
                    .Select(v => v as EventStageData)
                    .Where(v => v.Type == EventStageAppearType.FinalBoss)
                    .Where(v => v.Seq == seq)
                    .ToList();
                
                var randomIndex = Random.Range(0, findFinalBossStageData.Count);
                resultEventStage = findFinalBossStageData.ElementAt(randomIndex);
                resultEventStage.IsUsedStage = true;
            }
            //그 외
            else
            {
                //이벤트에서 사용할 리스트들
                var eventList = dataList
                    .Select(v => v as EventStageData)
                    .Where(v => v.Type == EventStageAppearType.None)
                    .ToList();
                
                //이미 사용된 리스트를 찾음
                var usedStageList = eventList
                    .Where(v => v.IsUsedStage)
                    .ToList();

                //다 한번씩 나왔다면
                if (usedStageList.Count >= eventList.Count)
                {
                    //전체 초기화
                    foreach (var data in eventList)
                    {
                        data.IsUsedStage = false;
                    }

                    DebugX.Log("이벤트 발생 플래그 리셋");
                }

                //사용 안된 이벤트를 찾음
                var dontUsedDataList = eventList
                    .Where(v => !(v.IsUsedStage))
                    .ToList();

                var randomIndex = Random.Range(0, dontUsedDataList.Count);
                resultEventStage = dontUsedDataList.ElementAt(randomIndex);
                resultEventStage.IsUsedStage = true;
            }
            
            InitEventStage(resultEventStage);

            //랜덤으로 선택된 이벤트 ID 저장
            currentSlotData.SetPlayerEventStageData(new PlayerEventStageData(resultEventStage.ID));
            //스테이지 데이터 갱신
            playerDB.Data.SetPlayerStageData(playerStageDataList);
            //디비 업데이트
            playerDB.UpdatePlayerDataBase(null);
        }
        
        //이벤트 스테이지가 닫힐때까지 플레이어 디비 업데이트 제한
        playerDB.SetUpdateFlag(false);
    }

    private void InitEventStage(EventStageData resultEventStage)
    {
        eventName.text = resultEventStage.Name;
        //eventDesc.text = resultEventStage.EventText;

        var effect = eventDesc.GetComponent<TextTypingEffect>();
        effect.SetText(resultEventStage.EventText);

        if (resultEventStage.EventImageName != "null")
        {
            eventImage.sprite = AtlasManager.Instance.GetUISprite(resultEventStage.EventImageName);
        }
        else eventImage.gameObject.SetActive(false);

        for (int i = 0; i < eventChoicesText.Count; i++)
        {
            var text = resultEventStage.EventChoices[i].EventChoiceText;
            if (text == "null" || text.Trim() == string.Empty)
            {
                eventChoicesText[i].transform.parent.gameObject.SetActive(false);
            }

            eventChoicesText[i].text = resultEventStage.EventChoices[i].EventChoiceText;

            //조건에 안맞는 경우 비활성화 처리
            if (!IsConditionCheck(resultEventStage.EventChoices[i]))
            {
                eventChoicesText[i].color = new Color(1, 1, 1, 0.5f);
                eventChoicesObj[i].SetEnable(false);
            }
        }
    }

    public void SelectEvent(int index)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
            return;

        selectEventSlotData = resultEventStage.EventChoices[index];
        ResultEventText.text = selectEventSlotData.EventChoiceChangeText;

        if (IsCondition(selectEventSlotData))
        {
            HideEventChoicesTextButton();
            //eventDesc.text = selectEventSlotData.EventChangeText;
            var effect = eventDesc.GetComponent<TextTypingEffect>();
            effect.SetText(selectEventSlotData.EventChangeText);
            GetRandomReward(selectEventSlotData.RandomRewardDataList);

            //상단UI 갱신
            topUI.Refresh(TopUI.SceneTitle.Stage);
        }
        else
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "조건을 만족하지 못해서 선택할 수 없습니다.");
        }
    }

    private bool IsConditionCheck(EventStageChoiceData eventStageChoiceData)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
            return false;

        var playerPuzzleData = playerDataBase.Data.GetPlayerPuzzleData();
        var isCondition = false;

        switch (eventStageChoiceData.ConditionType)
        {
            case ConditionType.None:
                isCondition = true;
                break;
            case ConditionType.LoseMoney:
                if (playerDataBase.Money >= eventStageChoiceData.ConditionMany)
                {
                    isCondition = true;
                }

                break;
            case ConditionType.LoseLife:
                if (playerDataBase.Heart > eventStageChoiceData.ConditionMany)
                {
                    isCondition = true;
                }

                break;
            case ConditionType.PollutionClear:
            {
                isCondition = true;
            }
                break;
            case ConditionType.LosePiece:
                if (playerPuzzleData.Count > eventStageChoiceData.ConditionMany)
                {
                    isCondition = true;
                }

                break;
            case ConditionType.LoseAbilityPiece:
                var abilityPieceList =
                    playerPuzzleData.Where(v => v.puzzleType == (int) PuzzleType.Ability).ToList();

                //강화 피스는 0개라도 상관 없음
                if (abilityPieceList.Count >= eventStageChoiceData.ConditionMany)
                {
                    isCondition = true;
                }

                break;
            case ConditionType.LoseCharacterPiece:
                var characterPieceList =
                    playerPuzzleData.Where(v => v.puzzleType == (int) PuzzleType.Character).ToList();

                //캐릭터 피스는 최소 1개를 남겨야 한다.
                if (characterPieceList.Count > eventStageChoiceData.ConditionMany)
                {
                    isCondition = true;
                }

                break;
            case ConditionType.LoseStatPiece:
                var statPieceList =
                    playerPuzzleData.Where(v => v.puzzleType == (int) PuzzleType.Stat).ToList();

                //강화 피스는 0개라도 상관 없음
                if (statPieceList.Count >= eventStageChoiceData.ConditionMany)
                {
                    isCondition = true;
                }
                break;
        }

        return isCondition;
    }

    private bool IsCondition(EventStageChoiceData eventStageChoiceData)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
            return false;

        var playerPuzzleData = playerDataBase.Data.GetPlayerPuzzleData();
        var playerInstalledPuzzleData = playerDataBase.Data.GetPlayerInstalledPuzzleData();
        var puzzleDataList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>().ToList();

        var isCondition = false;
        switch (eventStageChoiceData.ConditionType)
        {
            case ConditionType.None:
                isCondition = true;
                break;
            case ConditionType.LoseMoney:
                if (playerDataBase.Money >= eventStageChoiceData.ConditionMany)
                {
                    var realMoney = playerDataBase.Money - eventStageChoiceData.ConditionMany;
                    if (realMoney <= 0)
                        realMoney = playerDataBase.Money;
                    else realMoney = eventStageChoiceData.ConditionMany;

                    playerDataBase.SetMoney(-eventStageChoiceData.ConditionMany);
                    isCondition = true;

                    var confirm =
                        PopupManager.Instance.CreatePopup(PopupName.GoodsConfirmPopup, UIUtils.DEPTH_60) as
                            GoodsConfirmPopup;
                    confirm.Init("알림", -realMoney, GoodsType.Money);
                }

                break;
            case ConditionType.LoseLife:
                if (playerDataBase.Heart > eventStageChoiceData.ConditionMany)
                {
                    playerDataBase.SetHeart(-eventStageChoiceData.ConditionMany);
                    isCondition = true;

                    var confirm =
                        PopupManager.Instance.CreatePopup(PopupName.GoodsConfirmPopup, UIUtils.DEPTH_60) as
                            GoodsConfirmPopup;
                    confirm.Init("알림", -1, GoodsType.Heart);
                }

                break;
            case ConditionType.PollutionClear:
            {
                isCondition = true;
            }
                break;
            case ConditionType.LosePiece:
                if (playerPuzzleData.Count > eventStageChoiceData.ConditionMany)
                {
                    var randomRemovePieceList = playerPuzzleData;

                    var characterCount =
                        playerPuzzleData.FindAll(v => v.puzzleType == (int) PuzzleType.Character).Count;
                    if (characterCount <= 1)
                    {
                        //캐릭터 피스는 제외해야함
                        randomRemovePieceList = playerPuzzleData
                            .Where(v => v.puzzleType == (int) PuzzleType.Ability)
                            .ToList();
                    }

                    int count = 0;
                    while (count < eventStageChoiceData.ConditionMany)
                    {
                        var removeIndex = Random.Range(0, randomRemovePieceList.Count);
                        var removePiece = randomRemovePieceList[removeIndex];
                        
                        //한번 삭제한건 제외
                        randomRemovePieceList.Remove(removePiece);
                        
                        var findPuzzleData = puzzleDataList.Find(v => v.ID == removePiece.puzzleID);

                        playerPuzzleData.Remove(removePiece);
                        playerDataBase.Data.SetPlayerPuzzleData(playerPuzzleData);

                        //설치중인 피스라면 여기서도 삭제
                        var findInstalledData = playerInstalledPuzzleData.Find(v => v.uid == removePiece.uid);
                        if (findInstalledData != null)
                        {
                            //피스가 강화피스, 스탯피스라면 찾아서 제거
                            if ((PuzzleType) removePiece.puzzleType == PuzzleType.Ability || 
                                (PuzzleType) removePiece.puzzleType == PuzzleType.Stat)
                            {
                                //피스의 UID
                                var uid = findInstalledData.uid;
                                //설치된 피스 리스트에서 찾는다
                                foreach (var installed in playerInstalledPuzzleData)
                                {
                                    var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                                    bool isExists = installed.GetAppliedPuzzleUidList().Exists(v => v == uid);

                                    //해당 UID가 있으면 리스트에서 제거
                                    if (isExists)
                                    {
                                        appliedPuzzleUidList.Remove(uid);
                                        installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                                    }
                                }
                            }

                            playerInstalledPuzzleData.Remove(findInstalledData);
                            playerDataBase.Data.SetPlayerInstalledPuzzleData(playerInstalledPuzzleData);
                        }

                        var confirm =
                            PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as
                                PieceConfirmPopup;
                        confirm.Init("알림", "피스가 소실됐습니다!",
                            PieceData.GetNewPieceData(findPuzzleData as PuzzleData, removePiece));

                        count++;
                    }

                    isCondition = true;
                }

                break;
            case ConditionType.LoseAbilityPiece:
                var abilityPieceUidList = new List<ulong>();
                foreach (var data in playerPuzzleData)
                {
                    if (data.puzzleType == (int)PuzzleType.Ability)
                    {
                        abilityPieceUidList.Add(data.uid);
                    }
                }

                //강화 피스는 0개라도 상관 없음
                if (abilityPieceUidList.Count >= eventStageChoiceData.ConditionMany)
                {
                    int count = 0;
                    while (count < eventStageChoiceData.ConditionMany)
                    {
                        var removeEnhancePieceUid = abilityPieceUidList[Random.Range(0, abilityPieceUidList.Count)];
                        
                        //한번 삭제한건 제외
                        abilityPieceUidList.Remove(removeEnhancePieceUid);
                        
                        //uid로 탐색
                        var removePiece = playerPuzzleData.Find(v => v.uid == removeEnhancePieceUid);
                        var findPuzzleData = puzzleDataList.Find(v => v.ID == removePiece.puzzleID);

                        playerPuzzleData.Remove(removePiece);
                        playerDataBase.Data.SetPlayerPuzzleData(playerPuzzleData);

                        //설치중인 피스라면 여기서도 삭제
                        var findInstalledData = playerInstalledPuzzleData.Find(v => v.uid == removePiece.uid);
                        if (findInstalledData != null)
                        {
                            //피스가 강화피스, 스탯피스라면 찾아서 제거
                            if ((PuzzleType) removePiece.puzzleType == PuzzleType.Ability || 
                                (PuzzleType) removePiece.puzzleType == PuzzleType.Stat)
                            {
                                //피스의 UID
                                var uid = findInstalledData.uid;
                                //설치된 피스 리스트에서 찾는다
                                foreach (var installed in playerInstalledPuzzleData)
                                {
                                    var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                                    bool isExists = installed.GetAppliedPuzzleUidList().Exists(v => v == uid);

                                    //해당 UID가 있으면 리스트에서 제거
                                    if (isExists)
                                    {
                                        appliedPuzzleUidList.Remove(uid);
                                        installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                                    }
                                }
                            }

                            playerInstalledPuzzleData.Remove(findInstalledData);
                            playerDataBase.Data.SetPlayerInstalledPuzzleData(playerInstalledPuzzleData);
                        }

                        var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                        confirm.Init("알림", "피스가 소실됐습니다!", PieceData.GetNewPieceData(findPuzzleData as PuzzleData, removePiece));

                        count++;
                    }

                    isCondition = true;
                }

                break;
            case ConditionType.LoseCharacterPiece:
                var characterPieceUidList = new List<ulong>();
                foreach (var data in playerPuzzleData)
                {
                    if (data.puzzleType == (int)PuzzleType.Character)
                    {
                        characterPieceUidList.Add(data.uid);
                    }
                }

                //캐릭터 피스는 최소 1개를 남겨야 한다.
                if (characterPieceUidList.Count > eventStageChoiceData.ConditionMany)
                {
                    int count = 0;
                    while (count < eventStageChoiceData.ConditionMany)
                    {
                        var removeCharacterPieceUid =
                            characterPieceUidList[Random.Range(0, characterPieceUidList.Count)];

                        //한번 삭제한건 제외
                        characterPieceUidList.Remove(removeCharacterPieceUid);

                        var removePiece = playerPuzzleData.Find(v => v.uid == removeCharacterPieceUid);
                        var findPuzzleData = puzzleDataList.Find(v => v.ID == removePiece.puzzleID);

                        playerPuzzleData.Remove(removePiece);
                        playerDataBase.Data.SetPlayerPuzzleData(playerPuzzleData);

                        //설치중인 피스라면 여기서도 삭제
                        var findInstalledData = playerInstalledPuzzleData.Find(v => v.uid == removePiece.uid);
                        if (findInstalledData != null)
                        {
                            //피스가 강화피스, 스탯피스라면 찾아서 제거
                            if ((PuzzleType) removePiece.puzzleType == PuzzleType.Ability || 
                                (PuzzleType) removePiece.puzzleType == PuzzleType.Stat)
                            {
                                //피스의 UID
                                var uid = findInstalledData.uid;
                                //설치된 피스 리스트에서 찾는다
                                foreach (var installed in playerInstalledPuzzleData)
                                {
                                    var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                                    bool isExists = installed.GetAppliedPuzzleUidList().Exists(v => v == uid);

                                    //해당 UID가 있으면 리스트에서 제거
                                    if (isExists)
                                    {
                                        appliedPuzzleUidList.Remove(uid);
                                        installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                                    }
                                }
                            }

                            playerInstalledPuzzleData.Remove(findInstalledData);
                            playerDataBase.Data.SetPlayerInstalledPuzzleData(playerInstalledPuzzleData);
                        }

                        var confirm =
                            PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as
                                PieceConfirmPopup;
                        confirm.Init("알림", "피스가 소실됐습니다!",
                            PieceData.GetNewPieceData(findPuzzleData as PuzzleData, removePiece));

                        count++;
                    }

                    isCondition = true;
                }

                break;
            case ConditionType.LoseStatPiece:
                var statPieceUidList = new List<ulong>();
                foreach (var data in playerPuzzleData)
                {
                    if (data.puzzleType == (int)PuzzleType.Stat)
                    {
                        statPieceUidList.Add(data.uid);
                    }
                }

                //강화 피스는 0개라도 상관 없음
                if (statPieceUidList.Count >= eventStageChoiceData.ConditionMany)
                {
                    int count = 0;
                    while (count < eventStageChoiceData.ConditionMany)
                    {
                        var removeEnhancePieceUid = statPieceUidList[Random.Range(0, statPieceUidList.Count)];
                        
                        //한번 삭제한건 제외
                        statPieceUidList.Remove(removeEnhancePieceUid);
                        
                        //uid로 탐색
                        var removePiece = playerPuzzleData.Find(v => v.uid == removeEnhancePieceUid);
                        var findPuzzleData = puzzleDataList.Find(v => v.ID == removePiece.puzzleID);

                        playerPuzzleData.Remove(removePiece);
                        playerDataBase.Data.SetPlayerPuzzleData(playerPuzzleData);

                        //설치중인 피스라면 여기서도 삭제
                        var findInstalledData = playerInstalledPuzzleData.Find(v => v.uid == removePiece.uid);
                        if (findInstalledData != null)
                        {
                            //피스가 강화피스, 스탯피스라면 찾아서 제거
                            if ((PuzzleType) removePiece.puzzleType == PuzzleType.Ability || 
                                (PuzzleType) removePiece.puzzleType == PuzzleType.Stat)
                            {
                                //피스의 UID
                                var uid = findInstalledData.uid;
                                //설치된 피스 리스트에서 찾는다
                                foreach (var installed in playerInstalledPuzzleData)
                                {
                                    var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                                    bool isExists = installed.GetAppliedPuzzleUidList().Exists(v => v == uid);

                                    //해당 UID가 있으면 리스트에서 제거
                                    if (isExists)
                                    {
                                        appliedPuzzleUidList.Remove(uid);
                                        installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                                    }
                                }
                            }

                            playerInstalledPuzzleData.Remove(findInstalledData);
                            playerDataBase.Data.SetPlayerInstalledPuzzleData(playerInstalledPuzzleData);
                        }

                        var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                        confirm.Init("알림", "피스가 소실됐습니다!", PieceData.GetNewPieceData(findPuzzleData as PuzzleData, removePiece));

                        count++;
                    }

                    isCondition = true;
                }
                
                break;
        }

        return isCondition;
    }

    private void GetRandomReward(List<RandomRewardData> dataList)
    {
        if (dataList == null)
            return;

        var dataBaseManager = DataBaseManager.Instance;

        if (!(dataBaseManager.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
            return;

        var curFloor = playerDataBase.Data.Floor;
        var playerPuzzleData = playerDataBase.Data.GetPlayerPuzzleData();
        
        var randomRewardDataList = dataBaseManager.GetDataList<RandomRewardDataBase>().ToList();
        var puzzleDataList = dataBaseManager.GetDataList<PuzzleDataBase>().ToList();
        var rewardDataList = new List<RandomRewardData>();

        var battleType = RandomRewardData.BattleToStageType((StageType)currentSlotData.stageDataType);
        
        //보상 수 만큼 반복
        foreach (var data in dataList)
        {
            rewardDataList.Clear();

            rewardDataList.AddRange(randomRewardDataList
                .Select(randomData => randomData as RandomRewardData)
                .Where(reward => reward.RandomRewardId == data.RandomRewardId)
                .Where(v=> v.BattleType == BattleType.None || v.BattleType == battleType)
                .Where(v => v.Floor == 0 || v.Floor == curFloor));

            if (rewardDataList.Count <= 0)
                continue;

            var randomIndex = Random.Range(0, rewardDataList.Count);
            var resultReward = rewardDataList[randomIndex];

            switch (resultReward.RewardType)
            {
                case RandomRewardType.None:
                    break;
                case RandomRewardType.Money:
                {
                    var randomMoney = Random.Range(resultReward.MoneyMin, resultReward.MoneyMax + 1);

                    var realMoney = playerDataBase.Money + randomMoney;
                    if (realMoney <= 0)
                        realMoney = -playerDataBase.Money;
                    else realMoney = randomMoney;

                    playerDataBase.SetMoney(randomMoney);

                    var confirm =
                        PopupManager.Instance.CreatePopup(PopupName.GoodsConfirmPopup, UIUtils.DEPTH_60) as
                            GoodsConfirmPopup;
                    confirm.Init("알림", realMoney, GoodsType.Money);
                }
                    break;
                case RandomRewardType.Life:
                {
                    playerDataBase.SetHeart(resultReward.Life);

                    var confirm =
                        PopupManager.Instance.CreatePopup(PopupName.GoodsConfirmPopup, UIUtils.DEPTH_60) as
                            GoodsConfirmPopup;
                    confirm.Init("알림", resultReward.Life, GoodsType.Heart);
                }
                    break;
                case RandomRewardType.Pollution:
                {
                    for (int i = 0; i < playerPuzzleData.Count; i++)
                    {
                        playerPuzzleData[i].puzzlePollution = resultReward.Pollution;
                    }

                    playerDataBase.Data.SetPlayerPuzzleData(playerPuzzleData);

                    var confirm =
                        PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as
                            ConfirmPopup;
                    confirm.Init("알림", "모든 피스의 오염도가 정화되었습니다.");
                }
                    break;
                case RandomRewardType.Piece:
                {
                    if (puzzleDataList.Find(v => v.ID == resultReward.PieceDataId) is PuzzleData findData)
                    {
                        var newPuzzleData = new PlayerPuzzleData(findData.ID, (int) findData.Type);
                        playerPuzzleData.Add(newPuzzleData);

                        playerDataBase.Data.SetPlayerPuzzleData(playerPuzzleData);

                        var confirm =
                            PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as
                                PieceConfirmPopup;
                        confirm.Init("알림", "피스를 획득했습니다!", PieceData.GetNewPieceData(findData, newPuzzleData));
                    }
                }
                    break;
                case RandomRewardType.Soul:
                {
                    var randomSoul = Random.Range(resultReward.SoulMin, resultReward.SoulMax + 1);

                    var realSoul = playerDataBase.Soul + randomSoul;
                    if (realSoul <= 0)
                        realSoul = -playerDataBase.Soul;
                    else realSoul = randomSoul;

                    playerDataBase.SetSoul(randomSoul);

                    var confirm =
                        PopupManager.Instance.CreatePopup(PopupName.GoodsConfirmPopup, UIUtils.DEPTH_60) as
                            GoodsConfirmPopup;
                    confirm.Init("알림", realSoul, GoodsType.Soul);
                }
                    break;
                default:
                    DebugX.LogError("RewardType 에러");
                    break;
            }
        }
    }

    public void ResultEvent()
    {
        if (isClicked)
            return;

        isClicked = true;

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        //플레이어 디비 업데이트 제한 해제
        playerDB.SetUpdateFlag(true);
        
        //목숨이 0 이하 라면
        if (playerDB.Heart <= 0)
        {
            //게임 오버 처리
            var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
            tween.Init(false, 2f, () =>
            {
                PopupManager.Instance.HideAllPopup(false);
                LayerManager.Instance.ChangeLayer(Layer.GameOverLayer);
            });
            return;
        }

        var type = selectEventSlotData.BattleType;
        switch (type)
        {
            case EventBattleType.None:
                //해당 슬롯을 진행했다고 판정
                currentSlotData.isSlotEventEnd = true;

                //스테이지 데이터 갱신
                playerDB.Data.SetPlayerStageData(playerStageDataList);
                //세이브 데이터 갱신
                playerDB.UpdatePlayerDataBase(null);

                var stageUI = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
                //스테이지 선택 레이어에서 선택 가능한 슬롯들 트윈 시작
                stageUI.TweenMoveAbleSlots(true);
                //더이상 선택하지 못하는 슬롯들 비활성화
                stageUI.InActiveCannotChoiceSlot();

                PopupManager.Instance.HidePopup(this);
                AudioManager.Instance.BgmSound(AudioManager.BgmType.StageSelect);
                return;
            case EventBattleType.Normal:
                //이벤트가 끝나고 전투 스테이지로 변경하기 위해 데이터 Json 초기화
                currentSlotData.slotDataJson = string.Empty;
                //스테이지 타입 변경
                currentSlotData.stageDataType = (int) StageType.NormalBattle;
                break;
            case EventBattleType.Elite:
                //이벤트가 끝나고 전투 스테이지로 변경하기 위해 데이터 Json 초기화
                currentSlotData.slotDataJson = string.Empty;
                //스테이지 타입 변경
                currentSlotData.stageDataType = (int) StageType.EliteBattle;
                break;
        }

        //스테이지 데이터 갱신
        playerDB.Data.SetPlayerStageData(playerStageDataList);
        //세이브 데이터 갱신
        playerDB.UpdatePlayerDataBase(() =>
        {
            PopupManager.Instance.HidePopup(this);
            AudioManager.Instance.BgmSound(AudioManager.BgmType.StageSelect);

            if (type == EventBattleType.Normal ||
                type == EventBattleType.Elite)
            {
                //에디터일때 치트 고려함
#if UNITY_EDITOR
                if (GameSetting.Instance.isIgnoreBattle)
                {
                    var stageUI = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
                    //스테이지 선택 레이어에서 선택 가능한 슬롯들 트윈 시작
                    stageUI.TweenMoveAbleSlots(true);
                    //더이상 선택하지 못하는 슬롯들 비활성화
                    stageUI.InActiveCannotChoiceSlot();
                    return;
                }
#endif
                var spyingPopup = PopupManager.Instance.CreatePopup(PopupName.SpyingPopup) as SpyingPopup;
                spyingPopup?.Init(false);
            }
        });
    }

    private void HideEventChoicesTextButton()
    {
        for (int i = 0; i < eventChoicesText.Count; i++)
        {
            eventChoicesText[i].transform.parent.gameObject.SetActive(false);
        }
    }
}