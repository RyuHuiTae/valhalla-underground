using System;
using Coffee.UIExtensions;
using Cysharp.Threading.Tasks;
using UnityEngine.UI;

public class HeartLosePopup : CommonPopup
{
    public Image[] heartArray;
    public UIDissolve[] uiDissolveArray;
    public UITweenScale[] uiScaleTweenArray;

    private int index = 0;
    
    private void Start()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var playerStageDataList = playerDB.Data.GetPlayerStageData();
        var findData = playerStageDataList.Find(v => v.isCurrentSlot);
        if (findData == null)
            return;

        var stageType = (StageType) findData.stageDataType;
        if (stageType != StageType.FinalBoss)
            return;
        
        //플레이어의 승리로 이미 최종보스의 레벨이 증가된 상태
        index = playerDB.Data.FinalBossLevel - 2;

        for (var i = 0; i < heartArray.Length; i++)
        {
            if (i < index)
            {
                heartArray[i].gameObject.SetActive(false);
            }
        }
    }

    public async void Init()
    {
        for (var i = 0; i < heartArray.Length; i++)
        {
            if (i < index)
            {
                
            }
            else
            {
                heartArray[i].gameObject.SetActive(true);
                uiScaleTweenArray[i].TweenForward();
            }
        }
        
        while (popupAnimation.isPlaying)
        {
            await UniTask.WaitForEndOfFrame();
        }
        
        uiDissolveArray[index].Play();

        await UniTask.Delay(TimeSpan.FromSeconds(1));

        PopupManager.Instance.HidePopup(this);
    }
}
