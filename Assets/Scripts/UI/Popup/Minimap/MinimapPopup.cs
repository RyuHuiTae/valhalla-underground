using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MinimapPopup : CommonPopup
{
    [SerializeField] private Image playerIcon;
    [SerializeField] private Transform playerIconRoot;
    [SerializeField] private MinimapSlot slotPrefab;
    [SerializeField] private Transform slotRoot;

    [SerializeField] private Material grayScaleMaterial;
    [SerializeField] private List<TextMeshProUGUI> stageTextList;
    [SerializeField] private Image stageListCharacterIcon;
    
    private void Start()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;
        
        var stageList = StageManager.Instance.StageSlotDataList;

        if (stageList == null)
            return;
        
        var puzzleId = playerDB.Data.StarterCharacterPuzzleID;
        var findPuzzleData = puzzleDB.GetDataList().Find(v => v.ID == puzzleId);
        if (findPuzzleData != null)
        {
            var puzzleData = findPuzzleData as PuzzleData;
            var iconName = puzzleData.IconName;
            playerIcon.sprite = AtlasManager.Instance.GetIconSprite(iconName);
        }
        
        playerIcon.gameObject.SetActive(true);

        MinimapSlot curSlot = null;
        var minimapSlotList = new List<MinimapSlot>();
        
        foreach (var stage in stageList)
        {
            if (stage.data.StageStep1 != 0)
            {
                var newStageSlot = Instantiate(slotPrefab, slotRoot);
                minimapSlotList.Add(newStageSlot);
                newStageSlot.Init(stage);
                
                newStageSlot.transform.localPosition = new Vector3(
                    newStageSlot.GetSizeDelta().x * (stage.data.StageStep1 - 1),
                    newStageSlot.GetSizeDelta().y * stage.data.StageStep2,
                    0);
                
                newStageSlot.gameObject.SetActive(true);

                if (stage.isCurrentSlot)
                {
                    curSlot = newStageSlot;
                    playerIconRoot.transform.localPosition = newStageSlot.transform.localPosition;
                }
            }
        }

        var curSlotData = curSlot?.SlotData;
        
        var step1 = curSlotData.data.StageStep1;
        var findSlots = minimapSlotList.Where(v => v.SlotData.data.StageStep1 <= step1 + 1);

        foreach (var slot in findSlots)
        {
            if (slot != curSlot &&
                slot.SlotData.data.ID != curSlotData.data.TopNode &&
                slot.SlotData.data.ID != curSlotData.data.MidNode &&
                slot.SlotData.data.ID != curSlotData.data.BotNode) 
            {
                slot.SetClosedSlot(grayScaleMaterial);
            }
        }
        
        // foreach (var minimapSlot in minimapSlotList)
        // {
        //     if (minimapSlot.transform.localPosition.x <= playerIconRoot.transform.localPosition.x)
        //     {
        //         minimapSlot.SetClosedSlot(grayScaleMaterial);
        //     }
        // }

        var floor = playerDB.Data.Floor;
        for (int i = 0; i < floor - 1; i++)
        {
            stageTextList[i].text = "X";
        }

        var currentFloorText = stageTextList[floor - 1];
        currentFloorText.enabled = false;
        stageListCharacterIcon.sprite = playerIcon.sprite;
        stageListCharacterIcon.transform.SetParent(currentFloorText.transform);
        stageListCharacterIcon.transform.localPosition = Vector3.zero;
    }
}
