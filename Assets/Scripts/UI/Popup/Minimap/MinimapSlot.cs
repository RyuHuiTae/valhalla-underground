using UnityEngine;
using UnityEngine.UI;

public class MinimapSlot : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private Image backImage;
    [SerializeField] private Image iconImage;

    public StageSlotData SlotData { get; private set; }
    
    public void Init(StageSlotData data)
    {
        this.SlotData = data;
        
        var color = backImage.color;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;
        
        var stageType = data.type;
        
        //최심부 이벤트인 경우
        if (playerDB.Data.Floor > (int) basicDB.MaxFloor() && 
            stageType == StageType.Event)
        {
            stageType = StageType.FinalBoss;
        }
        
        switch (stageType)
        {
            case StageType.None:
            case StageType.Start:
                ColorUtility.TryParseHtmlString("#2D2D2D", out color);
                iconImage.gameObject.SetActive(false);
                break;
            // case StageType.Start:
            //     ColorUtility.TryParseHtmlString("#FFFFFF", out color);
            //     image.gameObject.SetActive(false);
            //     image.sprite = VResources.Load<Sprite>("Image/StageChoiceUI/stage_empty");
            //     break;
            case StageType.Boss:
                ColorUtility.TryParseHtmlString("#FF0000", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_boss_icon");
                break;
            case StageType.Event:
                ColorUtility.TryParseHtmlString("#7030A0", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_event_icon");
                break;
            case StageType.Rest:
                ColorUtility.TryParseHtmlString("#FBE5D6", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_rest_icon");
                break;
            case StageType.Shop:
                ColorUtility.TryParseHtmlString("#70AD47", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_store_icon");
                break;
            case StageType.EliteBattle:
                ColorUtility.TryParseHtmlString("#FFC000", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_strong_enemy_icon");
                break;
            case StageType.NormalBattle:
                ColorUtility.TryParseHtmlString("#00B0F0", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_enemy_icon");
                break;
            case StageType.FinalBoss:
                ColorUtility.TryParseHtmlString("#292039", out color);
                iconImage.sprite = AtlasManager.Instance.GetIconSprite("stage_last_boss_icon");
                break;
        }

        //backImage.color = color;
        backImage.color = new Color(color.r, color.g, color.b, 0.8f);
    }
    
    public Vector2 GetSizeDelta()
    {
        return rectTransform.sizeDelta;
    }

    private void SetMaterial(Material mat)
    {
        backImage.material = mat;
    }

    public void SetClosedSlot(Material mat)
    {
        ColorUtility.TryParseHtmlString("#00B0F0", out var color);
        backImage.color = new Color(color.r, color.g, color.b, 0.8f);
        backImage.material = mat;
    }
}
