using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DevSupport;
using TMPro;
using UnityEngine;

public class NextFloorAnimationPopup : CommonPopup
{
    [SerializeField] private UITweenAlpha nextFloorAnimation;
    [SerializeField] private TextMeshProUGUI floorText;
    [SerializeField] private TextMeshProUGUI currentFloorText;
    [SerializeField] private TextMeshProUGUI pastFloorText;

    private PopupManager popupManager;

    private void Awake()
    {
        popupManager = PopupManager.Instance;
    }

    /// <summary>
    /// 작성자: 류희태
    /// </summary>
    public async void PlayNextFloorAnimation(string floorString, int floor)
    {
        // 오브젝트가 꺼져있는 상태이기 때문에 시작할 때 켜준다 
        nextFloorAnimation.gameObject.SetActive(true);
        
        InitAnimationTexts(floorString, floor);

        await PlayGoingUpFloorTextAnimation();
        
        const float showingTextStopTime = 1f;
        await UniTask.Delay(TimeSpan.FromSeconds(showingTextStopTime));
        
        nextFloorAnimation.TweenForward();
        PlayHidingAnimation();

        await UniTask.WaitUntil(() => nextFloorAnimation.IsPlay() == false);
        nextFloorAnimation.gameObject.SetActive(false);
        
        popupManager.HidePopup(this);
    }

    private void PlayHidingAnimation()
    {
        var floorTextAlphaTween = floorText.GetComponent<UITweenAlpha>();
        var currentFloorAlphaTween = currentFloorText.GetComponent<UITweenAlpha>();
        var pastFloorAlphaTween = pastFloorText.GetComponent<UITweenAlpha>();
        
        currentFloorAlphaTween.TweenForward();
        pastFloorAlphaTween.TweenForward();
        floorTextAlphaTween.TweenForward();
    }

    private void InitAnimationTexts(string floorString, int floor)
    {
        floorText.text = floorString;
        currentFloorText.text = (floor).ToString();
        pastFloorText.text = (floor - 1).ToString();
    }
    
    private async UniTask PlayGoingUpFloorTextAnimation()
    {
        float height = nextFloorAnimation.GetComponent<RectTransform>().rect.height;
        float halfHeight = -((height * 0.5f) + pastFloorText.transform.GetComponent<RectTransform>().rect.height);

        var currentFloorTween = currentFloorText.GetComponent<UITweenPosition>();
        var pastFloorTween = pastFloorText.GetComponent<UITweenPosition>();
        currentFloorTween.@from = new Vector3(0f, halfHeight, 0f);
        pastFloorTween.to = new Vector3(0f, -halfHeight, 0f);

        currentFloorTween.TweenForward();
        pastFloorTween.TweenForward();
        
        await UniTask.WaitUntil(() => currentFloorTween.IsPlay() == false);
    }

    public async void PlayNextFloorAnimation(string floorString)
    {
        nextFloorAnimation.gameObject.SetActive(true);
        currentFloorText.gameObject.SetActive(false);
        pastFloorText.gameObject.SetActive(false);
        
        floorText.text = floorString;
        await UniTask.Delay(TimeSpan.FromSeconds(3f));
        
        var floorTextAlphaTween = floorText.GetComponent<UITweenAlpha>();
        nextFloorAnimation.TweenForward();
        floorTextAlphaTween.TweenForward();

        await UniTask.WaitUntil(() => nextFloorAnimation.IsPlay() == false);
        nextFloorAnimation.gameObject.SetActive(false);
        
        popupManager.HidePopup(this);
    }
}
