using System;
using UnityEngine;
using UnityEngine.Serialization;

public class InventoryPopup : CommonPopup
{
    [SerializeField] private GameObject allButton;
    [SerializeField] private GameObject characterButton;
    [SerializeField] private GameObject abilityButton;
    [SerializeField] private GameObject statButton;

    public InventoryList inventoryList;

    public void Init(Transform pieceRoot, string depth,
        Action<PieceControl> updateCallBack = null,
        Action<PieceControl> mouseDownCallBack = null,
        Action<PieceControl> mouseUpCallBack = null,
        Action<PieceControl, Collision> enterCallBack = null,
        Action<PieceControl, Collision> stayCallBack = null,
        Action<PieceControl, Collision> exitCallBack = null,
        Action refreshPuzzleData = null)
    {
        inventoryList.Init(
            updateCallBack, 
            mouseDownCallBack, 
            mouseUpCallBack,
            enterCallBack, 
            stayCallBack, 
            exitCallBack,
            refreshPuzzleData,
            pieceRoot, 
            depth);
    }

    public override void OnClickCancelButton()
    {
        var infoUI = PopupManager.Instance.GetOpenedPopup(PopupName.PuzzleInfoPopup);
        infoUI?.OnClickCancelButton();
        
        base.OnClickCancelButton();
    }

    // public void SetEnabledCollider(bool isActive)
    // {
    //     inventoryCollider.enabled = isActive;
    // }

    public void OnClickAllButton()
    {
        allButton.gameObject.SetActive(false);
        characterButton.gameObject.SetActive(true);
        abilityButton.gameObject.SetActive(false);
        statButton.gameObject.SetActive(false);
        
        inventoryList.ShowCharacterList();
    }
    
    public void OnClickCharacterButton()
    {
        allButton.gameObject.SetActive(false);
        characterButton.gameObject.SetActive(false);
        abilityButton.gameObject.SetActive(true);
        statButton.gameObject.SetActive(false);
        
        inventoryList.ShowAbilityList();
    }
    
    public void OnClickEnhanceButton()
    {
        allButton.gameObject.SetActive(false);
        characterButton.gameObject.SetActive(false);
        abilityButton.gameObject.SetActive(false);
        statButton.gameObject.SetActive(true);
        
        inventoryList.ShowStatList();
    }
    
    public void OnClickStatButton()
    {
        allButton.gameObject.SetActive(true);
        characterButton.gameObject.SetActive(false);
        abilityButton.gameObject.SetActive(false);
        statButton.gameObject.SetActive(false);
        
        inventoryList.ShowAllList();
    }
}
