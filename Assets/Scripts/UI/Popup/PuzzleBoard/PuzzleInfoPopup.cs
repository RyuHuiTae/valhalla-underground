using System;
using System.Linq;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleInfoPopup : CommonPopup
{
    [SerializeField] private GameObject root;
    [SerializeField] private TextMeshProUGUI puzzleName;
    [SerializeField] private TextMeshProUGUI type;
    [SerializeField] private TextMeshProUGUI characterType;
    [SerializeField] private TextMeshProUGUI cost;
    [SerializeField] private TextMeshProUGUI level;
    [SerializeField] private TextMeshProUGUI hp;
    [SerializeField] private TextMeshProUGUI attack;
    [SerializeField] private TextMeshProUGUI defence;
    [SerializeField] private TextMeshProUGUI speed;
    [SerializeField] private TextMeshProUGUI attackSpeed;
    [SerializeField] private TextMeshProUGUI coolTime;
    [SerializeField] private TextMeshProUGUI hpAdd;
    [SerializeField] private TextMeshProUGUI attackAdd;
    [SerializeField] private TextMeshProUGUI defenceAdd;
    [SerializeField] private TextMeshProUGUI speedAdd;
    [SerializeField] private TextMeshProUGUI attackSpeedAdd;
    [SerializeField] private TextMeshProUGUI coolTimeAdd;
    [SerializeField] private TextMeshProUGUI duplicate;
    [SerializeField] private TextMeshProUGUI pollusion;
    [SerializeField] private TextMeshProUGUI desc;
    [SerializeField] private Image typeImage;
    [SerializeField] private Image characterTypeImage;
    [SerializeField] private Image shape;
    [SerializeField] private Image duplicateImage;
    [SerializeField] private GameObject[] characterOnly;
    [SerializeField] private GameObject[] enhanceOnly;

    private SkillInfoPopup skillInfoUI;
    private PieceData pieceData;
    private PuzzleData puzzleData;

    private void Awake()
    {
        root.SetActive(false);
    }

    private void Start()
    {
        root.SetActive(true);
    }

    public void Init(PieceData data)
    {
        puzzleName.text = data.PuzzleData.Name;
        cost.text = "Cost." + data.PuzzleData.Cost.ToString();
        typeImage.sprite = PieceLoader.Instance.GetTypeImage((int) data.PuzzleData.Type);
        shape.sprite = PieceLoader.Instance.GetPieceShapeSprite(data.PuzzleData.Shape);

        if (data.PuzzleData.Type == PuzzleType.Character)
        {
            this.pieceData = data;
            this.puzzleData = data.PuzzleData;
            
            var list = DataBaseManager.Instance.GetDataList<CharacterDataBase>();

            for (int count = 0; count < characterOnly.Length; count++)
            {
                characterOnly[count].SetActive(true);
            }
            for (int count = 0; count < enhanceOnly.Length; count++)
            {
                enhanceOnly[count].SetActive(false);
            }
            
            type.text = "캐릭터";
            if (data.PuzzleData.CharacterData.Type == CharacterAttackType.NearField)
                characterType.text = "근거리 딜러";
            else if (data.PuzzleData.CharacterData.Type == CharacterAttackType.Ranged)
                characterType.text = "원거리 딜러";
            else if (data.PuzzleData.CharacterData.Type == CharacterAttackType.Supporter)
                characterType.text = "서포터";
            characterTypeImage.sprite = PieceLoader.Instance.GetCharacterTypeImage((int) data.PuzzleData.CharacterData.Type);

            if (list.FirstOrDefault(v => v.ID == data.PuzzleData.CharacterDataId) is CharacterData findData)
            {
                level.text = "Level. ";
                if (findData.Level / 10 < 1)
                    level.text += "0";
                level.text += findData.Level.ToString() + "/10";
                hp.text = findData.Hp.ToString();
                attack.text = findData.Attack.ToString();
                defence.text = findData.Defense.ToString();
                speed.text = findData.MoveSpeed.ToString();
                attackSpeed.text = findData.AttackSpeed.ToString();
                coolTime.text = "0%";
            }
            
            AddStat(data);
        }
        else if (data.PuzzleData.Type == PuzzleType.Ability)
        {
            var list = DataBaseManager.Instance.GetDataList<AbilityPieceDataBase>();
            
            for (int count = 0; count < characterOnly.Length; count++)
            {
                characterOnly[count].SetActive(false);
            }
            for (int count = 0; count < enhanceOnly.Length; count++)
            {
                enhanceOnly[count].SetActive(true);
            }

            type.text = "어빌리티";
            pollusion.text = "오염도: " + data.PiecePollution + "%";
            desc.text = data.PuzzleData.AbilityPieceData.Desc;

            duplicate.text = "중복 불가능";
            duplicateImage.sprite = PieceLoader.Instance.GetDuplicateImage(1);
        }
        else if (data.PuzzleData.Type == PuzzleType.Stat)
        {
            for (int count = 0; count < characterOnly.Length; count++)
            {
                characterOnly[count].SetActive(false);
            }
            for (int count = 0; count < enhanceOnly.Length; count++)
            {
                enhanceOnly[count].SetActive(true);
            }

            type.text = "스탯";
            pollusion.text = "오염도: " + data.PiecePollution + "%";
            desc.text = data.PuzzleData.StatPieceData.Desc;

            duplicate.text = "중복 가능";
            duplicateImage.sprite = PieceLoader.Instance.GetDuplicateImage(0);
        }
    }

    public void Init(PuzzleData data)
    {
        puzzleName.text = data.Name;
        cost.text = "Cost." + data.Cost.ToString();
        typeImage.sprite = PieceLoader.Instance.GetTypeImage((int) data.Type);
        shape.sprite = PieceLoader.Instance.GetPieceShapeSprite(data.Shape);

        if (data.Type == PuzzleType.Character)
        {
            this.pieceData = null;
            this.puzzleData = data;
            var list = DataBaseManager.Instance.GetDataList<CharacterDataBase>();
            
            for (int count = 0; count < characterOnly.Length; count++)
            {
                characterOnly[count].SetActive(true);
            }
            for (int count = 0; count < enhanceOnly.Length; count++)
            {
                enhanceOnly[count].SetActive(false);
            }
            
            type.text = "캐릭터";
            if (data.CharacterData.Type == CharacterAttackType.NearField)
                characterType.text = "근거리 딜러";
            else if (data.CharacterData.Type == CharacterAttackType.Ranged)
                characterType.text = "원거리 딜러";
            else if (data.CharacterData.Type == CharacterAttackType.Supporter)
                characterType.text = "서포터";
            
            characterTypeImage.sprite = PieceLoader.Instance.GetCharacterTypeImage((int) data.CharacterData.Type);

            if (list.FirstOrDefault(v => v.ID == data.CharacterDataId) is CharacterData findData)
            {
                level.text = "Level. ";
                if (findData.Level / 10 < 1)
                    level.text += "0";
                level.text += findData.Level.ToString() + "/10";
                hp.text = findData.Hp.ToString();
                attack.text = findData.Attack.ToString();
                defence.text = findData.Defense.ToString();
                speed.text = findData.MoveSpeed.ToString();
                attackSpeed.text = findData.AttackSpeed.ToString();
                coolTime.text = "0%";
            }
        }
    }

    public void DetailClickButton()
    {
        skillInfoUI = PopupManager.Instance.GetPopup(PopupName.SkillInfoPopup, UIUtils.DEPTH_50) as SkillInfoPopup;

        if (this.pieceData != null)
        {
            skillInfoUI.Init(this.pieceData);    
        }
        else skillInfoUI.Init(this.puzzleData);
    }

    public void RefreshAddStat()
    {
        if (pieceData == null)
            return;
        
        AddStat(pieceData);
    }
    
    private void AddStat(PieceData data)
    {
        var basicData = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;
        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;

        var characterData = data.PuzzleData.CharacterData;
        
        var addStatList = playerDB.GetPlayerCharacterAddStatList();
        var findStatData = addStatList.Find(v => v.Uid == data.Uid);
        
        if (findStatData?.Hp > 0)
        {
            float resultHp = 0;
            resultHp += characterData.Hp + findStatData.Hp;
            
            hpAdd.transform.gameObject.SetActive(true);
            hpAdd.text = "+" + findStatData.Hp.ToString();
            hp.text = resultHp.ToString();
        }
        else
        {
            hpAdd.transform.gameObject.SetActive(false);
            hp.text = characterData.Hp.ToString();
        }
        
        if (findStatData?.Attack > 0)
        {
            var resultAttack = characterData.Attack + findStatData.Attack;
            attackAdd.transform.gameObject.SetActive(true);
            attackAdd.text = "+" + findStatData.Attack.ToString();
            attack.text = resultAttack.ToString();
        }
        else
        {
            attackAdd.transform.gameObject.SetActive(false);
            attack.text = characterData.Attack.ToString();
        }
        
        if (findStatData?.Defense > 0)
        {
            float resultDefence = 0;
            resultDefence += characterData.Defense + findStatData.Defense;
            
            defenceAdd.transform.gameObject.SetActive(true);
            defenceAdd.text = "+" + findStatData.Defense.ToString();
            defence.text = resultDefence.ToString();
        }
        else
        {
            defenceAdd.transform.gameObject.SetActive(false);
            defence.text = characterData.Defense.ToString();
        }
        
        if (findStatData?.MoveSpeed > 0)
        {
            float resultSpeed = 0;
            resultSpeed += characterData.MoveSpeed + findStatData.MoveSpeed;
            
            speedAdd.transform.gameObject.SetActive(true);
            speedAdd.text = "+" + findStatData.MoveSpeed.ToString();
            speed.text = resultSpeed.ToString();
        }
        else
        {
            speedAdd.transform.gameObject.SetActive(false);
            speed.text = characterData.MoveSpeed.ToString();
        }
        
        if (findStatData?.AttackSpeed > 0)
        {
            float resultAttackSpeed = 0;
            resultAttackSpeed = characterData.AttackSpeed + characterData.AttackSpeed * findStatData.AttackSpeed * 0.01f;
            
            attackSpeedAdd.transform.gameObject.SetActive(true);
            attackSpeedAdd.text = "+" + findStatData.AttackSpeed.ToString() + "%";
            attackSpeed.text = resultAttackSpeed.ToString();
        }
        else
        {
            attackSpeedAdd.transform.gameObject.SetActive(false);
            attackSpeed.text = characterData.AttackSpeed.ToString();
        }
        
        if (findStatData?.SkillCoolTime > 0)
        {
            float resultCoolTime = 0;
            resultCoolTime = findStatData.SkillCoolTime;

            if (resultCoolTime > basicData.SkillCoolTimeLimit())
                resultCoolTime = basicData.SkillCoolTimeLimit();
            
            coolTimeAdd.transform.gameObject.SetActive(true);
            coolTimeAdd.text = "+" + findStatData.SkillCoolTime.ToString() + "%";
            coolTime.text = resultCoolTime.ToString() + "%";
        }
        else
        {
            coolTimeAdd.transform.gameObject.SetActive(false);
            coolTime.text = "0%";
        }
    }
}