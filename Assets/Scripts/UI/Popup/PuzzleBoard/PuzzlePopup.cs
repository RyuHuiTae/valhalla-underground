using DevSupport;
using UnityEngine;

public class PuzzlePopup : CommonPopup
{
    public PlayerBoardData playerBoardData;
    public Transform Board;
    
    public bool IsClosing { get; private set; }
    
    private InventoryPopup inventoryPopup;
    
    private void Start()
    {
        IsClosing = false;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)) 
            return;
        
        inventoryPopup = PopupManager.Instance.CreatePopup(PopupName.InventoryPopup) as InventoryPopup;

        inventoryPopup?.Init(
            Board,
            UIUtils.DEPTH_40,
            UpdateCallBack, 
            MouseDownCallBack, 
            MouseUpCallBack, 
            EnterCallBack,
            StayCallBack, 
            ExitCallBack,
            RefreshPlayerPuzzleData);

        var installedList = playerDB.Data.GetPlayerInstalledPuzzleData();
        if (installedList == null)
            return;
            
        var invenSlotList = inventoryPopup.inventoryList.inventoryPieceList;
        foreach (var installedPuzzleData in installedList)
        {
            var uid = installedPuzzleData.uid;

            var findSlot = invenSlotList.Find(v => v.pieceData.Uid == uid);
            findSlot.CreatePieceControlObj(false);
            findSlot.pieceControlObj.firstIsHoldCheck = false;
                
            var pieceControl = findSlot.pieceControlObj;
            if (pieceControl == null)
            {
                DebugX.LogError("pieceControl is NULL");
                return;
            }
            
            pieceControl.transform.position = installedPuzzleData.installPos;
            pieceControl.transform.localScale = Vector3.one;

            playerBoardData.Use_piece(pieceControl);
        }
        
        inventoryPopup?.inventoryList.UpdatePageCount();
        
        PopupManager.Instance.CheckTutorial(TutorialTextType.EnterPuzzlePopup);
    }

    private void UpdateCallBack(PieceControl pieceControl)
    {
        if (pieceControl.pointCheck && !pieceControl.isHold && (!pieceControl.overlap || pieceControl.position_check)
            && !pieceControl.firstIsHoldCheck)
        {
            for (int count = 0; count < pieceControl.points.Count; count++)
            {
                pieceControl.savePoints.Add(pieceControl.points[count]);
                pieceControl.savePoints[count].transform.GetComponent<BoardPoint>().usedCheck = true;
            }

            if (pieceControl.startCheck)
            {
                pieceControl.max_x = pieceControl.savePoints[0].transform.localPosition.x;
                pieceControl.max_y = pieceControl.savePoints[0].transform.localPosition.y;
                pieceControl.min_x = pieceControl.savePoints[0].transform.localPosition.x;
                pieceControl.min_y = pieceControl.savePoints[0].transform.localPosition.y;

                for (int count = 1; count < pieceControl.points.Count; count++)
                {
                    if (pieceControl.max_x < pieceControl.savePoints[count].transform.localPosition.x)
                        pieceControl.max_x = pieceControl.savePoints[count].transform.localPosition.x;
                    if (pieceControl.min_x > pieceControl.savePoints[count].transform.localPosition.x)
                        pieceControl.min_x = pieceControl.savePoints[count].transform.localPosition.x;
                    if (pieceControl.max_y < pieceControl.savePoints[count].transform.localPosition.y)
                        pieceControl.max_y = pieceControl.savePoints[count].transform.localPosition.y;
                    if (pieceControl.min_y > pieceControl.savePoints[count].transform.localPosition.y)
                        pieceControl.min_y = pieceControl.savePoints[count].transform.localPosition.y;
                }

                pieceControl.puzzle_position_x = pieceControl.max_x - (pieceControl.max_x - pieceControl.min_x) / 2;
                pieceControl.puzzle_position_y = pieceControl.max_y - (pieceControl.max_y - pieceControl.min_y) / 2;

                pieceControl.oneself = new Vector3(pieceControl.puzzle_position_x, pieceControl.puzzle_position_y, 0);
                
                pieceControl.startCheck = false;
            }

            pieceControl.pointCheck = false;
            pieceControl.position_check = true;
        }
    }

    private void MouseDownCallBack(PieceControl pieceControl)
    {
        for (int count = 0; count < pieceControl.savePoints.Count; count++)
        {
            pieceControl.savePoints[count].transform.GetComponent<BoardPoint>().usedCheck = false;
        }

        pieceControl.transform.localScale = new Vector3(0.9f, 0.9f, 1);
        for (int count = 0; count < pieceControl.colliders.Length; count++)
        {
            pieceControl.colliders[count].transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
        
        playerBoardData.Unused_piece(pieceControl);
        
        var uid = pieceControl.pieceData.Uid;
        if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
        {
            var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();
            var findData = installedPuzzleList.Find(v => v.uid == uid);

            installedPuzzleList.Remove(findData);
            
            playerDB.Data.SetPlayerInstalledPuzzleData(installedPuzzleList);
        }
    }
    
    private void MouseUpCallBack(PieceControl pieceControl)
    {
        if (DuplicateCheck(pieceControl))
        {
            DebugX.Log("피스 중복사용이 감지 : " + pieceControl.pieceData.PuzzleData.Name);
            pieceControl.ReturnInventory();

            if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Character)
            {
                var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                confirm.Init("알림", "동일한 캐릭터 피스가 이미 부착되어 있습니다.");
            }
            else if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Ability)
            {
                var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                confirm.Init("알림", "동일한 어빌리티 피스가 이미 부착되어 있습니다.");
            }

            return;
        }
        
        if (pieceControl.points.Count == pieceControl.pieceData.PuzzleData.Cost)
        {
            bool overLap = false;

            for (int count = 0; count < pieceControl.points.Count; count++)
            {
                if (pieceControl.points[count].transform.GetComponent<BoardPoint>().usedCheck)
                {
                    overLap = true;
                }
            }

            if (overLap)
            {
                if (pieceControl.points.Count > 0 && pieceControl.position_check) //보드판에 피스 설치
                {
                    pieceControl.overlap = false;
                    pieceControl.transform.localPosition = pieceControl.oneself;
                    pieceControl.transform.localPosition = new Vector3(pieceControl.transform.localPosition.x,
                        pieceControl.transform.localPosition.y, 0);

                    pieceControl.transform.localScale = new Vector3(1.0f, 1.0f, 1);
                    for (int count = 0; count < pieceControl.colliders.Length; count++)
                    {
                        pieceControl.colliders[count].transform.localScale = new Vector3(0.2f, 0.2f, 1.0f);
                    }

                    if (playerBoardData.Use_piece(pieceControl))
                    {
                        //퍼즐이 성공적으로 보드에 설치된 경우
                        var uid = pieceControl.pieceData.Uid;
                        var pos = pieceControl.transform.position;
                        pieceControl.firstIsHoldCheck = false;
                        
                        AudioManager.Instance.EffectSound(AudioManager.EffectType.PieceMounting, false, 0.3f);

                        if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
                        {
                            var newData = new PlayerInstalledPuzzleData(uid, pos);

                            var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();
                            installedPuzzleList.Add(newData);

                            playerDB.Data.SetPlayerInstalledPuzzleData(installedPuzzleList);
                        }
                        
                        inventoryPopup?.inventoryList.UpdatePageCount();
                    }
                }

                for (int count = 0; count < pieceControl.savePoints.Count; count++)
                {
                    pieceControl.savePoints[count].transform.GetComponent<BoardPoint>().usedCheck = true;
                }
                
                return;
            }

            pieceControl.max_x = pieceControl.points[0].transform.localPosition.x;
            pieceControl.max_y = pieceControl.points[0].transform.localPosition.y;
            pieceControl.min_x = pieceControl.points[0].transform.localPosition.x;
            pieceControl.min_y = pieceControl.points[0].transform.localPosition.y;

            for (int count = 1; count < pieceControl.points.Count; count++)
            {
                if (pieceControl.max_x < pieceControl.points[count].transform.localPosition.x)
                    pieceControl.max_x = pieceControl.points[count].transform.localPosition.x;
                if (pieceControl.min_x > pieceControl.points[count].transform.localPosition.x)
                    pieceControl.min_x = pieceControl.points[count].transform.localPosition.x;
                if (pieceControl.max_y < pieceControl.points[count].transform.localPosition.y)
                    pieceControl.max_y = pieceControl.points[count].transform.localPosition.y;
                if (pieceControl.min_y > pieceControl.points[count].transform.localPosition.y)
                    pieceControl.min_y = pieceControl.points[count].transform.localPosition.y;
            }

            pieceControl.puzzle_position_x = pieceControl.max_x - (pieceControl.max_x - pieceControl.min_x) / 2;
            pieceControl.puzzle_position_y = pieceControl.max_y - (pieceControl.max_y - pieceControl.min_y) / 2;
            
            pieceControl.oneself = new Vector3(pieceControl.puzzle_position_x, pieceControl.puzzle_position_y, 0);
            
            pieceControl.savePoints.RemoveRange(0,pieceControl.savePoints.Count);
            pieceControl.pointCheck = true;
            pieceControl.position_check = true;
        }
        
        if (pieceControl.points.Count > 0 && pieceControl.position_check)    //보드판에 피스 설치
        {
            pieceControl.overlap = false;
            pieceControl.transform.localPosition = pieceControl.oneself;
            pieceControl.transform.localPosition = new Vector3(pieceControl.transform.localPosition.x,
                pieceControl.transform.localPosition.y, 0);
            
            pieceControl.transform.localScale = new Vector3(1.0f, 1.0f, 1);
            for (int count = 0; count < pieceControl.colliders.Length; count++)
            {
                pieceControl.colliders[count].transform.localScale = new Vector3(0.2f, 0.2f, 1.0f);
            }

            if (playerBoardData.Use_piece(pieceControl))
            {
                //퍼즐이 성공적으로 보드에 설치된 경우
                var uid = pieceControl.pieceData.Uid;
                var pos = pieceControl.transform.position;
                pieceControl.firstIsHoldCheck = false;
                
                AudioManager.Instance.EffectSound(AudioManager.EffectType.PieceMounting, false, 0.3f);
                
                if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
                {
                    var newData = new PlayerInstalledPuzzleData(uid, pos);
            
                    var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();
                    installedPuzzleList.Add(newData);

                    playerDB.Data.SetPlayerInstalledPuzzleData(installedPuzzleList);
                }
                
                inventoryPopup?.inventoryList.UpdatePageCount();
            }
            
            for (int count = 0; count < pieceControl.savePoints.Count; count++)
            {
                pieceControl.savePoints[count].transform.GetComponent<BoardPoint>().usedCheck = true;
            }
        }
    }

    private void EnterCallBack(PieceControl pieceControl, Collision collision)
    {
        if (collision.gameObject.tag == "Point")
        {
            for (int count = 0; count < pieceControl.points.Count; count++)
            {
                if (collision.gameObject.name == pieceControl.points[count].name)
                {
                    return;
                }
            }
            
            pieceControl.points.Add(collision.gameObject);
        }
    }

    private void StayCallBack(PieceControl pieceControl, Collision collision)
    {
        if (collision.gameObject.tag == "Point")
        {
            for (int count = 0; count < pieceControl.points.Count; count++)
            {
                if (collision.gameObject.name == pieceControl.points[count].name)
                {
                    return;
                }
            }
            
            pieceControl.points.Add(collision.gameObject);
        }
    }

    private void ExitCallBack(PieceControl pieceControl, Collision collision)
    {
        if (collision.gameObject.tag == "Point")
            pieceControl.points.Remove(collision.gameObject);
    }

    private bool DuplicateCheck(PieceControl pieceControl)
    {
        if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Character) //피스 타입 - 캐릭터
        {
            for (int count = 0; count < playerBoardData.characterTypeList.Count; count++)
            {
                if (playerBoardData.characterTypeList[count].PuzzleData.CharacterData.CharacterType ==
                    pieceControl.pieceData.PuzzleData.CharacterData.CharacterType && pieceControl.points.Count > 0)
                    return true;
            }
        }
        else if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Ability)  //피스 타입 - 강화
        {
            for (int count = 0; count < playerBoardData.enhance_data.Count; count++)
            {
                if (playerBoardData.enhance_data[count].pieceData.PuzzleData.EnhancePieceDataId ==   
                    pieceControl.pieceData.PuzzleData.EnhancePieceDataId && pieceControl.points.Count > 0)
                    return true;
            }
        }
        return false;
    }

    public void RefreshPlayerPuzzleData()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)) 
            return;
        
        var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();
        foreach (var installedPuzzleData in installedPuzzleList)
        {
            //적용될 강화피스 초기화
            installedPuzzleData.appliedPuzzleUidArray = null;
        }
            
        //스탯 피스 추가
        var statList = playerBoardData.stat_data;
        foreach (var stat in statList)
        {
            foreach (var apply in stat.applyList)
            {
                if (apply == null)
                    continue;
                    
                //uid로 해당 피스를 찾은 뒤
                var uid = apply.pieceData.Uid;
                var findData = installedPuzzleList.Find(v => v.uid == uid);

                if (findData == null)
                    continue;
                
                //찾은 피스에 적용될 강화 피스 UID를 추가
                var appliedPuzzleUidList = findData.GetAppliedPuzzleUidList();
                appliedPuzzleUidList.Add(stat.pieceData.Uid);
                    
                //적용될 퍼즐 UID 리스트 갱신
                findData.SetAppliedPuzzleUidList(appliedPuzzleUidList);
            }
        }
            
        //각 캐릭터 피스에 적용될 강화 피스 ID 추가
        var enhanceList = playerBoardData.enhance_data;
        foreach (var enhance in enhanceList)
        {
            foreach (var apply in enhance.applyList)
            {
                if (apply == null)
                    continue;
                    
                //uid로 해당 피스를 찾은 뒤
                var uid = apply.pieceData.Uid;
                var findData = installedPuzzleList.Find(v => v.uid == uid);
                    
                if (findData == null)
                    continue;
                
                //찾은 피스에 적용될 강화 피스 UID를 추가
                var appliedPuzzleUidList = findData.GetAppliedPuzzleUidList();
                appliedPuzzleUidList.Add(enhance.pieceData.Uid);
                    
                //적용될 퍼즐 UID 리스트 갱신
                findData.SetAppliedPuzzleUidList(appliedPuzzleUidList);
            }
        }
            
        //설치된 피스 리스트 갱신
        playerDB.Data.SetPlayerInstalledPuzzleData(installedPuzzleList);
    }
    
    public override void OnClickCancelButton()
    {
        IsClosing = true;

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)) 
            return;
        
        //플레이어 퍼즐 데이터 업데이트
        RefreshPlayerPuzzleData();
            
        //세이브 데이터 갱신 후 닫기
        playerDB.UpdatePlayerDataBase(()=>
        {
            inventoryPopup?.OnClickCancelButton();    
            base.OnClickCancelButton();
        });
    }
}
