using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillInfoPopup : CommonPopup
{
    [SerializeField] private Image attackImage;
    [SerializeField] private Image classImage;
    [SerializeField] private Image uniqueImage;
    [SerializeField] private TextMeshProUGUI attackName;
    [SerializeField] private TextMeshProUGUI className;
    [SerializeField] private TextMeshProUGUI uniqueName;
    [SerializeField] private TextMeshProUGUI attackCoolDown;
    [SerializeField] private TextMeshProUGUI classCoolDown;
    [SerializeField] private TextMeshProUGUI uniqueCoolDown;
    [SerializeField] private TextMeshProUGUI attackDesc;
    [SerializeField] private TextMeshProUGUI classDesc;
    [SerializeField] private TextMeshProUGUI uniqueDesc;
    [SerializeField] private GameObject attackLock;
    [SerializeField] private GameObject classLock;
    [SerializeField] private GameObject uniqueLock;

    public void Init(PieceData pieceData)
    {
        var basicData = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;
        var playerDB = DataBaseManager.Instance.GetDataBase<PlayerDataBase>() as PlayerDataBase;

        var characterData = pieceData.PuzzleData.CharacterData;
        
        var addStatList = playerDB.GetPlayerCharacterAddStatList();
        var findStatData = addStatList.Find(v => v.Uid == pieceData.Uid);

        var resultAttack = characterData.Attack;
        var resultAttackSpeed = characterData.BasicSkill.AttackCycle;
        var resultClassCoolDown = characterData.ClassSkill.CoolDownTime;
        
        if (findStatData != null)
        {
            resultAttack += findStatData.Attack;

            if (findStatData?.AttackSpeed > 0)
                resultAttackSpeed /= 1 + findStatData.AttackSpeed / 100;
            if (findStatData?.SkillCoolTime > 0)
                resultClassCoolDown *= (100 - findStatData.SkillCoolTime) / 100;
        }
        
        //스킬 이미지
        attackImage.sprite = AtlasManager.Instance.GetIconSprite(characterData.BasicSkill.IconName);
        classImage.sprite = AtlasManager.Instance.GetIconSprite(characterData.ClassSkill.IconName);
        uniqueImage.sprite = AtlasManager.Instance.GetIconSprite(characterData.UniqueSkill.IconName);

        //스킬 이름
        attackName.text = characterData.BasicSkill.Name;
        className.text = characterData.ClassSkill.Name;
        uniqueName.text = characterData.UniqueSkill.Name;  

        //스킬 쿨타임
        attackCoolDown.text = resultAttackSpeed.ToString("N2") + " 초";
        classCoolDown.text = resultClassCoolDown.ToString("N2") + " 초";
        uniqueCoolDown.text = characterData.UniqueSkill.CoolDownTime.ToString("N2") + " 초" ;

        //공격스킬 설명
        attackDesc.text = characterData.BasicSkill.Desc;
        //클래스 스킬 설명
        classDesc.text = characterData.ClassSkill.Desc;
        //클래스 스킬 데미지
        if (characterData.ClassSkill.Damage != 0)
            classDesc.text = classDesc.text.Replace("(damage)", characterData.ClassSkill.Damage.ToString());
        else if(characterData.ClassSkill.IsSummons)
            classDesc.text = classDesc.text.Replace("(damage)", characterData.ClassSkill.AppliedSummonsData.Damage.ToString());
        if(characterData.ClassSkill.Heal != 0)
            classDesc.text = classDesc.text.Replace("(heal)", characterData.ClassSkill.Heal.ToString());
        if(characterData.ClassSkill.barrier != 0)
            classDesc.text = classDesc.text.Replace("(appliedValue)", characterData.ClassSkill.barrier.ToString());
        //클래스 스킬 계수
        if (!characterData.BasicSkill.IsSummons)
            classDesc.text = classDesc.text.Replace("attack",
                $"{resultAttack * characterData.ClassSkill.DamageCoefficient:0.##}");
        else if(characterData.BasicSkill.IsSummons)
            classDesc.text = classDesc.text.Replace("attack",
                $"{resultAttack * characterData.ClassSkill.AppliedSummonsData.DamageCoefficient:0.##}");
        //유니크 스킬 설명
        uniqueDesc.text = characterData.UniqueSkill.Desc;
        //유니크 스킬 데미지
        if (characterData.UniqueSkill.Damage != 0)
            uniqueDesc.text = uniqueDesc.text.Replace("(damage)", characterData.UniqueSkill.Damage.ToString());
        else if(characterData.UniqueSkill.IsSummons)
            uniqueDesc.text = uniqueDesc.text.Replace("(damage)", characterData.UniqueSkill.AppliedSummonsData.Damage.ToString());
        if(characterData.UniqueSkill.Heal != 0)
            uniqueDesc.text = uniqueDesc.text.Replace("(heal)", characterData.UniqueSkill.Heal.ToString());
        if(characterData.UniqueSkill.barrier != 0)
            uniqueDesc.text = uniqueDesc.text.Replace("(appliedValue)", characterData.UniqueSkill.barrier.ToString());
        //유니크 스킬 계수
        if (!characterData.UniqueSkill.IsSummons)
            uniqueDesc.text = uniqueDesc.text.Replace("attack",
                $"{resultAttack * characterData.UniqueSkill.DamageCoefficient:0.##}");
        else if( characterData.UniqueSkill.IsSummons)
            uniqueDesc.text = uniqueDesc.text.Replace("attack",
                $"{resultAttack * characterData.UniqueSkill.AppliedSummonsData.DamageCoefficient:0.##}");
        
        if (characterData.Level >= (int) basicData.BasicSkillUnlockLevel())
            attackLock.SetActive(false);
        if (characterData.Level >= (int) basicData.ClassSkillUnlockLevel())
            classLock.SetActive(false);
        if (characterData.Level >= (int) basicData.UniqueSkillUnlockLevel())
            uniqueLock.SetActive(false);
    }
    
    public void Init(PuzzleData data)
    {
        var basicData = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;

        float resultAttack = data.CharacterData.Attack;
        float resultAttackSpeed = data.CharacterData.BasicSkill.AttackCycle;
        float resultClassCoolDown = data.CharacterData.ClassSkill.CoolDownTime;

        //스킬 이미지
        attackImage.sprite = AtlasManager.Instance.GetIconSprite(data.CharacterData.BasicSkill.IconName);
        classImage.sprite = AtlasManager.Instance.GetIconSprite(data.CharacterData.ClassSkill.IconName);
        uniqueImage.sprite = AtlasManager.Instance.GetIconSprite(data.CharacterData.UniqueSkill.IconName);

        //스킬 이름
        attackName.text = data.CharacterData.BasicSkill.Name;
        className.text = data.CharacterData.ClassSkill.Name;
        uniqueName.text = data.CharacterData.UniqueSkill.Name;  

        //스킬 쿨타임
        attackCoolDown.text = resultAttackSpeed.ToString("N2") + " 초";
        classCoolDown.text = resultClassCoolDown.ToString("N2") + " 초";
        uniqueCoolDown.text = data.CharacterData.UniqueSkill.CoolDownTime.ToString("N2") + " 초" ;

        //공격스킬 설명
        attackDesc.text = data.CharacterData.BasicSkill.Desc;
        //클래스 스킬 설명
        classDesc.text = data.CharacterData.ClassSkill.Desc;
        //클래스 스킬 데미지
        if (data.CharacterData.ClassSkill.Damage != 0)
            classDesc.text = classDesc.text.Replace("(damage)", data.CharacterData.ClassSkill.Damage.ToString());
        else if(data.CharacterData.ClassSkill.IsSummons)
            classDesc.text = classDesc.text.Replace("(damage)", data.CharacterData.ClassSkill.AppliedSummonsData.Damage.ToString());
        if(data.CharacterData.ClassSkill.Heal != 0)
            classDesc.text = classDesc.text.Replace("(heal)", data.CharacterData.ClassSkill.Heal.ToString());
        if(data.CharacterData.ClassSkill.barrier != 0)
            classDesc.text = classDesc.text.Replace("(appliedValue)", data.CharacterData.ClassSkill.barrier.ToString());
        //클래스 스킬 계수
        if (!data.CharacterData.BasicSkill.IsSummons)
            classDesc.text = classDesc.text.Replace("attack",
                (resultAttack * data.CharacterData.ClassSkill.DamageCoefficient).ToString());
        else if(data.CharacterData.BasicSkill.IsSummons)
            classDesc.text = classDesc.text.Replace("attack",
                (resultAttack * data.CharacterData.ClassSkill.AppliedSummonsData.DamageCoefficient).ToString());
        //유니크 스킬 설명
        uniqueDesc.text = data.CharacterData.UniqueSkill.Desc;
        //유니크 스킬 데미지
        if (data.CharacterData.UniqueSkill.Damage != 0)
            uniqueDesc.text = uniqueDesc.text.Replace("(damage)", data.CharacterData.UniqueSkill.Damage.ToString());
        else if(data.CharacterData.UniqueSkill.IsSummons)
            uniqueDesc.text = uniqueDesc.text.Replace("(damage)", data.CharacterData.UniqueSkill.AppliedSummonsData.Damage.ToString());
        if(data.CharacterData.UniqueSkill.Heal != 0)
            uniqueDesc.text = uniqueDesc.text.Replace("(heal)", data.CharacterData.UniqueSkill.Heal.ToString());
        if(data.CharacterData.UniqueSkill.barrier != 0)
            uniqueDesc.text = uniqueDesc.text.Replace("(appliedValue)", data.CharacterData.UniqueSkill.barrier.ToString());
        //유니크 스킬 계수
        if (!data.CharacterData.UniqueSkill.IsSummons)
            uniqueDesc.text = uniqueDesc.text.Replace("attack",
                (resultAttack * data.CharacterData.UniqueSkill.DamageCoefficient).ToString());
        else if( data.CharacterData.UniqueSkill.IsSummons)
            uniqueDesc.text = uniqueDesc.text.Replace("attack",
                (resultAttack * data.CharacterData.UniqueSkill.AppliedSummonsData.DamageCoefficient).ToString());

        if (data.CharacterData.Level >= (int) basicData.BasicSkillUnlockLevel())
            attackLock.SetActive(false);
        if (data.CharacterData.Level >= (int) basicData.ClassSkillUnlockLevel())
            classLock.SetActive(false);
        if (data.CharacterData.Level >= (int) basicData.UniqueSkillUnlockLevel())
            uniqueLock.SetActive(false);
    }
}
