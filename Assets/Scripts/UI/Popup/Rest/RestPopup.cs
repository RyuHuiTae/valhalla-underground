using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RestPopup : CommonPopup
{
    private bool puzzleInfo = false;

    [SerializeField] private PieceControl materialPiece_1;
    [SerializeField] private PieceControl materialPiece_2;
    [SerializeField] private GameObject materialSlot_1;
    [SerializeField] private GameObject materialSlot_2;
    [SerializeField] private Image materialSlotImage_1;
    [SerializeField] private Image materialSlotImage_2;
    [SerializeField] private Image purifyImage;
    [SerializeField] private Image recreateImage;

    [SerializeField] private UITweenColor purifyButtonTween;
    [SerializeField] private UITweenColor recreateButtonTween;
    
    [SerializeField] private BoxCollider purifyCollider;
    [SerializeField] private BoxCollider recreateCollider;

    [SerializeField] private AnimationClip purifyAnim1;
    [SerializeField] private AnimationClip purifyAnim2;
    [SerializeField] private AnimationClip recreateAnim1;
    [SerializeField] private AnimationClip recreateAnim2;

    //private bool material_1;
    //private bool material_2;

    private List<Collision> collisionList = new List<Collision>();

    private TopUI topUI;
    private InventoryPopup inventoryPopup;

    //private bool purifyCheck;
    private int purifyCount;
    private int purifyCost;
    private int purifyAddCost;

    private bool recreateCheck;
    //private int recreateCount;

    private const int RANDOM_REWARD_ID = 4;

    private void Start()
    {
        this.purifyCount = 0;
        this.puzzleInfo = false;
        this.recreateCheck = false;
        
        if (DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB)
        {
            //휴식 스테이지가 닫힐때까지 플레이어 디비 업데이트 제한
            playerDB.SetUpdateFlag(false);
        }

        if (DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB)
        {
            purifyCost = (int) basicDB.PollutionClearCost();
            purifyAddCost = (int) basicDB.PollutionClearCostAddValue();
        }

        AudioManager.Instance.BgmSound(AudioManager.BgmType.RestStage);

        OpenTopUI();
        MoveMainRoot();
        
        PopupManager.Instance.CheckTutorial(TutorialTextType.EnterRest);
    }

    public void SetEnabledCollider(bool isActive)
    {
        purifyCollider.enabled = isActive;
        recreateCollider.enabled = isActive;
    }
    
    public void RefreshRestInvenUI()
    {
        if (inventoryPopup == null)
            return;

        //갱신
        inventoryPopup.inventoryList.RefreshList();
    }

    public void CheckInfoPopupShow()
    {
        if (puzzleInfo)
        {
            inventoryPopup.inventoryList.ClickFirstSlot();
        }
    }

    private void OpenTopUI()
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        topUI.Init(TopUI.SceneTitle.Rest);
    }

    private void OpenInvenUI()
    {
        inventoryPopup = PopupManager.Instance.CreatePopup(PopupName.InventoryPopup) as InventoryPopup;

        if (inventoryPopup != null)
        {
            inventoryPopup.Init(
                transform,
                UIUtils.DEPTH_40,
                null,
                MouseDownCallBack,
                MouseUpCallBack,
                EnterCallBack,
                null,
                null,
                null);
        }
    }
    
    private void MouseDownCallBack(PieceControl pieceControl)
    {
        if (pieceControl is EnhancePiece piece)
        {
            piece.applyPoint.SetActive(false);
        }
    }

    private void MouseUpCallBack(PieceControl pieceControl)
    {
        if (collisionList.Count <= 0)
            return;

        var collision = collisionList.LastOrDefault();

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<CharacterDataBase>() is CharacterDataBase characterDB))
            return;

        // 정화
        var restPurify = collision.gameObject.GetComponent<RestPurify>();
        if (restPurify != null)
        {
            PurifyProcess();
        }

        //분해
        var restRecreate = collision.gameObject.GetComponent<RestRecreate>();
        if (restRecreate != null)
        {
            RecreateProcess();
        }

        //인벤에 되돌림
        var invenPopup = collision.gameObject.GetComponent<InventoryPopup>();
        if (invenPopup != null)
        {
            if (materialPiece_1 == pieceControl)
            {
                Destroy(materialPiece_1);
                materialPiece_1 = null;
                //material_1 = false;
                pieceControl.ReturnInventory();
            }

            if (materialPiece_2 == pieceControl)
            {
                Destroy(materialPiece_2);
                materialPiece_2 = null;
                //material_2 = false;
                pieceControl.ReturnInventory();
            }
        }

        collisionList.Clear();
    }

    private void PurifyProcess()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var curHoldSlot = inventoryPopup.inventoryList.CurHoldSlot;
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();

        var findData = playerPuzzleDataList.Find(v => v.uid == curHoldSlot.pieceData.Uid);
        if (findData == null)
            return;

        //오염도가 0 이하인 경우
        if (findData.puzzlePollution <= 0)
        {
            var confirm =
                PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "오염되지 않은 피스는 정화할 수 없습니다.");
        }
        else
        {
            var cost = purifyCost + (purifyAddCost * purifyCount);

            var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
            confirm.Init("오염도 정화", "오염도를 정화하시겠습니까?", curHoldSlot.pieceData, -1, cost, ConfirmPopupType.YesNo, () =>
            {
                if (playerDB.Soul < cost)
                {
                    var confirm1 =
                        PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as
                            ConfirmPopup;
                    confirm1.Init("알림", "소울이 부족합니다.");
                    return;
                }

                //퍼즐 오염도 변경
                findData.puzzlePollution = 0;

                //소울 감소
                playerDB.SetSoul(-cost);

                //플레이어 데이터 변경
                playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);

                RefreshRestInvenUI();

                purifyCount++;
                // if (purifyCount >= 3)
                // {
                //     purifyCheck = true;
                //     purifyImage.color = new Color(1.0f, 0.5f,0.5f, 1.0f);
                // }

                curHoldSlot.pieceData.SetPiecePollution(0);

                topUI.Refresh(TopUI.SceneTitle.Rest);
                
                var confirm2 = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                confirm2.Init("오염도 정화", "오염도 정화 성공!", curHoldSlot.pieceData);

                AudioManager.Instance.EffectSound(AudioManager.EffectType.Purification, false, 0.3f);
            });
        }
    }

    private void RecreateProcess()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<CharacterDataBase>() is CharacterDataBase characterDB))
            return;

        if (recreateCheck)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "더이상 분해할 수 없습니다.");
            return;
        }

        var curHoldSlot = inventoryPopup.inventoryList.CurHoldSlot;
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();
        var playerInstalledList = playerDB.Data.GetPlayerInstalledPuzzleData();

        var findData = playerPuzzleDataList.Find(v => v.uid == curHoldSlot.pieceData.Uid);
        if (findData == null)
            return;

        var confirm1 = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
        confirm1.Init("피스 분해", "피스를 분해하시겠습니까?", curHoldSlot.pieceData, -1, -1, ConfirmPopupType.YesNo, () =>
        {
            var materialUid = curHoldSlot.pieceData.Uid;
            var materialPuzzleData = curHoldSlot.pieceData.PuzzleData;
            var materialType = materialPuzzleData.Type;
            var materialCharacterData = materialPuzzleData.CharacterData;

            if (materialPuzzleData.Type == PuzzleType.Character)
            {
                var characterDataList = characterDB.GetDataList()
                    .Select(v => v as CharacterData)
                    .ToList();

                //현재 캐릭터 레벨의 절반
                int level = materialCharacterData.Level / 2;
                if (level <= 0)
                    level = 1;
                
                var findSameTypeLevelDataList = characterDataList
                    .Where(v => v.Level == level)
                    .Where(v => v.Playerable)
                    .ToList();

                var randomIdx1 = Random.Range(0, findSameTypeLevelDataList.Count);
                var randomIdx2 = Random.Range(0, findSameTypeLevelDataList.Count);

                var randomCharData1 = findSameTypeLevelDataList[randomIdx1];
                var randomCharData2 = findSameTypeLevelDataList[randomIdx2];

                var puzzleDataList = puzzleDB.GetDataList()
                    .Select(v => v as PuzzleData)
                    .ToList();

                var randomPuzzleData1 = puzzleDataList.Find(v => v.CharacterDataId == randomCharData1.ID);
                var newPuzzleData1 = new PlayerPuzzleData(randomPuzzleData1.ID, (int) randomPuzzleData1.Type);
                var newPieceData1 = PieceData.GetNewPieceData(randomPuzzleData1, newPuzzleData1);

                var randomPuzzleData2 = puzzleDataList.Find(v => v.CharacterDataId == randomCharData2.ID);
                var newPuzzleData2 = new PlayerPuzzleData(randomPuzzleData2.ID, (int) randomPuzzleData2.Type);
                var newPieceData2 = PieceData.GetNewPieceData(randomPuzzleData2, newPuzzleData2);

                var findMaterialPlayerData = playerPuzzleDataList.Find(v => v.uid == materialUid);
                playerPuzzleDataList.Remove(findMaterialPlayerData);

                var findInstalledPlayerData = playerInstalledList.Find(v => v.uid == materialUid);
                if (findInstalledPlayerData != null)
                {
                    if (materialType == PuzzleType.Ability ||
                        materialType == PuzzleType.Stat)
                    {
                        foreach (var installed in playerInstalledList)
                        {
                            var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                            var isExist = installed.GetAppliedPuzzleUidList().Exists(v => v == materialUid);

                            if (isExist)
                            {
                                appliedPuzzleUidList.Remove(materialUid);
                                installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                            }
                        }
                    }

                    playerInstalledList.Remove(findInstalledPlayerData);
                    playerDB.Data.SetPlayerInstalledPuzzleData(playerInstalledList);
                }

                playerPuzzleDataList.Add(newPuzzleData1);
                playerPuzzleDataList.Add(newPuzzleData2);

                playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);

                RefreshRestInvenUI();

                var confirm2 = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                confirm2.Init("피스 분해", "피스 분해 성공!", newPieceData1);

                var confirm3 = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                confirm3.Init("피스 분해", "피스 분해 성공!", newPieceData2);
            }
            else
            {
                var findDataList = puzzleDB.GetDataList()
                    .Select(v => v as PuzzleData)
                    .Where(v => v.Type == materialPuzzleData.Type)
                    .ToList();

                var randomIdx1 = Random.Range(0, findDataList.Count);
                var randomIdx2 = Random.Range(0, findDataList.Count);

                var randomPuzzleData1 = findDataList[randomIdx1];
                var randomPuzzleData2 = findDataList[randomIdx2];

                var newPuzzleData1 = new PlayerPuzzleData(randomPuzzleData1.ID, (int) randomPuzzleData1.Type);
                var newPieceData1 = PieceData.GetNewPieceData(randomPuzzleData1, newPuzzleData1);

                var newPuzzleData2 = new PlayerPuzzleData(randomPuzzleData2.ID, (int) randomPuzzleData2.Type);
                var newPieceData2 = PieceData.GetNewPieceData(randomPuzzleData2, newPuzzleData2);

                var findMaterialPlayerData = playerPuzzleDataList.Find(v => v.uid == materialUid);
                playerPuzzleDataList.Remove(findMaterialPlayerData);

                var findInstalledPlayerData = playerInstalledList.Find(v => v.uid == materialUid);
                if (findInstalledPlayerData != null)
                {
                    if (materialType == PuzzleType.Ability ||
                        materialType == PuzzleType.Stat)
                    {
                        foreach (var installed in playerInstalledList)
                        {
                            var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                            var isExist = installed.GetAppliedPuzzleUidList().Exists(v => v == materialUid);

                            if (isExist)
                            {
                                appliedPuzzleUidList.Remove(materialUid);
                                installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                            }
                        }
                    }

                    playerInstalledList.Remove(findInstalledPlayerData);
                    playerDB.Data.SetPlayerInstalledPuzzleData(playerInstalledList);
                }

                playerPuzzleDataList.Add(newPuzzleData1);
                playerPuzzleDataList.Add(newPuzzleData2);

                playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);

                RefreshRestInvenUI();

                var confirm2 = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                confirm2.Init("피스 분해", "피스 분해 성공!", newPieceData1);

                var confirm3 = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
                confirm3.Init("피스 분해", "피스 분해 성공!", newPieceData2);
            }
            
            recreateCheck = true;
            recreateImage.color = new Color(1.0f, 0.5f,0.5f, 1.0f);
        });
    }

    private void EnterCallBack(PieceControl pieceControl, Collision collision)
    {
        if (!collisionList.Contains(collision))
            collisionList.Add(collision);
    }

    public void ClickPurify()
    {
        // if (purifyCheck)
        // {
        //     var confirm1 =
        //         PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
        //     confirm1.Init("알림", "더이상 정화할 수 없습니다.");
        //     return;
        // }

        if (popupAnimation.isPlaying)
            return;

        OpenInvenUI();
        
        purifyButtonTween.ResetColor();

        puzzleInfo = true;

        popupAnimation.clip = purifyAnim1;
        popupAnimation.Play();

        WaitAnimation(() =>
        {
            //inventoryPopup.gameObject.SetActive(true);
            //inventoryPopup.SetEnabledCollider(false);
            //inventoryPopup.inventoryList.ClickFirstSlot();
        });
    }

    public void ClickRecreate()
    {
        if (recreateCheck)
        {
            var confirm =
                PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "더이상 분해할 수 없습니다.");
            return;
        }

        if (popupAnimation.isPlaying)
            return;

        OpenInvenUI();
        
        recreateButtonTween.ResetColor();

        puzzleInfo = true;

        popupAnimation.clip = recreateAnim1;
        popupAnimation.Play();

        WaitAnimation(() =>
        {
            //inventoryPopup.gameObject.SetActive(true);
            //inventoryPopup.SetEnabledCollider(false);
            //inventoryPopup.inventoryList.ClickFirstSlot();
        });
    }

    public void MoveMainRoot()
    {
        if (materialPiece_1 != null)
        {
            materialPiece_1.ReturnInventory();
        }

        if (materialPiece_2 != null)
        {
            materialPiece_2.ReturnInventory();
        }

        puzzleInfo = false;

        materialPiece_1 = null;
        materialPiece_2 = null;
        materialSlot_1.SetActive(false);
        materialSlot_2.SetActive(false);

        inventoryPopup?.OnClickCancelButton();

        if (popupAnimation.clip == purifyAnim1)
        {
            popupAnimation.clip = purifyAnim2;
            popupAnimation.Play();
        }
        else if (popupAnimation.clip == recreateAnim1)
        {
            popupAnimation.clip = recreateAnim2;
            popupAnimation.Play();
        }
    }

    public override void OnClickCancelButton()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        //플레이어 디비 업데이트 제한 해제
        playerDB.SetUpdateFlag(true);

        var list = playerDB.Data.GetPlayerStageData();
        //현재 슬롯을 찾는다
        var findData = list.Find(v => v.isCurrentSlot);
        //해당 슬롯을 진행했다고 판정
        findData.isSlotEventEnd = true;
        //스테이지 데이터 갱신
        playerDB.Data.SetPlayerStageData(list);
        //세이브 데이터 갱신 후 닫기 
        playerDB.UpdatePlayerDataBase(() =>
        {
            inventoryPopup?.OnClickCancelButton();
            base.OnClickCancelButton();

            topUI.Refresh(TopUI.SceneTitle.Stage);
            AudioManager.Instance.BgmSound(AudioManager.BgmType.StageSelect);

            var stageUI = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
            //스테이지 선택 레이어에서 선택 가능한 슬롯들 트윈 시작
            stageUI.TweenMoveAbleSlots(true);
            //더이상 선택하지 못하는 슬롯들 비활성화
            stageUI.InActiveCannotChoiceSlot();
        });
    }

    private async void WaitAnimation(Action callback)
    {
        while (popupAnimation.isPlaying)
        {
            await UniTask.WaitForEndOfFrame();
        }

        callback?.Invoke();
    }

    public void ResetRecreatSlot1()
    {
        if (materialPiece_1 != null)
        {
            materialPiece_1.ReturnInventory();
            materialPiece_1 = null;
            //material_1 = false;
            materialSlot_1.SetActive(false);
        }
    }

    public void ResetRecreatSlot2()
    {
        if (materialPiece_2 != null)
        {
            materialPiece_2.ReturnInventory();
            materialPiece_2 = null;
            //material_2 = false;
            materialSlot_2.SetActive(false);
        }
    }
}