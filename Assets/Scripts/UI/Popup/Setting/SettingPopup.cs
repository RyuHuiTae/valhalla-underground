using System;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;

public class SettingPopup : CommonPopup
{
    [SerializeField] private GameObject bgmCheck;
    [SerializeField] private GameObject effectCheck;

    private bool isDataUpdateEnd = false;

    private void Update()
    {
        bgmCheck.SetActive(AudioManager.Instance.bgm);
        effectCheck.SetActive(AudioManager.Instance.effect);
    }

    public void ClickTitleButton()
    {
        var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
        confirm.Init("알림", "정말로 포기하시겠습니까?", ConfirmPopupType.YesNo, GameOverProcess);
    }

    private void GameOverProcess()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        playerDB.ResetPlayerDataBase(() =>
        {
            isDataUpdateEnd = true;
            StageManager.Instance.ClearStageSlotDataList();
        });

        var tween = PopupManager.Instance.CreatePopup(PopupName.TweenPopup, UIUtils.DEPTH_60) as TweenPopup;
        tween.Init(false, 3f, async () =>
        {
            //데이터 리셋까지 대기
            while (!isDataUpdateEnd)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(1f));
            }

            PopupManager.Instance.HideAllPopup(false);
            LayerManager.Instance.ChangeLayer(Layer.TitleLayer);
            
            var title = LayerManager.Instance.GetCurrentLayer() as TitleLayer;
            title.Init();
        });
    }

    public void ClickBgmSet()
    {
        if (AudioManager.Instance.bgm)
        {
            AudioManager.Instance.bgm = false;
            AudioManager.Instance.bgmSource.mute = false;
        }
        else if (!AudioManager.Instance.bgm)
        {
            AudioManager.Instance.bgm = true;
            AudioManager.Instance.bgmSource.mute = true;
        }
    }

    public void ClickEffectSet()
    {
        if (AudioManager.Instance.effect)
        {
            AudioManager.Instance.effect = false;
        }
        else if (!AudioManager.Instance.effect)
        {
            AudioManager.Instance.effect = true;
        }
    }
}