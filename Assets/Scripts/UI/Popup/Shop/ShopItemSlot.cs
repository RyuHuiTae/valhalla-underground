using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemSlot : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI priceText;

    public PieceData Data { get; private set; }
    public int Price { get; private set; }
    public bool IsBuy { get; private set; }
    
    private Action<ShopItemSlot> setCurrentSlot;

    public void Init(PieceData data, int price, int shape, Action<ShopItemSlot> setCurrentSlot)
    {
        this.Data = data;
        this.Price = price;
        this.nameText.text = data.PuzzleData.Name;
        this.priceText.text = price.ToString();
        this.setCurrentSlot = setCurrentSlot;
        this.IsBuy = false;
        
        this.image.sprite = AtlasManager.Instance.GetIconSprite(data.PuzzleData.IconName);
    }

    public void ClickSlot()
    {
        setCurrentSlot?.Invoke(this);
    }

    public void BuyProcess()
    {
        IsBuy = true;
        gameObject.SetActive(false);
    }
}