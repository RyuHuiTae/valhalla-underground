using System;
using System.Collections.Generic;
using System.Linq;
using Coffee.UIExtensions;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShopPopup : CommonPopup
{
    private bool puzzleInfo = false;
    private bool slotInfo = false;
    
    [SerializeField] private ShopItemSlot slotPrefab;
    [SerializeField] private Transform slotRoot;
    [SerializeField] private Transform[] slotPos;
    
    [SerializeField] private GameObject mainRoot;
    [SerializeField] private GameObject buyRoot;
    [SerializeField] private GameObject sellRoot;
    [SerializeField] private GameObject enhanceRoot;
    [SerializeField] private BoxCollider sellCollider;
    
    [SerializeField] private UITweenColor buyButtonTween;
    [SerializeField] private UITweenColor sellButtonTween;
    [SerializeField] private UITweenColor enhanceButtonTween;

    [SerializeField] private AnimationClip buyAnim1;
    [SerializeField] private AnimationClip buyAnim2;
    [SerializeField] private AnimationClip sellAnim1;
    [SerializeField] private AnimationClip sellAnim2;
    [SerializeField] private AnimationClip enhanceAnim1;
    [SerializeField] private AnimationClip enhanceAnim2;
    
    private List<ShopItemSlot> shopItemSlotList = new List<ShopItemSlot>();
    private List<Collision> collisionList = new List<Collision>();

    private TopUI topUI;
    private InventoryPopup invenUI;
    private ShopItemSlot curSlot = null;

    private float[] characterLevelPrice;
    private float[] characterEnhancePrice;

    private List<PlayerStageData> playerStageDataList;
    private PlayerStageData currentSlotData;

    public void Init(bool isForce)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDatabase))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BuyPieceDataBase>() is BuyPieceDataBase buyPieceDataBase))
            return;

        var buyPieceRandomList = DataBaseManager.Instance.GetDataList<BuyPieceRandomDataBase>();
        if (buyPieceRandomList == null)
            return;

        var randomDataIdList = new List<int>();
        AudioManager.Instance.BgmSound(AudioManager.BgmType.Shop);

        characterLevelPrice = basicDatabase.CharacterLevelPriceArray();
        characterEnhancePrice = basicDatabase.CharacterEnhancePriceArray();

        var curFloor = playerDB.Data.Floor;
        //현재 스테이지에 해당되는 데이터
        playerStageDataList = playerDB.Data.GetPlayerStageData();
        //현재 슬롯을 찾는다
        currentSlotData = playerStageDataList.Find(v => v.isCurrentSlot);

        //세이브 데이터에 저장된 데이터를 사용해서 상점 아이템 구성
        if (isForce)
        {
            var shopStageData = currentSlotData.GetPlayerShopStageData();
            for (var i = 0; i < shopStageData.randomDataIds.Length; i++)
            {
                var id = shopStageData.randomDataIds[i];
                var randomData = buyPieceRandomList.FirstOrDefault(v => v.ID == id) as BuyPieceRandomData;

                CreateShopItem(i, randomData);
            }
        }
        //랜덤으로 상점 아이템 구성
        else
        {
            var buyPieceIDArray = buyPieceDataBase.GetBuyPieceIDArray();
            buyPieceIDArray = Shuffle(buyPieceIDArray);

            for (var i = 0; i < buyPieceIDArray.Length; i++)
            {
                var randomArray = buyPieceRandomList
                    .Select(v => v as BuyPieceRandomData)
                    .Where(v=>v.Floor == curFloor)
                    .Where(v => v.BuyPieceId == buyPieceIDArray[i])
                    .ToArray();

                randomArray = Shuffle(randomArray);

                var randomData = randomArray.FirstOrDefault() as BuyPieceRandomData;
                CreateShopItem(i, randomData);

                randomDataIdList.Add(randomData.ID);
            }

            //상점에 보여지는 물품들은 랜덤이지만
            //플레이어가 상점에 진입해서 물품들을 확인한 경우
            //이 물품으로 고정하기 위해 저장
            currentSlotData.SetPlayerShopStageData(new PlayerShopStageData(randomDataIdList));

            //스테이지 데이터 갱신
            playerDB.Data.SetPlayerStageData(playerStageDataList);

            //디비 업데이트
            playerDB.UpdatePlayerDataBase(null);
        }

        //상점 스테이지가 닫힐때까지 플레이어 디비 업데이트 제한
        playerDB.SetUpdateFlag(false);

        puzzleInfo = false;
        slotInfo = false;
        OpenTopUI();
        MoveMainRoot();
        
        PopupManager.Instance.CheckTutorial(TutorialTextType.EnterShop);
    }

    private void CreateShopItem(int index, BuyPieceRandomData randomData)
    {
        var puzzleList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>();

        var findPuzzleData = puzzleList
            .FirstOrDefault(v => v.ID == randomData.PieceDataId) as PuzzleData;

        var newPieceData = PieceData.GetNewPieceData(findPuzzleData);

        var newSlot = Instantiate(slotPrefab, slotRoot);

        newSlot.transform.position = slotPos[index].position;
        newSlot.Init(newPieceData, randomData.PiecePrice, findPuzzleData.Shape, SetCurrentSlot);

        shopItemSlotList.Add(newSlot);
    }

    public void SetEnabledCollider(bool isActive)
    {
        sellCollider.enabled = isActive;
    }
    
    public void RefreshShopInvenUI()
    {
        if (invenUI == null)
            return;

        //갱신
        invenUI.inventoryList.RefreshList();
    }

    public void CheckInfoPopupShow()
    {
        if (slotInfo)
        {
            var firstSlot = shopItemSlotList.FirstOrDefault(v => !v.IsBuy);
            if (firstSlot != null)
            {
                SetCurrentSlot(firstSlot);
            }
            else invenUI.inventoryList.ClickFirstSlot();
        } 
        else if (puzzleInfo)
        {
            invenUI.inventoryList.ClickFirstSlot();
        }
    }

    private void OpenInvenUI()
    {
        invenUI = PopupManager.Instance.CreatePopup(PopupName.InventoryPopup, UIUtils.DEPTH_30) as InventoryPopup;
        if (invenUI != null)
        {
            invenUI.Init(
                transform,
                UIUtils.DEPTH_30,
                null,
                PieceMouseDownEvent,
                PieceMouseUpEvent,
                PieceCollisionEnterEvent,
                null,
                PieceCollisionExitEvent,
                null);
        }
    }
    
    private void OpenTopUI()
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        topUI.Init(TopUI.SceneTitle.Shop);
    }

    private void SetCurrentSlot(ShopItemSlot slot)
    {
        if (curSlot != null)
        {
            var stopShiny = curSlot.transform.Find("Image").gameObject;
            stopShiny.transform.GetComponent<UIShiny>().Stop();
        }   

        curSlot = slot;
        var imageShiny = curSlot.transform.Find("Image").gameObject;
        imageShiny.transform.GetComponent<UIShiny>().Play();

        var info = PopupManager.Instance.GetPopup(PopupName.PuzzleInfoPopup, UIUtils.DEPTH_30) as PuzzleInfoPopup;
        if (info != null)
        {
            info.Init(curSlot.Data);
        }
    }

    public void ClickBuyButton()
    {
        if (curSlot == null)
            return;

        var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
        confirm.Init("구매", "구매하시겠습니까?", curSlot.Data, curSlot.Price, -1, ConfirmPopupType.YesNo, BuyProcess);
    }

    private void BuyProcess()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        if (curSlot == null)
            return;

        if (invenUI == null)
            return;
        
        if (playerDB.Money < curSlot.Price)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "보유 골드가 부족합니다.");
            return;
        }
        
        var data = curSlot.Data.PuzzleData;

        //추가할 데이터 생성
        var newData = new PlayerPuzzleData(data.ID, (int) data.Type);

        //저장된 퍼즐 리스트를 받아온다
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();
        playerPuzzleDataList.Add(newData);

        //플레이어 데이터 변경
        playerDB.SetMoney(-curSlot.Price);
        playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);

        //돈 갱신
        topUI.Refresh(TopUI.SceneTitle.Shop);

        //인벤에 퍼즐 추가
        invenUI.inventoryList.AddPiece(curSlot.Data.PuzzleData, newData);

        //슬롯 구매 처리
        curSlot.BuyProcess();
        
        //갱신
        invenUI.inventoryList.RefreshList();
        
        curSlot = null;
        
        AudioManager.Instance.EffectSound(AudioManager.EffectType.Perchase, false, 0.3f);
    }

    private void SellProcess(int price)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var curHoldSlot = invenUI.inventoryList.CurHoldSlot;
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();
        var playerInstalledList = playerDB.Data.GetPlayerInstalledPuzzleData();
        
        var findData = playerPuzzleDataList.Find(v => v.uid == curHoldSlot.pieceData.Uid);
        if (findData == null) 
            return;

        //설치중인 피스라면 여기서도 삭제
        var findInstalledData = playerInstalledList.Find(v => v.uid == curHoldSlot.pieceData.Uid);
        if (findInstalledData != null)
        {
            //판 피스가 강화피스라면 찾아서 제거
            if ((PuzzleType) findData.puzzleType == PuzzleType.Ability ||
                (PuzzleType) findData.puzzleType == PuzzleType.Stat)
            {
                //판 피스의 UID
                var sellUid = findInstalledData.uid;
                //설치된 피스 리스트에서 찾는다
                foreach (var installed in playerInstalledList)
                {
                    var appliedPuzzleUidList = installed.GetAppliedPuzzleUidList();
                    bool isExists = installed.GetAppliedPuzzleUidList().Exists(v => v == sellUid);
                    
                    //해당 UID가 있으면 리스트에서 제거
                    if (isExists)
                    {
                        appliedPuzzleUidList.Remove(sellUid);
                        installed.SetAppliedPuzzleUidList(appliedPuzzleUidList);
                    }
                }
            }
            
            playerInstalledList.Remove(findInstalledData);
            playerDB.Data.SetPlayerInstalledPuzzleData(playerInstalledList);
        }
        
        //리스트에서 삭제
        playerPuzzleDataList.Remove(findData);

        //플레이어 데이터 변경
        playerDB.SetMoney(price);
        playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);
        
        //돈 갱신
        topUI.Refresh(TopUI.SceneTitle.Shop);

        //인벤에서 삭제
        invenUI.inventoryList.RemovePiece(curHoldSlot);
        
        //갱신
        invenUI.inventoryList.RefreshList();
        
        AudioManager.Instance.EffectSound(AudioManager.EffectType.Sale, false, 0.3f);
    }

    private void EnhanceProcess(int price, PieceData data)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (playerDB.Money < price)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "보유 골드가 부족합니다.");
            return;
        }

        //강화 처리
        var characterData = data.PuzzleData.CharacterData;
        var characterType = characterData.CharacterType;
        var characterLevel = characterData.Level;

        var puzzleList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>();

        //캐릭터의 다음 레벨 데이터 찾음
        var findNextCharacterData = puzzleList
            .Where(v => ((PuzzleData) v).CharacterData.CharacterType == characterType)
            .FirstOrDefault(v => ((PuzzleData) v).CharacterData.Level == (characterLevel + 1));

        if (findNextCharacterData == null)
        {
            DebugX.LogError("findNextCharacterData is NULL");
            return;
        }

        var curHoldSlot = invenUI.inventoryList.CurHoldSlot;
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();

        var findData = playerPuzzleDataList.Find(v => v.uid == curHoldSlot.pieceData.Uid);
        if (findData != null)
        {
            //퍼즐 아이디 변경
            findData.puzzleID = findNextCharacterData.ID;

            //플레이어 데이터 변경
            playerDB.SetMoney(-price);
            playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);

            //돈 갱신
            topUI.Refresh(TopUI.SceneTitle.Shop);

            //갱신
            invenUI.inventoryList.RefreshList();

            //인포가 열려있다면 새로운 데이터로 갱신함
            var infoUI = PopupManager.Instance.GetOpenedPopup(PopupName.PuzzleInfoPopup) as PuzzleInfoPopup;
            if (infoUI != null)
            {
                infoUI.Init(findNextCharacterData as PuzzleData);
            }
            
            var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
            confirm.Init("강화", "강화 성공!", PieceData.GetNewPieceData(findNextCharacterData as PuzzleData, findData));
            
            AudioManager.Instance.EffectSound(AudioManager.EffectType.Reinforce, false, 0.3f);
        }
    }
    
    private void PieceMouseDownEvent(PieceControl pieceControl)
    {
        if (pieceControl is EnhancePiece piece)
        {
            piece.applyPoint.SetActive(false);
        }
    }

    private void PieceMouseUpEvent(PieceControl pieceControl)
    {
        if (collisionList.Count <= 0)
            return;

        var collision = collisionList.LastOrDefault();
        
        //팔기
        var shopSell = collision.gameObject.GetComponent<ShopSell>();
        if (shopSell != null)
        {
            if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDataBase))
                return;
            
            var playerPuzzleData = playerDataBase.Data.GetPlayerPuzzleData();
            var puzzleDataList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>().ToList();
            var characterPieceIndexes = new List<int>();
            for (int i = 0; i < playerPuzzleData.Count; i++)
            {
                var puzzle = puzzleDataList[playerPuzzleData[i].puzzleID] as PuzzleData;
                if (puzzle.Type == PuzzleType.Character)
                {
                    characterPieceIndexes.Add(i);
                }
            }

            if (characterPieceIndexes.Count <= 1)
            {
                if (pieceControl.pieceData.PuzzleData.Type == PuzzleType.Character)
                {
                    var alram = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                    alram.Init("알림", "최소 1개의 캐릭터 피스를 남겨야 합니다.");

                    return;
                }
            }

            var price = GetSellPrice(pieceControl.pieceData);
            
            var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
            confirm.Init("판매", "판매하시겠습니까?", pieceControl.pieceData, price, -1, ConfirmPopupType.YesNo, () =>
            {
                SellProcess(price);
            });
        }
        
        //강화
        var shopEnhance = collision.gameObject.GetComponent<ShopEnhance>();
        if (shopEnhance != null)
        {
            var price = GetEnhancePrice(pieceControl.pieceData);

            if (price <= 0)
            {
                var confirm1 = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                confirm1.Init("알림", "더이상 강화할 수 없습니다.");
                return;
            }
                

            var confirm2 = PopupManager.Instance.CreatePopup(PopupName.EnhanceConfirmPopup, UIUtils.DEPTH_60) as EnhanceConfirmPopup;
            confirm2.Init("강화", "강화하시겠습니까?", pieceControl.pieceData, price, ConfirmPopupType.YesNo, () =>
            {
                EnhanceProcess(price, pieceControl.pieceData);
            });
        }

        collisionList.Clear();
    }

    private void PieceCollisionEnterEvent(PieceControl pieceControl, Collision collision)
    {
        if (!collisionList.Contains(collision))
            collisionList.Add(collision);
    }

    private void PieceCollisionExitEvent(PieceControl pieceControl, Collision collision)
    {
        if (!collisionList.Contains(collision))
            return;

        collisionList.Remove(collision);
    }

    private T[] Shuffle<T>(T[] array)
    {
        for (var i = 0; i < array.Length; ++i)
        {
            var random1 = Random.Range(0, array.Length);
            var random2 = Random.Range(0, array.Length);

            var temp = array[random1];
            array[random1] = array[random2];
            array[random2] = temp;
        }

        return array;
    }

    private int GetSellPrice(PieceData pieceData)
    {
        var basicDatabase = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;

        int price = 0;
        var cost = pieceData.PuzzleData.Cost;

        if (pieceData.PuzzleData.Type == PuzzleType.Character)
        {
            var level = pieceData.PuzzleData.CharacterData.Level;
            var ceilValue = Mathf.Ceil(characterLevelPrice[level - 1] * 10);

            price = (int) (ceilValue * ((cost * basicDatabase.SellCostValue()) + basicDatabase.CharacterBasePrice()) / 2) / 10;
            return price;
        }
        else if (pieceData.PuzzleData.Type == PuzzleType.Ability)
        {
            if (pieceData.PiecePollution >= (int) basicDatabase.PollutionMaxValue())
                return price;

            price = (int) (basicDatabase.EnhanceLevelPrice() *
                ((cost * basicDatabase.SellCostValue()) + basicDatabase.EnhancePuzzleBasePrice()) / 2);

            return price;
        }
        else if (pieceData.PuzzleData.Type == PuzzleType.Stat)
        {
            //DebugX.LogError("스탯 피스 추가 작업 필요");
            
            if (pieceData.PiecePollution >= (int) basicDatabase.PollutionMaxValue())
                return price;

            price = (int) (basicDatabase.EnhanceLevelPrice() *
                ((cost * basicDatabase.SellCostValue()) + basicDatabase.EnhancePuzzleBasePrice()) / 2);

            return price;
        }

        return price;
    }

    private int GetEnhancePrice(PieceData pieceData)
    {
        int price = 0;

        var level = pieceData.PuzzleData.CharacterData.Level;
        if (level > 0 && level < 10)
        {
            var ceilValue = Mathf.Ceil(characterEnhancePrice[level - 1] * 10);
            price = (int) ceilValue / 10;
            return price;
        }

        return price;
    }

    public void MoveMainRoot()
    {
        puzzleInfo = false;
        slotInfo = false;

        invenUI?.OnClickCancelButton();

        if (popupAnimation.clip == buyAnim1)
        {
            popupAnimation.clip = buyAnim2;
            popupAnimation.Play();
        }
        else if (popupAnimation.clip == sellAnim1)
        {
            popupAnimation.clip = sellAnim2;
            popupAnimation.Play();
        }
        else if (popupAnimation.clip == enhanceAnim1)
        {
            popupAnimation.clip = enhanceAnim2;
            popupAnimation.Play();
        }
    }

    public void ClickBuyRoot()
    {
        if (popupAnimation.isPlaying)
            return;
        
        OpenInvenUI();
        
        var firstSlot = shopItemSlotList.FirstOrDefault(v => !v.IsBuy);
        if (firstSlot != null)
        {
            SetCurrentSlot(firstSlot);
        }
        else invenUI.inventoryList.ClickFirstSlot();
        
        buyButtonTween.ResetColor();

        slotInfo = true;

        popupAnimation.clip = buyAnim1;
        popupAnimation.Play();

        WaitAnimation(() =>
        {
            // var firstSlot = shopItemSlotList.FirstOrDefault(v => !v.IsBuy);
            // if (firstSlot != null)
            // {
            //     SetCurrentSlot(firstSlot);
            // }
            // else invenUI.inventoryList.ClickFirstSlot();
        });
    }

    public void ClickSellRoot()
    {
        if (popupAnimation.isPlaying)
            return;
        
        OpenInvenUI();
        
        sellButtonTween.ResetColor();
        
        puzzleInfo = true;

        popupAnimation.clip = sellAnim1;
        popupAnimation.Play();

        WaitAnimation(() =>
        {
            //invenUI.inventoryList.ClickFirstSlot();
        });
    }

    public void ClickEnhanceRoot()
    {
        if (popupAnimation.isPlaying)
            return;
        
        OpenInvenUI();
        
        enhanceButtonTween.ResetColor();

        puzzleInfo = true;

        popupAnimation.clip = enhanceAnim1;
        popupAnimation.Play();

        WaitAnimation(() =>
        {
            //invenUI.gameObject.SetActive(true);
            //invenUI.SetEnabledCollider(false);

            //invenUI.inventoryList.ClickFirstSlot();
        });
    }

    private async void WaitAnimation(Action callback)
    {
        while (popupAnimation.isPlaying)
        {
            await UniTask.WaitForEndOfFrame();
        }
        
        callback?.Invoke();
    }
    
    public override void OnClickCancelButton()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        //플레이어 디비 업데이트 제한 해제
        playerDB.SetUpdateFlag(true);

        //해당 슬롯을 진행했다고 판정
        currentSlotData.isSlotEventEnd = true;
        //스테이지 데이터 갱신
        playerDB.Data.SetPlayerStageData(playerStageDataList);
        //세이브 데이터 갱신 후 닫기 
        playerDB.UpdatePlayerDataBase(() =>
        {
            invenUI?.OnClickCancelButton();
            base.OnClickCancelButton();
            
            topUI.Refresh(TopUI.SceneTitle.Stage);
            AudioManager.Instance.BgmSound(AudioManager.BgmType.StageSelect);
            
            var stageUI = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
            //스테이지 선택 레이어에서 선택 가능한 슬롯들 트윈 시작
            stageUI.TweenMoveAbleSlots(true);
            //더이상 선택하지 못하는 슬롯들 비활성화
            stageUI.InActiveCannotChoiceSlot();
        });
    }
}