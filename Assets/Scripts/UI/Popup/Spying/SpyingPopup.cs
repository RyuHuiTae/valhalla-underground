using System;
using System.Collections.Generic;
using System.Linq;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public interface SpyingSlotData
{
    public int GetPuzzleId();
    public int GetPosIndex();
    public void SetPosIndex(int posIndex);
}

public class PlayerSlotData : SpyingSlotData
{
    public ulong uid;
    public int puzzleId;
    public ulong[] appliedPuzzleUidArray;
    public int posIndex;

    public PlayerSlotData(ulong uid, int puzzleId, ulong[] appliedPuzzleUidArray, int posIndex)
    {
        this.uid = uid;
        this.puzzleId = puzzleId;
        this.appliedPuzzleUidArray = appliedPuzzleUidArray;
        this.posIndex = posIndex;
    }
    
    public int GetPuzzleId()
    {
        return puzzleId;
    }
    
    public int GetPosIndex()
    {
        return posIndex;
    }

    public void SetPosIndex(int posIndex)
    {
        this.posIndex = posIndex;
    }
}

public class EnemySlotData : SpyingSlotData
{
    public int puzzleId;
    public int groupId;
    public BattleStageType type;
    public int posIndex;
    
    public EnemySlotData(int puzzleId, int groupId, BattleStageType type, int posIndex)
    {
        this.puzzleId = puzzleId;
        this.groupId = groupId;
        this.type = type;
        this.posIndex = posIndex;
    }
    
    public int GetPuzzleId()
    {
        return puzzleId;
    }

    public int GetPosIndex()
    {
        return posIndex;
    }
    
    public void SetPosIndex(int posIndex)
    {
        this.posIndex = posIndex;
    }
}

public class SpyingPopup : CommonPopup
{
    [SerializeField] private SpyingSlot playerSlot;
    [SerializeField] private SpyingSlot enemySlot;

    [SerializeField] private GameObject enemyPosRoot;
    
    [SerializeField] private GameObject playerPosSlotRoot;
    [SerializeField] private GameObject enemyPosSlotRoot;

    [SerializeField] private GameObject playerPosSlotEditRoot;
    [SerializeField] private GameObject enemyPosSlotEditRoot;
    
    [SerializeField] private GameObject posEditRoot;
    [SerializeField] private AnimationClip editPopupOpenAnim;
    [SerializeField] private AnimationClip editPopupCloseAnim;
    
    [SerializeField] private TextMeshProUGUI enemyText;
    [SerializeField] private GameObject finalBossSlotRoot;

    [SerializeField] private Image finalBossSkillIcon1;
    [SerializeField] private Image finalBossSkillIcon2;
    [SerializeField] private Image finalBossSkillIcon3;

    [SerializeField] private GameObject finalBossSkillLockIcon1;
    [SerializeField] private GameObject finalBossSkillLockIcon2;
    [SerializeField] private GameObject finalBossSkillLockIcon3;

    private SpyingPosSlot[] playerPosSlot;
    private SpyingPosSlot[] enemyPosSlot;
    
    private SpyingPosSlot[] playerEditPosSlot;
    private SpyingPosSlot[] enemyEditPosSlot;
    
    private List<PlayerSlotData> playerSlotDataList = new List<PlayerSlotData>();
    private List<EnemySlotData> enemySlotDataList = new List<EnemySlotData>();

    private SpyingPosSlot curPlayerClickEditPosSlot;

    private TopUI topUI;

    private bool isOpenAnimPlay = true;
    private bool isUpdateEnd = false;
    private bool isClicked = false;

    private void Awake()
    {
        playerPosSlot = playerPosSlotRoot.GetComponentsInChildren<SpyingPosSlot>();
        enemyPosSlot = enemyPosSlotRoot.GetComponentsInChildren<SpyingPosSlot>();
        
        playerEditPosSlot = playerPosSlotEditRoot.GetComponentsInChildren<SpyingPosSlot>();
        enemyEditPosSlot = enemyPosSlotEditRoot.GetComponentsInChildren<SpyingPosSlot>();

        posEditRoot.SetActive(false);
    }

    public void Init(bool isForce)
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;

        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BattleStageDataBase>() is BattleStageDataBase battleStageDB))
            return;

        var playerStageDataList = playerDB.Data.GetPlayerStageData();
        var battleStageDataList = battleStageDB.GetDataList();

        InitPlayerSlot();
        enemySlotDataList.Clear();

        var findData = playerStageDataList.Find(v => v.isCurrentSlot);
        if (findData == null) 
            return;

        var battleStageData = findData.GetPlayerBattleStageData();
        if (battleStageData == null)
        {
            var floor = playerDB.Data.Floor;
            var stageType = findData.stageDataType;
            var level = GetStageLevel(findData);

            var findDataList = battleStageDataList
                .Select(v => v as BattleStageData)
                .Where(v => v.Floor == floor)
                .Where(v => (int) v.GetStageType() == stageType)
                .Where(v => v.Level == level)
                .ToList();

            if (findDataList.Count <= 0)
            {
                DebugX.LogError("이 슬롯에 해당되는 BattleStageData가 없습니다.");
                return;
            }

            //적 데이터 랜덤으로 결정됨
            var randomData = findDataList[Random.Range(0, findDataList.Count)];

            InitEnemyStageData(randomData);

            findData.SetPlayerBattleStageData(new PlayerBattleStageData(randomData.ID, false));
            playerDB.Data.SetPlayerStageData(playerStageDataList);

            playerDB.UpdatePlayerDataBase(() => isUpdateEnd = true);

            //튜토리얼 체크
            switch ((StageType)stageType)
            {
                case StageType.NormalBattle:
                    PopupManager.Instance.CheckTutorial(TutorialTextType.EnterNormalBattleSpyingPopup);
                    break;
                case StageType.EliteBattle:
                    PopupManager.Instance.CheckTutorial(TutorialTextType.EnterEliteBattleSpyingPopup);
                    break;
                case StageType.Boss:
                    PopupManager.Instance.CheckTutorial(TutorialTextType.EnterBossBattleSpyingPopup);
                    break;
            }
        }
        else
        {
            var findBattleStageData = battleStageDataList
                .Find(v => v.ID == battleStageData.battleStageDataId) as BattleStageData;

            InitEnemyStageData(findBattleStageData);

            isUpdateEnd = true;

            //염탐이 강제로 오픈된 상태고
            //이미 전투가 시작된 상태라면
            if (isForce && battleStageData.isStartBattle)
            {
                isOpenAnimPlay = false;
                //염탐 바로 건너뛰고 전투 진행
                ClickEnterButton();
            }
        }
        
    }

    private void SetFinalBossSkillSlot(int puzzleId)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;

        var puzzleDataList = puzzleDB.GetDataList();
        var findPuzzleData = puzzleDataList.Find(v => v.ID == puzzleId) as PuzzleData;
        var findCharacterData = findPuzzleData.CharacterData;

        enemyText.text = "???";

        // if (findCharacterData.BasicSkill.ID == (int)basicDB.FinalBossBasicSkillId1())
        // {
        //     finalBossSkillIcon1.sprite = AtlasManager.Instance.GetIconSprite(findCharacterData.BasicSkill.IconName);
        //     finalBossSkillLockIcon1.SetActive(false);
        // }
        // else if (findCharacterData.BasicSkill.ID == (int)basicDB.FinalBossBasicSkillId2())
        // {
        //     finalBossSkillIcon1.sprite = AtlasManager.Instance.GetIconSprite(findCharacterData.BasicSkill.IconName);
        //     finalBossSkillLockIcon1.SetActive(false);
        //     
        //     finalBossSkillIcon2.sprite = AtlasManager.Instance.GetIconSprite(findCharacterData.ClassSkill.IconName);
        //     finalBossSkillLockIcon2.SetActive(false);
        // }
        // else if (findCharacterData.BasicSkill.ID == (int)basicDB.FinalBossBasicSkillId3())
        // {
        //     finalBossSkillIcon1.sprite = AtlasManager.Instance.GetIconSprite(findCharacterData.BasicSkill.IconName);
        //     finalBossSkillLockIcon1.SetActive(false);
        //     
        //     finalBossSkillIcon2.sprite = AtlasManager.Instance.GetIconSprite(findCharacterData.ClassSkill.IconName);
        //     finalBossSkillLockIcon2.SetActive(false);
        //     
        //     finalBossSkillIcon3.sprite = AtlasManager.Instance.GetIconSprite(findCharacterData.UniqueSkill.IconName);
        //     finalBossSkillLockIcon3.SetActive(false);
        // }
    }

    private int GetStageLevel(PlayerStageData data)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return 0;

        switch ((StageType) data.stageDataType)
        {
            case StageType.NormalBattle:
                return data.stageNormalLevel;
            case StageType.EliteBattle:
                return data.stageEliteLevel;
            case StageType.Boss:
                return data.stageBossLevel;
            case StageType.FinalBoss:
                return playerDB.Data.FinalBossLevel;
        }

        return 0;
    }

    private void InitSpyingPosSlot(SpyingPosSlot[] slotArray, IEnumerable<SpyingSlotData> slotDataList, Action<SpyingPosSlot> clickPosSlot)
    {
        for (var i = 0; i < slotArray.Length; i++)
        {
            var slot = slotArray[i];
            slot.Init(i, clickPosSlot);
        }

        foreach (var slotData in slotDataList)
        {
            var posIdx = slotData.GetPosIndex();
            slotArray[posIdx].SetSlotData(slotData);
        }
    }

    public void InitPlayerSlot()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        // if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
        //     return;
        
        playerSlotDataList.Clear();

        var playerPuzzleList = playerDB.Data.GetPlayerPuzzleData();
        var playerInstalledPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();
        var posIndexList = playerDB.Data.GetPuzzlePosIndexData();
        
        // var newIndex = 0;
        //
        // //최대 장착 가능한 캐릭터 수 만큼 반복
        // while (posIndexList.Count < (int)basicDB.CharacterUseMaxCount())
        // {
        //     if (!posIndexList.Contains(newIndex))
        //     {
        //         //비어있는 인덱스 찾아서 추가
        //         posIndexList.Add(newIndex);
        //     }
        //
        //     newIndex++;
        // }
        
        var posIndexQueue = new Queue<int>();
        foreach (var posIndex in posIndexList)
        {
            posIndexQueue.Enqueue(posIndex);
        }
        
        foreach (var data in playerInstalledPuzzleList)
        {
            var findData = playerPuzzleList.Find(v => v.uid == data.uid);
            if (findData.puzzleType == (int) PuzzleType.Character)
            {
                var uid = data.uid;

                var puzzleId = findData.puzzleID;
                var appliedPuzzleUidArray = data.appliedPuzzleUidArray;

                playerSlotDataList.Add(new PlayerSlotData(uid, puzzleId, appliedPuzzleUidArray, posIndexQueue.Dequeue()));
            }
        }

        InitSpyingPosSlot(playerPosSlot, playerSlotDataList, ClickPlayerPosSlot);

        var firstSlotData = playerSlotDataList.FirstOrDefault();
        if (firstSlotData != null)
        {
            playerSlot.Init(firstSlotData.uid, firstSlotData.puzzleId, firstSlotData.appliedPuzzleUidArray);    
        }
        else playerSlot.Init(0, 0, null);
    }

    private void InitEnemyStageData(BattleStageData battleStageData)
    {
        var puzzleIDArray = battleStageData.GetEnemyPuzzleIdArray();
        var enhanceGroupIDArray = battleStageData.GetEnhanceGroupIdArray();
        var posIndexArray = battleStageData.GetEnemyPosIndexArray();
        
        for (var i = 0; i < puzzleIDArray.Length; i++)
        {
            var puzzleId = puzzleIDArray[i];
            var groupId = enhanceGroupIDArray[i];
            var posIndex = posIndexArray[i];
            
            enemySlotDataList.Add(new EnemySlotData(puzzleId, groupId, battleStageData.Type, posIndex));
        }

        InitSpyingPosSlot(enemyPosSlot, enemySlotDataList, ClickEnemyPosSlot);
        
        var first = enemySlotDataList.FirstOrDefault();
        if (first != null)
        {
            enemySlot.Init(first.puzzleId, first.groupId, first.type);
        }

        // if (battleStageData.Type == BattleStageType.FinalBoss)
        // {
        //     SetFinalBossSkillSlot(puzzleIDArray.FirstOrDefault());
        // }

        //enemyPosRoot.SetActive(battleStageData.Type != BattleStageType.FinalBoss);
        //finalBossSlotRoot.SetActive(battleStageData.Type == BattleStageType.FinalBoss);
    }
    
    private void ClickPlayerPosSlot(SpyingPosSlot clickSlot)
    {
        if (clickSlot.SpyingSlotData == null)
            return;
            
        if (clickSlot.SpyingSlotData is PlayerSlotData curSpyingSlotData)
            playerSlot.Init(curSpyingSlotData.uid, curSpyingSlotData.puzzleId, curSpyingSlotData.appliedPuzzleUidArray);
    }
    
    private void ClickEnemyPosSlot(SpyingPosSlot clickSlot)
    {
        if (clickSlot.SpyingSlotData == null)
            return;
            
        if (clickSlot.SpyingSlotData is EnemySlotData curSpyingSlotData)
            enemySlot.Init(curSpyingSlotData.puzzleId, curSpyingSlotData.groupId, curSpyingSlotData.type);
    }
    
    private void ClickPlayerEditPosSlot(SpyingPosSlot clickSlot)
    {
        if (curPlayerClickEditPosSlot != null)
        {
            var clickPosSlotData = clickSlot.SpyingSlotData;
            var curPosSlotData = curPlayerClickEditPosSlot.SpyingSlotData;
            
            clickSlot.ChangeSlot(curPosSlotData);
            curPlayerClickEditPosSlot.ChangeSlot(clickPosSlotData);

            foreach (var slot in playerEditPosSlot)
            {
                slot.SelectIconTween(false);
            }
            
            this.curPlayerClickEditPosSlot = null;
        }
        else
        {
            if (clickSlot.SpyingSlotData == null)
                return;
            
            foreach (var slot in playerEditPosSlot)
            {
                if (slot == clickSlot)
                    continue;
                
                slot.SelectIconTween(true);
            }
            
            this.curPlayerClickEditPosSlot = clickSlot;
        }
    }

    public void ClickEnterButton()
    {
        if (!isUpdateEnd)
        {
            DebugX.Log("DB 업데이트 진행 중..");
            return;
        }

        if (isClicked)
            return;

        if (isOpenAnimPlay)
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        var basicDB = DataBaseManager.Instance.GetDataBase<BasicDataBase>() as BasicDataBase;

        var pollutionList = new List<PlayerPuzzleData>();

        var characterCount = 0;

        var puzzleList = playerDB.Data.GetPlayerPuzzleData();
        var installedPuzzleList = playerDB.Data.GetPlayerInstalledPuzzleData();

        var playerStageDataList = playerDB.Data.GetPlayerStageData();

        foreach (var data in installedPuzzleList)
        {
            var findPlayerPuzzleData = puzzleList.Find(v => v.uid == data.uid);
            if (findPlayerPuzzleData == null)
            {
                DebugX.LogError("보드판에 설치된 퍼즐과 인벤에 있는 퍼즐이 매칭되지 않음");
                continue;
            }

            if ((PuzzleType) findPlayerPuzzleData.puzzleType == PuzzleType.Character)
            {
                characterCount++;
            }

            if ((PuzzleType) findPlayerPuzzleData.puzzleType == PuzzleType.Ability ||
                (PuzzleType) findPlayerPuzzleData.puzzleType == PuzzleType.Stat)
            {
                if (findPlayerPuzzleData.puzzlePollution >= (int) basicDB.PollutionMaxValue())
                {
                    pollutionList.Add(findPlayerPuzzleData);
                }
            }
        }

        if (characterCount <= 0)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "하나 이상의 캐릭터 피스 배치가 필요합니다.", ConfirmPopupType.Ok, () => { topUI.ClickPuzzleButton(); });
            return;
        }

        if (pollutionList.Count > 0)
        {
            var firstData = pollutionList.FirstOrDefault();
            var findPuzzleData = puzzleDB.GetDataList().FirstOrDefault(v => v.ID == firstData.puzzleID) as PuzzleData;

            var confirm = PopupManager.Instance.CreatePopup(PopupName.PollutionConfirmPopup, UIUtils.DEPTH_60) as PollutionConfirmPopup;
            confirm.Init("알림", $"오염된 피스가 당신의 발목을 붙잡고 있습니다.",
                PieceData.GetNewPieceData(findPuzzleData, firstData), pollutionList.Count);

            PopupManager.Instance.CheckTutorial(TutorialTextType.EnterBattleWithMaxPollution);
            return;
        }

        isClicked = true;

        PopupManager.Instance.HidePopup(this, true, true);
        PopupManager.Instance.HideAllPopup();

        LayerManager.Instance.ChangeLayer(Layer.BattleLayer);
        var battleLayer = LayerManager.Instance.GetCurrentLayer() as BattleLayer;
        battleLayer.Init();

        var findData = playerStageDataList.Find(v => v.isCurrentSlot);
        if (findData != null)
        {
            var battleStageData = findData.GetPlayerBattleStageData();
            if (battleStageData != null)
            {
                var stageType = findData.stageDataType;

                switch (stageType)
                {
                    case (int) StageType.NormalBattle:
                        AudioManager.Instance.BgmSound(AudioManager.BgmType.BattleStage);
                        break;
                    case (int) StageType.EliteBattle:
                        AudioManager.Instance.BgmSound(AudioManager.BgmType.BattleStage);
                        break;
                    case (int) StageType.Boss:
                        AudioManager.Instance.BgmSound(AudioManager.BgmType.BossStage);
                        break;
                    case (int) StageType.FinalBoss:
                        AudioManager.Instance.BgmSound(AudioManager.BgmType.LastBossBattle);
                        break;
                }
            }
        }
    }

    public void ClickRunButton()
    {
        if (isClicked)
            return;

        if (isOpenAnimPlay)
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        //플레이어 스테이지 데이터에서
        var list = playerDB.Data.GetPlayerStageData();
        //현재 스테이지(슬롯)를 찾는다
        var findData = list.Find(v => v.isCurrentSlot);

        if (findData == null)
            return;

        var stageType = (StageType) findData.stageDataType;

        //스테이지 타입이 노말, 엘리트 전투일때 
        if (stageType == StageType.NormalBattle || stageType == StageType.EliteBattle)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("경고", "다음 스테이지로 도망치시겠습니까?", ConfirmPopupType.YesNo, () =>
            {
                isClicked = true;

                //해당 슬롯을 진행했다고 판정
                findData.isSlotEventEnd = true;
                //목숨 감소
                //playerDB.SetHeart(-1);

                //스테이지 데이터 갱신
                playerDB.Data.SetPlayerStageData(list);
                //디비 업데이트
                playerDB.UpdatePlayerDataBase(null);

                //상단UI 갱신
                topUI.Refresh(TopUI.SceneTitle.Stage);
                //염탐 닫음
                PopupManager.Instance.HidePopup(this);

                var stageUI = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
                //스테이지 선택 레이어에서 선택 가능한 슬롯들 트윈 시작
                stageUI.TweenMoveAbleSlots(true);
                //더이상 선택하지 못하는 슬롯들 비활성화
                stageUI.InActiveCannotChoiceSlot();
            });
        }
        else
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "도망칠 수 없습니다.");
        }
    }

    public void InitPlayerSpyingEditPosSlot()
    {
        InitSpyingPosSlot(playerEditPosSlot, playerSlotDataList, ClickPlayerEditPosSlot);
    }
    
    public void ClickEditUIButton()
    {
        if (isOpenAnimPlay)
            return;
        
        popupAnimation.clip = editPopupOpenAnim;
        popupAnimation.Play();

        InitPlayerSpyingEditPosSlot();
        InitSpyingPosSlot(enemyEditPosSlot, enemySlotDataList, null);
    }
    
    public void ClickEditSaveButton()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        foreach (var slot in playerEditPosSlot)
        {
            slot.SpyingSlotData?.SetPosIndex(slot.Index);
        }

        var posIndexArray = playerSlotDataList
            .Select(slotData => slotData.posIndex)
            .ToArray();
        
        playerDB.Data.SetPuzzlePosIndexData(posIndexArray);

        InitPlayerSlot();
        ClickEditCancelButton();
    }
    
    public void ClickEditCancelButton()
    {
        popupAnimation.clip = editPopupCloseAnim;
        popupAnimation.Play();
        
        curPlayerClickEditPosSlot = null;
    }
    
    protected override void ExitAnimEndEvent()
    {
        var battleLayer = LayerManager.Instance.GetCurrentLayer() as BattleLayer;
        if (battleLayer != null)
        {
            CloseAnimCallback();
        }
    }

    private void CloseAnimCallback()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var playerStageDataList = playerDB.Data.GetPlayerStageData();
        var findData = playerStageDataList.Find(v => v.isCurrentSlot);

        if (findData != null)
        {
            //전투가 시작됐다는 플래그 세팅
            var playerBattleStageData = findData.GetPlayerBattleStageData();
            playerBattleStageData.isStartBattle = true;

            findData.SetPlayerBattleStageData(playerBattleStageData);
            playerDB.Data.SetPlayerStageData(playerStageDataList);

            playerDB.UpdatePlayerDataBase(null);
        }

        GameField.Instance.BattleStart();
    }

    private void OpenAnimPlayEnd()
    {
        this.isOpenAnimPlay = false;
    }
}