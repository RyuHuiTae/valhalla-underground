using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SpyingPosSlot : MonoBehaviour
{
    public Image icon;
    public Image iconBack;
    public Image selectIcon;
    public UITweenScale selectIconTween;
    
    public int Index { get; private set; }
    public SpyingSlotData SpyingSlotData { get; private set; }

    private Action<SpyingPosSlot> clickEvent;
    
    public void Init(int index, Action<SpyingPosSlot> clickEvent)
    {
        this.Index = index;
        this.SpyingSlotData = null;
        this.clickEvent = clickEvent;
        this.iconBack.gameObject.SetActive(false);
        this.selectIcon.gameObject.SetActive(false);
    }
    
    public void SetSlotData(SpyingSlotData slotData)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        var findData = puzzleDB
            .GetDataList()
            .Select(v => v as PuzzleData)
            .FirstOrDefault(v => v.ID == slotData.GetPuzzleId());

        if (findData == null || 
            findData.ID == 0)
            return;

        this.icon.sprite = AtlasManager.Instance.GetIconSprite(findData.IconName); 
        this.iconBack.gameObject.SetActive(true);
        this.SpyingSlotData = slotData;
    }
    
    public void ChangeSlot(SpyingSlotData slotData)
    {
        this.SpyingSlotData = slotData;
        
        if (slotData != null)
        {
            SetSlotData(slotData);
        }
        else
        {
            this.iconBack.gameObject.SetActive(false);    
        }
    }

    public void SelectIconTween(bool isActive)
    {
        this.selectIcon.gameObject.SetActive(isActive);

        if (isActive)
        {
            selectIconTween.ResetValue();
            selectIconTween.TweenForward();
        }
        else
        {
            selectIconTween.Stop();
        }
    }
    
    public void ClickSlot()
    {
        clickEvent?.Invoke(this);   
    }
}
