using System.Collections.Generic;
using DevSupport;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpyingSlot : MonoBehaviour
{
    [SerializeField] private Image characterImage;
    [SerializeField] private TextMeshProUGUI characterName;
    [SerializeField] private Transform scrollRoot;
    [SerializeField] private GameObject enhancePrefab;
    [SerializeField] private GameObject hideRoot;

    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private TextMeshProUGUI hpText;
    [SerializeField] private TextMeshProUGUI attackText;
    [SerializeField] private TextMeshProUGUI defenceText;
    [SerializeField] private TextMeshProUGUI speedText;
    [SerializeField] private TextMeshProUGUI attSpeedText;
    [SerializeField] private TextMeshProUGUI coolTimeText;
    
    [SerializeField] private TextMeshProUGUI hpPlusText;
    [SerializeField] private TextMeshProUGUI attackPlusText;
    [SerializeField] private TextMeshProUGUI defencePlusText;
    [SerializeField] private TextMeshProUGUI speedPlusText;
    [SerializeField] private TextMeshProUGUI attSpeedPlusText;
    [SerializeField] private TextMeshProUGUI coolTimePlusText;

    private List<GameObject> enhanceImageList = new List<GameObject>();

    public void Init(ulong puzzleUid, int puzzleId, ulong[] appliedPuzzleUidArray)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<CharacterDataBase>() is CharacterDataBase characterDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;
        
        var puzzleDataList = puzzleDB.GetDataList();
        var characterDataList = characterDB.GetDataList();
        var playerPuzzleList = playerDB.Data.GetPlayerPuzzleData();
        var statDataList = playerDB.GetPlayerCharacterAddStatList();
        
        if (puzzleId != 0)
        {
            var findPuzzleData = puzzleDataList.Find(v => v.ID == puzzleId) as PuzzleData;
            var findCharacterData = characterDataList.Find(v => v.ID == findPuzzleData.CharacterDataId) as CharacterData;

            var findStatData = statDataList.Find(v => v.Uid == puzzleUid);

            var hpValue = findCharacterData.Hp + findStatData?.Hp;
            var hpPlusValue = findStatData?.Hp;
            
            var defenceValue = findCharacterData.Defense + findStatData?.Defense;
            var defencePlusValue = findStatData?.Defense;
            
            var speedValue = findCharacterData.MoveSpeed + findStatData?.MoveSpeed;
            var speedPlusValue = findStatData?.MoveSpeed;
            
            var attackValue = findCharacterData.Attack + findStatData?.Attack;
            var attackPlusValue = findStatData?.Attack;

            var attackSpeedValue = findCharacterData.AttackSpeed + findCharacterData.AttackSpeed * findStatData?.AttackSpeed * 0.01f;
            var attackSpeedPlusValue = findStatData?.AttackSpeed;

            var coolTimeValue = findStatData?.SkillCoolTime;
            if (coolTimeValue > basicDB.SkillCoolTimeLimit())
                coolTimeValue = basicDB.SkillCoolTimeLimit();
            var coolTimePlusValue = findStatData?.SkillCoolTime;
            
            this.characterImage.sprite = AtlasManager.Instance.GetIconSprite(findPuzzleData.IconName);
            this.characterName.text = findCharacterData.Name;

            this.levelText.text = "Lv. ";
            if (findCharacterData.Level / 10 < 1)
                this.levelText.text += "0";
            this.levelText.text += findCharacterData.Level;
            
            this.hpText.text = hpValue.ToString();
            this.hpPlusText.text = "+" + hpPlusValue;
            this.hpPlusText.gameObject.SetActive(hpPlusValue > 0);
            
            this.attackText.text = attackValue.ToString();
            this.attackPlusText.text = "+" + attackPlusValue;
            this.attackPlusText.gameObject.SetActive(attackPlusValue > 0);
            
            this.defenceText.text = defenceValue.ToString();
            this.defencePlusText.text =  "+" + defencePlusValue;
            this.defencePlusText.gameObject.SetActive(defencePlusValue > 0);
            
            this.speedText.text = speedValue.ToString();
            this.speedPlusText.text =  "+" + speedPlusValue;
            this.speedPlusText.gameObject.SetActive(speedPlusValue > 0);
            
            this.attSpeedText.text = attackSpeedValue.ToString();
            this.attSpeedPlusText.text = "+" + attackSpeedPlusValue + "%";;
            this.attSpeedPlusText.gameObject.SetActive(attackSpeedPlusValue > 0);
            
            this.coolTimeText.text = coolTimeValue + "%";
            this.coolTimePlusText.text = "+" + coolTimePlusValue + "%";
            this.coolTimePlusText.gameObject.SetActive(coolTimePlusValue > 0);
        }

        foreach (var image in enhanceImageList)
        {
            Destroy(image.gameObject);
        }

        enhanceImageList.Clear();

        if (appliedPuzzleUidArray != null)
        {
            foreach (var uid in appliedPuzzleUidArray)
            {
                var findPlayerPuzzleData = playerPuzzleList.Find(v => v.uid == uid);
                if (findPlayerPuzzleData == null)
                {
                    DebugX.LogError("findPlayerPuzzleData is NULL");
                    continue;
                }
                
                var findPuzzleData = puzzleDataList.Find(v => v.ID == findPlayerPuzzleData.puzzleID) as PuzzleData;
                if (findPuzzleData == null)
                {
                    DebugX.LogError("findPuzzleData is NULL");
                    continue;
                }
                
                //어빌리티 피스만 염탐에서 나옴
                if (findPuzzleData.Type != PuzzleType.Ability)
                {
                    continue;
                }
                
                var newEnhanceObj = Instantiate(enhancePrefab, scrollRoot);
                var newObjImage = newEnhanceObj.transform.GetChild(0).GetComponent<Image>();

                newObjImage.sprite = AtlasManager.Instance.GetIconSprite(findPuzzleData.IconName);
                newEnhanceObj.gameObject.SetActive(true);
                enhanceImageList.Add(newEnhanceObj);
            }
        }

        hideRoot.SetActive(puzzleId == 0);
    }

    public void Init(int puzzleId, int groupId, BattleStageType type)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PuzzleDataBase>() is PuzzleDataBase puzzleDB))
            return;

        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDB))
            return;
        
        if (!(DataBaseManager.Instance.GetDataBase<BattleStageEnhanceDataBase>() is BattleStageEnhanceDataBase battleStageEnhanceDB))
            return;

        var puzzleDataList = puzzleDB.GetDataList();
        var battleStageEnhanceDataList = battleStageEnhanceDB.GetDataList();

        if (puzzleId != 0)
        {
            var findPuzzleData = puzzleDataList.Find(v => v.ID == puzzleId) as PuzzleData;
            var findCharacterData = findPuzzleData.CharacterData;

            var addStat = GameField.Instance.GetEnemyCharacterAddStat(groupId);

            var hpValue = findCharacterData.Hp + addStat?.Hp;
            var hpPlusValue = addStat?.Hp;
            
            var defenceValue = findCharacterData.Defense + addStat?.Defense;
            var defencePlusValue = addStat?.Defense;
            
            var speedValue = findCharacterData.MoveSpeed + addStat?.MoveSpeed;
            var speedPlusValue = addStat?.MoveSpeed;
            
            var attackValue = findCharacterData.Attack + addStat?.Attack;
            var attackPlusValue = addStat?.Attack;
            
            var attackSpeedValue = findCharacterData.AttackSpeed + findCharacterData.AttackSpeed * addStat?.AttackSpeed * 0.01f;
            var attackSpeedPlusValue = addStat?.AttackSpeed;

            var coolTimeValue = addStat?.SkillCoolTime;
            if (coolTimeValue > basicDB.SkillCoolTimeLimit())
                coolTimeValue = basicDB.SkillCoolTimeLimit();
            var coolTimePlusValue = addStat?.SkillCoolTime;
            
            this.characterImage.sprite = AtlasManager.Instance.GetIconSprite(findPuzzleData.IconName);
            if (type == BattleStageType.FinalBoss)
            {
                this.characterName.text = "???";
                this.levelText.text = "???";
                
                this.hpText.text = "???";
                this.hpPlusText.text = string.Empty;
                
                this.attackText.text = "???";
                this.attackPlusText.text = string.Empty;
                
                this.defenceText.text = "???";
                this.defencePlusText.text = string.Empty;
                
                this.speedText.text = "???";
                this.speedPlusText.text = string.Empty;
                
                this.attSpeedText.text = "???";
                this.attSpeedPlusText.text = string.Empty;
                
                this.coolTimeText.text = "???";
                this.coolTimePlusText.text = string.Empty;
            }
            else
            {
                this.characterName.text = findCharacterData.Name;
                
                this.levelText.text = "Lv. ";
                if (findCharacterData.Level / 10 < 1)
                    this.levelText.text += "0";
                this.levelText.text += findCharacterData.Level;

                this.hpText.text = hpValue.ToString();
                this.hpPlusText.text = "+" + hpPlusValue;
                this.hpPlusText.gameObject.SetActive(hpPlusValue > 0);
            
                this.attackText.text = attackValue.ToString();
                this.attackPlusText.text = "+" + attackPlusValue;
                this.attackPlusText.gameObject.SetActive(attackPlusValue > 0);
            
                this.defenceText.text = defenceValue.ToString();
                this.defencePlusText.text =  "+" + defencePlusValue;
                this.defencePlusText.gameObject.SetActive(defencePlusValue > 0);
            
                this.speedText.text = speedValue.ToString();
                this.speedPlusText.text =  "+" + speedPlusValue;
                this.speedPlusText.gameObject.SetActive(speedPlusValue > 0);
            
                this.attSpeedText.text = attackSpeedValue.ToString();
                this.attSpeedPlusText.text = "+" + attackSpeedPlusValue + "%";;
                this.attSpeedPlusText.gameObject.SetActive(attackSpeedPlusValue > 0);
            
                this.coolTimeText.text = coolTimeValue + "%";
                this.coolTimePlusText.text = "+" + coolTimePlusValue + "%";
                this.coolTimePlusText.gameObject.SetActive(coolTimePlusValue > 0);
            }
        }
        else
        {
            if (type == BattleStageType.FinalBoss)
            {
                gameObject.SetActive(false);
            }
        }

        foreach (var image in enhanceImageList)
        {
            Destroy(image.gameObject);
        }

        enhanceImageList.Clear();

        if (groupId != 0)
        {
            var findStageEnhanceDataList =
                battleStageEnhanceDataList.FindAll(v => ((BattleStageEnhanceData) v).GroupId == groupId);
            foreach (var data in findStageEnhanceDataList)
            {
                var stageEnhanceData = data as BattleStageEnhanceData;
                var findPuzzleData = puzzleDataList.Find(v => v.ID == stageEnhanceData.PuzzleDataId) as PuzzleData;

                //어빌리티 피스만 염탐에서 나옴
                if (findPuzzleData.Type != PuzzleType.Ability)
                {
                    continue;
                }
                
                var newEnhanceObj = Instantiate(enhancePrefab, scrollRoot);
                var newObjImage = newEnhanceObj.transform.GetChild(0).GetComponent<Image>();
                
                newObjImage.sprite = AtlasManager.Instance.GetIconSprite(findPuzzleData.IconName);
                newEnhanceObj.gameObject.SetActive(true);
                enhanceImageList.Add(newEnhanceObj);
            }
        }

        hideRoot.SetActive(puzzleId == 0);
    }
}