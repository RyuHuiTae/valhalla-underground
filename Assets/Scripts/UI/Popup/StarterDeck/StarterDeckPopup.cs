using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DevSupport;
using UnityEngine;

public class StarterDeckPopup : CommonPopup
{
    [SerializeField] private Transform modelRoot;
    [SerializeField] private StarterDeckSlot listSlotPrefab;
    [SerializeField] private Transform listSlotRoot;
    
    private List<StarterDeckSlot> characterSlotList = new List<StarterDeckSlot>();

    private StarterDeckSlot curSlot = null;
    
    private PuzzleInfoPopup infoUI = null;
    private TopUI topUI;
    private GameObject model;
    private Animator modelAnimator;

    private string animBasic;
    private static readonly int AnimState = Animator.StringToHash("AnimState");

    private bool isClicked = false;
    
    private async void Start()
    {
        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
        topUI.Init(TopUI.SceneTitle.StarterDeck);

        AudioManager.Instance.BgmSound(AudioManager.BgmType.StarterDeck);

        var starterDataList = DataBaseManager.Instance.GetDataList<StarterDeckDataBase>();
        foreach (var data in starterDataList)
        {
            var starterData = data as StarterDeckData;

            var newSlot = Instantiate(listSlotPrefab, listSlotRoot);
            newSlot.Init(starterData, ClickSlotEvent);
            characterSlotList.Add(newSlot);
        }

        var slot = characterSlotList.FirstOrDefault();
        if (slot != null)
        {
            ClickSlotEvent(slot);
        }
       
        await PlayAnimation();
    }

    private void ShowInfo(PuzzleData data)
    {
        if (infoUI == null)
            infoUI = PopupManager.Instance.GetPopup(PopupName.PuzzleInfoPopup) as PuzzleInfoPopup;

        infoUI?.Init(data);
    }

    private void CreateModel(CharacterData data)
    {
        if (model != null)
        {
            Destroy(model);
            model = null;
        }

        var modelPath = data.Path + "_UI";
        model = Instantiate(VResources.Load<GameObject>(modelPath), modelRoot);
        modelAnimator = model.GetComponent<Animator>();
        animBasic = data.BasicSkill.AnimationTriggerName;
    }
    
    public void ClickSlotEvent(StarterDeckSlot slot)
    {
        if (slot == null)
            return;

        ShowInfo(slot.PuzzleData);
        CreateModel(slot.CharData);
        
        curSlot = slot;
    }

    private async UniTask PlayAnimation()
    {
        var animArray = new Action[]
        {
            () => modelAnimator.SetInteger(AnimState, 0),
            () => modelAnimator.SetInteger(AnimState, 1),
            () =>
            {
                modelAnimator.SetInteger(AnimState, 0);
                modelAnimator.SetTrigger(animBasic);
            }
        };

        int index = 0;
        int count = animArray.Length;
        while (model != null)
        {
            animArray[index]?.Invoke();
            await UniTask.Delay(TimeSpan.FromSeconds(2f));
            
            index++;
            if (index >= count)
                index = 0;
        }
    }
    
    public void ClickSelectButton()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (curSlot == null)
            return;

        if (isClicked)
            return;

        isClicked = true;
        
        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();
        
        var id = curSlot.PuzzleData.ID;
        var type = (int)curSlot.PuzzleData.Type;

        var newData = new PlayerPuzzleData(id, type);
        playerPuzzleDataList.Add(newData);
        playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);
        playerDB.Data.SetStarterCharacterPuzzleID(id);
        
        var money = curSlot.StarterData.Money;
        var soul = curSlot.StarterData.Soul;
        
        playerDB.SetMoney(money);
        playerDB.SetSoul(soul);
        
        playerDB.Data.SetIntroFlag(true);
        playerDB.UpdatePlayerDataBase(() =>
        {
            PopupManager.Instance.HideAllPopup(false);
            LayerManager.Instance.ChangeLayer(Layer.StageSelectLayer, UIUtils.DEPTH_10);
                            
            var curLayer = LayerManager.Instance.GetCurrentLayer() as StageSelectLayer;
            if (curLayer != null)
            {
                curLayer.Init();
            }
        });
    }
}