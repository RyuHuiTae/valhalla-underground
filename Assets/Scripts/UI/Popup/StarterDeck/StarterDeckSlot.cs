using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StarterDeckSlot : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private UIButton uiButton;
    [SerializeField] private UITweenColor tween;
    public StarterDeckData StarterData { get; private set; }
    public PuzzleData PuzzleData { get; private set; }
    public CharacterData CharData { get; private set; }
    
    private Action<StarterDeckSlot> clickSlotEvent;

    public void Init(StarterDeckData starterData, Action<StarterDeckSlot> clickSlotEvent)
    {
        var puzzleDataList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>();
        PuzzleData = puzzleDataList.FirstOrDefault(v => v.ID == starterData.CharacterPieceId) as PuzzleData;

        var charDataList = DataBaseManager.Instance.GetDataList<CharacterDataBase>();
        CharData = charDataList.FirstOrDefault(v => v.ID == PuzzleData.CharacterDataId) as CharacterData;
        
        var sprite = AtlasManager.Instance.GetIconSprite(PuzzleData.IconName);
        this.image.sprite = sprite;
        this.StarterData = starterData;
        this.clickSlotEvent = clickSlotEvent;
        
        uiButton.SetBaseSprite(sprite);
        
        gameObject.SetActive(true);
    }

    public void ResetTween()
    {
        tween.ResetColor();
    }
    
    public void ClickSlot()
    {
        clickSlotEvent?.Invoke(this);
    }
}
