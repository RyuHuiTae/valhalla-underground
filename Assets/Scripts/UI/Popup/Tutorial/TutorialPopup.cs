using System;
using System.Linq;
using DevSupport;
using TMPro;

public class TutorialPopup : CommonPopup
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI mainText;

    public TutorialTextType Type { get; private set; }

    public void Init(TutorialTextType type)
    {
        this.Type = type;

        if (!(DataBaseManager.Instance.GetDataBase<TutorialTextDataBase>() is TutorialTextDataBase tutorialTextDB))
            return;

        var findData = tutorialTextDB
            .GetDataList()
            .Select(v => v as TutorialTextData)
            .FirstOrDefault(v => v.Type == type);

        if (findData == null)
        {
            DebugX.LogError(type + " 타입의 TutorialTextData가 없습니다.");
            return;
        }

        titleText.text = findData.Name;
        mainText.text = findData.TutorialText;
    }

    public void ClickNextButton()
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var index = (int)Type;
        
        var tutorialCheckArray = playerDB.Data.GetTutorialCheckData();
        tutorialCheckArray[index] = true;
        playerDB.Data.SetTutorialCheckData(tutorialCheckArray);

        playerDB.UpdatePlayerDataBase(null);

        PopupManager.Instance.HidePopup(this);
    }
}