using System;
using System.Collections;
using DevSupport;
using UnityEngine;
using UnityEngine.UI;

public class TweenPopup : CommonPopup
{
    [SerializeField] private Image image;
    [SerializeField] private UITweenAlpha tweenAlpha;

    private Action tweenEndCallback;

    public void Init(bool isStartBlack, float duration = 2f, Action tweenEndCallback = null)
    {
        this.image.color = isStartBlack ? Color.black : Color.clear;
        this.tweenAlpha.duration = duration;
        this.tweenEndCallback = tweenEndCallback;

        if (isStartBlack)
        {
            TweenBlackToClear();
        }
        else
        {
            TweenClearToBlack();
        }
    }
    
    public void TweenBlackToClear()
    {
        StartCoroutine(TweenForwardStart());
    }
    
    public void TweenClearToBlack()
    {
        StartCoroutine(TweenReverseStart());
    }
    
    private IEnumerator TweenForwardStart()
    {
        tweenAlpha.TweenForward();

        while (tweenAlpha.IsPlay())
        {
            yield return null;
        }
        
        tweenEndCallback?.Invoke();
        PopupManager.Instance.HidePopup(this);
    }
    
    private IEnumerator TweenReverseStart()
    {
        tweenAlpha.TweenReverse();

        while (tweenAlpha.IsPlay())
        {
            yield return null;
        }
        
        tweenEndCallback?.Invoke();
        PopupManager.Instance.HidePopup(this);
    }
}
