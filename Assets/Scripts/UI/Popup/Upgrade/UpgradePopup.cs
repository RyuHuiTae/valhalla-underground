using System.Collections.Generic;
using System.Linq;
using DevSupport;
using UnityEngine;

public class UpgradePopup : CommonPopup
{
    private TopUI topUI;
    private InventoryPopup invenUI;

    private float[] characterEnhancePrice;
    private List<Collision> collisionList = new List<Collision>();

    public bool IsClosing { get; private set; }
    
    public void Init()
    {
        IsClosing = false;
        
        if (!(DataBaseManager.Instance.GetDataBase<BasicDataBase>() is BasicDataBase basicDatabase))
            return;

        characterEnhancePrice = basicDatabase.CharacterEnhancePriceArray();

        invenUI = PopupManager.Instance.CreatePopup(PopupName.InventoryPopup) as InventoryPopup;
        if (invenUI != null)
        {
            invenUI.Init(
                transform,
                UIUtils.DEPTH_40,
                null,
                PieceMouseDownEvent,
                PieceMouseUpEvent,
                PieceCollisionEnterEvent,
                null,
                PieceCollisionExitEvent,
                null);
        }

        topUI = PopupManager.Instance.GetPopup(PopupName.TopUI, UIUtils.DEPTH_50) as TopUI;
    }

    private void PieceMouseDownEvent(PieceControl pieceControl)
    {
        if (pieceControl is EnhancePiece piece)
        {
            piece.applyPoint.SetActive(false);
        }
    }

    private void PieceCollisionEnterEvent(PieceControl pieceControl, Collision collision)
    {
        if (!collisionList.Contains(collision))
            collisionList.Add(collision);
    }

    private void PieceCollisionExitEvent(PieceControl pieceControl, Collision collision)
    {
        if (!collisionList.Contains(collision))
            return;

        collisionList.Remove(collision);
    }

    private void PieceMouseUpEvent(PieceControl pieceControl)
    {
        if (collisionList.Count <= 0)
            return;

        var collision = collisionList.LastOrDefault();

        //강화
        var shopEnhance = collision.gameObject.GetComponent<ShopEnhance>();
        if (shopEnhance != null)
        {
            var price = GetEnhancePrice(pieceControl.pieceData);

            if (price <= 0)
            {
                var confirm1 =
                    PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
                confirm1.Init("알림", "더이상 강화할 수 없습니다.");
                return;
            }


            var confirm2 =
                PopupManager.Instance.CreatePopup(PopupName.EnhanceConfirmPopup, UIUtils.DEPTH_60) as
                    EnhanceConfirmPopup;
            confirm2.Init("강화", "강화하시겠습니까?", pieceControl.pieceData, price, ConfirmPopupType.YesNo,
                () => { UpgradeProcess(price, pieceControl.pieceData); });
        }

        collisionList.Clear();
    }

    private int GetEnhancePrice(PieceData pieceData)
    {
        int price = 0;

        var level = pieceData.PuzzleData.CharacterData.Level;
        if (level > 0 && level < 10)
        {
            var ceilValue = Mathf.Ceil(characterEnhancePrice[level - 1] * 10);
            price = (int) ceilValue / 10;
            return price;
        }

        return price;
    }

    private void UpgradeProcess(int price, PieceData data)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        if (playerDB.Soul < price)
        {
            var confirm = PopupManager.Instance.CreatePopup(PopupName.ConfirmPopup, UIUtils.DEPTH_60) as ConfirmPopup;
            confirm.Init("알림", "보유 소울이 부족합니다.");
            return;
        }

        //강화 처리
        var characterData = data.PuzzleData.CharacterData;
        var characterType = characterData.CharacterType;
        var characterLevel = characterData.Level;

        var puzzleList = DataBaseManager.Instance.GetDataList<PuzzleDataBase>();

        //캐릭터의 다음 레벨 데이터 찾음
        var findNextCharacterData = puzzleList
            .Where(v => ((PuzzleData) v).CharacterData.CharacterType == characterType)
            .FirstOrDefault(v => ((PuzzleData) v).CharacterData.Level == (characterLevel + 1));

        if (findNextCharacterData == null)
        {
            DebugX.LogError("findNextCharacterData is NULL");
            return;
        }

        var playerPuzzleDataList = playerDB.Data.GetPlayerPuzzleData();

        var findData = playerPuzzleDataList.Find(v => v.uid == data.Uid);
        if (findData != null)
        {
            //퍼즐 아이디 변경
            findData.puzzleID = findNextCharacterData.ID;

            //플레이어 데이터 변경
            playerDB.SetSoul(-price);
            playerDB.Data.SetPlayerPuzzleData(playerPuzzleDataList);

            //돈 갱신
            topUI.Refresh(TopUI.SceneTitle.Stage);

            //갱신
            invenUI.inventoryList.RefreshList();

            //인포가 열려있다면 새로운 데이터로 갱신함
            var infoUI = PopupManager.Instance.GetOpenedPopup(PopupName.PuzzleInfoPopup) as PuzzleInfoPopup;
            if (infoUI != null)
            {
                //퍼즐 데이터 갱신 후 인포 보여줌
                data.Init(findNextCharacterData as PuzzleData, data.Uid, data.PiecePollution);
                
                infoUI.Init(data);
            }

            var confirm = PopupManager.Instance.CreatePopup(PopupName.PieceConfirmPopup, UIUtils.DEPTH_60) as PieceConfirmPopup;
            confirm.Init("강화", "강화 성공!", PieceData.GetNewPieceData(findNextCharacterData as PuzzleData, findData));

            AudioManager.Instance.EffectSound(AudioManager.EffectType.Reinforce, false, 0.3f);
        }
    }

    public override void OnClickCancelButton()
    {
        IsClosing = true;
        
        invenUI?.OnClickCancelButton();
        base.OnClickCancelButton();
    }
}