using System.Linq;
using Coffee.UIExtensions;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;

public class TopUI : CommonPopup
{
    [SerializeField] private Heart[] hpArray;
    [SerializeField] private TextMeshProUGUI hpPlusText;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private TextMeshProUGUI soulText;
    [SerializeField] private UITweenScale moneyIconScaleTween;
    [SerializeField] private UITweenScale soulIconScaleTween;
    [SerializeField] private TextMeshProUGUI floorText;
    
    [SerializeField] private AnimationClip invenOpenAnim;
    [SerializeField] private AnimationClip invenCloseAnim;

    [SerializeField] private UIButton minimapButton;
    [SerializeField] private UIButton puzzleButton;

    private UIShiny inventoryUiShiny;
    private UITweenScale inventoryUiTweenScale;
    private bool isOpenInvenSelect = false;
    
    public CommonPopup MinimapUI { get; private set; }
    public CommonPopup SettingUI { get; private set; }
    public PuzzlePopup PuzzleUI { get; private set; }

    public UpgradePopup UpgradeUI { get; private set; }
    
    public enum SceneTitle
    {
        Stage,
        Shop,
        Result,
        Rest,
        StarterDeck,
    }

    private void Awake()
    {
        inventoryUiShiny = puzzleButton.GetComponent<UIShiny>();
        inventoryUiTweenScale = puzzleButton.GetComponent<UITweenScale>();
    }

    public void Init(SceneTitle title)
    {
        Refresh(title);
    }

    public void Refresh(SceneTitle title)
    {
        if (!(DataBaseManager.Instance.GetDataBase<PlayerDataBase>() is PlayerDataBase playerDB))
            return;

        var heart = playerDB.Heart;
        for (int i = 0; i < hpArray.Length; i++)
        {
            var isHeartActive = heart > 0;
            hpArray[i].gameObject.SetActive(isHeartActive);
            heart--;
        }

        if (heart > 0)
        {
            hpPlusText.text = "+ " + heart;
        }

        hpPlusText.gameObject.SetActive(heart > 0);

        if (playerDB.IsHeartChanged)
        {
            var isAdd = heart > 0;
            DissolveHeart(isAdd ? playerDB.Heart - 1 : (int)playerDB.Heart, isAdd);
            playerDB.IsHeartChanged = false;
        }
        
        var curLayer = LayerManager.Instance.GetCurrentLayer();
        puzzleButton.SetEnable(curLayer is StageSelectLayer);

        minimapButton.SetEnable(StageManager.Instance.IsCreateStage);

        if (playerDB.IsMoneyChanged)
        {
            var numberTween = moneyText.GetComponent<TweenNumber>();
            numberTween.duration = 2f;
            numberTween.from = playerDB.PreMoney;
            numberTween.to = playerDB.Money;
            numberTween.TweenForward();
            playerDB.IsMoneyChanged = false;
            
            moneyIconScaleTween.TweenForward();
        }
        else 
            moneyText.text = playerDB.Money + " G";

        if (playerDB.IsSoulChanged)
        {
            var numberTween = soulText.GetComponent<TweenNumber>();
            numberTween.duration = 2f;
            numberTween.from = playerDB.PreSoul;
            numberTween.to = playerDB.Soul;
            numberTween.TweenForward();
            playerDB.IsSoulChanged = false;
            
            soulIconScaleTween.TweenForward();
        }
        else soulText.text = playerDB.Soul.ToString();
        
        switch (title)
        {
            case SceneTitle.Stage:
                if (playerDB.Data.Floor < 6)
                    floorText.text = "지하" + playerDB.Data.Floor + "층";
                if(playerDB.Data.Floor == 6)
                    floorText.text = "최심부";
                break;
            case SceneTitle.Shop:
                floorText.text = "상점";
                break;
            case SceneTitle.Result:
                floorText.text = "전투결과";
                break;
            case SceneTitle.Rest:
                floorText.text = "휴식";
                break;
            case SceneTitle.StarterDeck:
                floorText.text = "캐릭터 선택";
                break;
        }
    }

    public void DissolveHeart(int heartIndex, bool isActive)
    {
        if (hpArray.Length <= heartIndex)
            return;
        
        hpArray[heartIndex].SetActiveHeart(isActive);
    }
    
    public void CloseDetailUI()
    {
        if (MinimapUI != null)
        {
            MinimapUI.OnClickCancelButton();
        }

        if (PuzzleUI != null)
        {
            PuzzleUI.OnClickCancelButton();
        }

        if (UpgradeUI != null)
        {
            UpgradeUI.OnClickCancelButton();
        }
        
        if (SettingUI != null)
        {
            SettingUI.OnClickCancelButton();
        }
    }

    public void ClickMapButton()
    {
        if (!StageManager.Instance.IsCreateStage)
            return;

        if (MinimapUI != null)
        {
            MinimapUI.OnClickCancelButton();
        }
        else MinimapUI = PopupManager.Instance.CreatePopup(PopupName.MinimapPopup, UIUtils.DEPTH_60);
    }

    public void ClickInvenButton()
    {
        if (!StageManager.Instance.IsCreateStage)
            return;

        if (PuzzleUI == null && UpgradeUI == null)
        {
            popupAnimation.clip = !isOpenInvenSelect ? invenOpenAnim : invenCloseAnim;
            popupAnimation.Play();
            
            isOpenInvenSelect = !isOpenInvenSelect;
        }
        
        if (PuzzleUI != null)
        {
            if (PuzzleUI.IsClosing)
                return;

            puzzleButton.SetEnable(false);
            PuzzleUI.OnClickCancelButton();
            
            //상점 스테이지에서 퍼즐 보드판을 열고 변경사항이 생긴 경우 상점의 인벤토리 갱신 필요
            var shopPopup = PopupManager.Instance.GetOpenedPopup(PopupName.ShopPopup) as ShopPopup;
            if (shopPopup != null)
            {
                shopPopup.RefreshShopInvenUI();
                //shopPopup.CheckInfoPopupShow();
            }
            //휴식 스테이지에서 퍼즐 보드판을 열고 변경사항이 생긴 경우 휴식의 인벤토리 갱신 필요
            var restPopup = PopupManager.Instance.GetOpenedPopup(PopupName.RestPopup) as RestPopup;
            if (restPopup != null)
            {
                restPopup.RefreshRestInvenUI();
                //restPopup.CheckInfoPopupShow();
                restPopup.ResetRecreatSlot1();
                restPopup.ResetRecreatSlot2();
            }
            var spyingPopup = PopupManager.Instance.GetOpenedPopup(PopupName.SpyingPopup) as SpyingPopup;
            if (spyingPopup != null)
            {
                spyingPopup.InitPlayerSlot();
                spyingPopup.InitPlayerSpyingEditPosSlot();
            }
        }
        else if (UpgradeUI != null)
        {
            if (UpgradeUI.IsClosing)
                return;

            puzzleButton.SetEnable(false);
            UpgradeUI.OnClickCancelButton();
            
            //상점 스테이지에서 퍼즐 보드판을 열고 변경사항이 생긴 경우 상점의 인벤토리 갱신 필요
            var shopPopup = PopupManager.Instance.GetOpenedPopup(PopupName.ShopPopup) as ShopPopup;
            if (shopPopup != null)
            {
                shopPopup.RefreshShopInvenUI();
                //shopPopup.CheckInfoPopupShow();
                
                shopPopup.SetEnabledCollider(true);
            }
            //휴식 스테이지에서 퍼즐 보드판을 열고 변경사항이 생긴 경우 휴식의 인벤토리 갱신 필요
            var restPopup = PopupManager.Instance.GetOpenedPopup(PopupName.RestPopup) as RestPopup;
            if (restPopup != null)
            {
                restPopup.RefreshRestInvenUI();
                //restPopup.CheckInfoPopupShow();
                restPopup.ResetRecreatSlot1();
                restPopup.ResetRecreatSlot2();

                restPopup.SetEnabledCollider(true);
            }
            var spyingPopup = PopupManager.Instance.GetOpenedPopup(PopupName.SpyingPopup) as SpyingPopup;
            if (spyingPopup != null)
            {
                spyingPopup.InitPlayerSlot();
                spyingPopup.InitPlayerSpyingEditPosSlot();
            }
        }
    }

    public void ClickPuzzleButton()
    {
        if (PuzzleUI != null)
            return;
        
        if (UpgradeUI != null)
            return;
        
        PuzzleUI = PopupManager.Instance.CreatePopup(PopupName.PuzzlePopup) as PuzzlePopup;
        PuzzleUI.SetCallbackCancel(() =>
        {
            puzzleButton.SetNormalColor(new Color32(255, 255, 255, 255));
            puzzleButton.SetEnable(true);
            PlayInventoryPuzzleEffectLoop(false);
        });
            
        puzzleButton.SetNormalColor(new Color32(200, 200, 200, 255));
        puzzleButton.SetEnable(true);
        PlayInventoryPuzzleEffectLoop(true);
        
        popupAnimation.clip = invenCloseAnim;
        popupAnimation.Play();

        isOpenInvenSelect = false;
    }
    
    public void ClickUpgradeButton()
    {
        if (PuzzleUI != null)
            return;
        
        if (UpgradeUI != null)
            return;
        
        //상점 스테이지에서 강화UI를 열면 상점에 있는 콜라이더 끔
        var shopPopup = PopupManager.Instance.GetOpenedPopup(PopupName.ShopPopup) as ShopPopup;
        if (shopPopup != null)
        {
            shopPopup.SetEnabledCollider(false);
        }
        
        //휴식 스테이지에서 강화UI를 열면 휴식에 있는 콜라이더 끔
        var restPopup = PopupManager.Instance.GetOpenedPopup(PopupName.RestPopup) as RestPopup;
        if (restPopup != null)
        {
            restPopup.SetEnabledCollider(false);
        }
        
        UpgradeUI = PopupManager.Instance.CreatePopup(PopupName.UpgradePopup) as UpgradePopup;
        UpgradeUI.SetCallbackCancel(() =>
        {
            puzzleButton.SetNormalColor(new Color32(255, 255, 255, 255));
            puzzleButton.SetEnable(true);
            PlayInventoryPuzzleEffectLoop(false);
        });
        UpgradeUI.Init();
        
        puzzleButton.SetNormalColor(new Color32(200, 200, 200, 255));
        puzzleButton.SetEnable(true);
        PlayInventoryPuzzleEffectLoop(true);
        
        popupAnimation.clip = invenCloseAnim;
        popupAnimation.Play();

        isOpenInvenSelect = false;
    }
    
    public async UniTask PlayGetInventoryPuzzleEffect()
    {
        inventoryUiShiny.loop = false;
        inventoryUiShiny.Play();
        inventoryUiTweenScale.TweenForward();
        
        await UniTask.WaitUntil(() => inventoryUiTweenScale.IsPlay() == false);
        inventoryUiTweenScale.TweenForward();
        await UniTask.WaitUntil(() => inventoryUiTweenScale.IsPlay() == false);
        await UniTask.WaitUntil(() => inventoryUiShiny.isPlay == false);

    }

    public void PlayInventoryPuzzleEffectLoop(bool isPlay)
    {
        if (isPlay)
        {
            inventoryUiShiny.loop = true;
            inventoryUiShiny.Play();
        }
        else
        {
            inventoryUiShiny.loop = false;
            inventoryUiShiny.Stop();
        }
    }
    
    
    public void ClickSettingButton()
    {
        if (SettingUI != null)
        {
            SettingUI.OnClickCancelButton();
        }
        else SettingUI = PopupManager.Instance.CreatePopup(PopupName.SettingPopup, UIUtils.DEPTH_60);
    }
}